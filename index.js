/**
 * @format
 */
// @flow
import React, {useEffect} from 'react';
import {AppRegistry} from 'react-native';
import {DefaultTheme, configureFonts, Provider as PaperProvider} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {useTranslation} from 'react-i18next';

import {USER_LANGUAGE_STORAGE_KEY, DEFAULT_LANGUAGE} from './src/constants/i18n';
import './src/core/clients/i18n';

import {COLOR_AIA_RED, COLOR_AIA_GREY, COLOR_AIA_ERROR, WHITE} from './src/core/colors';

import {name as appName} from './app.json';
import App from './App';

const Main = () => {
  const {i18n} = useTranslation();

  const initLanguage = async () => {
    const userLanguage = await AsyncStorage.getItem(USER_LANGUAGE_STORAGE_KEY);
    i18n.changeLanguage(userLanguage || DEFAULT_LANGUAGE);
  };

  const fontConfig = {
    default: {
      regular: {
        fontFamily: 'NotoSans-Regular',
        fontWeight: 'normal',
      },
    },
  };

  const aiaTheme = {
    ...DefaultTheme,
    roundness: 10,
    colors: {
      ...DefaultTheme.colors,
      primary: COLOR_AIA_RED,
      accent: COLOR_AIA_GREY,
      background: WHITE,
      surface: WHITE,
      error: COLOR_AIA_ERROR,
    },
    fonts: configureFonts(fontConfig),
  };

  useEffect(() => {
    initLanguage();
  }, []);

  return (
    <PaperProvider theme={aiaTheme}>
      <App />
    </PaperProvider>
  );
};

AppRegistry.registerComponent(appName, () => Main);
