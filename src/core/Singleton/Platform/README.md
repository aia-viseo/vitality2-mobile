## Device Platform Singleton
Get Device OS by singleton design

# Usage
```js
import GetOS from './src/core/Singleton/Platform';

GetOS.getInstance();// return OS
```
