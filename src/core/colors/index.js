// AIA DDS main color pallet

// Primary colours
export const COLOR_AIA_RED =  '#d31145';
export const COLOR_AIA_GREY = '#596c80';
export const WHITE = '#fff';
export const BLACK = '#000';

// States
export const COLOR_AIA_DARK_GREY = '#363e3f';
export const COLOR_AIA_GREEN = '#00a3ad';
export const COLOR_AIA_ERROR = '#b00020';
export const COLOR_AIA_SUCCESS = '#34c759';

// Gradients
export const COLOR_AIA_GD_RED_1 = '#dc1022';
export const COLOR_AIA_GD_RED_2 = '#ff0092';

// Active Gradients
export const COLOR_AIA_GD_ACTIVE_1 = '#00a3e0';
export const COLOR_AIA_GD_ACTIVE_2 = '#0957c3';

// Backgound colours
export const COLOR_AIA_BG_1 = '#f4f4f4';

// Shades colors
export const COLOR_AIA_RED_100 = '#fbe7ec';
export const COLOR_AIA_RED_200 = '#f6cfda';
export const COLOR_AIA_RED_300 = '#eda0b5';
export const COLOR_AIA_RED_400 = '#e5708f';
export const COLOR_AIA_RED_500 = '#dc416a';
export const COLOR_AIA_RED_600 = COLOR_AIA_RED;
export const COLOR_AIA_RED_700 = '#a90e37';
export const COLOR_AIA_RED_800 = '#7f0a29';
export const COLOR_AIA_RED_900 = '#54071c';

export const COLOR_AIA_GREY_50 = '#f4f7f6';
export const COLOR_AIA_GREY_100 = '#eef0f2';
export const COLOR_AIA_GREY_200 = '#dee2e6';
export const COLOR_AIA_GREY_300 = '#bdc4cc';
export const COLOR_AIA_GREY_400 = '#9ba7b3';
export const COLOR_AIA_GREY_500 = '#7a8999';
export const COLOR_AIA_GREY_600 = COLOR_AIA_GREY;
export const COLOR_AIA_GREY_700 = '#475666';
export const COLOR_AIA_GREY_800 = '#2d3640';
export const COLOR_AIA_GREY_900 = '#1b2026';

export const COLOR_AIA_DARK_GREY_100 = '#ebecec';
export const COLOR_AIA_DARK_GREY_200 = '#d7d8d9';
export const COLOR_AIA_DARK_GREY_300 = '#c3c5c5';
export const COLOR_AIA_DARK_GREY_400 = '#afb2b2';
export const COLOR_AIA_DARK_GREY_500 = '#868b8c';
export const COLOR_AIA_DARK_GREY_600 = '#5e6565';
export const COLOR_AIA_DARK_GREY_700 = COLOR_AIA_DARK_GREY;
export const COLOR_AIA_DARK_GREY_800 = '#202526';
export const COLOR_AIA_DARK_GREY_900 = '#050606';

// Secondary colours
export const COLOR_AIA_ORANGE = '#e35205';
export const COLOR_AIA_VITALITY_ORANGE = '#f89820';
export const COLOR_AIA_YELLOW = '#f1c400';
export const COLOR_AIA_VITALITY_LIME = '#c1d82f';
export const COLOR_AIA_LIME = '#64a70b';
export const COLOR_AIA_VITALITY_BLUE = '#27aae1';
export const COLOR_AIA_CYAN = '#00a3e0';
export const COLOR_AIA_BLUE = '#0957c3';
export const COLOR_AIA_VIOLET = '#6244bb';
export const COLOR_AIA_PURPLE = '#87027b';
export const COLOR_AIA_BRONZE = '#DA7516';
export const COLOR_AIA_SILVER = '#86919A';
export const COLOR_AIA_GOLD = '#FFB222';
export const COLOR_AIA_PLATINUM = '#52A7E9';
export const COLOR_AIA_ICON_GRAY = '#C1D5E3';

export const PRIMARY_COLOR = COLOR_AIA_GREEN;
export const PRIMARY_COLOR_GREY = COLOR_AIA_GREY;
export const SECONDARY_COLOR = COLOR_AIA_RED;
export const BLUE = COLOR_AIA_BLUE;
export const INDIGO = COLOR_AIA_VIOLET;
export const PURPLE = COLOR_AIA_PURPLE;
export const RED = COLOR_AIA_RED;
export const ORANGE = COLOR_AIA_ORANGE;
export const YELLOW = COLOR_AIA_YELLOW;
export const GREEN = COLOR_AIA_LIME;

// TRANSPARENT colours
export const TRANSPARENT = 'rgba(0,0,0,0)';

// OVERLAY colours
export const OVERLAY = 'rgba(0,0,0,0.2)';

// SELECTION component gradient border
export const COLOR_AIA_SELECTION_GRADIENT_LEFT = '#0AA5BE';
export const COLOR_AIA_SELECTION_GRADIENT_RIGHT = '#0BA2E0';
export const COLOR_AIA_LIGHT_GREY = '#7A899A';
export const COLOR_AIA_SELECTION_BORDER_SOLID = '#0BA2E0';
