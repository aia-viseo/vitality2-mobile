/**
 *
 * OnlineAssessmentsApiCenter
 * @format
 *
 */

// @flow

import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNSecureKeyStore from 'react-native-secure-key-store';

import {
  AEM_SIT_HOST_NAME,
  X_VITALITY_LEGAL_ENTITY_ID,
  X_AIA_REQUEST_ID,
  AEM_ORIGIN,
} from '@constants/Request';

import {VITALITY_TOKEN_KEY} from '@clients/AuthCenter/config';
import {MEMBERSHIP_NUMBER} from '@clients/HealthSync/config';

import {
  sampleData,
  VITALITY_AGE_COMPLETE_DATA,
  VITALITY_AGE_USER_DATA,
  VITALITY_AGE_USER_ANSWERS,
  COMPLETED_ONLINE_ASSESSMENTS,
} from './config';
import type {OnlineAssessmentsDataType, VitalityAgeCompleteDataType} from './types';

const getAsyncStorageData = async (key: string) => {
  const data = await AsyncStorage.getItem(key).catch(() => null);
  return data;
};

const getKeyStoreData = async (key: string) => {
  const data = await RNSecureKeyStore.get(key).catch(() => null);
  return data;
};

const postData = (
  membershipNo: string,
  token: string,
  bodyData: OnlineAssessmentsDataType,
  specificApi: string,
  callback?: ({}) => void,
  errCallback?: (msg?: string) => void
) => {
  const url = `${AEM_SIT_HOST_NAME}/assessments/${specificApi}?membership-no=${membershipNo}`;
  const config = {
    headers: {
      'X-AIA-Request-Id': X_AIA_REQUEST_ID,
      'X-Vitality-Legal-Entity-Id': X_VITALITY_LEGAL_ENTITY_ID,
      Authorization: `Bearer ${token}`,
      Origin: AEM_ORIGIN,
      'Content-Type': 'application/json',
    },
    timeout: 30000,
    withCredentials: true,
  };
  AsyncStorage.setItem(
    `${VITALITY_AGE_USER_DATA}_${membershipNo}`,
    JSON.stringify(bodyData || sampleData)
  ).catch(() => {
    if (errCallback) {
      errCallback('Error caching vitality age user data');
    }
  });

  axios
    .post(url, bodyData || sampleData, config)
    .then((res) => {
      const {status, data, reason} = res;
      if (status === 200) {
        AsyncStorage.setItem(
          `${VITALITY_AGE_COMPLETE_DATA}_${membershipNo}`,
          JSON.stringify(data)
        ).catch(() => {
          if (errCallback) {
            errCallback('Error caching vitality age data');
          }
        });
        if (callback) {
          callback(data);
        }
      } else if (errCallback) {
        if (reason) {
          errCallback(reason);
        } else {
          errCallback(`Error :${res.status}`);
        }
      }
    })
    .catch(() => {
      if (errCallback) {
        errCallback('Fetch error.');
      }
      if (callback) {
        callback({});
      }
    });
};

const submitOnlineAssessment = (
  membershipNo: string,
  token: string,
  data: OnlineAssessmentsDataType,
  specificApi: string,
  callback?: ({}) => void,
  errCallback?: (msg?: string) => void
) => {
  if (!membershipNo) {
    getAsyncStorageData(MEMBERSHIP_NUMBER)
      .then((no) => {
        if (!token) {
          getKeyStoreData(VITALITY_TOKEN_KEY)
            .then((jwtToken) => {
              postData(
                no,
                jwtToken,
                data,
                specificApi,
                (result) => {
                  if (callback) {
                    callback(result);
                  }
                },
                (errMsg?: string) => {
                  if (errCallback) {
                    errCallback(errMsg);
                  }
                }
              );
            })
            .catch(() => {
              if (errCallback) {
                errCallback('No token available.');
              }
            });
        } else {
          postData(
            no,
            token,
            data,
            specificApi,
            (result) => {
              if (callback) {
                callback(result);
              }
            },
            (errMsg?: string) => {
              if (errCallback) {
                errCallback(errMsg);
              }
            }
          );
        }
      })
      .catch(() => {
        if (errCallback) {
          errCallback('Membership number is required.');
        }
      });
  }
  if (membershipNo && !token) {
    getKeyStoreData(VITALITY_TOKEN_KEY)
      .then((jwt) => {
        postData(
          membershipNo,
          jwt,
          data,
          specificApi,
          (result) => {
            if (callback) {
              callback(result);
            }
          },
          (errMsg?: string) => {
            if (errCallback) {
              errCallback(errMsg);
            }
          }
        );
      })
      .catch(() => {
        if (errCallback) {
          errCallback('No token available.');
        }
      });
  }
  if (membershipNo && token) {
    postData(
      membershipNo,
      token,
      data,
      specificApi,
      (result) => {
        if (callback) {
          callback(result);
        }
      },
      (errMsg?: string) => {
        if (errCallback) {
          errCallback(errMsg);
        }
      }
    );
  }
};

const getDataFromAsync = (key: string) => {
  return new Promise<any>((resolve, reject) => {
    getAsyncStorageData(MEMBERSHIP_NUMBER)
      .then((membershipNo: string) => {
        getAsyncStorageData(`${key}_${membershipNo}`)
          .then((data) => resolve(data))
          .catch(() => reject(new Error(`Error getting data with key ${key}_${membershipNo}`)));
      })
      .catch(() => reject(new Error('Membership number not found in Async Storage.')));
  });
};

const getVitalityAgeUserData = () => {
  return getDataFromAsync(VITALITY_AGE_USER_DATA);
};

const getVitalityAgeUserAnswers = () => {
  return getDataFromAsync(VITALITY_AGE_USER_ANSWERS);
};

const getVitalityAgeCompleteData = () => {
  return getDataFromAsync(VITALITY_AGE_COMPLETE_DATA);
};

const getCompletedOnlineAssessments = () => {
  return getDataFromAsync(COMPLETED_ONLINE_ASSESSMENTS);
};

const getVitalityAge = (): Promise<any> => {
  return new Promise((resolve, reject) => {
    getVitalityAgeCompleteData()
      .then((data) => {
        const objData: VitalityAgeCompleteDataType = JSON.parse(data);
        resolve({
          age: objData.vitalityAge.age,
          diffYears: objData.vitalityAge.age - objData.actualAge,
        });
      })
      .catch(() => reject(new Error('Error')));
  });
};

const OnlineAssessmentsApiCenter = {
  submitOnlineAssessment,
  getVitalityAgeCompleteData,
  getVitalityAge,
  getVitalityAgeUserData,
  getVitalityAgeUserAnswers,
  getCompletedOnlineAssessments,
};

export default OnlineAssessmentsApiCenter;
