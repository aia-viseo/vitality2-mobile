/**
 *
 * ChallengeApiCenter
 * @format
 *
 */

// @flow

import {
  AEM_SIT_TOKEN_HOST_NAME,
  CHALLENGE_LIST_PATH,
  WEEKLY_CHALLENGE_PATH,
  EARNED_POINT,
} from '@constants/Request';

import DateTime from '@core/handler/DateTime';

import ApiCache from './ApiCache';
import ApiRequest from './ApiRequest';

const fetchChallengeList = (
  membershipId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(`challengeList`, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${CHALLENGE_LIST_PATH}?membership-no=${membershipId}`,
      method: 'GET',
    };

    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`challengeList`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const fetchWeeklyChallenge = (
  membershipId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(`weeklyChallenge`, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
      return;
    }
    const range = 14;
    const date = new Date();
    const start = new Date(date.getTime() - range * 24 * 60 * 60 * 1000);
    const startDate = DateTime.format(start, 'yyyy-MM-dd');
    const endDate = DateTime.format(new Date(), 'yyyy-MM-dd');
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${WEEKLY_CHALLENGE_PATH}?membership-no=${membershipId}&startDate=${startDate}&endDate=${endDate}`,
      method: 'GET',
    };

    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`weeklyChallenge`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const fetchEarnedToday = (
  membershipId: string,
  numberOfYears: number,
  activityId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(`earnedToday`, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
      return;
    }

    const today = DateTime.format(new Date(), 'yyyy-MM-dd');
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${EARNED_POINT}?membership-no=${membershipId}&number-of-years=${numberOfYears}&activity-id=${activityId}&activity-status=LimitsApplied&startDate=${today}&endDate=${today}`,
      method: 'GET',
    };
    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`earnedToday`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const clearChallengeCache = (callback: () => void) => {
  ApiCache.removeCachedContent(['challengeList'], () => {
    callback();
  });
};

const ChallengeApiCenter = {
  fetchChallengeList,
  fetchWeeklyChallenge,
  fetchEarnedToday,
  clearChallengeCache,
};

export default ChallengeApiCenter;
