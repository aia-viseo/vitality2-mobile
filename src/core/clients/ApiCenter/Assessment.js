/**
 *
 * AssessmentApiCenter
 * @format
 *
 */

// @flow

import {AEM_SIT_TOKEN_HOST_NAME, ASSESSMENT_LIST_PATH} from '@constants/Request';

import ApiCache from './ApiCache';
import ApiRequest from './ApiRequest';

const fetchAssessmentList = (membershipId: string, callback: (response: any) => void) => {
  ApiCache.getCacheContent(`assessmentList`, (response) => {
    if (response) {
      callback(response);
    }
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${ASSESSMENT_LIST_PATH}?membership-no=${membershipId}`,
      method: 'GET',
    };

    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`assessmentList`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const clearAssessmentCache = (callback: () => void) => {
  ApiCache.removeCachedContent(['assessmentList'], () => {
    callback();
  });
};

const AssessmentApiCenter = {
  fetchAssessmentList,
  clearAssessmentCache,
};

export default AssessmentApiCenter;
