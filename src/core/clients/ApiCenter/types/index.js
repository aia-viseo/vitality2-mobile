/**
 *
 * @format
 *
 */

// @flow

export type AuthConfigType = {
  method: string,
  headers: {
    Accept: string,
    Authorization: string,
    'X-Vitality-Legal-Entity-Id': string,
    'X-AIA-Request-Id': string,
    'x-Forward-API': string,
    Origin: string,
  },
};
