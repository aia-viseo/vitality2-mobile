/**
 *
 * Api Request
 * @format
 *
 */

// @flow

import axios from 'axios';
import RNSecureKeyStore from 'react-native-secure-key-store';

import {VITALITY_TOKEN_KEY} from '@clients/AuthCenter/config';
import type {ConfigType} from '@clients/Request/types';
import {
  AEM_ORIGIN,
  X_VITALITY_LEGAL_ENTITY_ID,
  X_AIA_REQUEST_ID,
  X_FORWARD_API,
} from '@constants/Request';

import type {AuthConfigType} from './types';

const getAuthConfig = (callback: (AuthConfigType) => void) => {
  RNSecureKeyStore.get(VITALITY_TOKEN_KEY).then((authToken) => {
    const authConfig = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${authToken}`,
        'X-Vitality-Legal-Entity-Id': X_VITALITY_LEGAL_ENTITY_ID,
        'X-AIA-Request-Id': X_AIA_REQUEST_ID,
        'x-Forward-API': X_FORWARD_API,
        Origin: AEM_ORIGIN,
      },
    };
    callback(authConfig);
  });
};

const createRequest = (customConfig: ConfigType, callback: (Object) => void) => {
  getAuthConfig((authConfig) => {
    const {url} = customConfig;
    const {method, headers} = authConfig;
    axios({url, method, headers})
      .then((res) => {
        const {status} = res;
        if (status !== 200) {
          callback(null);
        }
        callback(res.data);
      })
      .catch(() => {
        callback(null);
      });
  });
};

const ApiRequest = {
  createRequest,
};

export default ApiRequest;
