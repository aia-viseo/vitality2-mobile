/**
 *
 * PointsApiCenter
 * @format
 * @flow
 *
 */

import lodash from 'lodash';

import {
  AEM_SIT_TOKEN_HOST_NAME,
  POINTS_HISTORY_PATH,
  POINTS_RANGEBARS_PATH,
} from '@constants/Request';

import ApiCache from './ApiCache';
import ApiRequest from './ApiRequest';

const CACHE_KEYS = {
  rangeBar: 'pointsRangeBarList',
  history: 'pointsHistory',
};

const fetchRangebarList = (
  membershipId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(CACHE_KEYS.rangeBar, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
    }
    const request = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${POINTS_RANGEBARS_PATH}?membership-no=${membershipId}`,
      method: 'GET',
    };

    ApiRequest.createRequest(request, (result) => {
      ApiCache.cacheContent(CACHE_KEYS.rangeBar, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const fetchHistoryItems = (
  membershipId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(CACHE_KEYS.history, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
    }
    const request = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${POINTS_HISTORY_PATH}?membership-no=${membershipId}&eventType=All`,
      method: 'GET',
    };

    ApiRequest.createRequest(request, (result) => {
      ApiCache.cacheContent(CACHE_KEYS.history, result, (callbackData) => {
        callback({...callbackData, hasNewPoints: !lodash.isEqual(response, callbackData)});
      });
    });
  });
};

const clearCache = () => {
  ApiCache.removeCachedContent([CACHE_KEYS.rangeBar, CACHE_KEYS.history], () => {});
};

const PointsApiCenter = {
  clearCache,
  fetchHistoryItems,
  fetchRangebarList,
};

export default PointsApiCenter;
