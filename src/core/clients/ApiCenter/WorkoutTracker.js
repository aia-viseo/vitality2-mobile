/**
 *
 * WorkoutTrackerApiCenter
 * @format
 *
 */

// @flow

import {
  AEM_SIT_TOKEN_HOST_NAME,
  WORKOUT_TRACKER_DEVICES_PATH,
  WORKOUT_TRACKER_WORKOUTS_PATH,
  WORKOUT_TRACKER_FITNESS_POINTS_PATH,
} from '@constants/Request';
import DateTime from '@core/handler/DateTime';

import ApiCache from './ApiCache';
import ApiRequest from './ApiRequest';

const fetchWorkoutTrackerDevices = (membershipId: string, callback: (response: any) => void) => {
  ApiCache.getCacheContent(`workout_tracker_devices`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${WORKOUT_TRACKER_DEVICES_PATH.replace(
        '$membershipNumber',
        membershipId
      )}`,
      method: 'GET',
    };

    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`workout_tracker_devices`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const fetchWorkoutData = (
  membershipId: string,
  deviceName: string,
  startDate: Date,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  const startDateStr = DateTime.format(startDate, 'yyyy-MM-dd');
  const endDateStr = DateTime.format(new Date(), 'yyyy-MM-dd');

  ApiCache.getCacheContent(`workout_tracker_data_${deviceName}_${startDateStr}`, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${WORKOUT_TRACKER_WORKOUTS_PATH}?membership-no=${membershipId}&device-name=${deviceName}&endDate=${endDateStr}&startDate=${startDateStr}`,
      method: 'GET',
    };
    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(
        `workout_tracker_data_${deviceName}_${startDateStr}`,
        result,
        (callbackData) => {
          callback(callbackData);
        }
      );
    });
  });
};

const fetchWorkoutFitnessPoint = (
  membershipId: string,
  callback: (response: any) => void,
  shouldRefresh: boolean = false
) => {
  ApiCache.getCacheContent(`workout_tracker_fitness_point`, (response) => {
    if (response && !shouldRefresh) {
      callback(response);
      return;
    }

    const customConfig = {
      url: `${AEM_SIT_TOKEN_HOST_NAME}${WORKOUT_TRACKER_FITNESS_POINTS_PATH}?membership-no=${membershipId}&number-of-years=0&activity-id=STE4,STE5,PHY3,PHY4,PHY5,SLPT&activity-status=LimitsApplied`,
      method: 'GET',
    };

    ApiRequest.createRequest(customConfig, (result) => {
      ApiCache.cacheContent(`workout_tracker_fitness_point`, result, (callbackData) => {
        callback(callbackData);
      });
    });
  });
};

const WorkoutTrackerApiCenter = {
  fetchWorkoutTrackerDevices,
  fetchWorkoutData,
  fetchWorkoutFitnessPoint,
};

export default WorkoutTrackerApiCenter;
