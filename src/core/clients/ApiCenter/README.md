## Api Center
Contain all api call in this files

Steps to integrate
1. Create api call function
```js
const function = () => {

};
```

2. Create Mock (optional)
```js
const function = () => {
  Mock.mockGetSuccess(path, expectedData);
};
```

3. Create config objects
Please read /Request/README.md if you want to understand the structure of the config should be.
```js
const function = () => {
  Mock.mockGetSuccess(path, expectedData);
  const config = {
    url
    method
  }
};
```

4. Call and return createRequest
```js
const function = () => {
  Mock.mockGetSuccess(path, expectedData);
  const config = {
    url
    method
  }
  return createRequest(customConfig);
};
```

5. Export api call function in ApiCenter object
```js
const function = () => {
  Mock.mockGetSuccess(path, expectedData);
  const config = {
    url
    method
  }
  return createRequest(customConfig);
};

const ApiCenter = {
  function
  //add here
}

export default ApiCenter;
```
