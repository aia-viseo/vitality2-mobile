/**
 *
 * @format
 *
 */

// @flow

import {AxiosError, AxiosResponse} from 'axios';
import {Request} from '../Request';
import type {ConfigType} from '../Request/types';

const createRequest = (customConfig: ConfigType) => {
  return new Promise<{}>(
    (resolve: (response: AxiosResponse) => void, reject: (response: AxiosResponse) => void) => {
      Request.apiRequestBuilder(
        customConfig,
        (response: AxiosResponse) => {
          const {data} = response;
          resolve(data);
        },
        (error: AxiosError) => {
          reject(error);
        }
      );
    }
  );
};

const getWhatsOns = () => {
  const WHATSON_DATA = [
    {
      title: 'Weekly fitness challenge',
      iconName: 'assessment_thumbnail',
      description:
        'Urna molestie at elementum eu facilisis sed odio. At auctor urna nunc id cursus metud cursus metud cursus metud cursus metud.',
    },
    {
      title: 'Weekly fitness challenge',
      iconName: 'assessment_thumbnail',
      description:
        'Urna molestie at elementum eu facilisis sed odio. At auctor urna nunc id cursus metud cursus metud cursus metud cursus metud.',
    },
  ];

  return WHATSON_DATA;
};

const getRewards = () => {
  const REWARD_DATA = [
    {
      id: 1,
      name: 'GoFood',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: 'Rp 40,000',
      quantity: 2,
      targetDate: new Date(),
    },
    {
      id: 2,
      name: 'GoCar',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: 'Rp 15,000',
      quantity: 1,
      terms: 'Use on your next purchase',
    },
    {
      id: 3,
      name: 'Prodia',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: '50%',
      quantity: 1,
      terms: 'Use on your next health check',
    },
    {
      id: 4,
      name: 'Prodia',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: '50%',
      quantity: 1,
      terms: 'Use on your next health check',
    },
    {
      id: 5,
      name: 'Prodia',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: '50%',
      quantity: 1,
      terms: 'Use on your next health check',
    },
    {
      id: 6,
      name: 'Prodia',
      iconName: 'go_car',
      iconGroup: 'partners',
      discount: '50%',
      quantity: 1,
      terms: 'Use on your next health check',
    },
  ];
  return REWARD_DATA;
};

const getVitalityAge = () => {
  const AGE_DATA = {
    age: 38,
    diffYears: 10,
  };
  return AGE_DATA;
};

const getDashboardNotifications = () => {
  const DASHBOARD_NOTIFICATION_DATA = [
    {
      title: 'Your report is almost ready',
      iconName: 'assessment_thumbnail',
      description: 'Finish your assessment to get your full health report.',
      show: false,
    },
    {
      title: 'You have unfinished assessments',
      iconName: 'assessment_thumbnail',
      description:
        'Go to the Assessments in the Dashboard to see what assessments are pending. Go to the Assessments in the Dashboard to see what assessments are pending. Go to the Assessments in the Dashboard to see what assessments are pending.',
      show: false,
    },
  ];

  return DASHBOARD_NOTIFICATION_DATA;
};

const ApiCenter = {
  createRequest,
  getWhatsOns,
  getRewards,
  getVitalityAge,
  getDashboardNotifications,
};

export default ApiCenter;
