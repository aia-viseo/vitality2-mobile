/**
 *
 * Api Cache
 * @format
 *
 */

// @flow

import {Cache} from '../Request';

const cacheContent = (key: string, data: any, callback: function): void => {
  Cache.cacheUrlContent(
    key,
    data,
    () => {
      callback(data);
    },
    () => {
      callback(data);
    }
  );
};

const getCacheContent = (key: string, callback: function) => {
  Cache.getCachedUrlContent(
    key,
    (response) => {
      delete response.expireAt;
      callback(response);
    },
    () => {
      callback(null);
    }
  );
};

const removeCachedContent = (keys: Array<string>, callback: function) => {
  Cache.removeCachedUrlContents(
    keys,
    () => {
      callback();
    },
    () => {
      callback();
    }
  );
};

const ApiCache = {
  cacheContent,
  getCacheContent,
  removeCachedContent,
};

export default ApiCache;
