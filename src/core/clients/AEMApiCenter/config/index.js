/**
 *
 * @format
 *
 */

// @flow

export const HOW_HEALTHY_ARE_YOU_ASSESSMENT = 'how-healthy-are-you-assessment';
export const AME_CONTENT_KEYS = [
  'general_en',
  'general_id',
  'quickLinks_en',
  'quickLinks_id',
  'assessmentList_en',
  'assessmentList_id',
  'challengeList_en',
  'challengeList_id',
  'bottomNavigationList_en',
  'bottomNavigationList_id',
  'eating_well_assessment_en',
  'eating_well_assessment_id',
  'howHealthyAreYouAssessment_en',
  'howHealthyAreYouAssessment_id',
  'non_smoker_declaration_assessment_en',
  'non_smoker_declaration_assessment_id',
  'how_well_do_you_sleep_assessment_en',
  'how_well_do_you_sleep_assessment_id',
  'how_active_are_you_assessment_en',
  'how_active_are_you_assessment_id',
  'how_stressed_are_you_assessment_en',
  'how_stressed_are_you_assessment_id',
];
export const HOW_WELL_ARE_YOU_EATING_ASSESSMENT = 'how-well-are-you-eating-assessment';
export const HOW_STRESSED_ARE_YOU_ASSESSMENT = 'how-stressed-are-you-assessment';
export const HOW_ACTIVE_ARE_YOU_ASSESSMENT = 'how-active-are-you-assessment';
export const HOW_WELL_DO_YOU_SLEEP_ASSESSMENT = 'how-well-do-you-sleep-assessment';
export const NON_SMOKER_DECLARATION_ASSESSMENT = 'non-smoker-declaration-assessment';
export const BASIC_HEALTH_SCREENING_ASSESSMENT = 'basic-health-screening';
export const ASSESSMENT_LIST = 'Assessment List';
export const CHALLENGE_LIST = 'challenge-list';
export const EARN_POINT_STEPS = 'earn-point-steps';
export const EARN_POINT_HEART_RATE = 'earn-point-heart-rate';
