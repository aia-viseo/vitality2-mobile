/**
 *
 * AEM Api Center
 * @format
 *
 */

// @flow

import {
  AEM_NEW_CHALLENGE_LIST_MODEL_URL,
  AEM_NEW_BASE_URL,
  AEM_NEW_PATH,
  AEM_NEW_ASSESSMENT_LIST_MODEL_URL,
  AEM_HOW_WELL_ARE_YOU_EATING_ASSESSMENT_MODEL_URL,
  AEM_HOW_HEALTHY_ARE_YOU_ASSESSMENT_MODEL_URL,
  AEM_NEW_BOTTOM_NAV_MODEL_URL,
  AEM_NEW_QUICK_LINKS_MODEL_URL,
  AEM_NEW_GENERAL_MODEL_URL,
  AEM_HOW_STRESSED_ARE_YOU_ASSESSMENT_MODEL_URL,
  AEM_HOW_ACTIVE_ARE_YOU_ASSESSMENT_MODEL_URL,
  AEM_HOW_WELL_DO_YOU_SLEEP_ASSESSMENT_MODEL_URL,
  AEM_NON_SMOKER_DECLARATION_ASSESSMENT_MODEL_URL,
  AEM_BASIC_HEALTH_SCREENING_ASSESSMENT_MODEL_URL,
  AEM_EARN_POINT_STEPS_URL,
  AEM_EARN_POINT_HEART_RATE_URL,
  AEM_POINTS_MODEL_URL,
} from '@constants/Request';

import {
  HOW_HEALTHY_ARE_YOU_ASSESSMENT,
  HOW_WELL_ARE_YOU_EATING_ASSESSMENT,
  HOW_STRESSED_ARE_YOU_ASSESSMENT,
  HOW_ACTIVE_ARE_YOU_ASSESSMENT,
  HOW_WELL_DO_YOU_SLEEP_ASSESSMENT,
  NON_SMOKER_DECLARATION_ASSESSMENT,
  BASIC_HEALTH_SCREENING_ASSESSMENT,
  ASSESSMENT_LIST,
  CHALLENGE_LIST,
} from './config';

import AEMResponseParser from '../../handler/AEMResponseParser';
import AEMApiRequest from './AEMApiRequest';
import AEMApiCache from './AEMApiCache';

const getAssessmentList = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`assessmentList_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NEW_ASSESSMENT_LIST_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[ASSESSMENT_LIST];
        AEMApiCache.cacheNewAEMContent(`assessmentList_${lang}`, parseData.card, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getChallengeList = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`challengeList_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NEW_CHALLENGE_LIST_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[CHALLENGE_LIST];
        AEMApiCache.cacheNewAEMContent(`challengeList_${lang}`, parseData.card, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getGeneral = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`general_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NEW_GENERAL_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result).general.General;
        AEMApiCache.cacheNewAEMContent(`general_${lang}`, parseData, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getQuickLinks = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`quickLinks_${lang}`, (response) => {
    if (response) {
      callback(response);
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NEW_QUICK_LINKS_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)['Quick links'][
          'Quick links'
        ];
        AEMApiCache.cacheNewAEMContent(`quickLinks_${lang}`, parseData, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getPoints = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`points_${lang}`, (response) => {
    if (response) {
      callback(response);
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_POINTS_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result).points;
        AEMApiCache.cacheNewAEMContent(`points_${lang}`, parseData, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getBottomNavigationList = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`bottomNavigationList_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NEW_BOTTOM_NAV_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)['Bottom Navigation'][
          'Bottom Navigation'
        ];
        AEMApiCache.cacheNewAEMContent(
          `bottomNavigationList_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getHowHealthyAreYouAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`howHealthyAreYouAssessment_${lang}`, (response) => {
    if (response) {
      callback(response);
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_HOW_HEALTHY_ARE_YOU_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          HOW_HEALTHY_ARE_YOU_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `howHealthyAreYouAssessment_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const removeAEMCachedContents = (callback: () => void) => {
  AEMApiCache.removeAEMCachedContents(() => {
    callback();
  });
};

const getEatingWellAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`eating_well_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_HOW_WELL_ARE_YOU_EATING_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          HOW_WELL_ARE_YOU_EATING_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `eating_well_assessment_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getHowStressedAreYouAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`how_stressed_are_you_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_HOW_STRESSED_ARE_YOU_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          HOW_STRESSED_ARE_YOU_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `how_stressed_are_you_assessment${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getHowActiveAreYouAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`how_active_are_you_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_HOW_ACTIVE_ARE_YOU_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          HOW_ACTIVE_ARE_YOU_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `how_active_are_you_assessment${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getHowWellDoYouSleepAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`how_well_do_you_sleep_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_HOW_WELL_DO_YOU_SLEEP_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          HOW_WELL_DO_YOU_SLEEP_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `how_well_do_you_sleep_assessment_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getNonSmokerDeclarationAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`non_smoker_declaration_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_NON_SMOKER_DECLARATION_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          NON_SMOKER_DECLARATION_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `non_smoker_declaration_assessment_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getBasicHealthScreeningAssessment = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`basic_health_screening_assessment_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_BASIC_HEALTH_SCREENING_ASSESSMENT_MODEL_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then((result) => {
        const parseData = AEMResponseParser.parseNewAEMContent(result)[
          BASIC_HEALTH_SCREENING_ASSESSMENT
        ];
        AEMApiCache.cacheNewAEMContent(
          `basic_health_screening_assessment_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const getEarnPointSteps = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`earn_point_steps_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_EARN_POINT_STEPS_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then(() => {
        // const parseData = AEMResponseParser.parseNewAEMContent(result)[EARN_POINT_STEPS];
        const parseData = [
          {
            title: '60% of your age max. for 30 mins',
            subTitle: 'For you, that’s 138 bpm ',
            value: '50',
          },
          {
            title: '70% of your age max. for 30 mins',
            subTitle: 'For you, that’s 129 bpm ',
            value: '100',
          },
          {
            title: '60% of your age max. for 60 mins',
            subTitle: 'For you, that’s 146 bpm ',
            value: '100',
          },
        ];
        AEMApiCache.cacheNewAEMContent(`earn_point_steps_${lang}`, parseData, (callbackData) => {
          callback(callbackData);
        });
      })
      .catch(() => {
        callback({});
      });
  });
};

const getEarnPointHeartRate = (lang: string, callback: ({}) => void) => {
  AEMApiCache.getCacheAEMContent(`earn_point_heart_rate_${lang}`, (response) => {
    if (response) {
      callback(response);
      return;
    }
    const customConfig = {
      url: `${AEM_NEW_BASE_URL}${AEM_NEW_PATH}${lang}${AEM_EARN_POINT_HEART_RATE_URL}`,
      method: 'GET',
    };
    AEMApiRequest.createRequest(customConfig)
      .then(() => {
        // const parseData = AEMResponseParser.parseNewAEMContent(result)[EARN_POINT_HEART_RATE];
        const parseData = [
          {title: 'Up to 7,500', value: '50'},
          {title: 'Up to 12,500', value: '100'},
        ];
        AEMApiCache.cacheNewAEMContent(
          `earn_point_heart_rate_${lang}`,
          parseData,
          (callbackData) => {
            callback(callbackData);
          }
        );
      })
      .catch(() => {
        callback({});
      });
  });
};

const AEMApiCenter = {
  getChallengeList,
  getGeneral,
  getQuickLinks,
  getBottomNavigationList,
  getEatingWellAssessment,
  getAssessmentList,
  getHowHealthyAreYouAssessment,
  removeAEMCachedContents,
  getHowStressedAreYouAssessment,
  getHowActiveAreYouAssessment,
  getHowWellDoYouSleepAssessment,
  getNonSmokerDeclarationAssessment,
  getBasicHealthScreeningAssessment,
  getEarnPointSteps,
  getEarnPointHeartRate,
  getPoints,
};

export default AEMApiCenter;
