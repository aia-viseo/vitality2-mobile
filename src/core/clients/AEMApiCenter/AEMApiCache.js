/**
 *
 * AEM Api Cache
 * @format
 *
 */

// @flow

import AEMResponseParser from '../../handler/AEMResponseParser';
import {Cache} from '../Request';
import {AME_CONTENT_KEYS} from './config';

const cacheAEMContent = (key: string, result: any, callback: function): void => {
  const parseData = AEMResponseParser.parseAEMContent(result);
  Cache.cacheUrlContent(
    key,
    parseData,
    () => {
      callback(parseData);
    },
    () => {
      callback(parseData);
    }
  );
};

const cacheNewAEMContent = (key: string, data: any, callback: function): void => {
  Cache.cacheUrlContent(
    key,
    data,
    () => {
      callback(data);
    },
    () => {
      callback(data);
    }
  );
};

const getCacheAEMContent = (key: string, callback: function) => {
  Cache.getCachedUrlContent(
    key,
    (response) => {
      delete response.expireAt;
      callback(response);
    },
    () => {
      callback(null);
    }
  );
};

const removeAEMCachedContents = (callback: function) => {
  Cache.removeCachedUrlContents(
    AME_CONTENT_KEYS,
    () => {
      callback();
    },
    () => {
      callback();
    }
  );
};

const AEMApiCache = {
  getCacheAEMContent,
  cacheNewAEMContent,
  cacheAEMContent,
  removeAEMCachedContents,
};

export default AEMApiCache;
