/**
 *
 * AEM Api Request
 * @format
 *
 */

// @flow

import axios, {AxiosResponse} from 'axios';
import FormData from 'form-data';

import {AEM_AUTH_URL, AEM_AUTH_USERNAME, AEM_AUTH_PASSWORD} from '@constants/Request';
import type {ConfigType} from '@clients/Request/types';

const getAuthConfig = () => {
  const formData = new FormData();
  formData.append('j_username', AEM_AUTH_USERNAME);
  formData.append('j_password', AEM_AUTH_PASSWORD);
  formData.append('j_validate', true);
  const config = {
    url: AEM_AUTH_URL,
    method: 'POST',
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };
  return config;
};

const createRequest = (customConfig: ConfigType) => {
  return new Promise<{}>(
    (resolve: (response: AxiosResponse) => void, reject: (response: AxiosResponse) => void) => {
      axios(getAuthConfig())
        .then(() => {
          axios(customConfig)
            .then((res) => {
              const {status} = res;
              if (status !== 200) {
                resolve({});
              } else {
                resolve(res.data);
              }
            })
            .catch((error) => {
              reject(error);
            });
        })
        .catch((error) => {
          reject(error);
        });
    }
  );
};

const AEMApiRequest = {
  createRequest,
};

export default AEMApiRequest;
