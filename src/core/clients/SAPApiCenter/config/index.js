/**
 *
 * @format
 *
 */

// @flow

// keys
const MEMBERSHIP_INFO_DATA = 'MEMBERSHIP_INFO_DATA';

export default MEMBERSHIP_INFO_DATA;
