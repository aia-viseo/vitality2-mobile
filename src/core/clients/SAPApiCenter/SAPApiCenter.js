/**
 *
 * SAPApiCenter
 * @format
 *
 */

// @flow

import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import type {UserInfoType} from '@contexts/Global/types';
import {
  SAP_MPV_UAT_HOST,
  X_AIA_REQUEST_ID,
  X_VITALITY_LEGAL_ENTITY_ID,
  AEM_ORIGIN,
} from '@constants/Request';
import AuthCenter from '@clients/AuthCenter';

import MEMBERSHIP_INFO_DATA from './config';

const getMembershipInfo = (
  jwt: string,
  membershipNo: string,
  callback: (info?: UserInfoType) => void,
  errorCallback: (msg: string) => void
) => {
  const config = {
    url: `https://${SAP_MPV_UAT_HOST}/memberships/${membershipNo}`,
    method: 'GET',
    headers: {
      'X-AIA-Request-Id': X_AIA_REQUEST_ID,
      'X-Vitality-Legal-Entity-Id': X_VITALITY_LEGAL_ENTITY_ID,
      Authorization: `Bearer ${jwt}`,
      Origin: AEM_ORIGIN,
    },
    timeout: 30000,
    withCredentials: true,
  };
  axios(config)
    .then((res) => {
      const {data, status} = res;
      if (status === 200) {
        const {members, membership} = data;
        const userInfo: UserInfoType = {
          username: members.principleMember.firstName || members.principleMember.fullName,
          firstname: members.principleMember.firstName,
          lastname: members.principleMember.lastName,
          fullname: members.principleMember.fullName,
          membershipEndDate: membership.membershipEndDate,
          membershipNumber: membership.vitalityMembershipNo,
          level: membership.currentVitalityStatus,
          nextLevel: membership.nextVitalityStatus,
          points: parseFloat(membership.pointsEarned),
          pointsToMaintainStatus: parseFloat(membership.pointsToMaintainStatus),
          pointsToNextStatus: parseFloat(membership.pointsToNextStatus),
          currentPolicyPeriodToDate: membership.currentPolicyPeriodToDate,
        };
        AuthCenter.setMembershipNumber(membershipNo);
        AsyncStorage.setItem(MEMBERSHIP_INFO_DATA, JSON.stringify(data)).catch(() => {});
        callback(userInfo);
      } else {
        callback({});
      }
    })
    .catch(() => {
      if (errorCallback) {
        errorCallback(`Error: Unable to fetch data.`);
      }
    });
};

const SAPApiCenter = {
  getMembershipInfo,
};

export default SAPApiCenter;
