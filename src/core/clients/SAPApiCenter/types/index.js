/**
 *
 * @format
 *
 */

// @flow

export type MembershipInfoType = {
  membership: {
    benefitStatus: string,
    currentPolicyPeriodFromDate: string,
    currentPolicyPeriodToDate: string,
    currentVitalityStatus: string,
    eCardNo: string,
    membershipType: string,
    nextVitalityStatus: string,
    membershipStartDate: string,
    membershipEndDate: string,
    pointsEarned: string,
    pointsToMaintainStatus: string,
    pointsToNextStatus: string,
    region: string,
    vitalityMembershipNo: string,
  },
  members: {
    principleMember: {
      firstName: string,
      lastName: string,
      fullName: string,
    },
  },
};
