/**
 *
 * HealthSync
 * @format
 *
 */

// @flow

import {NativeModules} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import lodash from 'lodash';
import RNSecureKeyStore from 'react-native-secure-key-store';
import axios, {AxiosResponse} from 'axios';

import GetOS from '@platform';
import {
  X_AIA_REQUEST_ID,
  AEM_SIT_HOST_NAME,
  AEM_SIT_HEALTH_DATA_PATH,
  AEM_ORIGIN,
  TENANT_ID,
  STRING_BY_APPENDING_STRING,
} from '@constants/Request';

import {
  AUTH_DEVICE,
  AUTH_IOS,
  AUTH_ANDROID,
  UPLOAD_DATE_TIME,
  MEMBERSHIP_NUMBER,
  TOKEN_TYPE,
  APPLE_HEALTH,
  MSG_DATA_SUBMITTED_SUCCESSFULLY,
  MSG_DATA_SUBMITTED_FAILED,
  MSG_NO_DATA,
  MSG_PARSE_DATA_ERROR,
  MSG_NO_MEMBERSHIP_NUMBER,
  MSG_VERIFY_APPLE_HEALTH_ERROR,
  MSG_VERIFY_ANDROID_HEALTH_ERROR,
  VITALITY_TOKEN_KEY,
  MSG_NO_AUTH,
} from './config';

import DateTime from '../../handler/DateTime';

const {TSSBridgeModule} = NativeModules;

const isIOS = GetOS.getInstance() === 'ios';

const saveHealthAuthState = () => {
  AsyncStorage.setItem(AUTH_DEVICE, isIOS ? AUTH_IOS : AUTH_ANDROID);
};

const checkAuth = (callback?: () => void) => {
  AsyncStorage.getItem(AUTH_DEVICE).then((value) => {
    if (value === AUTH_IOS || value === AUTH_ANDROID) {
      if (callback) {
        callback();
      }
    }
  });
};

const uploadHealthDataRequest = (
  dataArray: Array<{[key: string]: any}>,
  membershipNo: string,
  authorization: string,
  callback?: (msg: string, result?: boolean) => void
) => {
  let flag = 0;
  let successFlag = 0;
  let errorFlag = 0;
  const promise = [];
  const axiosConfig = {
    url: `${AEM_SIT_HOST_NAME}${AEM_SIT_HEALTH_DATA_PATH}?membership-no=${membershipNo}`,
    method: 'POST',
    headers: {
      Authorization: authorization,
      'X-Vitality-Legal-Entity-Id': TENANT_ID,
      'X-AIA-Request-Id': X_AIA_REQUEST_ID,
      'Content-Type': 'application/json',
      Origin: AEM_ORIGIN,
    },
    data: {},
    timeout: 30000,
    credentials: 'include',
  };
  lodash.forEach(dataArray, (data) => {
    axiosConfig.data = data;
    const fetchCall = new Promise((resolve, reject) => {
      axios(axiosConfig).then(
        (res: AxiosResponse) => {
          flag += 1;
          const {status} = res;
          if (status === 200) {
            successFlag += 1;
          }

          if (flag === dataArray.length) {
            // all api call end
            if (successFlag === dataArray.length) {
              // add api call success
              if (callback) {
                callback(`${dataArray.length} ${MSG_DATA_SUBMITTED_SUCCESSFULLY}`, true);
              }

              AsyncStorage.setItem(
                UPLOAD_DATE_TIME,
                DateTime.format(new Date(), 'dd MMM yyyy, HH:mm')
              );
              if (isIOS) {
                TSSBridgeModule.saveUploadDate();
              }
            } else if (callback) {
              callback(MSG_DATA_SUBMITTED_FAILED, false);
            }

            resolve(true);
          }
        },
        (error) => {
          errorFlag += 1;
          if (errorFlag === 1) {
            const {message} = error;
            try {
              if (message) {
                if (callback) {
                  callback(MSG_DATA_SUBMITTED_FAILED, false);
                }
              }
            } catch (exception) {
              if (callback) {
                callback(MSG_DATA_SUBMITTED_FAILED, false);
              }
            }
          }
          reject(error);
        }
      );
    });
    promise.push(fetchCall);
  });
  Promise.all(promise);
};

const getDataToSync = (
  memberShipNum: string,
  data: string,
  callback?: (msg: string, result?: boolean) => void
) => {
  if (data.match('NoAuth')) {
    if (callback) {
      callback(MSG_NO_AUTH, false);
    }
    return;
  }

  saveHealthAuthState();

  if (data === '[]') {
    if (callback) {
      callback(MSG_NO_DATA, true);
    }
    return;
  }

  const dataArray = JSON.parse(data);

  if (dataArray == null) {
    if (callback) {
      callback(MSG_PARSE_DATA_ERROR, true);
    }
    return;
  }
  if (memberShipNum !== null && memberShipNum !== undefined && memberShipNum !== '') {
    RNSecureKeyStore.get(VITALITY_TOKEN_KEY).then((token) => {
      uploadHealthDataRequest(dataArray, memberShipNum, `${TOKEN_TYPE} ${token}`, callback);
    });
  } else if (callback) {
    callback(MSG_NO_MEMBERSHIP_NUMBER, false);
  }
};

const uploadHealthData = (callback?: (msg: string, result?: boolean, data?: string) => void) => {
  AsyncStorage.getItem(MEMBERSHIP_NUMBER).then((memberShipNum) => {
    if (isIOS) {
      TSSBridgeModule.verifyAppleHealth(
        APPLE_HEALTH,
        STRING_BY_APPENDING_STRING,
        TENANT_ID,
        (error, events) => {
          if (error) {
            if (callback) {
              callback(MSG_VERIFY_APPLE_HEALTH_ERROR, false);
            }
          } else {
            if (events[0].notAvailable) {
              if (callback) {
                callback(MSG_NO_DATA, false);
              }
              return;
            }
            if (events[0].noData) {
              if (callback) {
                callback(MSG_NO_DATA, true);
              }
              return;
            }
            const data = JSON.stringify(events);
            getDataToSync(memberShipNum, data, (msg: string, result?: boolean) => {
              if (callback) {
                callback(msg, result, data);
              }
            });
          }
        }
      );
    } else {
      TSSBridgeModule.querySHealthData(
        memberShipNum,
        (error) => {
          if (error && callback) {
            callback(MSG_VERIFY_ANDROID_HEALTH_ERROR, false);
          }
        },
        (events) => {
          let data;
          if (typeof events === 'string') {
            data = events;
          } else {
            data = JSON.stringify(events);
          }

          getDataToSync(memberShipNum, data, (msg: string, result?: boolean) => {
            if (callback) {
              callback(msg, result, data);
            }
          });
        }
      );
    }
  });
};

const HealthSync = {
  saveHealthAuthState,
  checkAuth,
  uploadHealthData,
  uploadHealthDataRequest,
};

export default HealthSync;
