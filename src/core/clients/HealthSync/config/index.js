/**
 *
 * @format
 *
 */

// @flow

// keys
export const AUTH_DEVICE = 'AUTH_DEVICE';
export const AUTH_IOS = 'AUTH_IOS';
export const AUTH_ANDROID = 'AUTH_ANDROID';
export const UPLOAD_DATE_TIME = 'UPLOAD_DATE_TIME';
export const MEMBERSHIP_NUMBER = 'MEMBERSHIP_NUMBER';
export const LOGIN_TIME_CRYPT = 'LOGIN_TIME_CRYPT';
export const TOKEN_TYPE = 'Bearer';
export const APPLE_HEALTH = 'appleHealth';
export const VITALITY_TOKEN_KEY = 'VITALITY_TOKEN_KEY';

// messages
export const MSG_DATA_SUBMITTED_SUCCESSFULLY = `day's data submitted successfully`;
export const MSG_DATA_SUBMITTED_FAILED = `Upload health data failed, please try again later.`;
export const MSG_NO_DATA = `There isn't new data need to upload`;
export const MSG_PARSE_DATA_ERROR = `Some error happened when parse the data`;
export const MSG_GET_ACCOUNT_ERROR = `Get account info error, please try again`;
export const MSG_NO_MEMBERSHIP_NUMBER = `No Membership number`;
export const MSG_VERIFY_APPLE_HEALTH_ERROR = `Verify Apple Health error`;
export const MSG_VERIFY_ANDROID_HEALTH_ERROR = `Verify Android Health error`;
export const MSG_NO_AUTH = `No Authorization`;

// fake data
export const FAKE_SYNC_DATA = [
  {
    header: {
      uploadDateTime: '2019-10-08T11:10:17',
      partnerSystem: 'Apple',
      processingType: 'REALTIME',
      rawUploadData: '',
      verified: 'true',
    },
    device: {
      deviceId: '1121A8B9F2-8F5A-4CE2-A78E-EB44DF750BB91',
      make: 'Apple',
      manufacturer: 'Apple',
      model: 'iOSHealthApp',
    },
    readings: [
      {
        dataCategory: 'FITNESS',
        endTime: '2019-10-08T23:59:59',
        integrity: 'VERIFIED',
        manufacturer: 'Apple',
        model: 'iOSHealthApp',
        notes: 'Excercise',
        partnerCreateDate: '',
        partnerReadingId: '21A8B9F2-8F5A-4CE2-A78E-EB44DF750BB1',
        readingType: 'Walking',
        startTime: '2019-10-08T00:00:00',
        workout: {
          distance: {
            unitOfMeasurement: 'METERS',
            value: '1500',
          },
          intensity: 'General',
          totalSteps: '13701',
        },
      },
    ],
  },
];
