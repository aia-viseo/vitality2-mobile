## Health Sync
Contain health information sync functions

# Usage: saveHealthAuthState
```js
import HealthSync from '/core/clients/HealthSync';

HealthSync.saveHealthAuthState();
```

# Usage: checkAuth
```js
import HealthSync from '/core/clients/HealthSync';

HealthSync.checkAuth(() => {

});
```

Key       | Type       | Required | Description         |
--------- | ---------- | -------- | ------------------- |
callback  | `function` | `False`  | Call back function. |

# Usage: uploadHealthDataRequest
```js
import HealthSync from '/core/clients/HealthSync';

const dataArr = [{any}, {any}];
const membershipNo = `xxx`;
const authorization = `xxx`;

HealthSync.uploadHealthDataRequest(dataArr, membershipNo, authorization, () => {

});
```

Key             | Type       | Required | Description                                                |
--------------- | ---------- | -------- | ---------------------------------------------------------- |
dataArr         | `Array`    | `True`   | Array of data pending for sync.                            |
membershipNo    | `string`   | `True`   | User's membership number.                                  |
authorization   | `string`   | `True`   | Authorization token combine by token type and token value. |
callback        | `function` | `False`  | Call back function.                                        |

# Usage: uploadHealthData
```js
import HealthSync from '/core/clients/HealthSync';

HealthSync.uploadHealthData(() => {

});
```

Key       | Type       | Required | Description         |
--------- | ---------- | -------- | ------------------- |
callback  | `function` | `False`  | Call back function. |
