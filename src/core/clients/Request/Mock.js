/**
 *
 * @format
 *
 */

// @flow

import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);

const Mock = {
  mockGetSuccess: (path: string, expectedValue: {}) => {
    mock.onGet(path).reply(200, expectedValue);
  },
  mockGetFail: (path: string, expectedValue: {}) => {
    mock.onGet(path).reply(400, expectedValue);
  },
  mockPostSuccess: (path: string, expectedValue: {}) => {
    mock.onPost(path).reply(200, expectedValue);
  },
  mockPostFail: (path: string, expectedValue: {}) => {
    mock.onPost(path).reply(400, expectedValue);
  },
  mockPutSuccess: (path: string, expectedValue: {}) => {
    mock.onPut(path).reply(200, expectedValue);
  },
  mockPutFail: (path: string, expectedValue: {}) => {
    mock.onPut(path).reply(400, expectedValue);
  },
  mockDeleteSuccess: (path: string, expectedValue: {}) => {
    mock.onDelete(path).reply(200, expectedValue);
  },
  mockDeleteFail: (path: string, expectedValue: {}) => {
    mock.onDelete(path).reply(400, expectedValue);
  },
  resetMock: () => {
    mock.resetMock();
  },
};

export default Mock;
