/**
 *
 * @format
 *
 */

// @flow

import axios, {AxiosError, AxiosResponse} from 'axios';
import Qs from 'qs';
import lodash from 'lodash';

import {AXIOS_AUTH_TOKEN, AXIOS_BASE_URL, AXIOS_CONTENT_TYPE} from '@constants/Request';

import type {ConfigType} from './types';

axios.defaults.headers.common.Authorization = AXIOS_AUTH_TOKEN;
axios.defaults.headers.common['Content-Type'] = AXIOS_CONTENT_TYPE;

const urlProtocolConverter = (url: string, protocol?: string) => {
  const protocolRegex = /([^://]+)/;
  let result = '';

  if (protocol != null) {
    result = url.replace(protocolRegex, protocol);
  }
  result = url;
  return result;
};

const getAxiosConfig = (customConfig: ConfigType) => {
  const {CancelToken} = axios;
  const {
    url,
    method,
    baseURL,
    protocol,
    transformRequestHandler,
    transformResponseHandler,
    headers,
    params,
    paramsSerializerArrayFormat,
    data,
    timeout,
    withCredentials,
    auth,
    responseType,
    responseEncoding,
    xsrfCookieName,
    xsrfHeaderName,
    maxContentLength,
    maxBodyLength,
    validateStatusMin,
    validateStatusMax,
    maxRedirects,
    cancelToken,
    decompress,
  } = customConfig;
  return {
    url: url || '/',
    method: method || 'GET',
    baseURL: urlProtocolConverter(baseURL || AXIOS_BASE_URL, protocol),
    transformRequest: [
      (transformRes: {}): {} => {
        let resultData = lodash.cloneDeep(transformRes);
        if (transformRequestHandler) {
          resultData = transformRequestHandler(transformRes);
        }
        return resultData;
      },
    ],
    transformResponse: [
      (transformResp: {}): {} => {
        let resultData = lodash.cloneDeep(transformResp);
        if (transformResponseHandler) {
          resultData = transformResponseHandler(transformResp);
        }
        return resultData;
      },
    ],
    headers: headers || {'X-Requested-With': 'XMLHttpRequest'},
    params: params || {},
    paramsSerializer: (paramsSerialize: string): {} => {
      return Qs.stringify(paramsSerialize, {
        arrayFormat: paramsSerializerArrayFormat || 'brackets',
      });
    },
    data: data || {},
    timeout: timeout || 5000,
    withCredentials: withCredentials || false,
    auth: auth || {},
    responseType: responseType || 'json',
    responseEncoding: responseEncoding || 'utf8',
    xsrfCookieName: xsrfCookieName || 'XSRF-TOKEN',
    xsrfHeaderName: xsrfHeaderName || 'X-XSRF-TOKEN',
    maxContentLength: maxContentLength || 2000,
    maxBodyLength: maxBodyLength || 2000,
    validateStatus: (status: number): boolean => {
      return status >= (validateStatusMin || 200) && status < (validateStatusMax || 300); // default
    },
    maxRedirects: maxRedirects || 5,
    cancelToken: new CancelToken((cancel: string) => {
      if (cancelToken) {
        cancelToken(cancel);
      }
    }),
    decompress: decompress || true,
  };
};

const apiRequestBuilder = (
  customConfig: ConfigType,
  successCallback?: (response: AxiosResponse) => void,
  errorCallback?: (error: AxiosError) => void,
  finallyCallback?: () => void
) => {
  axios
    .request(getAxiosConfig(customConfig))
    .then((response: AxiosResponse) => {
      if (successCallback) {
        successCallback(response);
      }
    })
    .catch((error: AxiosError) => {
      if (errorCallback) {
        errorCallback(error);
      }
    })
    .finally(() => {
      if (finallyCallback) {
        finallyCallback();
      }
    });
};

const Request = {
  apiRequestBuilder,
};

export default Request;
