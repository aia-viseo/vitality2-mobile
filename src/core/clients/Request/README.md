## Request
Request call helper functions

apiRequestBuilder
A function to build and call an axios request

Usage
```js
import { Request } from 'core/clients/Request'

Request.apiRequestBuilder(customConfig, successCallback, errorCallback, finallyCallback);
```

Example
```js
import { Request } from 'core/clients/Request'

const customConfig = {
  url,
  method,
  baseURL,
  transformRequestHandler,
  transformResponseHandler,
  headers,
  params,
  paramsSerializerArrayFormat,
  data,
  timeout,
  withCredentials,
  auth,
  responseType,
  responseEncoding,
  xsrfCookieName,
  xsrfHeaderName,
  maxContentLength,
  maxBodyLength,
  validateStatusMin,
  validateStatusMax,
  maxRedirects,
  cancelToken,
  decompress,
  protocol
};

Request.apiRequestBuilder(customConfig,
  (response) => {
    //Do something with response
  }, (error) => {
    //Do something with error
  }, () => {
    //Do something finally
  });
```

| key              | type        | Required | Description                                                 |
| ---------------- | ----------- | -------- | ----------------------------------------------------------- |
| customConfig     | `Object`    | `True`   | Axios custom config object                                  |
| successCallback  | `function`  | `False`  | Trigger when api call getting success                       |
| errorCallback    | `function`  | `False`  | Trigger when api call getting error                         |
| finallyCallback  | `function`  | `False`  | Trigger whether the promise was fulfilled success or error  |

# Local Function

getAxiosConfig
A function to build an Axios config

Usage
```js
import { Request } from 'core/clients/Request'

Request.getAxiosConfig(customConfig);
```

Example
```js
import { Request } from 'core/clients/Request'

const customConfig = {
  url,
  method,
  baseURL,
  transformRequestHandler,
  transformResponseHandler,
  headers,
  params,
  paramsSerializerArrayFormat,
  data,
  timeout,
  withCredentials,
  auth,
  responseType,
  responseEncoding,
  xsrfCookieName,
  xsrfHeaderName,
  maxContentLength,
  maxBodyLength,
  validateStatusMin,
  validateStatusMax,
  maxRedirects,
  cancelToken,
  decompress,
  protocol
};

Request.getAxiosConfig(customConfig);
```

customConfig Structure
| key                         | type                                            | Required | Description                                                               |
| --------------------------- | ----------------------------------------------- | -------- | ------------------------------------------------------------------------- |
| url                         | `string`                                        | `False`  | The endpoint that will be used for the request.                           |
| method                      | `GET | POST | PUT | DELETE`                     | `False`  | The request method to be used when making the request.                    |
| baseURL                     | `string`                                        | `False`  | An instance of axios to pass relative URLs to methods of that instance.   |
| transformRequestHandler     | `function`                                      | `False`  | Function to changes the request data before it is sent to the server.     |
| transformResponseHandler    | `function`                                      | `False`  | Function allows changes to the response data to be made before.           |
| headers                     | `object`                                        | `False`  | Custom headers to be sent with the request.                               |
| params                      | `object`                                        | `False`  | URL parameters to be sent with the request.                               |
| paramsSerializerArrayFormat | `brackets | indices | repeat | comma`           | `False`  | Function in charge of serializing `params`.                               |
| data                        | `object`                                        | `False`  | Data to be sent as the request body.                                      |
| timeout                     | `number`                                        | `False`  | Number of milliseconds before the request times out.                      |
| withCredentials             | `bool`                                          | `False`  | Indicates whether or not cross-site Access-Control requests.              |
| auth                        | `object`                                        | `False`  | Authorization custom headers you have set using `headers` <br> note that only HTTP Basic auth is configurable through this parameter.  |
| responseType                | `arraybuffer | document | json | text | stream` | `False`  | Indicates the type of data that the server will respond with.             |
| responseEncoding            | `string`                                        | `False`  | Indicates encoding to use for decoding responses.                         |
| xsrfCookieName              | `string`                                        | `False`  | Name of the cookie to use as a value for xsrf token.                      |
| xsrfHeaderName              | `string`                                        | `False`  | Name of the http header that carries the xsrf token value.                |
| maxContentLength            | `number`                                        | `False`  | The max size of the http response content in bytes allowed.               |
| maxBodyLength               | `number`                                        | `False`  | Defines the max size of the http request content in bytes allowed.        |
| validateStatusMin           | `number`                                        | `False`  | Defines Max code whether to resolve or reject the promise for a given.    |
| validateStatusMax           | `number`                                        | `False`  | Defines Min code whether to resolve or reject the promise for a given.    |
| maxRedirects                | `number`                                        | `False`  | Defines the maximum number of redirects to follow in node.js.             |
| cancelToken                 | `function`                                      | `False`  | Function to handel a cancel token that can be used to cancel the request. |
| decompress                  | `bool`                                          | `False`  | Indicates whether or not the response body should be decompressed.        |
| protocol                     | `string`                                        | `False`  | Defines the url protocol                                                   |

urlProtocolConverter
A function to convert url protocol

Usage
```js
import {Request} from 'core/clients/Request'

Request.urlProtocolConverter(url, protocol);
```

Example
```js
import {Request} from 'core/clients/Request'

const url = 'https://www.google.com';
const protocol = 'http';

Request.urlProtocolConverter(url, protocol);
```
| key      | type           | Required | Description                     |
| -------- | -------------- | -------- | ------------------------------- |
| url      | `string`       | `True`   | The url to be converte.         |
| protocol | `http | https` | `False`  | The protocol replace to the url. |

## Mock
Mock Request call helper functions

mockGetSuccess
Create Get success mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockGetSuccess(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};

Mock.mockGetSuccess(path, expectedValue);
```

mockGetFail
Create Get fail mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockGetFail(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};

Mock.mockGetFail(path, expectedValue);
```

mockPostSuccess
Create Post success mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockPostSuccess(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockPostSuccess(path, expectedValue);
```

mockPostFail
Create Post fail mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockPostFail(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockPostFail(path, expectedValue);
```

mockPutSuccess
Create Put success mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockPutSuccess(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockPutSuccess(path, expectedValue);
```

mockPutFail
Create Put fail mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockPutFail(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockPutFail(path, expectedValue);
```

mockDeleteSuccess
Create Delete success mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockDeleteSuccess(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockDeleteSuccess(path, expectedValue);
```

mockDeleteFail
Create Delete fail mock api return

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.mockDeleteFail(path, expectedValue);
```

Example
```js
import {Mock} from 'core/clients/Request'

const path = '/test';
const expectedValue = {result: true};
Mock.mockDeleteFail(path, expectedValue);
```

resetMock
Reset registered mock

Usage
```js
import {Mock} from 'core/clients/Request'

Mock.resetMock();
```

Example
```js
import {Mock} from 'core/clients/Request'

Mock.resetMock();
```
| key              | type        | Required | Description                |
| ---------------- | ----------- | -------- | -------------------------- |
| path             | `string`    | `True`   | End point of the API       |
| expectedValue    | `object`    | `True`   | expected mock return value |

## Cache
Cache related helper functions

getCachedUrlContent
Function to get content by key from AsyncStorage

Usage
```js
import { Cache } from 'core/clients/Request';

Cache.getCachedUrlContent(urlAsKey, callback, error);
```

Example
```js
import { Cache } from 'core/clients/Request';

const urlAsKey = '/test';
Cache.getCachedUrlContent(urlAsKey,
  (response) => {
    //do sth with response
  }, (error) => {
    //do sth with error
  });
```
| key         | type        | Required | Description                            |
| ----------- | ----------- | -------- | -------------------------------------- |
| urlAsKey    | `string`    | `True`   | Cache key, should be same as the path. |
| callback    | `function`  | `False`  | Trigger when api call getting success. |
| error       | `function`  | `False`  | Trigger when api call getting fail.    |


cacheUrlContent
Function to cache content by key into AsyncStorage

```js
import { Cache } from 'core/clients/Request';

Cache.cacheUrlContent(urlAsKey, data, callback, error);
```
| key         | type        | Required | Description                                      |
| ----------- | ----------- | -------- | ------------------------------------------------ |
| urlAsKey    | `string`    | `True`   | Cache key, should be same as the path.           |
| data        | `object`    | `True`   | Cache value, should be get from the api request. |
| callback    | `function`  | `False`  | Trigger when api call getting success.           |
| error       | `function`  | `False`  | Trigger when api call getting fail.              |


removeCachedUrlContents
Function to remove cache content by keys into AsyncStorage

```js
import { Cache } from 'core/clients/Request';

Cache.removeCachedUrlContents(urlAsKeys, callback, error);
```
| key         | type            | Required | Description                                      |
| ----------- | --------------- | -------- | ------------------------------------------------ |
| urlAsKeys   | `Array<string>` | `True`   | Remove cache keys, should be same as the path.   |
| callback    | `function`      | `False`  | Trigger when api call getting success.           |
| error       | `function`      | `False`  | Trigger when api call getting fail.              |


## Cache Mock Request Example
Example to show how to use Cache Mock Request together

Example
```js
import { Request, Mock, Cache } from 'core/clients/Request';

//Mocking request
Mock.mockGetSuccess(path, data)

//apiRequestBuilder with cacheUrlContent
Request.apiRequestBuilder(customConfig, (response) => {
  //do sth after apiRequestBuilder success
  Cache.cacheUrlContent(path, response, () => {
    //do sth after cache success
  }, (error) => {
    //do sth after cache error
  })
}, (error) => {
  //do sth after apiRequestBuilder error
}, () => {
  //do sth finally
})

//get content from cache
Cache.getCachedUrlContent(path, (data) => {
  //do sth after get cache success
}, (error) => {
  //do sth after get cache error
})
```
