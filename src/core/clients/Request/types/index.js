/**
 *
 * @format
 *
 */

// @flow

export type ConfigType = {
  url?: string,
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE',
  baseURL?: string,
  transformRequestHandler?: (transformRes: {}) => {},
  transformResponseHandler?: (transformRes: {}) => {},
  headers?: {},
  params?: {},
  paramsSerializerArrayFormat?: 'brackets' | 'indices' | 'repeat' | 'comma',
  data?: {},
  timeout?: number,
  withCredentials?: boolean,
  auth?: {},
  responseType?: 'arraybuffer' | 'document' | 'json' | 'text' | 'stream',
  responseEncoding?: string,
  xsrfCookieName?: string,
  xsrfHeaderName?: string,
  maxContentLength?: number,
  maxBodyLength?: number,
  validateStatusMin?: number,
  validateStatusMax?: number,
  maxRedirects?: number,
  cancelToken?: (token: string) => string,
  decompress?: boolean,
  protocol?: string,
};
