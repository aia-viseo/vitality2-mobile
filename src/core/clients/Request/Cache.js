/**
 *
 * Cache Client
 * @format
 *
 */

// @flow

import lodash from 'lodash';
import AsyncStorage from '@react-native-community/async-storage';

import {CACHE_EXPIRE_IN_MINS} from '@constants/Request';

const getExpireDate = () => {
  const now = new Date();
  const expireTime = new Date(now);
  expireTime.setMinutes(now.getMinutes() + CACHE_EXPIRE_IN_MINS);
  return expireTime;
};

const getCachedUrlContent = async (
  urlAsKey: string,
  callback?: (data: {}) => void,
  error?: (err: Error) => void
) => {
  let data = {};
  try {
    await AsyncStorage.getItem(urlAsKey, async (err: Error, value: string) => {
      data = JSON.parse(value);
      // There is data in cache && cache is expired
      if (data !== null && data.expireAt && new Date(data.expireAt) < new Date()) {
        // Clear Cache
        AsyncStorage.removeItem(urlAsKey);
        // Update data to be null
        data = null;
      }
    });
    if (callback) {
      callback(data);
    }
  } catch (err) {
    if (error) {
      error(err);
    }
  }
};

const cacheUrlContent = (
  urlAsKey: string,
  data: {expireAt: Date},
  callback?: () => void,
  error?: (err: Error) => void
) => {
  try {
    const item = lodash.cloneDeep(data);
    item.expireAt = getExpireDate();
    // Stringify object
    const objectToStore = JSON.stringify(item);
    // Store object
    AsyncStorage.setItem(urlAsKey, objectToStore);
  } catch (err) {
    if (error) {
      error(err);
    }
  } finally {
    if (callback) {
      callback();
    }
  }
};

const removeCachedUrlContents = (
  urlAsKeys: Array<string>,
  callback?: () => void,
  error?: (err: Error) => void
) => {
  try {
    AsyncStorage.multiRemove(urlAsKeys);
  } catch (err) {
    if (error) {
      error(err);
    }
  } finally {
    if (callback) {
      callback();
    }
  }
};

const Cache = {
  getCachedUrlContent,
  cacheUrlContent,
  removeCachedUrlContents,
};

export default Cache;
