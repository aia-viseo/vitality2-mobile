/**
 *
 * AuthCenter
 * @format
 *
 */

// @flow

import AsyncStorage from '@react-native-community/async-storage';
import {Base64} from 'js-base64';
import axios from 'axios';
import RNSecureKeyStore, {ACCESSIBLE} from 'react-native-secure-key-store';

import {
  AEM_SIT_TOKEN_HOST_NAME,
  AEM_SIT_TOKEN_PATH,
  TENANT_ID,
  SIT_APP_ID,
  AEM_ORIGIN,
  AXIOS_CONTENT_TYPE,
  SIT_TOKEN_USERNAME,
  SIT_TOKEN_PASSWORD,
} from '@constants/Request';

import {MEMBERSHIP_NUMBER} from '../HealthSync/config';
import {VITALITY_TOKEN_KEY} from './config';

const getJWT = (membershipNo: string, callback: (jwt: string) => void) => {
  const basicAuth = Base64.encode(`${SIT_TOKEN_USERNAME}:${SIT_TOKEN_PASSWORD}`);
  const url = `${AEM_SIT_TOKEN_HOST_NAME}${AEM_SIT_TOKEN_PATH}?entity-id=${TENANT_ID}&app-id=${SIT_APP_ID}&membership-no=${membershipNo}&Origin=${AEM_ORIGIN}`;
  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': AXIOS_CONTENT_TYPE,
      Authorization: `Basic ${basicAuth}`,
      Origin: AEM_ORIGIN,
    },
    timeout: 30000,
    withCredentials: true,
  };
  axios
    .post(url, {}, config)
    .then(async (result) => {
      const {data, status} = result;
      if (status === 200) {
        const {jwt} = data;
        if (jwt) {
          await RNSecureKeyStore.set(VITALITY_TOKEN_KEY, jwt, {
            accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY,
          }).then(() => {
            callback(jwt);
          });
        }
      } else {
        callback('');
      }
    })
    .catch(() => {
      callback('');
    });
};

const setMembershipNumber = (membershipNo: string) => {
  AsyncStorage.setItem(MEMBERSHIP_NUMBER, membershipNo);
};

const getMembershipNumber = (callback: (membershipNo: string) => void) => {
  AsyncStorage.getItem(MEMBERSHIP_NUMBER).then((membershipNo: string) => {
    callback(membershipNo);
  });
};

const AuthCenter = {
  getJWT,
  setMembershipNumber,
  getMembershipNumber,
};

export default AuthCenter;
