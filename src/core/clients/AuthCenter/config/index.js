/**
 *
 * @format
 *
 */

// @flow

// keys
export const MEMBERSHIP_NO_KEY = 'MEMBERSHIP_NO_KEY';
export const VITALITY_TOKEN_KEY = 'VITALITY_TOKEN_KEY';
