/**
 *
 * @format
 *
 */

import Assessment from './Assessment.json';
import Card from './Card.json';
import Challenge from './Challenge.json';
import Dashboard from './Dashboard.json';
import General from './General.json';
import Header from './Header.json';
import Login from './Login.json';
import Points from './Points.json';
import Profile from './Profile.json';
import SectionHeader from './SectionHeader.json';
import VitalityAgeCard from './VitalityAgeCard.json';
import Rewards from './Rewards.json';
import EarnPoints from './EarnPoints.json';
import WorkoutTracker from './WorkoutTracker.json';
import HealthReport from './HealthReport.json';
import Help from './Help.json';
import Notification from './Notification.json';

export default {
  translation: {
    assessment: Assessment,
    card: Card,
    challenge: Challenge,
    dashboard: Dashboard,
    general: General,
    header: Header,
    login: Login,
    points: Points,
    profile: Profile,
    sectionHeader: SectionHeader,
    vitalityAgeCard: VitalityAgeCard,
    rewards: Rewards,
    earnpoints: EarnPoints,
    workoutTracker: WorkoutTracker,
    healthReport: HealthReport,
    help: Help,
    notification: Notification,
  },
};
