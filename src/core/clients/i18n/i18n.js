/**
 *
 * I18n
 * @format
 *
 */

// @flow

import i18next from 'i18next';
import {initReactI18next} from 'react-i18next';

import {DEFAULT_LANGUAGE} from '@constants/i18n';
import en from './translations/en';
import id from './translations/id';

import type {ResourceType} from './types';

const resources = {
  en,
  id,
};

export const addResource = (
  language: string,
  resource: ResourceType,
  deep: boolean,
  overWrite: boolean
) => {
  i18next.addResourceBundle(language, 'translation', resource, deep, overWrite);
};

export default i18next.use(initReactI18next).init({
  lng: DEFAULT_LANGUAGE,
  resources,
  keySeparator: '.',
});
