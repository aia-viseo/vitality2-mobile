## The I18next configuration.

* Step to add languages
  1. Create a .json in translations/
  2. Import that .json
  3. Add into "resources"
```js
  const resources = {
    en: enTranslations
  }
```

* Step to use
  1. import { useTranslation } from 'react-i18next';
  2. const { t, i18n } = useTranslation();
  3. Add {t('labelName')} inside the components

* Language List

| Name              |  Code  |
|:-----------------:|:------:|
| Indonesian        |  id    |
| English           |  en    |


# addResource
Add new language resource in to resources
e.g Can use this function to add the content from AEM

Usage
```js
import { addResource } from 'core/clients/i18n';

addResource(language, resource, deep, overWrite);
```

General Example
```js
import { addResource } from 'core/clients/i18n';

const language = 'en';
const resource = {
  "translation": {
    "Test": "This is Testing",
    "Your Language": "Your Language is {{language}}"
  }
};
const deep = true;
const overWrite = true;

addResource(language, resource, deep, overWrite);
```

AEM Content Example
```js
import { addResource } from 'core/clients/i18n';

const language = 'en';
const deep = true;
const overWrite = true;

GetContentFromAEM((response) => {
  addResource(language, response, deep, overWrite);
})
```
| key        |  type    | Required | Description                                                                   |
| ---------- | -------- | -------- | ----------------------------------------------------------------------------- |
| language   | `string` | `True`   | Target language (language code) you want to add.                              |
| resource   | `object` | `True`   | Language resource {key: value}.                                               |
| deep       | `bool`   | `False`  | (default false) Param to true will extend existing translations in that file. |
| overWrite  | `bool`   | `False`  | (default false) to true it will overwrite existing translations in that file. |
