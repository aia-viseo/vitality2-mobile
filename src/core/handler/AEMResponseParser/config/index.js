/**
 *
 * @format
 *
 */

// @flow

export const ROOT_KEY = 'root';
export const ITEMS_KEY = ':items';
export const TYPE_KEY = ':type';

export const CONTENT_FRAGMENT = 'contentfragment';
export const CONTENT_FRAGMENT_LIST = 'contentfragmentlist';

export const CONTENT_FRAGMENT_1530783676 = 'contentfragment_1530783676';
export const CONTENT_FRAGMENT_1011331187 = 'contentfragment_1011331187';
export const CONTENT_FRAGMENT_1872927630 = 'contentfragment_1872927630';
export const CONTENT_FRAGMENT_429141885 = 'contentfragment_429141885';

export const ASSESSMENT_LIST_CARD = 'card';
export const QUICK_LINKS = 'Quick links';
export const BOTTOM_NAVIGATION = 'Bottom Navigation';
export const GENERAL = 'General';
export const ASSESSMENT_DETAIL = 'Assessment Detail';
export const ASSESSMENT_QUESTION = 'Assessment Question';
export const ASSESSMENT_REVIEW = 'Assessment Review';
export const ASSESSMENT_DONE = 'Assessment Done';
export const OFFLINE_ASSESSMENT_FIND_MORE = 'offline-assessment-find-more';
