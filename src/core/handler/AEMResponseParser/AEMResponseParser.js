/**
 *
 * AEMResponseParser
 * @format
 *
 */

// @flow

import {
  ITEMS_KEY,
  ROOT_KEY,
  TYPE_KEY,
  CONTENT_FRAGMENT,
  CONTENT_FRAGMENT_LIST,
  ASSESSMENT_LIST_CARD,
  QUICK_LINKS,
  BOTTOM_NAVIGATION,
  GENERAL,
  ASSESSMENT_DETAIL,
  ASSESSMENT_QUESTION,
  ASSESSMENT_REVIEW,
  ASSESSMENT_DONE,
  CONTENT_FRAGMENT_1530783676,
  CONTENT_FRAGMENT_1011331187,
  CONTENT_FRAGMENT_1872927630,
  CONTENT_FRAGMENT_429141885,
} from './config';

const isKeyMatched = (key: string): boolean => {
  return (
    key === ASSESSMENT_LIST_CARD ||
    key === QUICK_LINKS ||
    key === BOTTOM_NAVIGATION ||
    key === GENERAL ||
    key === ASSESSMENT_DETAIL ||
    key === ASSESSMENT_QUESTION ||
    key === ASSESSMENT_REVIEW ||
    key === ASSESSMENT_DONE
  );
};

const isKeyPointsFragmentMatched = (key: string): boolean => {
  return (
    key === CONTENT_FRAGMENT ||
    key === CONTENT_FRAGMENT_1530783676 ||
    key === CONTENT_FRAGMENT_1011331187 ||
    key === CONTENT_FRAGMENT_1872927630 ||
    key === CONTENT_FRAGMENT_429141885
  );
};

const parseContentFragmentList = (keyName: string, data: any): any => {
  const {items} = data;
  const parsedItems = items.map((item) => {
    const {elements} = item;
    let parsedItem = {};
    const keys = Object.keys(elements);
    keys.forEach((key) => {
      const {value} = elements[key];
      parsedItem = Object.assign(parsedItem, {[key]: value});
    });
    return parsedItem;
  });
  let parsedCardContent = {};
  parsedItems.forEach((parsedItem) => {
    const {id} = parsedItem;
    parsedCardContent = Object.assign(parsedCardContent, {
      [id]: parsedItem,
    });
  });
  return {[keyName]: parsedCardContent};
};

const parseContentFragment = (keyName: string, data: any): any => {
  const {elements} = data;
  let content = {};
  const keys = Object.keys(elements);
  keys.forEach((key) => {
    const {value} = elements[key];
    content = Object.assign(content, {[key]: value});
  });
  return {[keyName]: content};
};

const parsePointsContentFragment = (data: any): any => {
  const {elements, title} = data;
  let content = {};
  const keys = Object.keys(elements);
  keys.forEach((key) => {
    const {value} = elements[key];
    content = Object.assign(content, {[key]: value});
  });
  return {[title]: content};
};

const isContentFragmentList = (contentType: string): boolean => {
  return contentType.includes(CONTENT_FRAGMENT_LIST);
};

const parseNewAEMContent = (data: any): any => {
  const {title} = data;
  let content = {};
  function parseContent(json) {
    const keys = Object.keys(json);
    keys.forEach((key) => {
      if (key === ITEMS_KEY || key === ROOT_KEY) {
        parseContent(json[key]);
      } else {
        const initialParseJSON = json[key];
        const contentType = initialParseJSON[TYPE_KEY];
        if (contentType && isKeyMatched(key)) {
          content = Object.assign(
            content,
            isContentFragmentList(contentType)
              ? parseContentFragmentList(key, initialParseJSON)
              : parseContentFragment(key, initialParseJSON)
          );
        } else if (contentType && isKeyPointsFragmentMatched(key)) {
          content = Object.assign(content, parsePointsContentFragment(initialParseJSON));
        }
      }
    });
  }
  parseContent(data);

  return {[title]: content};
};

const parseOldContentFragmentList = (data: any): any => {
  const {items} = data;
  const parseItems = items.map((item) => {
    const {elements} = item;
    let element = {};
    const keys = Object.keys(elements);
    keys.forEach((key) => {
      const {value} = elements[key];
      element = Object.assign(element, {[key]: value});
    });
    return element;
  });
  return {items: parseItems};
};

const parseOldContentFragment = (data: any): any => {
  const {title, elements} = data;
  let content = {};
  content = {title};
  const keys = Object.keys(elements);
  keys.forEach((key) => {
    const {value} = elements[key];
    content = Object.assign(content, {[key]: value});
  });
  return content;
};

const parseAEMContent = (data: any): any => {
  let content = {};
  function parseContent(json) {
    const keys = Object.keys(json);
    keys.forEach((key) => {
      if (key === ITEMS_KEY || key === ROOT_KEY) {
        parseContent(json[key]);
      }
      if (keys.indexOf(CONTENT_FRAGMENT_LIST) !== -1 && keys.indexOf(CONTENT_FRAGMENT) !== -1) {
        if (key === CONTENT_FRAGMENT_LIST) {
          content = Object.assign(content, parseOldContentFragmentList(json[key]));
        } else if (key === CONTENT_FRAGMENT) {
          content = Object.assign(content, parseOldContentFragment(json[key]));
        }
      }
    });
  }
  parseContent(data);

  return content;
};

const AEMResponseParser = {
  parseNewAEMContent,
  parseAEMContent,
};

export default AEMResponseParser;
