/**
 *
 * DateTime
 * @format
 *
 */

// @flow

import {
  format as libFormat,
  differenceInMilliseconds as libDifferenceInMilliseconds,
  isToday as libIsToday,
} from 'date-fns';

// https://date-fns.org/v2.14.0/docs/format
const format = (date: Date, formatSpec?: string) => {
  return libFormat(date, formatSpec || 'dd MMM yyyy, HH:mm');
};

const differenceInMilliseconds = (newDate: Date, oldDate: Date) => {
  return libDifferenceInMilliseconds(newDate, oldDate || new Date());
};

const isToday = (date: Date) => {
  return libIsToday(date);
};

const getDaysBeforeDate = (daysPasted: number) => {
  const date = new Date();
  return date.setDate(date.getDate() - daysPasted);
};
const DateTime = {
  format,
  differenceInMilliseconds,
  isToday,
  getDaysBeforeDate,
};

export default DateTime;
