/**
 *
 * @format
 *
 */

// @flow

import GetOS from '../../Singleton/Platform';

const isIOS = GetOS.getInstance() === 'ios';

const toLocaleString = (number: number): string => {
  let numberStr = '';
  if (isIOS) {
    numberStr = number.toLocaleString();
  } else {
    numberStr = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  return numberStr;
};

const NumberString = {
  toLocaleString,
};

export default NumberString;
