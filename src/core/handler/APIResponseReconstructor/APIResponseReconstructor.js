/**
 *
 * APIResponseReconstructor
 * @format
 *
 */

// @flow

const restructureAEMAssessmentList = (
  aemAssessmentsContent: Object,
  assessments: Array<Object>
): Array<Object> => {
  const restructuredAssessments =
    assessments && assessments !== {} && assessments.length > 0
      ? assessments.map((item: Object) => {
          const {keyName, rewardValue} = item;
          const aemAssessmentContent = aemAssessmentsContent[keyName];
          if (aemAssessmentContent) {
            const {
              title,
              description,
              id,
              iconName,
              iconGroup,
              imageName,
              imageGroup,
            } = aemAssessmentContent;
            return {
              ...item,
              title,
              iconName,
              iconGroup,
              imageName,
              imageGroup,
              points: rewardValue,
              description,
              id,
            };
          }
          return undefined;
        })
      : [];
  return restructuredAssessments.filter((item) => item);
};

const restructureAEMChallengeList = (
  challengesContent: Object,
  challenges: Array<Object>
): Array<Object> => {
  const restructuredChallenges =
    challenges && challenges !== {} && challenges.length > 0
      ? challenges.map((item: Object) => {
          const {keyName} = item;
          const aemChallengeContent = challengesContent[keyName];
          if (aemChallengeContent) {
            const {rewardValue, targetValue, statusFromDate, statusToDate} = item;
            const {
              title,
              subTitle,
              variation,
              challengeDaysLeft,
              description,
              highlightText,
              id,
              imageName,
              imageGroup,
              inDashboard,
              iconName,
              iconGroup,
            } = aemChallengeContent;
            return {
              ...item,
              id,
              variationNumber: variation,
              title,
              subTitle,
              iconName,
              iconGroup,
              imageName,
              imageGroup,
              inDashboard,
              points: rewardValue,
              challengeDaysLeft,
              endDate: statusToDate ? new Date(statusToDate) : null,
              minPoints: rewardValue,
              maxPoints: targetValue,
              voucher: variation === 2 ? 1 : 0,
              description,
              highlightText,
              statusFromDate: statusFromDate ? new Date(statusFromDate) : null,
              statusToDate: statusToDate ? new Date(statusToDate) : null,
            };
          }
          return undefined;
        })
      : [];
  return restructuredChallenges.filter((item) => item);
};

const APIResponseReconstructor = {
  restructureAEMAssessmentList,
  restructureAEMChallengeList,
};

export default APIResponseReconstructor;
