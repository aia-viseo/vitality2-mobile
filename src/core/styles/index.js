/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {HEADER_MAX_HEIGHT} from '@constants/Header';
import {COLOR_AIA_DARK_GREY, WHITE, COLOR_AIA_RED, COLOR_AIA_GREEN} from '@colors';

import {projectFontFamily} from '@core/config';

import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const AppStyles = StyleSheet.create({
  white: {
    color: WHITE,
  },
  primary: {
    color: COLOR_AIA_RED,
  },
  padding_0: {
    padding: 0,
  },
  padding_10: {
    padding: 10,
  },
  paddingVertical_5: {
    paddingVertical: 5,
  },
  paddingVertical_10: {
    paddingVertical: 10,
  },
  paddingVertical_20: {
    paddingVertical: 20,
  },
  paddingLeft_20: {
    paddingLeft: 20,
  },
  paddingLeft_10: {
    paddingLeft: 10,
  },
  paddingRight_20: {
    paddingRight: 20,
  },
  paddingRight_40: {
    paddingRight: 40,
  },
  paddingRight_10: {
    paddingLeft: 10,
  },
  paddingHorizontal_10: {
    paddingHorizontal: 10,
  },
  paddingHorizontal_20: {
    paddingHorizontal: 20,
  },
  paddingHorizontal_60: {
    paddingHorizontal: 60,
  },
  paddingTop_10: {
    paddingTop: 10,
  },
  paddingTop_20: {
    paddingTop: 20,
  },
  paddingBottom_20: {
    paddingBottom: 20,
  },
  generalButtonContentStyle: {
    fontFamily: projectFontFamily,
    paddingVertical: 9,
  },
  generalButton: {
    borderRadius: 10,
    color: WHITE,
    fontFamily: projectFontFamily,
  },
  generalOutlinedButton: {
    borderWidth: 1,
    borderColor: COLOR_AIA_RED,
    backgroundColor: WHITE,
  },
  padding_20: {
    padding: 20,
  },
  padding_25: {
    padding: 25,
  },
  margin_10: {
    margin: 10,
  },
  margin_20: {
    margin: 20,
  },
  marginRight_10: {
    marginRight: 10,
  },
  marginRight_20: {
    marginRight: 20,
  },
  marginRight_40: {
    marginRight: 40,
  },
  marginLeft_15: {
    marginLeft: 15,
  },
  marginLeft_20: {
    marginLeft: 20,
  },
  marginLeft_5: {
    marginLeft: 5,
  },
  marginLeft_10: {
    marginLeft: 10,
  },
  marginVertical_5: {
    marginVertical: 5,
  },
  marginVertical_10: {
    marginVertical: 10,
  },
  marginVertical_20: {
    marginVertical: 20,
  },
  marginHorizontal_10: {
    marginHorizontal: 10,
  },
  marginHorizontal_20: {
    marginHorizontal: 20,
  },
  marginHorizontal_25: {
    marginHorizontal: 25,
  },
  marginTop_10: {
    marginTop: 10,
  },
  marginTop_15: {
    marginTop: 15,
  },
  marginTop_16: {
    marginTop: 16,
  },
  marginTop_20: {
    marginTop: 20,
  },
  marginTop_24: {
    marginTop: 24,
  },
  marginTop_25: {
    marginTop: 25,
  },
  marginTop_30: {
    marginTop: 30,
  },
  marginTop_40: {
    marginTop: 40,
  },
  marginTop_60: {
    marginTop: 60,
  },
  marginBottom_20: {
    marginBottom: 20,
  },
  marginBottom_10: {
    marginBottom: 10,
  },
  marginBottom_5: {
    marginBottom: 5,
  },
  marginTop_5: {
    marginTop: 5,
  },
  marginTop_minus_40: {
    marginTop: -40,
  },
  marginTop_minus_100: {
    marginTop: -100,
  },
  marginBottom_16: {
    marginBottom: 16,
  },
  alignItemFlexStart: {
    alignItems: 'flex-start',
  },
  alignItemCenter: {
    alignItems: 'center',
  },
  alignItemFlexEnd: {
    alignItems: 'flex-end',
  },
  justifyContentSpaceAround: {
    justifyContent: 'space-around',
  },
  justifyContentSpaceBetween: {
    justifyContent: 'space-between',
  },
  justifyContentFlexStart: {
    justifyContent: 'flex-start',
  },
  justifyContentCenter: {
    justifyContent: 'center',
  },
  justifyContentFlexEnd: {
    justifyContent: 'flex-end',
  },
  textAlignCenter: {
    textAlign: 'center',
  },
  fontSize_20: {
    fontSize: RFValue(20),
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  flex_1: {
    flex: 1,
  },
  bodyContainer: {
    position: 'relative',
    flexDirection: 'column',
    overflow: 'visible',
  },
  extraBody: {
    height: 2000,
  },
  backgroundColorTransparent: {
    backgroundColor: 'transparent',
  },
  headerTitle: {
    fontSize: RFValue(20),
    color: WHITE,
    fontWeight: isIOS ? '600' : 'bold',
  },
  androidCardContainerPosition: {
    top: -45,
  },
  iosBody: {
    zIndex: 97,
  },
  androidBody: {
    elevation: 97,
  },
  iosBodyContent: {},
  androidBodyContent: {
    elevation: 999,
  },
  generalCardFilterGroup: {
    marginTop: HEADER_MAX_HEIGHT,
    position: 'relative',
    elevation: 100,
  },
  mainCard: {
    backgroundColor: WHITE,
    borderRadius: 10,
    shadowColor: COLOR_AIA_DARK_GREY,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 10,
  },
  headerCardContainer: {
    marginHorizontal: 20,
    zIndex: 100,
    elevation: 100,
  },
  mainFilterContainer: {
    zIndex: 97,
  },
  generalHeaderContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  buttonGroupPrimary: {
    width: '78%',
  },
  smallIconContainer: {
    width: '15%',
  },
  fullWidthButton: {
    width: '100%',
  },
  fullWidthInput: {
    width: '100%',
  },
  lazyLoadItems: {
    height: 30,
    borderRadius: 50,
  },
  firstTopLayer: {
    elevation: 99,
    zIndex: 99,
  },
  // Progress Bar
  minPts: {
    color: COLOR_AIA_GREEN,
    fontSize: RFValue(12),
  },
  maxPts: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(12),
  },
  snackbarTitle: {
    color: WHITE,
    fontSize: RFValue(14),
    fontWeight: isIOS ? '600' : 'bold',
  },
});

export default AppStyles;
