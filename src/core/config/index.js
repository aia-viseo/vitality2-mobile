/**
 *
 * @format
 *
 */

// @flow

import {
  COLOR_AIA_RED,
  COLOR_AIA_GREEN,
  COLOR_AIA_GD_ACTIVE_1,
  COLOR_AIA_GD_ACTIVE_2,
  COLOR_AIA_GD_RED_2,
  COLOR_AIA_GD_RED_1,
} from '@colors';

export const LinearLocations: number[] = [0, 0.62, 1];
export {LinearLocations as default};

export const projectFontFamily = 'NotoSans-Regular';
export const generalPadding = 20;
export const generalHorizontalPadding = 20;
export const generalVerticalPadding = 20;

export const greenLinearLocations = [0, 0.62, 1];
export const redLinearLocations = [0, 0.72, 1];
export const redLinearColors = [COLOR_AIA_GD_RED_1, COLOR_AIA_RED, COLOR_AIA_GD_RED_2];
export const greenLinearColors = [COLOR_AIA_GREEN, COLOR_AIA_GD_ACTIVE_1, COLOR_AIA_GD_ACTIVE_2];
