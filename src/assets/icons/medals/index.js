/**
 * Medal Icons
 * @format
 *
 */

import React from 'react';

import Bronze from './bronze.svg';
import Gold from './gold.svg';
import Silver from './silver.svg';
import Platinum from './platinum.svg';

const MedalIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    bronze: <Bronze style={extraStyle} {...widthProps} {...heightProps} />,
    gold: <Gold style={extraStyle} {...widthProps} {...heightProps} />,
    silver: <Silver style={extraStyle} {...widthProps} {...heightProps} />,
    platinum: <Platinum style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default MedalIcons;
