/**
 *
 * Test LinkSection Icon
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import LinkSection from '../index';

test('Test LinkSection Icon', () => {
  const icon = LinkSection('e_card');
  const tree = renderer.create(<View>{icon}</View>).toJSON();
  expect(tree).toMatchSnapshot();
});
