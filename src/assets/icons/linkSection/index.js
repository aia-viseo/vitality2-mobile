/**
 *
 * @format
 *
 */

import React from 'react';

import LinksArrow from './links_section_arrow.svg';
import ECard from './e_card.svg';
import EarnPoints from './earn_points.svg';
import HealthReport from './health_report.svg';
import Help from './help.svg';
import Notification from './notification.svg';
import UseCoupon from './use_coupons.svg';
import WhatsOn from './whats_on.svg';
import WorkoutTracker from './workout_tracker.svg';

const LinkSectionIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    e_card: <ECard style={extraStyle} {...widthProps} {...heightProps} />,
    earn_points: <EarnPoints style={extraStyle} {...widthProps} {...heightProps} />,
    health_report: <HealthReport style={extraStyle} {...widthProps} {...heightProps} />,
    help: <Help style={extraStyle} {...widthProps} {...heightProps} />,
    links_section_arrow: <LinksArrow style={extraStyle} {...widthProps} {...heightProps} />,
    notification: <Notification style={extraStyle} {...widthProps} {...heightProps} />,
    use_coupons: <UseCoupon style={extraStyle} {...widthProps} {...heightProps} />,
    whats_on: <WhatsOn style={extraStyle} {...widthProps} {...heightProps} />,
    workout_tracker: <WorkoutTracker style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default LinkSectionIcons;
