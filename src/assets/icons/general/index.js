/**
 * General Icons
 * @format
 *
 */

import React from 'react';

import ArrowRight from './arrow_right.svg';
import AssessmentThumbnail from './assessment_thumbnail.svg';
import BackArrow from './back_arrow.svg';
import Dismiss from './dismiss.svg';
import Increase from './increase.svg';
import Decrease from './decrease.svg';
import Information from './information.svg';
import ManRunning from './man_running.svg';
import ThumbnailOne from './sample_1a.svg';
import ThumbnailTwo from './sample_1a2.svg';
import Language from './language.svg';
import FaceRecognition from './face_recognition.svg';
import Cross from './cross.svg';
import ArrowLeftNoTail from './arrow_left_no_tail.svg';
import HeartRate from './heart_rate.svg';
import Steps from './steps.svg';
import EarnPoint from './earn_points.svg';
import Hourglass from './hourglass.svg';
import EatApple from './eat-apple.svg';
import DrinkAlcohol from './drink-alcohol.svg';
import MenuVertical from './menu_vertical.svg';
import Refresh from './refresh.svg';
import Sleep from './sleep.svg';
import HeartRateSmall from './heart_rate_small.svg';
import StepsSmall from './steps_small.svg';
import VitalityLogo from './aia_vitality_white.svg';

const GeneralIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    arrow_right: <ArrowRight style={extraStyle} {...widthProps} {...heightProps} />,
    assessment_thumbnail: (
      <AssessmentThumbnail style={extraStyle} {...widthProps} {...heightProps} />
    ),
    back: <BackArrow style={extraStyle} {...widthProps} {...heightProps} />,
    dismiss: <Dismiss style={extraStyle} {...widthProps} {...heightProps} />,
    increase: <Increase style={extraStyle} {...widthProps} {...heightProps} />,
    decrease: <Decrease style={extraStyle} {...widthProps} {...heightProps} />,
    information: <Information style={extraStyle} {...widthProps} {...heightProps} />,
    manRunning: <ManRunning style={extraStyle} {...widthProps} {...heightProps} />,
    thumbnailOne: <ThumbnailOne style={extraStyle} {...widthProps} {...heightProps} />,
    thumbnailTwo: <ThumbnailTwo style={extraStyle} {...widthProps} {...heightProps} />,
    language: <Language style={extraStyle} {...widthProps} {...heightProps} />,
    face_recognition: <FaceRecognition style={extraStyle} {...widthProps} {...heightProps} />,
    cross: <Cross style={extraStyle} {...widthProps} {...heightProps} />,
    arrow_left_no_tail: <ArrowLeftNoTail style={extraStyle} {...widthProps} {...heightProps} />,
    heart_rate: <HeartRate style={extraStyle} {...widthProps} {...heightProps} />,
    steps: <Steps style={extraStyle} {...widthProps} {...heightProps} />,
    earn_points: <EarnPoint style={extraStyle} {...widthProps} {...heightProps} />,
    hourglass: <Hourglass style={extraStyle} {...widthProps} {...heightProps} />,
    apple_health: <HeartRate style={extraStyle} {...widthProps} {...heightProps} />,
    eat_apple: <EatApple style={extraStyle} {...widthProps} {...heightProps} />,
    drink_alcohol: <DrinkAlcohol style={extraStyle} {...widthProps} {...heightProps} />,
    menu_vertical: <MenuVertical style={extraStyle} {...widthProps} {...heightProps} />,
    refresh: <Refresh style={extraStyle} {...widthProps} {...heightProps} />,
    sleep: <Sleep style={extraStyle} {...widthProps} {...heightProps} />,
    heart_rate_small: <HeartRateSmall style={extraStyle} {...widthProps} {...heightProps} />,
    steps_small: <StepsSmall style={extraStyle} {...widthProps} {...heightProps} />,
    aia_vitality_white: <VitalityLogo style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default GeneralIcons;
