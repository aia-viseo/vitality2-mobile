/**
 *
 *  Test General Icon
 *  @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import General from '../index';

test('Test General Icon', () => {
  const icon = General('manRunning');
  const tree = renderer.create(<View>{icon}</View>).toJSON();
  expect(tree).toMatchSnapshot();
});
