/**
 * BottomNavigationIcons
 * @format
 *
 */

import React from 'react';

import DashboardIconActive from './dashboard_active.svg';
import DashboardIcon from './dashboard.svg';
import AssessmentIconActive from './assessments_active.svg';
import AssessmentIcon from './assessments.svg';
import ChallengeIconActive from './challenges_active.svg';
import ChallengeIcon from './challenges.svg';
import RewardIconActive from './rewards_active.svg';
import RewardIcon from './rewards.svg';
import ProfileIconActive from './profile_active.svg';
import ProfileIcon from './profile.svg';

const BottomNavigationIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    dashboardActive: <DashboardIconActive style={extraStyle} {...widthProps} {...heightProps} />,
    dashboard: <DashboardIcon style={extraStyle} {...widthProps} {...heightProps} />,
    assessmentActive: <AssessmentIconActive style={extraStyle} {...widthProps} {...heightProps} />,
    assessment: <AssessmentIcon style={extraStyle} {...widthProps} {...heightProps} />,
    challengeActive: <ChallengeIconActive style={extraStyle} {...widthProps} {...heightProps} />,
    challenge: <ChallengeIcon style={extraStyle} {...widthProps} {...heightProps} />,
    rewardActive: <RewardIconActive style={extraStyle} {...widthProps} {...heightProps} />,
    reward: <RewardIcon style={extraStyle} {...widthProps} {...heightProps} />,
    profileActive: <ProfileIconActive style={extraStyle} {...widthProps} {...heightProps} />,
    profile: <ProfileIcon style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default BottomNavigationIcons;
