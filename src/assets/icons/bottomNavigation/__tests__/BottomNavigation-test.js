/**
 *
 *  Test BottomNavigation Icon
 *  @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import BottomNavigation from '../index';

test('Test BottomNavigation Icon', () => {
  const icon = BottomNavigation('dashboardActive');
  const tree = renderer.create(<View>{icon}</View>).toJSON();
  expect(tree).toMatchSnapshot();
});
