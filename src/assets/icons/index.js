/**
 *
 * @format
 *
 */

import BottomNavigationIcons from './bottomNavigation';
import AssessmentImages from './assessments';
import AssessmentDetailIcons from './assessmentDetails';
import GeneralIcons from './general';
import LinkSectionIcons from './linkSection';
import MedalIcons from './medals';
import PartnersIcons from './partners';
import WorkoutTrackers from './workTrackers';

const Icons = {
  general: GeneralIcons,
  assessments: AssessmentImages,
  assessmentDetail: AssessmentDetailIcons,
  bottomNavigation: BottomNavigationIcons,
  linkSection: LinkSectionIcons,
  medals: MedalIcons,
  partners: PartnersIcons,
  workTrackers: WorkoutTrackers,
};

export default Icons;
