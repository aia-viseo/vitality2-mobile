/**
 * Assessment Detail Icons
 * @format
 *
 */

import React from 'react';

import AssessmentInput from './assessment-inputs.svg';
import EarnPoints from './earn-points.svg';
import Refresh from './refresh.svg';

const AssessmentDetailIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    assessment_input: <AssessmentInput style={extraStyle} {...widthProps} {...heightProps} />,
    earn_points: <EarnPoints style={extraStyle} {...widthProps} {...heightProps} />,
    refresh: <Refresh style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default AssessmentDetailIcons;
