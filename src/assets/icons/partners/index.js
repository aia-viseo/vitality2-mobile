/**
 * Partner Icons
 * @format
 *
 */

import React from 'react';

import GoCar from './go_car.svg';

const PartnersIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    go_car: <GoCar style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default PartnersIcons;
