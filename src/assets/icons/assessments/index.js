/**
 * Assessment Images
 * @format
 *
 */

import React from 'react';

import AverageSleep from './average-sleep.svg';
import BloodPressure from './blood-pressure.svg';
import Clock from './clock.svg';
import DrinkAlcohol from './drink-alcohol.svg';
import EatApple from './eat-apple.svg';
import HulaHoop from './hula-hoop.svg';
import KettleBell from './kettlebell.svg';
import Question from './question.svg';
import SmartWatch from './smart-watch.svg';
import Sofa from './sofa.svg';
import Strength from './strength.svg';
import Treadmill from './treadmill.svg';
import WeightLifting from './weight-lifting.svg';
import YogaBall from './yoga-ball.svg';
import DifficultySleeping from './difficulty-sleeping.svg';
import Doctor from './doctor.svg';
import DoctorDiagnose from './doctor-diagnose.svg';
import Exercise from './exercise.svg';
import FeelAboutDiet from './feel-about-diet.svg';
import HappierLife from './happier-life.svg';
import Height from './height.svg';
import HowMuchSalt from './how-much-salt.svg';
import HowStressed from './how-stressed.svg';
import OftenMilk from './often-milk.svg';
import SaltyFood from './salty-food.svg';
import SugarDrink from './sugar-drink.svg';

const AssessmentImages = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    blood_pressure: <BloodPressure style={extraStyle} {...widthProps} {...heightProps} />,
    clock: <Clock style={extraStyle} {...widthProps} {...heightProps} />,
    drink_alcohol: <DrinkAlcohol style={extraStyle} {...widthProps} {...heightProps} />,
    eat_apple: <EatApple style={extraStyle} {...widthProps} {...heightProps} />,
    hula_hoop: <HulaHoop style={extraStyle} {...widthProps} {...heightProps} />,
    kettlebell: <KettleBell style={extraStyle} {...widthProps} {...heightProps} />,
    question: <Question style={extraStyle} {...widthProps} {...heightProps} />,
    smart_watch: <SmartWatch style={extraStyle} {...widthProps} {...heightProps} />,
    sofa: <Sofa style={extraStyle} {...widthProps} {...heightProps} />,
    strength: <Strength style={extraStyle} {...widthProps} {...heightProps} />,
    treadmill: <Treadmill style={extraStyle} {...widthProps} {...heightProps} />,
    weight_lifting: <WeightLifting style={extraStyle} {...widthProps} {...heightProps} />,
    yoga_ball: <YogaBall style={extraStyle} {...widthProps} {...heightProps} />,
    average_sleep: <AverageSleep style={extraStyle} {...widthProps} {...heightProps} />,
    difficulty_sleeping: <DifficultySleeping style={extraStyle} {...widthProps} {...heightProps} />,
    doctor: <Doctor style={extraStyle} {...widthProps} {...heightProps} />,
    doctor_diagnose: <DoctorDiagnose style={extraStyle} {...widthProps} {...heightProps} />,
    exercise: <Exercise style={extraStyle} {...widthProps} {...heightProps} />,
    feel_about_diet: <FeelAboutDiet style={extraStyle} {...widthProps} {...heightProps} />,
    happier_life: <HappierLife style={extraStyle} {...widthProps} {...heightProps} />,
    height: <Height style={extraStyle} {...widthProps} {...heightProps} />,
    how_much_salt: <HowMuchSalt style={extraStyle} {...widthProps} {...heightProps} />,
    how_stressed: <HowStressed style={extraStyle} {...widthProps} {...heightProps} />,
    often_milk: <OftenMilk style={extraStyle} {...widthProps} {...heightProps} />,
    salty_food: <SaltyFood style={extraStyle} {...widthProps} {...heightProps} />,
    sugar_drink: <SugarDrink style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default AssessmentImages;
