/**
 *
 *  Test Icons
 *  @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import Icons from '../index';

test('Test Icons', () => {
  const icon = Icons.general('manRunning');
  const tree = renderer.create(<View>{icon}</View>).toJSON();
  expect(tree).toMatchSnapshot();
});
