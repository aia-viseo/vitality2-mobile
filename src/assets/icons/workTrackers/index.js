/**
 * WorkoutTrackerIcons
 * @format
 *
 */

import React from 'react';

import Fitbit from './fitbit.svg';
import HeartRate from './heart_rate.svg';
import WorkoutTracker from './workout_tracker.svg';

const WorkoutTrackerIcons = (name, extraStyle, width, height) => {
  let widthProps = {};
  let heightProps = {};
  if (width) {
    widthProps = {width};
  }
  if (height) {
    heightProps = {heightProps};
  }

  const icons = {
    fitbit: <Fitbit style={extraStyle} {...widthProps} {...heightProps} />,
    'apple health': <HeartRate style={extraStyle} {...widthProps} {...heightProps} />,
    'samsung health': <WorkoutTracker style={extraStyle} {...widthProps} {...heightProps} />,
  };
  return icons[name];
};

export default WorkoutTrackerIcons;
