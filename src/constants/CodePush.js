/* istanbul ignore file */

/**
 *
 * @format
 *
 */

// @flow
import CodePush from 'react-native-code-push';

const deploymentKey = process.env.APPCENTER_DEPLOYMENT_KEY || 'NONE';

export default {
  deploymentKey,
  checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
  installMode: CodePush.InstallMode.IMMEDIATE
};
