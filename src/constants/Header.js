/**
 *
 * @format
 *
 */

// @flow

import env from 'react-native-config';
import {isIphoneX} from 'react-native-iphone-x-helper';
import GetOS from '../core/Singleton/Platform';

const isIOS = GetOS.getInstance() === 'ios';

// Header height
export const HEADER_MAX_HEIGHT = parseInt(env.HEADER_MAX_HEIGHT, 10);

let height = parseInt(env.HEADER_ANDROID_MIN_HEIGHT, 10);
if (isIOS) {
  height = isIphoneX()
    ? parseInt(env.HEADER_IPHONE_X_MIN_HEIGHT, 10)
    : parseInt(env.HEADER_MIN_HEIGHT, 10);
}
export const HEADER_MIN_HEIGHT = height;

export const AGE_CARD_HEIGHT = 90;

export const HEADER_CARD_HEIGHT = 90;
