/**
 *
 * @format
 *
 */

// @flow

import env from 'react-native-config';

// Axios Authorization Token
export const {AXIOS_AUTH_TOKEN} = env;

// Axios Base URL
export const {AXIOS_BASE_URL} = env;

// Axios Content Type
export const {AXIOS_CONTENT_TYPE} = env;

// Cache Expire Time
export const {CACHE_EXPIRE_IN_MINS} = env;

// AEM Authentication Url
export const {AEM_AUTH_URL} = env;

// AEM Authentication Username
export const {AEM_AUTH_USERNAME} = env;

// AEM Authentication Password
export const {AEM_AUTH_PASSWORD} = env;

// AEM Base Url
export const {AEM_BASE_URL} = env;

// AEM Top Menu Model Url
export const {AEM_TOP_MENU_MODEL_URL} = env;

// AEM Assessment Card Model Url
export const {AEM_ASSESSMENT_CARD_MODEL_URL} = env;

// AEM Challenge Card Model Url
export const {AEM_CHALLENGE_CARD_MODEL_URL} = env;

// AEM Navigation Model Url
export const {AEM_NAVIGATION_MODEL_URL} = env;

// Membership number
export const {MEMBERSHIP_NO} = env;

// X-Vitality-Legal-Entity-id
export const {X_VITALITY_LEGAL_ENTITY_ID} = env;

// X-AIA-Request-id
export const {X_AIA_REQUEST_ID} = env;

// X-Forward-API
export const {X_FORWARD_API} = env;

// AEM Sit Token Host Name
export const {AEM_SIT_TOKEN_HOST_NAME} = env;

// AEM Sit Token Path
export const {AEM_SIT_TOKEN_PATH} = env;

// AEM Sit Host Name
export const {AEM_SIT_HOST_NAME} = env;

// AEM Sit Health Data Path
export const {AEM_SIT_HEALTH_DATA_PATH} = env;

// AEM Sit Origin
export const {AEM_ORIGIN} = env;

// AEM Tenant Id
export const {TENANT_ID} = env;

// AEM SIT App Id
export const {SIT_APP_ID} = env;

// AEM SIT Token Username
export const {SIT_TOKEN_USERNAME} = env;

// AEM SIT Token Password
export const {SIT_TOKEN_PASSWORD} = env;

// SAP UAT host
export const {SAP_MPV_UAT_HOST} = env;

// Membership number 2
export const {MEMBERSHIP_NO_TWO} = env;

// X-Vitality-Legal-Entity-id number 2
export const {X_VITALITY_LEGAL_ENTITY_ID_TWO} = env;

// STRING BY APPENDING STRING
export const {STRING_BY_APPENDING_STRING} = env;

// Assessment List Path
export const {ASSESSMENT_LIST_PATH} = env;

// Challenges List Path
export const {CHALLENGE_LIST_PATH} = env;

// AEM New Base URL
export const {AEM_NEW_BASE_URL} = env;

// AEM New Common Path
export const {AEM_NEW_PATH} = env;

// AEM New Assessment List Model
export const {AEM_NEW_ASSESSMENT_LIST_MODEL_URL} = env;

// AEM New Challenge List Model
export const {AEM_NEW_CHALLENGE_LIST_MODEL_URL} = env;

// AEM Health Assessment Model
export const {AEM_HEALTH_CHECK_ASSESSMENT_MODEL_URL} = env;

// AEM How Well Are You Eating Assessment Model
export const {AEM_HOW_WELL_ARE_YOU_EATING_ASSESSMENT_MODEL_URL} = env;

// AEM How Healthy Are You Assessment Model
export const {AEM_HOW_HEALTHY_ARE_YOU_ASSESSMENT_MODEL_URL} = env;

// AEM How Stressed Are You Assessment Model
export const {AEM_HOW_STRESSED_ARE_YOU_ASSESSMENT_MODEL_URL} = env;

// AEM How Active Are You Assessment Model
export const {AEM_HOW_ACTIVE_ARE_YOU_ASSESSMENT_MODEL_URL} = env;

// AEM How Well do you sleep Assessment Model
export const {AEM_HOW_WELL_DO_YOU_SLEEP_ASSESSMENT_MODEL_URL} = env;

// AEM Non Smoker Decleration Assessment Model
export const {AEM_NON_SMOKER_DECLARATION_ASSESSMENT_MODEL_URL} = env;

// AEM Basic Health Screening Assessment Model
export const {AEM_BASIC_HEALTH_SCREENING_ASSESSMENT_MODEL_URL} = env;

// AEM New Bottom Navigation Model
export const {AEM_NEW_BOTTOM_NAV_MODEL_URL} = env;

// AEM New Quick Links Model
export const {AEM_NEW_QUICK_LINKS_MODEL_URL} = env;

// AEM New General Model
export const {AEM_NEW_GENERAL_MODEL_URL} = env;

// Weekly Challenges Path
export const {WEEKLY_CHALLENGE_PATH} = env;

// Get Earned Point
export const {EARNED_POINT} = env;

// AEM Earn Point Steps
export const {AEM_EARN_POINT_STEPS_URL} = env;

// AEM Earn Point Heart Rate
export const {AEM_EARN_POINT_HEART_RATE_URL} = env;

// Points History
export const {POINTS_RANGEBARS_PATH} = env;
export const {POINTS_HISTORY_PATH} = env;

// Workout Tracker Devices Path
export const {WORKOUT_TRACKER_DEVICES_PATH} = env;

// Workout Tracker Add Device Path
export const {WORKOUT_TRACKER_ADD_DEVICE_PATH} = env;

// Workout Tracker Workout Data Path
export const {WORKOUT_TRACKER_WORKOUTS_PATH} = env;

// Workout Tracker Workout fitness Point
export const {WORKOUT_TRACKER_FITNESS_POINTS_PATH} = env;

// AEM Points Model Url
export const {AEM_POINTS_MODEL_URL} = env;
