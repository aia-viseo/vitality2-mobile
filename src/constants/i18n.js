/**
 *
 * @format
 *
 */

// @flow

import env from 'react-native-config';

// Default Language
export const {DEFAULT_LANGUAGE} = env;

// AsyncStorage User Language Key
export const {USER_LANGUAGE_STORAGE_KEY} = env;

// Localization en key
export const {EN} = env;

// Localization id key
export const {ID} = env;
