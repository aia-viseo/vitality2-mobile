/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  COLOR_AIA_GREEN,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREY_500,
  COLOR_AIA_ICON_GRAY,
  WHITE,
  COLOR_AIA_RED,
  BLACK,
} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';
const styles = StyleSheet.create({
  title: {
    color: COLOR_AIA_GREEN,
    fontWeight: isIOS ? '600' : 'bold',
  },
  subTitle: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(22),
    fontWeight: isIOS ? '600' : 'bold',
    flexWrap: 'wrap',
  },
  itemDate: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
  },
  itemTitle: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(14),
    fontWeight: isIOS ? '600' : 'bold',
    flexWrap: 'wrap',
  },
  separator: {
    backgroundColor: COLOR_AIA_ICON_GRAY,
    flex: 1,
    height: 1,
  },
  weeklyProgressContainer: {
    backgroundColor: WHITE,
    shadowColor: BLACK,
    shadowOpacity: 0.15,
    shadowRadius: 15,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
  red: {
    fontSize: RFValue(14),
    color: COLOR_AIA_RED,
  },
  green: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREEN,
  },
  bold: {
    fontWeight: isIOS ? '600' : 'bold',
  },
  rightLabel: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(14),
  },
});

export default styles;
