/**
 *
 * Challenge Weekly Progress
 * @format
 *
 */

// @flow

import React, {useContext, useState, useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  TouchableWithoutFeedback,
  FlatList,
  Text as RnText,
} from 'react-native';
import lodash from 'lodash';
import {useTranslation, Trans} from 'react-i18next';

import AppStyles from '@styles';
import Text from '@atoms/Text';
import Icon from '@atoms/Icon';
import ProgressBar from '@molecules/ProgressBar';
import ArrowControl from '@molecules/ArrowControl';
import DateTime from '@core/handler/DateTime';
import {ChallengesContext} from '@contexts/Challenges';

import {
  greenLinearLocations,
  redLinearLocations,
  redLinearColors,
  greenLinearColors,
} from '@core/config';

import HeaderFlowStyles from '@organisms/HeaderFlow/styles';
import {NO_ACHIEVED, STATUS} from './config';

import type {PropsType, PeriodEventType} from './types';
import Styles from './styles';

const ChallengeWeeklyProgress = (props: PropsType) => {
  const {navigation} = props;
  const [selectedWeeklyIndex, setSelectedWeeklyIndex] = useState(0);
  const {t} = useTranslation();
  const {challengePeriodList} = useContext(ChallengesContext);

  const setNewWeek = (isNextWeek: boolean) => {
    if (isNextWeek) {
      if (selectedWeeklyIndex < challengePeriodList.length - 1) {
        setSelectedWeeklyIndex(selectedWeeklyIndex + 1);
      }
    } else if (selectedWeeklyIndex > 0) {
      setSelectedWeeklyIndex(selectedWeeklyIndex - 1);
    }
  };

  const onPressBack = () => {
    navigation.goBack();
  };

  useEffect(() => {
    setSelectedWeeklyIndex(challengePeriodList.length - 1);
  }, [challengePeriodList]);

  const renderBackButton = () => {
    return (
      <View
        style={[
          AppStyles.row,
          AppStyles.marginHorizontal_10,
          AppStyles.marginTop_15,
          AppStyles.firstTopLayer,
        ]}>
        <TouchableWithoutFeedback onPress={onPressBack}>
          <View style={HeaderFlowStyles.closeButton}>
            <Icon group="general" name="cross" width={24} height={24} />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <View
        style={[
          AppStyles.row,
          AppStyles.justifyContentSpaceBetween,
          AppStyles.marginHorizontal_20,
        ]}>
        <View style={[AppStyles.marginTop_60, AppStyles.flex_1, AppStyles.paddingRight_40]}>
          <Text customStyle={[Styles.title]}>{t('challenge.Your daily stats')}</Text>
          <Text customStyle={[Styles.subTitle]}>{t(`challenge.See how you've done`)}</Text>
        </View>
        <View style={[AppStyles.flex_1]}>
          <Icon group="general" name="manRunning" height={145} />
        </View>
      </View>
    );
  };

  const renderProgressBar = () => {
    const {periodActualValue, periodTargetValue, periodStatus} = challengePeriodList[
      selectedWeeklyIndex
    ];

    const isMissed = periodStatus === NO_ACHIEVED;
    const customLinearLocations = isMissed ? redLinearLocations : greenLinearLocations;
    const customLinearColors = isMissed ? redLinearColors : greenLinearColors;
    const leftLabelStyle = isMissed ? Styles.red : Styles.green;

    const actualValue = parseInt(periodActualValue, 10);
    const targetValue = parseInt(periodTargetValue, 10);
    const progress = (actualValue / targetValue) * 100;
    return (
      <View>
        <ProgressBar
          leftLabel={
            <Trans
              defaults={t('challenge.Earned Point', {points: actualValue})}
              parent={RnText}
              components={[<RnText style={Styles.bold} />]}
            />
          }
          rightLabel={t('challenge.Goal Point', {points: targetValue})}
          progress={progress}
          customLinearLocations={customLinearLocations}
          customLinearColors={customLinearColors}
          barHeight={15}
          leftLabelStyle={[leftLabelStyle]}
          rightLabelStyle={[Styles.rightLabel]}
        />
      </View>
    );
  };

  const renderWeeklyProgress = () => {
    const {periodEffectiveFrom, periodEffectiveTo, periodStatus} = challengePeriodList[
      selectedWeeklyIndex
    ];

    const startDate = periodEffectiveFrom ? new Date(periodEffectiveFrom) : new Date();
    const endDate = periodEffectiveTo ? new Date(periodEffectiveTo) : new Date();

    const sameMonth = DateTime.format(startDate, 'MMM') === DateTime.format(endDate, 'MMM');

    const periodStr = `${DateTime.format(startDate, 'MMM d')} - ${DateTime.format(
      endDate,
      sameMonth ? 'd' : 'MMM d'
    )}`;

    return (
      <View style={[AppStyles.column, AppStyles.padding_20, Styles.weeklyProgressContainer]}>
        <View style={[AppStyles.alignItemCenter, AppStyles.marginBottom_20]}>
          <ArrowControl
            onPress={setNewWeek}
            disableLeft={selectedWeeklyIndex === 0}
            disableRight={selectedWeeklyIndex === challengePeriodList.length - 1}>
            <View
              style={[AppStyles.column, AppStyles.alignItemCenter, AppStyles.marginHorizontal_20]}>
              <Text customStyle={[Styles.itemTitle]}>{periodStr}</Text>
              <Text customStyle={[Styles.itemDate]}>{t(`challenge.${STATUS[periodStatus]}`)}</Text>
            </View>
          </ArrowControl>
        </View>
        {renderProgressBar()}
      </View>
    );
  };

  const renderItem = ({item}: {item: PeriodEventType}) => {
    const {eventDate, eventDesc, eventValue} = item;
    return (
      <View
        style={[
          AppStyles.row,
          AppStyles.alignItemCenter,
          AppStyles.justifyContentSpaceBetween,
          AppStyles.padding_20,
        ]}>
        <View>
          <Text customStyle={[Styles.itemDate]}>
            {DateTime.format(new Date(eventDate), 'MMM d')}
          </Text>
          <Text customStyle={[Styles.itemTitle]}>{eventDesc}</Text>
        </View>
        <View>
          <Text customStyle={[Styles.title]}>{`${eventValue} pts`}</Text>
        </View>
      </View>
    );
  };

  const renderList = () => {
    const {periodEvents} = challengePeriodList[selectedWeeklyIndex];

    const data = lodash.sortBy(periodEvents, (periodEvent) => {
      return new Date(periodEvent.eventDate);
    });

    return (
      <FlatList
        style={[AppStyles.flex_1]}
        bounces={false}
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, index) => `${index}`}
        ItemSeparatorComponent={() => <View style={[Styles.separator]} />}
      />
    );
  };

  const renderBody = () => {
    return (
      <View style={[AppStyles.flex_1, AppStyles.marginTop_minus_40]}>
        {renderHeader()}
        {renderWeeklyProgress()}
        {renderList()}
      </View>
    );
  };

  return (
    <SafeAreaView style={[AppStyles.flex_1]}>
      <StatusBar barStyle="dark-content" />
      {renderBackButton()}
      {renderBody()}
    </SafeAreaView>
  );
};

export default ChallengeWeeklyProgress;
