/**
 *
 * @format
 *
 */

// @flow

type NavigationSubPath = {
  screen: string,
};

export type PropsType = {
  navigation: {
    navigate: (path: string, subPath?: NavigationSubPath) => void,
    goBack: (path?: string) => void,
  },
};

export type PeriodEventType = {
  captureDateTime: string,
  eventDate: string,
  eventDesc: string,
  eventId: string,
  eventStatus: string,
  eventType: string,
  eventValue: string,
  remarks: string,
};
