/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  COLOR_AIA_GREY,
  BLACK,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREEN,
  COLOR_AIA_GD_RED_1,
} from '@colors';

const ChallengeStyles = StyleSheet.create({
  summaryContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  statusContainer: {
    alignItems: 'center',
  },
  textValue: {
    fontSize: RFValue(18),
    fontWeight: '700',
    color: BLACK,
  },
  label: {
    marginLeft: 5,
    fontSize: RFValue(12),
    color: COLOR_AIA_DARK_GREY,
  },
  dot: {
    width: 7,
    height: 7,
    borderRadius: 5,
    marginTop: 4,
  },
  inProgressDot: {
    backgroundColor: COLOR_AIA_GREY,
  },
  completedDot: {
    backgroundColor: COLOR_AIA_GREEN,
  },
  targetNotMeetDot: {
    backgroundColor: COLOR_AIA_GD_RED_1,
  },
});

export default ChallengeStyles;
