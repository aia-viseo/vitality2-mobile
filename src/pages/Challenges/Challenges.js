/**
 *
 * Challenges Page
 * @format
 *
 */

// @flow

import React, {useContext, useState, useRef, useEffect} from 'react';
import {StatusBar, View, FlatList} from 'react-native';
import Animated from 'react-native-reanimated';
import {useTranslation} from 'react-i18next';

import {ApplicationContext} from '@contexts/Application';
import {ChallengesContext} from '@contexts/Challenges';

import Text from '@atoms/Text';
import Card from '@molecules/Card';
import ChipFilter from '@molecules/ChipFilter';
import Header from '@molecules/Header';
import GetOS from '@platform';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';
import AppStyles from '@styles';
import ChallengesStyles from './styles';

import type {PropsType} from './types';
import {FILTERS, HIDE_BACKGROUND_LIMIT, IN_PROGRESS, COMPLETED, TARGET_NOT_MET} from './config';

const Challenges = (props: PropsType) => {
  const {selectChallenge, challenges, isChallengesLoading, challengesSummary} = useContext(
    ApplicationContext
  );
  const {challengePeriodList} = useContext(ChallengesContext);
  const [scrollY] = useState(new Animated.Value(0));
  const [hideBackground, setHideBackground] = useState(true);
  const [selectedFilter, setSelectedFilter] = useState('recommended');
  const [selectedWeeklyIndex, setSelectedWeeklyIndex] = useState(0);
  const scrollViewRef = useRef();
  const {t} = useTranslation();
  const isIOS = GetOS.getInstance() === 'ios';
  const {navigation} = props;

  const headerHeightDiff = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

  useEffect(() => {
    setSelectedWeeklyIndex(challengePeriodList.length - 1);
  }, [challengePeriodList]);

  const renderHeaderItem = (label: string, value: number) => {
    const dotStyles = {
      inProgress: ChallengesStyles.inProgressDot,
      completed: ChallengesStyles.completedDot,
      targetNotMet: ChallengesStyles.targetNotMeetDot,
    };
    return (
      <View style={[AppStyles.column, ChallengesStyles.statusContainer]}>
        <Text customStyle={[ChallengesStyles.textValue]}>{value}</Text>
        <View style={[AppStyles.row]}>
          <View style={[ChallengesStyles.dot, dotStyles[label]]} />
          <Text customStyle={[ChallengesStyles.label]}>{t(`general.${label}`)}</Text>
        </View>
      </View>
    );
  };

  const renderHeaderCard = () => {
    const {inProgress, completed, targetNotMet} = challengesSummary;
    return (
      <View style={[AppStyles.flex_1, AppStyles.padding_10]}>
        <View style={[AppStyles.row, ChallengesStyles.summaryContainer]}>
          {renderHeaderItem(IN_PROGRESS, inProgress)}
          {renderHeaderItem(COMPLETED, completed)}
          {renderHeaderItem(TARGET_NOT_MET, targetNotMet)}
        </View>
      </View>
    );
  };

  const navigateChallenge = (challenge: Object) => {
    selectChallenge(challenge);
    navigation.navigate('ChallengesTab', {screen: 'ChallengeDetail', initial: false});
  };

  const renderWeeklyChallengeList = () => {
    return (
      <View style={[isIOS ? AppStyles.iosBodyContent : AppStyles.androidBodyContent]}>
        <FlatList
          data={challenges}
          renderItem={({item}) => {
            const {title, iconName, variationNumber, imageSrc, endDate, voucher} = item;

            const challengePeriod = challengePeriodList
              ? challengePeriodList[selectedWeeklyIndex]
              : null;
            const {periodActualValue, periodTargetValue} = challengePeriod || {};
            const actualValue = parseInt(periodActualValue, 10);
            const targetValue = parseInt(periodTargetValue, 10);

            return (
              <View style={[AppStyles.marginLeft_20]}>
                <Card
                  variationNumber={variationNumber}
                  title={title}
                  iconName={iconName}
                  points={actualValue || 0}
                  onPressItem={() => navigateChallenge(item)}
                  imageSrc={imageSrc}
                  endDate={endDate}
                  minPoints={0}
                  maxPoints={targetValue || 0}
                  voucher={voucher}
                />
              </View>
            );
          }}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  };

  const renderChipFilter = () => {
    return (
      <ChipFilter
        data={FILTERS}
        selectedName={selectedFilter}
        onPressChip={(item) => {
          const {name} = item;
          setSelectedFilter(name);
        }}
        hideBackground={hideBackground}
        scrollY={scrollY}
        headerHeightDiff={headerHeightDiff}
        withContainer
      />
    );
  };

  const renderHeader = () => {
    return (
      <Header
        setTop
        scrollY={scrollY}
        scaleHeight={Animated.interpolate(scrollY, {
          inputRange: [0, headerHeightDiff * 2 - 1, headerHeightDiff * 2],
          outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
        })}
        cardContent={renderHeaderCard}
        title={t('general.challengesTitle')}
      />
    );
  };

  const renderStatusBar = () => {
    return <StatusBar barStyle="light-content" />;
  };

  const renderAnimatedCode = () => {
    return (
      <Animated.Code>
        {() =>
          Animated.call([scrollY], ([val]: number[]) => {
            if (hideBackground) {
              if (val >= HIDE_BACKGROUND_LIMIT) {
                setHideBackground(false);
              }
            } else if (val <= HIDE_BACKGROUND_LIMIT) {
              setHideBackground(true);
            }
          })
        }
      </Animated.Code>
    );
  };

  const renderBody = () => {
    return (
      <Animated.ScrollView
        ref={scrollViewRef}
        bounces={false}
        style={[AppStyles.marginTop_minus_100]}
        scrollEventThrottle={16}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {y: scrollY}}}], {
          useNativeDriver: true,
        })}>
        {!isChallengesLoading && renderWeeklyChallengeList()}
        {renderAnimatedCode()}
      </Animated.ScrollView>
    );
  };

  return (
    <View style={AppStyles.flex_1}>
      {renderStatusBar()}
      {renderHeader()}
      {renderChipFilter()}
      {renderBody()}
    </View>
  );
};

export default Challenges;
