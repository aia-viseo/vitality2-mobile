/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

import GetOS from '@platform';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';

const isIOS = GetOS.getInstance() === 'ios';

export const FILTERS = [
  {id: 1, key: 'challenge.Recommended', name: 'recommended'},
  {id: 2, key: 'challenge.In Progress', name: 'inProgress'},
  {id: 3, key: 'challenge.Completed', name: 'completed'},
  {id: 4, key: 'challenge.Fitness', name: 'fitness'},
  {id: 5, key: 'challenge.Personalized', name: 'personalized'},
  {id: 6, key: 'challenge.Community', name: 'community'},
];

const ANDROID_FILTER_RANGE = [-20, (HEADER_MIN_HEIGHT - HEADER_MAX_HEIGHT) * 2 - 5];
const IPHONE_FILTER_RANGE = [25, (HEADER_MIN_HEIGHT - HEADER_MAX_HEIGHT) * 2 + 40];
const IPHONE_X_FILTER_RANGE = [25, (HEADER_MIN_HEIGHT - HEADER_MAX_HEIGHT) * 2 + 25];
let filterRanges = ANDROID_FILTER_RANGE;

if (isIOS) {
  filterRanges = isIphoneX() ? IPHONE_X_FILTER_RANGE : IPHONE_FILTER_RANGE;
}

export const FILTER_RANGE = filterRanges;

const ANDROID_AGE_CARD_RANGE = [-165, -600];
const IPHONE_AGE_CARD_RANGE = [-160, -600];
const IPHONE_X_AGE_CARD_RANGE = [-165, -600];
let ageCardRanges = ANDROID_AGE_CARD_RANGE;

if (isIOS) {
  ageCardRanges = isIphoneX() ? IPHONE_X_AGE_CARD_RANGE : IPHONE_AGE_CARD_RANGE;
}

export const AGE_CARD_RANGE = ageCardRanges;

export const HIDE_BACKGROUND_LIMIT = 20;

export const WEEKLY_CHALLENGE = '7 days';

export const IN_PROGRESS = 'inProgress';
export const COMPLETED = 'completed';
export const TARGET_NOT_MET = 'targetNotMet';
