/**
 *
 * @format
 *
 */

// @flow

import React, {useContext, useState, useRef, useEffect} from 'react';
import lodash from 'lodash';
import type {Element} from 'react';
import {View, TouchableWithoutFeedback, StatusBar, RefreshControl} from 'react-native';
import Animated from 'react-native-reanimated';
import {Dynatrace} from '@dynatrace/react-native-plugin';
import AsyncStorage from '@react-native-community/async-storage';
import {useTranslation} from 'react-i18next';
import RNSecureKeyStore from 'react-native-secure-key-store';

import {MEMBERSHIP_NO_KEY, VITALITY_TOKEN_KEY} from '@core/clients/AuthCenter/config';
import {
  VITALITY_AGE_USER_DATA,
  VITALITY_AGE_USER_ANSWERS,
  COMPLETED_ONLINE_ASSESSMENTS,
} from '@core/clients/OnlineAssessmentsApiCenter/config';
import {MEMBERSHIP_NUMBER} from '@core/clients/HealthSync/config';

import {GlobalContext} from '@contexts/Global';
import {ApplicationContext} from '@contexts/Application';
import {DashboardProvider, DashboardContext} from '@contexts/Dashboard';
import Text from '@atoms/Text';
import Slider from '@atoms/Slider';
import FlexibleText from '@atoms/FlexibleText';
import Button from '@atoms/Button';
import {LazyLoad} from '@atoms/LazyLoad';
import Header from '@molecules/Header';
import MembershipBadge from '@molecules/MembershipBadge';
import LinkSection from '@molecules/LinkSection';
import Card from '@molecules/Card';
import SectionHeader from '@molecules/SectionHeader';
import Drawer, {DrawerContainer} from '@molecules/Drawer';
import ModalContainer from '@molecules/Modal';
import AppStyles from '@styles';
import GetOS from '@platform';
import HealthSync from '@clients/HealthSync';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';
import {USER_LANGUAGE_STORAGE_KEY, EN, ID} from '@constants/i18n';

import type {
  PropsType,
  AssessmentItemType,
  ChallengeItemType,
  WhatsOnItemType,
  RewardItemType,
} from './types';
import Sedentary from '../Sedentary';
import DashboardStyles from './styles';
import {HIDE_BACKGROUND_LIMIT, COMPLETED_ASSESSMENT, DASHBOARD_SCREEN} from './config';

const Dashboard = (props: PropsType) => {
  const {
    userInfo,
    refreshUserInfo,
    setUserInfo,
    setSnackbarProps,
    clearSnackbar,
    setMsgSnackbarProps,
  } = useContext(GlobalContext);
  const {
    assessments,
    challenges,
    quickLinks,
    rewards,
    isAssessmentsLoading,
    isChallengesLoading,
    removeAEMCachedContents,
    selectChallenge,
    selectAssessment,
    refreshData,
    setHealthData,
    reloadScreen,
  } = useContext(ApplicationContext);
  const {whatsOns, isLoading, dashboardNotifications, setDashboardNotifications} = useContext(
    DashboardContext
  );
  const [showLevel, setShowLevel] = useState(true);
  const [scrollY] = useState(new Animated.Value(0));
  const [isDrawerOpen, setIsDrawerOpen] = useState(true);
  const heightDiff = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
  const {t, i18n} = useTranslation();
  const {navigation} = props;
  const isIOS = GetOS.getInstance() === 'ios';
  const scrollViewRef = useRef();
  const [sedentaryModalVisible, setSedentaryModalVisible] = useState(false);
  const userLanguage = i18n.language;
  const [refresh, setRefresh] = useState(false);
  const [links, setLinks] = useState([]);

  const syncHealthData = () => {
    HealthSync.uploadHealthData((msg, result, data) => {
      if (result) {
        setHealthData(data);
      }
      setMsgSnackbarProps(msg);
    });
  };

  const changeLanguage = async () => {
    if (userLanguage === ID) {
      i18n.changeLanguage(EN);
      await AsyncStorage.setItem(USER_LANGUAGE_STORAGE_KEY, EN);
      refreshData(EN);
    } else {
      i18n.changeLanguage(ID);
      await AsyncStorage.setItem(USER_LANGUAGE_STORAGE_KEY, ID);
      refreshData(ID);
    }
    const languageChangeAction = Dynatrace.enterAction('Language Change');
    languageChangeAction.reportStringValue('Tap Language Change');
    languageChangeAction.leaveAction();
  };

  const onPressLogOut = () => {
    RNSecureKeyStore.remove(MEMBERSHIP_NO_KEY);
    RNSecureKeyStore.remove(VITALITY_TOKEN_KEY).then(() => {
      setUserInfo({});
      navigation.navigate('Login');
    });
  };

  const onPressLinkItem = (path: string) => {
    switch (path) {
      case 'Rewards': {
        navigation.navigate('RewardsTab');
        break;
      }
      default: {
        navigation.navigate('DashboardTab', {
          screen: path,
          initial: false,
        });
        break;
      }
    }
  };

  const navigateAssessments = () => {
    navigation.navigate('AssessmentsTab');
  };

  const navigateAssessment = (data: any) => {
    selectAssessment(data.data);
    navigation.navigate('AssessmentsTab', {
      screen: 'AssessmentDetail',
      initial: false,
      params: data,
    });
  };

  const renderAssessmentSlider = () => {
    const itemWidthPercentage = 75;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: AssessmentItemType, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {title, points, iconName} = item;

      return (
        <Card
          key={index}
          variationNumber={1}
          title={title}
          iconName={iconName}
          iconGroup="general"
          points={points}
          onPressItem={() => navigateAssessment({data: item})}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_20}>
        <Slider
          isLoading={isAssessmentsLoading}
          title={t('general.assessmentTitle')}
          linkText={t('general.seeAllTitle')}
          data={assessments.filter(({status}) => status !== COMPLETED_ASSESSMENT)}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={{paddingLeft: 20}}
          onPressSeeAll={navigateAssessments}
        />
      </View>
    );
  };

  const navigateChallenges = () => {
    navigation.navigate('ChallengesTab');
  };

  const navigateChallenge = (challenge: ChallengeItemType) => {
    selectChallenge(challenge);
    navigation.navigate('ChallengesTab', {screen: 'ChallengeDetail', initial: false});
  };

  const navigateProfile = () => {
    navigation.navigate('ProfileTab');
  };

  const navigatePointsHistory = () => {
    navigation.navigate('ProfileTab', {
      screen: 'PointsHistory',
      initial: false,
    });
  };

  const renderChallengeSlider = () => {
    const itemWidthPercentage = 42;
    const slideAlign = 'start';
    const renderItem = (itemData: {item: ChallengeItemType, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {
        variationNumber,
        title,
        iconName,
        points,
        endDate,
        minPoints,
        maxPoints,
        voucher,
        highlightText,
        description,
      } = item;

      if (variationNumber === 2) {
        return (
          <Card
            key={index}
            variationNumber={2}
            title={title}
            iconName={iconName}
            iconGroup="general"
            points={points}
            minPoints={minPoints}
            maxPoints={maxPoints}
            voucher={voucher}
            endDate={endDate}
            onPressItem={() => navigateChallenge(item)}
          />
        );
      }
      return (
        <Card
          key={index}
          variationNumber={3}
          title={title}
          iconName={iconName}
          iconGroup="general"
          highlightText={highlightText}
          description={description}
          onPressItem={() => navigateChallenge(item)}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_25}>
        <Slider
          isLoading={isChallengesLoading}
          title={t('general.challengesTitle')}
          linkText={t('general.seeAllTitle')}
          data={challenges}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={{paddingLeft: 20}}
          onPressSeeAll={navigateChallenges}
        />
      </View>
    );
  };

  const renderRewardList = () => {
    const rewardList = rewards.slice(0, 3);
    const renderItem = (item: RewardItemType, index: number): Element<*> => {
      const {name, iconName, iconGroup, discount, quantity, targetDate, terms} = item;
      return (
        <Card
          key={index}
          variationNumber={4}
          name={name}
          iconName={iconName}
          iconGroup={iconGroup}
          discount={discount}
          quantity={quantity}
          targetDate={targetDate}
          terms={terms}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_25}>
        <SectionHeader title={t(`dashboard.Rewards`)} onPressSeeAll={() => {}} />
        <View style={AppStyles.marginHorizontal_20}>
          {!isLoading &&
            lodash.map(rewardList, (reward: RewardItemType, index: number) => {
              return renderItem(reward, index);
            })}
        </View>
      </View>
    );
  };

  const renderWhatsOnSlider = () => {
    const itemWidthPercentage = 90;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: WhatsOnItemType, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {title, iconName, description} = item;
      return (
        <Card
          key={index}
          variationNumber={5}
          title={title}
          iconName={iconName}
          iconGroup="general"
          description={description}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_25}>
        {!isLoading && (
          <Slider
            title={t(`dashboard.What's on`)}
            data={whatsOns}
            renderItem={renderItem}
            itemWidthPercentage={itemWidthPercentage}
            slideAlign={slideAlign}
            containerCustomStyle={{paddingLeft: 20}}
            onPressSeeAll={() => {}}
          />
        )}
      </View>
    );
  };

  const renderLinksSection = () => {
    return <LinkSection data={links} onPressItem={onPressLinkItem} />;
  };

  const renderHeader = () => {
    const content = (
      <View
        style={[
          DashboardStyles.headerContent,
          AppStyles.alignItemFlexStart,
          AppStyles.paddingVertical_10,
        ]}>
        <TouchableWithoutFeedback onPress={navigateProfile}>
          <View>
            <FlexibleText
              row
              scalableText={t('general.salutation')}
              flexibleText={userInfo.username}
            />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={navigatePointsHistory}>
          <View>
            <MembershipBadge
              showLevel={showLevel}
              points={userInfo.points}
              level={userInfo.level}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
    const lazyContent = (
      <LazyLoad style={[AppStyles.marginTop_20, AppStyles.marginHorizontal_20]}>
        <View style={{flexDirection: 'row'}}>
          <View style={[DashboardStyles.badgeLazyLoadItems, {width: 200}]} />
          <View style={{flexDirection: 'row-reverse', width: 175}}>
            <View style={[DashboardStyles.badgeLazyLoadItems, {width: 100}]} />
          </View>
        </View>
      </LazyLoad>
    );
    return (
      <Header
        scrollY={scrollY}
        setTop
        scaleHeight={Animated.interpolate(scrollY, {
          inputRange: [0, heightDiff * 2 - 1, heightDiff * 2],
          outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
          extrapolate: 'clamp',
        })}
        customPadding={DashboardStyles.dashboardHeaderLinksPadding}
        cardContent={renderLinksSection}>
        {Object.entries(userInfo).length > 0 ? content : lazyContent}
      </Header>
    );
  };

  const getNotificationData = (): {
    currentNotification: any,
    currentNotificationIndex: number,
  } => {
    const currentNotification = dashboardNotifications.find(
      (notification) => notification.show === true
    );
    return {
      currentNotification,
      currentNotificationIndex: dashboardNotifications.indexOf(currentNotification),
    };
  };

  const handleDrawerOpen = (drawerStatus: boolean) => {
    setIsDrawerOpen(drawerStatus);
    const {currentNotificationIndex} = getNotificationData();
    const nextDashboardNotificationsState = dashboardNotifications.map((notification, index) => {
      if (index === currentNotificationIndex) {
        return {...notification, show: false};
      }
      return notification;
    });
    setDashboardNotifications(nextDashboardNotificationsState);
    setIsDrawerOpen(true);
  };

  const renderDrawer = () => {
    const {currentNotification} = getNotificationData();
    const displayDrawer = currentNotification && isDrawerOpen;

    return (
      <View style={[DrawerContainer]}>
        {displayDrawer && (
          <Drawer
            isDrawerOpen
            delayBeforeDrawerOpens={0}
            changeIsDrawerOpenValueFunction={handleDrawerOpen}>
            <Text />
          </Drawer>
        )}
      </View>
    );
  };

  const openDrawer = () => {
    const nextDashboardNotificationsState = dashboardNotifications.map((notification, index) => {
      if (index === 0) {
        return {...notification, show: true};
      }
      return notification;
    });
    setDashboardNotifications(nextDashboardNotificationsState);
    setIsDrawerOpen(true);
  };

  const sedentaryModal = () => {
    return (
      <ModalContainer
        modalVisible={sedentaryModalVisible}
        setModalVisible={setSedentaryModalVisible}>
        <Sedentary />
      </ModalContainer>
    );
  };

  const renderChangeLanguageButton = () => {
    return (
      <View style={[AppStyles.marginTop_25]}>
        <SectionHeader title={t(`dashboard.Change Language`)} />
        <View style={[AppStyles.paddingHorizontal_20]}>
          <Button
            variation="contained"
            accessibilityLabel="Language Button"
            onPress={changeLanguage}
            buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
            {t('dashboard.Change Language')}
          </Button>
        </View>
      </View>
    );
  };

  const renderHealthSyncButton = () => {
    return (
      <View style={[AppStyles.marginTop_25]}>
        <SectionHeader title={t(`dashboard.Test Health Sync`)} />
        <View style={[AppStyles.paddingHorizontal_20]}>
          <Button
            variation="contained"
            accessibilityLabel="Health Sync Button"
            onPress={syncHealthData}
            buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
            {t('dashboard.Test Health Sync')}
          </Button>
        </View>
      </View>
    );
  };

  const renderAnimatedCode = () => {
    return (
      <Animated.Code>
        {() =>
          Animated.call([scrollY], ([val]: number[]) => {
            if (showLevel) {
              if (val >= HIDE_BACKGROUND_LIMIT) {
                setShowLevel(false);
              }
            } else if (val <= HIDE_BACKGROUND_LIMIT) {
              setShowLevel(true);
            }
          })
        }
      </Animated.Code>
    );
  };

  const handleRemoveCachedAssessmentsData = () => {
    AsyncStorage.getItem(MEMBERSHIP_NUMBER)
      .then((membershipNo) => {
        AsyncStorage.removeItem(`${VITALITY_AGE_USER_DATA}_${membershipNo}`)
          .then(() => {
            AsyncStorage.removeItem(`${VITALITY_AGE_USER_ANSWERS}_${membershipNo}`)
              .then(() => onPressLogOut())
              .catch(() => {
                setMsgSnackbarProps('Failed to remove cached assessments user answers');
              });
            AsyncStorage.removeItem(`${COMPLETED_ONLINE_ASSESSMENTS}_${membershipNo}`)
              .then(() => onPressLogOut())
              .catch(() => {
                setMsgSnackbarProps('Failed to remove cached completed online assessments');
              });
          })
          .catch(() => {
            setMsgSnackbarProps('failed to remove cached assessments user data');
          });
      })
      .catch(() => {
        setMsgSnackbarProps('No membership ID found');
      });
  };

  const renderRemoveCachedAssessmentsData = () => {
    return (
      <View style={[AppStyles.padding_20]}>
        <Button
          variation="outlined"
          accessibilityLabel="Flush Assessments Data"
          onPress={handleRemoveCachedAssessmentsData}
          buttonStyle={[
            AppStyles.generalButton,
            AppStyles.generalOutlinedButton,
            AppStyles.fullWidthButton,
          ]}>
          Flush Assessments Data
        </Button>
      </View>
    );
  };

  const navigateChallengeCompleteScreen = () => {
    navigation.navigate('ChallengesTab', {screen: 'ChallengeComplete', initial: false});
  };

  const renderNavigateChallengeCompleteButton = () => {
    return (
      <View style={[AppStyles.padding_20]}>
        <Button
          variation="outlined"
          accessibilityLabel="Challenge Complete Screen"
          onPress={navigateChallengeCompleteScreen}
          buttonStyle={[
            AppStyles.generalButton,
            AppStyles.generalOutlinedButton,
            AppStyles.fullWidthButton,
          ]}>
          Challenge Complete Screen
        </Button>
      </View>
    );
  };

  const onRefresh = React.useCallback(() => {
    setRefresh(true);
    setUserInfo({});
    removeAEMCachedContents(() => {});
    reloadScreen(DASHBOARD_SCREEN);
    refreshUserInfo();
    const timer = setTimeout(() => {}, 2000);
    if (timer) {
      clearTimeout(timer);
      setRefresh(false);
    }
  }, [refresh]);

  const onPressOpenSnackbar = () => {
    setSnackbarProps({
      visible: true,
      dismissCallback: clearSnackbar,
      duration: 5000,
      title: <Text customStyle={[AppStyles.minPts]}>This is Snackbar</Text>,
      actionLabel: <Text customStyle={[AppStyles.headerTitle]}>Action</Text>,
      onPressAction: clearSnackbar,
    });
  };

  const renderSnackbarButton = () => {
    return (
      <View style={[AppStyles.padding_20]}>
        <Button
          variation="outlined"
          accessibilityLabel="Open Snackbar"
          onPress={onPressOpenSnackbar}
          buttonStyle={[
            AppStyles.generalButton,
            AppStyles.generalOutlinedButton,
            AppStyles.fullWidthButton,
          ]}>
          Open Snackbar
        </Button>
      </View>
    );
  };

  const processLinks = () => {
    if (Object.keys(quickLinks).length > 0) {
      const {hReport, notify, wkTrack, voucher, points, ecard, help} = quickLinks;
      const orderedLinks = {
        hReport,
        notify,
        wkTrack,
        voucher,
        points,
        ecard,
        help,
      };
      setLinks(orderedLinks);
    }
  };

  useEffect(() => {
    processLinks();
  }, [quickLinks]);

  return (
    <View style={DashboardStyles.container}>
      <StatusBar barStyle="light-content" />
      {renderHeader()}
      <Animated.ScrollView
        ref={scrollViewRef}
        // bounces={false}
        bounces
        style={[
          DashboardStyles.body,
          isIOS ? DashboardStyles.iosBody : DashboardStyles.androidBody,
        ]}
        scrollEventThrottle={16}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {y: scrollY}}}], {
          useNativeDriver: true,
        })}
        refreshControl={
          <RefreshControl refreshing={refresh} onRefresh={onRefresh} progressViewOffset={190} />
        }>
        <View
          style={[
            DashboardStyles.bodyContent,
            isIOS ? DashboardStyles.iosBodyContent : DashboardStyles.androidBodyContent,
          ]}>
          {renderAssessmentSlider()}
          {renderChallengeSlider()}
          {renderRewardList()}
          {renderWhatsOnSlider()}
          {renderChangeLanguageButton()}
          {renderHealthSyncButton()}

          <View style={AppStyles.padding_20}>
            <Button
              variation="outlined"
              accessibilityLabel="Open Drawer Button"
              onPress={openDrawer}
              buttonStyle={[
                AppStyles.generalButton,
                AppStyles.generalOutlinedButton,
                AppStyles.fullWidthButton,
              ]}>
              {t('general.open-drawer')}
            </Button>
          </View>
          <View style={AppStyles.padding_20}>
            <Button
              variation="outlined"
              accessibilityLabel="Open Modal Button"
              onPress={() => setSedentaryModalVisible(true)}
              buttonStyle={[
                AppStyles.generalButton,
                AppStyles.generalOutlinedButton,
                AppStyles.fullWidthButton,
              ]}>
              {t('general.open-modal')}
            </Button>
          </View>
          <View style={AppStyles.padding_20}>
            <Button
              variation="contained"
              accessibilityLabel="Setup Biometric Login"
              onPress={() => navigation.navigate('BiometricScreen')}
              buttonTextStyle={AppStyles.white}
              buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
              {t('general.setup-biometrics')}
            </Button>
          </View>
          {renderRemoveCachedAssessmentsData()}
          {renderNavigateChallengeCompleteButton()}
          {renderSnackbarButton()}
          <View style={AppStyles.padding_20}>
            <Button
              variation="outlined"
              accessibilityLabel="Log out button"
              onPress={onPressLogOut}
              buttonStyle={[
                AppStyles.generalButton,
                AppStyles.generalOutlinedButton,
                AppStyles.fullWidthButton,
              ]}>
              {t('dashboard.Log Out')}
            </Button>
          </View>
        </View>
        <View style={DashboardStyles.extraBody} />
        {renderAnimatedCode()}
      </Animated.ScrollView>
      {sedentaryModal()}
      {renderDrawer()}
    </View>
  );
};

const DashboardWrapper = (props: PropsType) => {
  const {navigation} = props;

  return (
    <DashboardProvider>
      <Dashboard navigation={navigation} />
    </DashboardProvider>
  );
};
export default DashboardWrapper;
