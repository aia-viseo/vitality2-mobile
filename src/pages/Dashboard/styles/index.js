/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const DashboardStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    zIndex: 0,
    position: 'relative',
    flexDirection: 'column',
    overflow: 'visible',
  },
  iosBody: {
    marginTop: -55,
    zIndex: 97,
  },
  androidBody: {
    marginTop: -250,
  },
  bodyContent: {},
  iosBodyContent: {},
  androidBodyContent: {
    marginTop: 190,
    elevation: 999,
  },
  linkSectionContainer: {
    marginHorizontal: 20,
    zIndex: 99,
    elevation: 99,
  },
  extraBody: {
    height: 1000,
  },
  marginVertical: {
    marginVertical: 20,
  },
  headerContent: {
    marginTop: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  badgeLazyLoadItems: {
    height: 30,
    borderRadius: 50,
  },
  dashboardHeaderLinksPadding: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
});

export default DashboardStyles;
