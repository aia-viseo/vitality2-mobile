/**
 *
 * @format
 *
 */

// @flow

export const HIDE_BACKGROUND_LIMIT = 20;
export const COMPLETED_ASSESSMENT = 'update';
export const DASHBOARD_SCREEN = 'dashboard_screen';
export const ASSESSMENTS_SCREEN = 'assessments_screen';
export const CHALLENGES_SCREEN = 'challenges_screen';

const config = {};

export default config;
