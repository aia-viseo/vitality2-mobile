/**
 *
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

// eslint-disable-next-line import/prefer-default-export
export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
