/**
 *
 * Sedentary
 * @format
 *
 */

// @flow

import React, {useState} from 'react';
import {ScrollView, View, Text} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import Icon from '@atoms/Icon';
import CircularProgressBar from '@molecules/CircularProgressBar';
import DatePicker from '@molecules/DatePicker';
import Input from '@atoms/Input';

import SedentaryStyles from './styles';

const Sedentary = () => {
  const [date, setDate] = useState(null);
  const [side, setSide] = useState(50);
  const [progress, setProgress] = useState(50);
  const [iconSize] = useState(26);
  const circularProgressIcon = (
    <VIcon name="clipboard-text-outline" size={iconSize} color="#1795DA" />
  );

  return (
    <ScrollView>
      <View>
        <Icon group="general" name="manRunning" />
      </View>
      <View style={SedentaryStyles.exerciseContainer}>
        <Text style={SedentaryStyles.exerciseText}>Avoid Sedentary Behavior</Text>
      </View>
      <View style={SedentaryStyles.sedentaryContainer}>
        <Text style={SedentaryStyles.sedentaryText}>
          Your sedentary if you are not moving at all e.g. when you are setting down. You should
          only do this for up to 10 hours a day.
        </Text>
      </View>
      <View style={{paddingTop: 20}}>
        <DatePicker onChange={(selectedDate) => setDate(selectedDate)} date={date} />
      </View>
      <View style={{paddingTop: 10}}>
        <Input
          label="Progress (0 - 100)"
          numbersOnly
          changeText={(text) => {
            const intText = parseInt(text, 10);
            if (intText >= 0 && intText <= 100) {
              setProgress(text);
            } else {
              setProgress(50);
            }
          }}
          value={progress}
        />
        <Input
          label="Component size"
          numbersOnly
          changeText={(text) => {
            setSide(text);
          }}
          value={side}
          customStyle={{marginTop: 10}}
        />
        <CircularProgressBar
          progress={progress}
          icon={circularProgressIcon}
          iconSize={iconSize}
          side={side}
          customStyle={{marginTop: 10}}
        />
      </View>
    </ScrollView>
  );
};

Sedentary.defaultProps = {};

export default Sedentary;
