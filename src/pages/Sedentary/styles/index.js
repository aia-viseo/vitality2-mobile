/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const SedentaryStyles = StyleSheet.create({
  exerciseContainer: {
    paddingTop: 20,
  },
  exerciseText: {
    fontSize: RFValue(24),
  },
  sedentaryContainer: {
    paddingTop: 20,
  },
  sedentaryText: {
    fontSize: RFValue(16),
    color: 'gray',
  },
});

export default SedentaryStyles;
