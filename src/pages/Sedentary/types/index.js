/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  navigation?: {
    navigate: (path: string) => void,
  },
};
