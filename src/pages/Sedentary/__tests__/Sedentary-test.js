/**
 *
 * Sedentary Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Sedentary from '../Sedentary';

test('Sedentary', () => {
  const tree = renderer.create(<Sedentary />).toJSON();
  expect(tree).toMatchSnapshot();
});
