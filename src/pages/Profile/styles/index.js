/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import GetOS from '@platform';
import {WHITE} from '@colors';

const isIOS = GetOS.getInstance() === 'ios';

const ProfileStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    position: 'relative',
    flexDirection: 'column',
    overflow: 'visible',
    marginTop: -240,
  },
  title: {
    fontSize: RFValue(20),
    color: WHITE,
    fontWeight: isIOS ? '600' : 'bold',
  },
  iosBody: {
    zIndex: 97,
  },
  androidBody: {
    elevation: 97,
  },
  bodyContent: {
    marginTop: 130,
  },
  iosBodyContent: {},
  androidBodyContent: {
    elevation: 999,
  },
  linkSectionContainer: {
    marginHorizontal: 20,
    zIndex: 99,
    elevation: 99,
  },
  ageCardContainer: {
    marginHorizontal: 20,
    zIndex: 99,
    elevation: 99,
  },
  extraBody: {
    height: 1000,
  },
  marginVertical: {
    marginVertical: 20,
  },
  headerContent: {
    flexDirection: 'row',
  },
  profile: {
    flex: 1,
    zIndex: 99,
  },
  links: {
    flex: 1,
    zIndex: 99,
  },
  badgeLazyLoadItems: {
    height: 30,
    borderRadius: 50,
  },
});

export default ProfileStyles;
