/**
 *
 * @format
 *
 */

// @flow
import type {Node} from 'react';

type NavigationSubpath = {
  screen: string,
  params?: any,
};

export type PropsType = {
  navigation: {
    navigate: (path: string, subpath?: NavigationSubpath) => void,
    goBack: (path?: string) => void,
  },
};

type SliderItemCoreType = {|
  title?: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points?: number,
  age?: number,
  diffYears?: number,
|};

export type HealthReportItemType = SliderItemCoreType & {
  variationNumber: number,
};

export type ChallengeItemType = SliderItemCoreType & {
  variationNumber: number,
  endDate?: Date,
  minPoints?: number,
  maxPoints?: number,
  voucher?: number,
  highlightText?: string,
  description?: string,
};

export type WhatsOnItemType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  description: string,
};

export type RewardItemType = {
  name: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  discount: string,
  quantity: number,
  targetDate?: Date,
  terms?: string,
};

export type DashboardNotificationItemType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  description: string,
  show: boolean,
};

export type HeaderMenuType = {
  icon?: Node,
  path?: string,
  labelKey?: string,
};
