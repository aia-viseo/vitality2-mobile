/**
 *
 * @format
 *
 */

// @flow

import React, {useContext, useState, useRef} from 'react';
import type {Element} from 'react';
import {View, StatusBar} from 'react-native';
import Animated from 'react-native-reanimated';
import {useTranslation} from 'react-i18next';

import {ApplicationContext} from '@contexts/Application';
import {GlobalContext} from '@contexts/Global';
import {ProfileProvider} from '@contexts/Profile';

import Header from '@molecules/Header';
import {LazyLoad} from '@atoms/LazyLoad';
import MembershipProfile from '@molecules/MembershipProfile';
import Slider from '@atoms/Slider';
import VitalityAgeCard from '@molecules/VitalityAgeCard';
import SectionHeader from '@molecules/SectionHeader';
import CardContainer from '@molecules/Card/CardContainer';
import LinkSection from '@molecules/LinkSection';
import GetOS from '@platform';
import AppStyles from '@styles';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';

import {HIDE_BACKGROUND_LIMIT} from './config';

import ProfileStyles from './styles';
import type {PropsType, HealthReportItemType} from './types';

const Profile = (props: PropsType) => {
  const {quickLinks, vitalityAge} = useContext(ApplicationContext);
  const {userInfo} = useContext(GlobalContext);
  const [showLevel, setShowLevel] = useState(true);
  const {navigation} = props;
  const [scrollY] = useState(new Animated.Value(0));
  const heightDiff = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
  const {t} = useTranslation();
  const isIOS = GetOS.getInstance() === 'ios';
  const scrollViewRef = useRef();

  const getHealthReportData = () => {
    const {age, diffYears} = vitalityAge;
    return [{age, diffYears}];
  };

  const onPressLinkItem = (path: string) => {
    navigation.navigate(path);
  };

  const renderHealthReport = () => {
    const itemWidthPercentage = 75;
    const slideAlign = 'start';
    const healthReportData = getHealthReportData();

    const renderItem = (itemData: {item: HealthReportItemType, index: number}): Element<*> => {
      const {item} = itemData;
      const {age, diffYears} = item;

      return (
        <CardContainer>
          <VitalityAgeCard age={age} diffYears={diffYears} unRead />
        </CardContainer>
      );
    };

    return (
      <View style={AppStyles.marginTop_20}>
        <Slider
          title={t('profile.healthReport')}
          linkText={t('general.seeAllTitle')}
          data={healthReportData}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={{paddingLeft: 20}}
          onPressSeeAll={() => {}}
        />
      </View>
    );
  };

  const renderWorkoutTracker = () => {
    return (
      <View style={AppStyles.marginTop_25}>
        <SectionHeader title={t(`profile.workoutTracker`)} onPressSeeAll={() => {}} />
        <View style={AppStyles.marginHorizontal_20}>
          {/* {!isLoading &&
            lodash.map(rewards, (reward: RewardItemType, index: number) => {
              return renderItem(reward, index);
            })} */}
        </View>
      </View>
    );
  };

  const renderNotificationsSlider = () => {
    return (
      <View style={AppStyles.marginTop_25}>
        <SectionHeader title={t(`profile.notifications`)} onPressSeeAll={() => {}} />
        <View style={AppStyles.marginHorizontal_20}>
          {/* {!isLoading &&
            lodash.map(rewards, (reward: RewardItemType, index: number) => {
              return renderItem(reward, index);
            })} */}
        </View>
      </View>
    );
  };

  const renderLinksSection = () => {
    const {ecard, help, points, voucher} = quickLinks;
    const profileLinks = {
      ecard,
      voucher,
      points,
      help,
    };

    const lazyContent = () => {
      return (
        <LazyLoad style={[AppStyles.marginTop_20, AppStyles.marginHorizontal_20]}>
          <View style={{flexDirection: 'row'}}>
            <View style={[AppStyles.lazyLoadItems]} />
          </View>
        </LazyLoad>
      );
    };

    const content = () => {
      return (
        <View style={[AppStyles.column, AppStyles.flex_1]}>
          <MembershipProfile user={userInfo} style={ProfileStyles.profile} />
          <LinkSection data={profileLinks} onPressItem={onPressLinkItem} max={4} />
        </View>
      );
    };
    return Object.entries(userInfo).length > 0 ? content() : lazyContent();
  };

  const renderHeader = () => {
    return (
      <Header
        scrollY={scrollY}
        setTop
        scaleHeight={Animated.interpolate(scrollY, {
          inputRange: [0, heightDiff * 2 - 1, heightDiff * 2],
          outputRange: [HEADER_MAX_HEIGHT + 55, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
        })}
        title={t('profile.profile')}
        cardContent={renderLinksSection}
        translateY={-105}
      />
    );
  };

  const renderAnimatedCode = () => {
    return (
      <Animated.Code>
        {() =>
          Animated.call([scrollY], ([val]: number[]) => {
            if (showLevel) {
              if (val >= HIDE_BACKGROUND_LIMIT) {
                setShowLevel(false);
              }
            } else if (val <= HIDE_BACKGROUND_LIMIT) {
              setShowLevel(true);
            }
          })
        }
      </Animated.Code>
    );
  };

  return (
    <View style={ProfileStyles.container}>
      <StatusBar barStyle="light-content" />
      {renderHeader()}
      <Animated.ScrollView
        ref={scrollViewRef}
        bounces={false}
        style={[ProfileStyles.body, isIOS ? ProfileStyles.iosBody : ProfileStyles.androidBody]}
        scrollEventThrottle={16}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {y: scrollY}}}], {
          useNativeDriver: true,
        })}>
        <View
          style={[
            ProfileStyles.bodyContent,
            isIOS ? ProfileStyles.iosBodyContent : ProfileStyles.androidBodyContent,
          ]}>
          {renderHealthReport()}
          {renderWorkoutTracker()}
          {renderNotificationsSlider()}
        </View>
        <View style={ProfileStyles.extraBody} />
        {renderAnimatedCode()}
      </Animated.ScrollView>
    </View>
  );
};

const ProfileWrapper = (props: PropsType) => {
  const {navigation} = props;

  return (
    <ProfileProvider>
      <Profile navigation={navigation} />
    </ProfileProvider>
  );
};
export default ProfileWrapper;
