/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const LoginScreenStyles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
  },
  mainImage: {},
  loginTextContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loginTextHeader: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  loginLanguage: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  loginLanguageIcon: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    fontSize: RFValue(22),
    fontWeight: '600',
  },
  language: {
    fontSize: RFValue(20),
  },
});

export default LoginScreenStyles;
