/**
 *
 * Login Page
 * @format
 *
 */

// @flow

import React, {useContext, useState, useEffect} from 'react';
import {SafeAreaView, View, ScrollView} from 'react-native';
import {HelperText} from 'react-native-paper';
import {Dynatrace} from '@dynatrace/react-native-plugin';

import AsyncStorage from '@react-native-community/async-storage';
import {useTranslation} from 'react-i18next';

import VIcon from 'react-native-vector-icons/MaterialIcons';

import {AuthContext} from '@contexts/Auth';
import {GlobalContext} from '@contexts/Global';
import AuthCenter from '@clients/AuthCenter';

import Button from '@atoms/Button';
import Icon from '@atoms/Icon';
import Input from '@atoms/Input';
import Text from '@atoms/Text';
import Checkbox from '@molecules/Checkbox';
import BiometricValidator from '@atoms/Biometric/BiometricValidator';
import AppStyles from '@styles';
import {COLOR_AIA_GREY, COLOR_AIA_RED} from '@colors';
import {USER_LANGUAGE_STORAGE_KEY, EN, ID} from '@constants/i18n';

import type {PropsType} from './types';
import LoginScreenStyles from './styles';

const Login = (props: PropsType) => {
  const {t, i18n} = useTranslation();
  const {
    loginForm,
    setLoginForm,
    membershipNumber,
    setMembershipNumber,
    usesBiometric,
  } = useContext(AuthContext);
  const {refreshUserInfo, clearCache, setMsgSnackbarProps} = useContext(GlobalContext);
  const {navigation} = props;
  const [checked, setChecked] = useState(true);

  const getStorageMembershipNo = async () => {
    AuthCenter.getMembershipNumber((membershipNumberResult) => {
      setMembershipNumber(membershipNumberResult);
    });
  };

  const handleCheckboxClick = () => {
    setChecked(!checked);
  };

  const changeLanguage = async () => {
    const userLanguage = i18n.language;

    if (userLanguage === ID) {
      i18n.changeLanguage(EN);
      await AsyncStorage.setItem(USER_LANGUAGE_STORAGE_KEY, EN);
    } else {
      i18n.changeLanguage(ID);
      await AsyncStorage.setItem(USER_LANGUAGE_STORAGE_KEY, ID);
    }
  };

  const onPressLogin = () => {
    if (membershipNumber) {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        if (membershipNumber !== membershipNumberResult) {
          AuthCenter.setMembershipNumber(membershipNumber);
          clearCache();
        }
        refreshUserInfo();
        Dynatrace.identifyUser(membershipNumber);
        const loginAction = Dynatrace.enterAction('Login Button');
        loginAction.reportStringValue('Tap Login Button');
        loginAction.leaveAction();
        navigation.navigate('Application');
      });
    } else {
      setMsgSnackbarProps(t('login.Please enter your Membership Number'));
    }
  };

  const onChangeText = (fieldName: string, value: string) => {
    setLoginForm(fieldName, value);
  };

  const onMembershipNoChangeText = (value: string) => {
    setMembershipNumber(value);
  };

  const hasErrors = () => {
    return !membershipNumber;
  };

  useEffect(() => {
    getStorageMembershipNo();
  }, []);

  const renderImage = () => {
    return (
      <View>
        <Icon group="general" name="manRunning" />
      </View>
    );
  };

  const renderLoginText = () => {
    return (
      <View style={[AppStyles.marginTop_10, LoginScreenStyles.loginTextContainer]}>
        <View style={LoginScreenStyles.loginTextHeader}>
          <Text customStyle={LoginScreenStyles.loginText}>{t('login.Login')} </Text>
        </View>
        <View style={LoginScreenStyles.loginLanguage}>
          <Button
            variation="text"
            accessibilityLabel="Language Button"
            onPress={changeLanguage}
            buttonTextStyle={AppStyles.primary}>
            {t('login.language')}
          </Button>
          <VIcon name="language" size={15} color={COLOR_AIA_RED} />
        </View>
      </View>
    );
  };

  const renderInput = () => {
    const theme = {colors: {primary: COLOR_AIA_GREY}};
    const {mobileNumber, password} = loginForm;
    return (
      <View style={AppStyles.marginTop_20}>
        <Input
          label={t('login.mobileNumber')}
          placeholder={t('login.mobileNumber')}
          customStyle={{display: 'none'}}
          underlineColor="transparent"
          theme={theme}
          changeText={(text) => onChangeText('mobileNumber', text)}
          defaultValue={mobileNumber}
        />
        <Input
          label={t('login.password')}
          placeholder={t('login.password')}
          underlineColor="transparent"
          secureTextEntry
          theme={theme}
          changeText={(text) => onChangeText('password', text)}
          defaultValue={password}
          customStyle={[AppStyles.marginTop_16, {display: 'none'}]}
        />
        <Input
          label={t('login.membershipNumber')}
          placeholder={t('login.membershipNumber')}
          underlineColor="transparent"
          theme={theme}
          changeText={(text) => onMembershipNoChangeText(text)}
          defaultValue={membershipNumber}
          value={membershipNumber}
        />
        <HelperText type="error" visible={hasErrors()}>
          Membership number is invalid!
        </HelperText>
      </View>
    );
  };

  const renderRememberMe = () => {
    return (
      <View>
        <Checkbox
          variation={1}
          checked={checked}
          onPress={handleCheckboxClick}
          text={t('login.rememberLogin')}
        />
      </View>
    );
  };

  const renderForgotLogin = () => {
    return (
      <View style={AppStyles.marginTop_40}>
        <Text customStyle={{color: COLOR_AIA_RED}}>{t('login.forgotLogin')}</Text>
      </View>
    );
  };

  const renderBioLogin = () => {
    BiometricValidator.createSignature(
      () => {
        onPressLogin();
      },
      () => {
        setMsgSnackbarProps('Biometric login failed.');
      }
    );
  };

  const renderControlSection = () => {
    return (
      <View style={AppStyles.marginTop_20}>
        <View style={AppStyles.row}>
          <Button
            variation="contained"
            accessibilityLabel="Login Button"
            onPress={() => onPressLogin()}
            buttonStyle={[
              AppStyles.generalButton,
              {
                width: usesBiometric ? '79%' : '100%',
              },
            ]}
            buttonTextStyle={AppStyles.white}>
            {`${t('login.Login').toUpperCase()}`}
          </Button>
          {usesBiometric && (
            <Button
              variation="outlined"
              accessibilityLabel="Biometrics Button"
              onPress={renderBioLogin}
              buttonStyle={[
                AppStyles.generalButton,
                AppStyles.generalOutlinedButton,
                AppStyles.marginLeft_10,
              ]}>
              <VIcon name="fingerprint" size={15} color={COLOR_AIA_RED} />
            </Button>
          )}
        </View>
      </View>
    );
  };

  const renderActivateAccount = () => {
    return (
      <View style={[AppStyles.row, AppStyles.marginTop_24, AppStyles.marginBottom_16]}>
        <Text customStyle={{color: COLOR_AIA_GREY}}>{t('login.newToVitality')}</Text>
        <Text customStyle={{color: COLOR_AIA_RED}}>{t('login.activateAccount')}</Text>
      </View>
    );
  };

  const renderTandC = () => {
    return (
      <View style={[AppStyles.row]}>
        <Text customStyle={{color: COLOR_AIA_GREY}}>{t('login.read')}</Text>
        <Text customStyle={{color: COLOR_AIA_RED}}>{t('login.termsOfUse')}</Text>
        <Text customStyle={{color: COLOR_AIA_GREY}}>{t('login.and')}</Text>
        <Text customStyle={{color: COLOR_AIA_RED}}>{t('login.privacyPolicy')}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={[AppStyles.flex_1, AppStyles.marginHorizontal_20]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {renderImage()}
        {renderLoginText()}
        {renderInput()}
        {renderRememberMe()}
        {renderForgotLogin()}
        {renderControlSection()}
        {renderActivateAccount()}
        {renderTandC()}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Login;
