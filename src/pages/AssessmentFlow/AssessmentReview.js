/**
 *
 * Assessment Review
 * @format
 *
 */

// @flow

import React, {useContext, useEffect} from 'react';
import {View, TouchableWithoutFeedback, FlatList} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';
import {useTranslation} from 'react-i18next';

import {MEMBERSHIP_NUMBER} from '@clients/HealthSync/config';
import OnlineAssessmentsApiCenter from '@clients/OnlineAssessmentsApiCenter';
import {OnlineAssessmentsContext} from '@contexts/OnlineAssessments';
import {AssessmentsContext} from '@contexts/Assessments';
import {GlobalContext} from '@contexts/Global';
import {ASSESSMENT_DETAIL, ASSESSMENT_QUESTION} from '@core/handler/AEMResponseParser/config';
import {
  VITALITY_AGE_USER_DATA,
  VITALITY_AGE_USER_ANSWERS,
  COMPLETED_ONLINE_ASSESSMENTS,
} from '@clients/OnlineAssessmentsApiCenter/config';

import AppStyles from '@styles';
import GetOS from '@platform';
import Text from '@atoms/Text';
import Button from '@atoms/Button';
import Journey from '@layouts/Journey';
import HorizontalHeader from '@organisms/HorizontalHeader';

import {COLOR_AIA_VITALITY_BLUE, COLOR_AIA_RED} from '@colors';

import {IdentifierQuestionIdMapping, specificApiMap, ANSWER_TYPE} from './config';

import AssessmentFlowStyles from './styles';
import type {PropsType} from './types';

const AssessmentReview = (props: PropsType) => {
  const {navigation} = props;
  const {
    onlineAssessmentsUserAnswers,
    setOnlineAssessmentsUserAnswers,
    setOnlineAssessmentsCompletedData,
    onlineAssessmentsUserData,
    completedOnlineAssessments,
    setCompletedOnlineAssessments,
    answeredOnlineAssessments,
    setAnsweredOnlineAssessments,
  } = useContext(OnlineAssessmentsContext);
  const {setMsgSnackbarProps} = useContext(GlobalContext);

  const {
    setAssessmentCurrentPage,
    currentAssessment,
    assessmentCurrentPage,
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
    assessmentReview,
    howActiveAreYouAll,
    howHealthyAreYouAll,
    howStressedAreYouAll,
    howWellAreYouEatingAll,
    howWellDoYouSleepAll,
    nonSmokerDeclarationAll,
  } = useContext(AssessmentsContext);
  const isIOS = GetOS.getInstance() === 'ios';

  const ALL_ASSESSMENTS = [
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
  ];

  const assessmentId = currentAssessment || 'HowHealthyAreYou';
  const assessmentUserData = onlineAssessmentsUserData[assessmentId] || {};
  const assessmentSet = ALL_ASSESSMENTS.find(
    (question) => question[ASSESSMENT_DETAIL] && question[ASSESSMENT_DETAIL].id === assessmentId
  );
  const newCompletedAssessments = [
    ...completedOnlineAssessments.filter((assess) => assess !== assessmentId),
    assessmentId,
  ];
  const newAnsweredAssessments = [
    ...answeredOnlineAssessments.filter((assess) => assess !== assessmentId),
    assessmentId,
  ];
  const currentQuestionSet = assessmentSet ? assessmentSet[ASSESSMENT_QUESTION] : {};
  const currentQuestionKeys = Object.keys(currentQuestionSet).sort((a, b) => {
    const questionA = parseInt(a.replace(/[^0-9]/g, ''), 10);
    const questionB = parseInt(b.replace(/[^0-9]/g, ''), 10);
    return questionA - questionB;
  });
  const lastQuestion = currentQuestionSet[currentQuestionKeys.pop()];
  const {i18n, t} = useTranslation();
  const userLanguage = i18n.language;
  const specificApi: string = specificApiMap[assessmentId] || 'general';
  const {title, subtitle} = assessmentReview;
  const {CHECKBOXS, CHIPS} = ANSWER_TYPE;

  useEffect(() => {
    setAnsweredOnlineAssessments(newAnsweredAssessments);
  }, []);

  const dataForSubmission = Object.keys(assessmentUserData).reduce((acc, val) => {
    if (assessmentUserData[val]) {
      acc[`${val[0].toLowerCase()}${val.slice(1)}`] = assessmentUserData[val];
    }
    return acc;
  }, {});

  const mapObject = (obj) => {
    return [...Object.keys(obj).map((quest) => obj[quest])];
  };

  const reduceIdentifiers = (arr) => {
    return arr.reduce((acc, val) => {
      if (val.identifiers && val.identifiers.length > 0) {
        const identifiersObject = val.identifiers.reduce((acc2, val2) => {
          return {...acc2, ...{[val2]: val.answers}};
        }, {});
        return {...acc, ...identifiersObject};
      }
      return acc;
    }, {});
  };

  const replicateUserAnswers = (language: string) => {
    const allQuestions = {
      en: [
        ...mapObject(howActiveAreYouAll.en[ASSESSMENT_QUESTION]),
        ...mapObject(howHealthyAreYouAll.en[ASSESSMENT_QUESTION]),
        ...mapObject(howStressedAreYouAll.en[ASSESSMENT_QUESTION]),
        ...mapObject(howWellAreYouEatingAll.en[ASSESSMENT_QUESTION]),
        ...mapObject(howWellDoYouSleepAll.en[ASSESSMENT_QUESTION]),
        ...mapObject(nonSmokerDeclarationAll.en[ASSESSMENT_QUESTION]),
      ],
      id: [
        ...mapObject(howActiveAreYouAll.id[ASSESSMENT_QUESTION]),
        ...mapObject(howHealthyAreYouAll.id[ASSESSMENT_QUESTION]),
        ...mapObject(howStressedAreYouAll.id[ASSESSMENT_QUESTION]),
        ...mapObject(howWellAreYouEatingAll.id[ASSESSMENT_QUESTION]),
        ...mapObject(howWellDoYouSleepAll.id[ASSESSMENT_QUESTION]),
        ...mapObject(nonSmokerDeclarationAll.id[ASSESSMENT_QUESTION]),
      ],
    };
    const allIdentifiers = {
      en: reduceIdentifiers(allQuestions.en),
      id: reduceIdentifiers(allQuestions.id),
    };
    const replicatedUserAnswers = Object.keys(onlineAssessmentsUserAnswers[userLanguage]).reduce(
      (acc, val) => {
        const userAnswer = onlineAssessmentsUserAnswers[userLanguage][val];
        const indexOfUserAnswer =
          userAnswer !== '' &&
          allIdentifiers[userLanguage][val] &&
          allIdentifiers[userLanguage][val].indexOf(userAnswer);
        if (userAnswer === '' || allIdentifiers[language][val] === undefined) {
          acc[val] = userAnswer;
          return acc;
        }
        if (
          userAnswer !== '' &&
          indexOfUserAnswer !== undefined &&
          allIdentifiers[language][val] &&
          allIdentifiers[language][val][indexOfUserAnswer]
        ) {
          acc[val] = allIdentifiers[language][val][indexOfUserAnswer];
          return acc;
        }
        return acc;
      },
      {}
    );
    return {...onlineAssessmentsUserAnswers, ...{[language]: replicatedUserAnswers}};
  };

  const handleSubmitOnlineAssessment = () => {
    const replicatedUserAnswers = replicateUserAnswers(userLanguage === 'en' ? 'id' : 'en');
    setOnlineAssessmentsUserAnswers(replicatedUserAnswers);
    OnlineAssessmentsApiCenter.submitOnlineAssessment(
      null,
      null,
      dataForSubmission,
      specificApi,
      (completeData) => {
        AsyncStorage.getItem(MEMBERSHIP_NUMBER)
          .then((membershipNo) => {
            AsyncStorage.setItem(
              `${VITALITY_AGE_USER_DATA}_${membershipNo}`,
              JSON.stringify(onlineAssessmentsUserData)
            ).catch(() => {
              setMsgSnackbarProps('Error storing user data to cache.');
            });
            AsyncStorage.setItem(
              `${VITALITY_AGE_USER_ANSWERS}_${membershipNo}`,
              JSON.stringify(replicatedUserAnswers)
            ).catch(() => {
              setMsgSnackbarProps('Error storing user data to cache.');
            });
            AsyncStorage.setItem(
              `${COMPLETED_ONLINE_ASSESSMENTS}_${membershipNo}`,
              JSON.stringify(newCompletedAssessments)
            )
              .then(() => {
                setCompletedOnlineAssessments(newCompletedAssessments);
              })
              .catch(() => {
                setMsgSnackbarProps('Error storing completed assessment to cache.');
              });
          })
          .catch(() => {
            setMsgSnackbarProps('Error getting membership number.');
          });
        setOnlineAssessmentsCompletedData(completeData);
        navigation.navigate('AssessmentDone');
      },
      (msg) => {
        setMsgSnackbarProps(`Submit failure. ${msg}`);
      }
    );
  };

  const onEdit = (item: Object, index: number) => {
    setAssessmentCurrentPage({
      ...assessmentCurrentPage,
      [assessmentId]: {
        assessmentQuestion: item,
        page: index + 1,
      },
    });
    navigation.navigate('AssessmentEdit');
  };

  const getAllAnswers = (identifiers: string[]) => {
    return identifiers.reduce((acc, val) => {
      return onlineAssessmentsUserAnswers[userLanguage][val]
        ? `${acc}${onlineAssessmentsUserAnswers[userLanguage][val]},`
        : acc;
    }, '');
  };

  const renderItem = (item: Object, index: number) => {
    const {id, identifiers, answerType} = item;
    const VHKey = identifiers ? identifiers[0] : IdentifierQuestionIdMapping[id];
    const answerString =
      identifiers && identifiers.length > 0
        ? getAllAnswers(identifiers)
        : onlineAssessmentsUserAnswers[userLanguage][VHKey];
    return answerString || answerType === CHECKBOXS || answerType === CHIPS ? (
      <TouchableWithoutFeedback onPress={() => onEdit(item, index)}>
        <View
          style={[AppStyles.row, AppStyles.paddingVertical_20, AssessmentFlowStyles.cardContainer]}>
          <View
            style={[
              AppStyles.column,
              AppStyles.paddingLeft_20,
              AssessmentFlowStyles.answerReviewContainer,
            ]}>
            <Text customStyle={[AssessmentFlowStyles.cardItemLabel]}>{item.title}</Text>
            <Text customStyle={[AppStyles.marginTop_5, AssessmentFlowStyles.cardItemAnswer]}>
              {answerString.replace(/,/g, ' ')}
            </Text>
          </View>
          <View style={[AppStyles.paddingRight_20, AppStyles.smallIconContainer]}>
            <VIcon
              size={24}
              color={COLOR_AIA_VITALITY_BLUE}
              name="edit"
              onPress={() => onEdit(item, index)}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <View />
    );
  };

  const renderQuestions = () => {
    const questionKeys = currentQuestionSet ? Object.keys(currentQuestionSet) : [];
    const sortedQuestions = questionKeys.sort((a, b) => {
      const questionA = parseInt(a.replace(/[^0-9]/g, ''), 10);
      const questionB = parseInt(b.replace(/[^0-9]/g, ''), 10);
      return questionA - questionB;
    });
    const questions = sortedQuestions.map((key) => currentQuestionSet[key]);
    return (
      <View
        style={[
          isIOS ? AppStyles.iosBodyContent : AppStyles.androidBodyContent,
          AssessmentFlowStyles.questionAndAnswersContainer,
        ]}>
        <FlatList
          data={questions}
          renderItem={({item, index}) => renderItem(item, index)}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  };

  const handleBack = () => {
    setAssessmentCurrentPage({
      ...assessmentCurrentPage,
      [assessmentId]: {
        assessmentQuestion: lastQuestion,
        page: Object.keys(currentQuestionSet).length,
      },
    });
    navigation.navigate('AssessmentFlow');
  };

  const renderButtonGroup = () => {
    return (
      <View style={[AppStyles.row, AssessmentFlowStyles.buttonContainer]}>
        <Button
          variation="outlined"
          accessibilityLabel="Assessment Review back button"
          onPress={() => handleBack()}
          buttonStyle={[AppStyles.generalButton, AppStyles.generalOutlinedButton]}>
          <VIcon name="arrow-back" size={15} color={COLOR_AIA_RED} />
        </Button>
        <View style={AppStyles.buttonGroupPrimary}>
          <Button
            variation="contained"
            accessibilityLabel="Submit assessment"
            onPress={() => handleSubmitOnlineAssessment()}
            buttonStyle={AppStyles.generalButton}>
            {t('assessment.submit-assessment')}
          </Button>
        </View>
      </View>
    );
  };

  return (
    <Journey
      onPress={() => navigation.navigate('AssessmentDetail')}
      hasProgressBar={false}
      headerContainer={
        <HorizontalHeader
          title={title}
          description={subtitle}
          imageName="manRunning"
          imageGroup="general"
          imageWidth={200}
          imageHeight={200}
        />
      }
      reviewJourney
      questionContainer={renderQuestions()}
      buttonContainer={renderButtonGroup()}
    />
  );
};

const AssessmentReviewWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <AssessmentReview navigation={navigation} />;
};

export default AssessmentReviewWrapper;
