/**
 *
 * Assessment Done
 * @format
 *
 */

// @flow

import React, {useContext, useEffect} from 'react';
import {View, TouchableWithoutFeedback, SafeAreaView} from 'react-native';

import VIcon from 'react-native-vector-icons/MaterialIcons';

import {ApplicationContext} from '@contexts/Application';
import {AssessmentsContext} from '@contexts/Assessments';

import AppStyles from '@styles';
import Icon from '@atoms/Icon';
import Text from '@atoms/Text';
import {COLOR_AIA_GREY_500, COLOR_AIA_GREY_50} from '@colors';

import HeaderFlowStyles from '@organisms/HeaderFlow/styles';
import AssessmentFlowStyles from './styles';
import type {PropsType} from './types';

const AssessmentDone = (props: PropsType) => {
  const {refreshVitalityAge} = useContext(ApplicationContext);
  const {assessmentDone} = useContext(AssessmentsContext);
  const {navigation} = props;
  const {image, title, subtitle} = assessmentDone;

  const handleClose = () => {
    navigation.navigate('Assessments');
  };

  useEffect(() => {
    refreshVitalityAge();
  }, []);

  return (
    <SafeAreaView style={AssessmentFlowStyles.container}>
      <View style={[AppStyles.paddingVertical_20, {backgroundColor: COLOR_AIA_GREY_50}]}>
        <View
          style={[
            AppStyles.row,
            AppStyles.paddingHorizontal_20,
            AssessmentFlowStyles.headerTopSection,
          ]}>
          <TouchableWithoutFeedback onPress={() => handleClose()}>
            <View style={HeaderFlowStyles.closeButton}>
              <VIcon size={24} name="close" color={COLOR_AIA_GREY_500} />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
      <View style={{alignItems: 'center'}}>
        <Icon group="general" name={image} />
        <Text customStyle={AssessmentFlowStyles.doneAssessment}>{title}</Text>
        <Text customStyle={AssessmentFlowStyles.seeWhatYouGot}>{subtitle}</Text>
      </View>
      <View style={AssessmentFlowStyles.doneBoxStyles} />
    </SafeAreaView>
  );
};

const AssessmentDoneWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <AssessmentDone navigation={navigation} />;
};

export default AssessmentDoneWrapper;
