/**
 *
 * Assessment Edit
 * @format
 *
 */

// @flow

import React, {useContext, useState, useEffect} from 'react';
import {View} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialIcons';
import {useTranslation} from 'react-i18next';

import {AssessmentsContext} from '@contexts/Assessments';
import {OnlineAssessmentsContext} from '@contexts/OnlineAssessments';
import {GlobalContext} from '@contexts/Global';

import AppStyles from '@styles';

import Button from '@atoms/Button';
import Checkbox from '@molecules/Checkbox';
import Chip from '@atoms/Chip';
import Input from '@atoms/Input';
import Selection from '@atoms/Selection';
import Toggle from '@molecules/Toggle';
import {RadioOne} from '@molecules/Radio';
import HorizontalHeader from '@organisms/HorizontalHeader';
import VerticalHeader from '@organisms/VerticalHeader';
import Journey from '@layouts/Journey';

import {COLOR_AIA_GREY, COLOR_AIA_RED} from '@colors';
import {ASSESSMENT_DETAIL, ASSESSMENT_QUESTION} from '@core/handler/AEMResponseParser/config';

import type {PropsType} from './types';
import AssessmentFlowStyles from './styles';
import {
  IdentifierQuestionIdMapping,
  ANSWER_TYPE,
  AnswerIdentifierMap,
  QUESTION_KEY_MAP,
} from './config';

const AssessmentEdit = (props: PropsType) => {
  const {i18n} = useTranslation();
  const userLanguage = i18n.language;
  const {navigation} = props;
  const {
    onlineAssessmentsUserData,
    setOnlineAssessmentsUserData,
    onlineAssessmentsUserAnswers,
    setOnlineAssessmentsUserAnswers,
  } = useContext(OnlineAssessmentsContext);
  const {
    assessmentCurrentPage,
    currentAssessment,
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
    setAssessmentCurrentPage,
  } = useContext(AssessmentsContext);
  const {setMsgSnackbarProps} = useContext(GlobalContext);
  const ALL_ASSESSMENTS = [
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
  ];
  const assessmentId = currentAssessment || 'HowHealthyAreYou';
  const assessmentUserData = onlineAssessmentsUserData[assessmentId] || {};
  const assessmentSet = ALL_ASSESSMENTS.find(
    (question) => question[ASSESSMENT_DETAIL] && question[ASSESSMENT_DETAIL].id === assessmentId
  );
  const currentQuestionSet = assessmentSet ? assessmentSet[ASSESSMENT_QUESTION] : {};
  const {assessmentQuestion} = assessmentCurrentPage[assessmentId];
  const [currentAnswer, setCurrentAnswer] = useState({});
  const [currentAnswerForSubmit, setCurrentAnswerForSubmit] = useState({});
  const [showBackButton, setShowBackButton] = useState(false);
  const [containsInput, setContainsInput] = useState(false);
  const {t} = useTranslation();
  const {RADIOS, CHECKBOXS, INPUT, TOGGLE, CHIPS, SELECTION} = ANSWER_TYPE;
  const currentQuestionPrefix =
    Object.keys(currentQuestionSet).length > 0
      ? Object.keys(currentQuestionSet)[0].replace(/[0-9]{1,2}$/, '')
      : QUESTION_KEY_MAP[assessmentId];

  const {
    id,
    answers,
    answerType,
    identifiers,
    title,
    description,
    unit,
    identifiersValue,
    imageName,
    dependency,
  } = assessmentQuestion;

  const isChips = answerType === CHIPS;
  const isRadios = answerType === RADIOS;
  const isCheckbox = answerType === CHECKBOXS;

  const initNewPage = () => {
    let VHKey = null;

    if (identifiers) {
      switch (answerType) {
        case RADIOS: {
          setContainsInput(false);
          [VHKey] = identifiers;
          setCurrentAnswer(onlineAssessmentsUserAnswers[userLanguage][VHKey] || null);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
          break;
        }

        case INPUT: {
          setContainsInput(true);
          const forCurrentAnswer = identifiers.reduce((acc, val) => {
            acc[val] = onlineAssessmentsUserAnswers[userLanguage][val] || '';
            return acc;
          }, {});
          setCurrentAnswer(forCurrentAnswer);
          const forCurrentSubmitAnswer = identifiers.reduce((acc, val) => {
            acc[val] = assessmentUserData[val] || '';
            return acc;
          }, {});
          setCurrentAnswerForSubmit(forCurrentSubmitAnswer);
          break;
        }

        case CHECKBOXS:
        case CHIPS: {
          setContainsInput(false);
          const forCurrentAnswer = identifiers.reduce((acc, val) => {
            acc[val] = onlineAssessmentsUserAnswers[userLanguage][val] || '';
            return acc;
          }, {});
          setCurrentAnswer(forCurrentAnswer);
          const forCurrentSubmitAnswer = identifiers.reduce((acc, val) => {
            acc[val] = assessmentUserData[val] || 'No';
            return acc;
          }, {});
          setCurrentAnswerForSubmit(forCurrentSubmitAnswer);
          break;
        }

        case SELECTION: {
          setContainsInput(false);
          [VHKey] = identifiers;
          setCurrentAnswer(onlineAssessmentsUserAnswers[userLanguage][VHKey] || answers[0]);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || answers[0]);
          break;
        }

        case TOGGLE: {
          setContainsInput(false);
          [VHKey] = identifiers;
          setCurrentAnswer(onlineAssessmentsUserAnswers[userLanguage][VHKey] || 'Yes');
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || 'Yes');
          break;
        }

        default: {
          setContainsInput(false);
          [VHKey] = identifiers;
          setCurrentAnswer(onlineAssessmentsUserAnswers[userLanguage][VHKey] || null);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
          break;
        }
      }
    } else {
      VHKey = IdentifierQuestionIdMapping[id];
      setCurrentAnswer(onlineAssessmentsUserAnswers[userLanguage][VHKey] || null);
      setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
    }
  };

  useEffect(() => {
    initNewPage();
  }, [assessmentCurrentPage]);

  const selectHeader = () => {
    if (isChips || isRadios || isCheckbox) {
      return (
        <HorizontalHeader
          title={title}
          description={description}
          imageName={imageName}
          imageGroup="general"
          imageWidth={200}
          imageHeight={200}
        />
      );
    }
    return (
      <VerticalHeader
        title={title}
        description={description}
        imageName={imageName}
        imageGroup="general"
      />
    );
  };

  const handleRadioPress = (answer) => {
    setCurrentAnswer(answer);
    const indexOfAnswer: number = assessmentQuestion.answers.indexOf(answer);
    const idValuesLength: number = assessmentQuestion.identifiersValue
      ? assessmentQuestion.identifiersValue.length
      : 0;
    if (
      idValuesLength &&
      assessmentQuestion.answers[indexOfAnswer] ===
        assessmentQuestion.identifiersValue[indexOfAnswer]
    ) {
      setCurrentAnswerForSubmit(answer);
    } else {
      const idntfr =
        assessmentQuestion.identifiers && assessmentQuestion.identifiers.length > 0
          ? assessmentQuestion.identifiers[0]
          : 'NoIdentifiersAvailable';
      const idAnswer = AnswerIdentifierMap[`${idntfr} ${answer}`];
      const answerForSubmit =
        idAnswer !== undefined
          ? AnswerIdentifierMap[`${idntfr} ${answer}`]
          : AnswerIdentifierMap[answer];
      setCurrentAnswerForSubmit(answerForSubmit);
    }
  };

  const renderRadiosAnswers = () => {
    return (
      <View style={[AppStyles.column, AssessmentFlowStyles.answersContainer]}>
        <View style={[AppStyles.marginHorizontal_20, AppStyles.marginVertical_20]}>
          {answers.map((answer, index) => {
            const newKey = index.toString();
            return (
              <RadioOne
                key={newKey}
                value={answer}
                selected={currentAnswer === answer}
                onPress={() => handleRadioPress(answer)}
                itemSelectable
              />
            );
          })}
        </View>
      </View>
    );
  };

  const handleCheckBoxPress = (answer, index) => {
    if (!identifiers) {
      setCurrentAnswer(answer);
      setCurrentAnswerForSubmit(answer);
    } else if (identifiers[index]) {
      setCurrentAnswer({
        ...currentAnswer,
        [identifiers[index]]: currentAnswer[identifiers[index]] === '' ? answer : '',
      });
      setCurrentAnswerForSubmit({
        ...currentAnswerForSubmit,
        [identifiers[index]]: currentAnswerForSubmit[identifiers[index]] === 'No' ? 'Yes' : 'No',
      });
    }
  };

  const renderCheckBoxAnswers = () => {
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.column,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        {answers.map((answer, index) => {
          const newKey = index.toString();
          return (
            <Checkbox
              variation={2}
              key={newKey}
              text={answer}
              checked={
                !identifiers
                  ? currentAnswer === answer
                  : currentAnswer[identifiers[index]] === answer
              }
              onPress={() => handleCheckBoxPress(answer, index)}
            />
          );
        })}
      </View>
    ) : (
      <View style={[AppStyles.column, AssessmentFlowStyles.answersContainer]}>
        <View style={[AppStyles.marginHorizontal_20, AppStyles.marginVertical_20]}>
          {answers.map((answer, index) => {
            const newKey = index.toString();
            return (
              <Checkbox
                variation={2}
                key={newKey}
                text={answer}
                checked={currentAnswer === answer}
                onPress={() => setCurrentAnswer(answer)}
              />
            );
          })}
        </View>
      </View>
    );
  };

  const renderInputAnswers = () => {
    const theme = {colors: {primary: COLOR_AIA_GREY}};
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.column,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        <Input
          customStyle={[AssessmentFlowStyles.inputAnswer]}
          theme={theme}
          underlineColor="transparent"
          placeholder={title}
          label={title}
          numbersOnly
          keyboardType="number-pad"
          value={currentAnswer[identifiers[0]]}
          changeText={(text) => {
            setCurrentAnswer({...currentAnswer, [identifiers[0]]: text});
            setCurrentAnswerForSubmit({...currentAnswerForSubmit, [identifiers[0]]: text});
          }}
        />
        {unit === undefined ? (
          <View />
        ) : (
          <View>
            {Object.keys(currentAnswer).length > 1 && unit.length > 1 ? (
              <View style={[AppStyles.row, AppStyles.flex_1]}>
                <View style={[AppStyles.alignItemCenter, AppStyles.paddingVertical_10]}>
                  <Toggle
                    variationNumber={2}
                    buttonLabel1={unit[0] ? unit[0].toString().toLowerCase() : ''}
                    buttonLabel2={unit[1] ? unit[1].toString().toLowerCase() : ''}
                    buttonValue1={identifiersValue[0]}
                    buttonValue2={identifiersValue[1]}
                    selectedValue={currentAnswer[identifiers[1]]}
                    onPress={(value) => {
                      setCurrentAnswer({
                        ...currentAnswer,
                        [identifiers[1]]: value,
                      });
                      setCurrentAnswerForSubmit({
                        ...currentAnswerForSubmit,
                        [identifiers[1]]: value,
                      });
                    }}
                  />
                </View>
              </View>
            ) : (
              <View>
                {Object.keys(currentAnswer).length > 1 && unit.length === 1 && (
                  <View style={[AppStyles.marginTop_20]}>
                    <Input
                      customStyle={[AssessmentFlowStyles.inputAnswer]}
                      theme={theme}
                      underlineColor="transparent"
                      placeholder={identifiers[1]}
                      label={title}
                      numbersOnly
                      keyboardType="number-pad"
                      value={currentAnswer[identifiers[1]]}
                      changeText={(text) => {
                        setCurrentAnswer({
                          ...currentAnswer,
                          [identifiers[1]]: text,
                        });
                        setCurrentAnswerForSubmit({
                          ...currentAnswer,
                          [identifiers[1]]: text,
                        });
                      }}
                    />
                  </View>
                )}
              </View>
            )}
          </View>
        )}
      </View>
    ) : (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AppStyles.marginHorizontal_20,
        ]}>
        <Input
          customStyle={AppStyles.fullWidthInput}
          theme={theme}
          underlineColor="transparent"
          placeholder={title}
          label={title}
          numbersOnly
          keyboardType="number-pad"
          value={currentAnswer}
          changeText={(text) => {
            setCurrentAnswer(text);
            setCurrentAnswerForSubmit(text);
          }}
        />
      </View>
    );
  };

  const handleChipsClick = (answer, index) => {
    if (!identifiers) {
      setCurrentAnswer(answer);
      setCurrentAnswerForSubmit(answer);
    } else if (identifiers[index]) {
      setCurrentAnswer({
        ...currentAnswer,
        [identifiers[index]]: currentAnswer[identifiers[index]] === '' ? answer : '',
      });
      setCurrentAnswerForSubmit({
        ...currentAnswerForSubmit,
        [identifiers[index]]: currentAnswerForSubmit[identifiers[index]] === 'No' ? 'Yes' : 'No',
      });
    }
  };

  const renderChipsAnswers = () => {
    const selectChip = currentAnswer;
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AssessmentFlowStyles.chipsContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        {answers.map((answer, index) => {
          const selectedChipStyle =
            currentAnswer[identifiers[index]] === answer
              ? AssessmentFlowStyles.selectedChip
              : AssessmentFlowStyles.unSelectedChip;
          const selectedChipTextStyle =
            currentAnswer[identifiers[index]] === answer
              ? AssessmentFlowStyles.selectedChipText
              : AssessmentFlowStyles.unSelectedChipText;
          const newKey = index.toString();
          return (
            <View key={newKey} style={AssessmentFlowStyles.chipSpacing}>
              <Chip
                text={answer}
                style={[selectedChipStyle]}
                textStyle={[selectedChipTextStyle]}
                onPress={() => handleChipsClick(answer, index)}
              />
            </View>
          );
        })}
      </View>
    ) : (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AssessmentFlowStyles.chipsContainer,
        ]}>
        <View
          style={[
            AppStyles.row,
            AssessmentFlowStyles.answersContainer,
            AssessmentFlowStyles.chipsContainer,
            AppStyles.paddingHorizontal_20,
            AppStyles.paddingVertical_20,
          ]}>
          {answers.map((answer, index) => {
            const selectedChipStyle =
              selectChip === answer
                ? AssessmentFlowStyles.selectedChip
                : AssessmentFlowStyles.unSelectedChip;
            const selectedChipTextStyle =
              selectChip === answer
                ? AssessmentFlowStyles.selectedChipText
                : AssessmentFlowStyles.unSelectedChipText;
            const newKey = index.toString();
            return (
              <View key={newKey} style={AssessmentFlowStyles.chipSpacing}>
                <Chip
                  text={answer}
                  style={[selectedChipStyle]}
                  textStyle={selectedChipTextStyle}
                  onPress={() => setCurrentAnswer(answer)}
                />
              </View>
            );
          })}
        </View>
      </View>
    );
  };

  const renderToggleAnswers = () => {
    const label1 =
      assessmentQuestion.answers &&
      assessmentQuestion.answers.length > 1 &&
      assessmentQuestion.answers[0];
    const label2 =
      assessmentQuestion.answers &&
      assessmentQuestion.answers.length > 1 &&
      assessmentQuestion.answers[1];
    const labelYes = label1 === 'Yes' || label1 === 'Ya' ? label1 : label2;
    const labelNo = label1 === 'Yes' || label1 === 'Ya' ? label2 : label1;
    return (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingVertical_20,
          AppStyles.paddingLeft_20,
        ]}>
        <Toggle
          variationNumber={1}
          buttonLabel1={labelYes || t('general.yes')}
          buttonLabel2={labelNo || t('general.no')}
          buttonValue1={labelYes || 'Yes'}
          buttonValue2={labelNo || 'No'}
          selectedValue={currentAnswer}
          onPress={(value) => {
            setCurrentAnswer(value);
            setCurrentAnswerForSubmit(
              assessmentQuestion.identifiersValue &&
                assessmentQuestion.identifiersValue[assessmentQuestion.answers.indexOf(value)]
            );
          }}
        />
      </View>
    );
  };

  const renderSelection = () => {
    return typeof currentAnswer === 'string' && !Number.isNaN(parseInt(currentAnswer, 10)) ? (
      <Selection
        answers={answers}
        unit={unit && unit[0]}
        selected={currentAnswer}
        onPress={(newAnswer) => {
          setCurrentAnswer(newAnswer);
          setCurrentAnswerForSubmit(newAnswer);
        }}
      />
    ) : (
      <View />
    );
  };

  const renderQuestion = () => {
    switch (answerType) {
      case RADIOS:
        return renderRadiosAnswers();
      case CHECKBOXS:
        return renderCheckBoxAnswers();
      case INPUT:
        return renderInputAnswers();
      case TOGGLE:
        return renderToggleAnswers();
      case CHIPS:
        return renderChipsAnswers();
      case SELECTION:
        return renderSelection();
      default:
        return renderInputAnswers();
    }
  };

  const saveAnswers = (clearAnswers: Object) => {
    const VHKey =
      identifiers && identifiers.length > 0 ? identifiers[0] : IdentifierQuestionIdMapping[id];
    setOnlineAssessmentsUserData({
      ...onlineAssessmentsUserData,
      [assessmentId]: {
        ...onlineAssessmentsUserData[assessmentId],
        ...(typeof currentAnswerForSubmit === 'object'
          ? currentAnswerForSubmit
          : {[`${VHKey}`]: currentAnswerForSubmit}),
        ...clearAnswers,
      },
    });
    setOnlineAssessmentsUserAnswers({
      ...onlineAssessmentsUserAnswers,
      [userLanguage]: {
        ...onlineAssessmentsUserAnswers[userLanguage],
        ...(typeof currentAnswer === 'object' ? currentAnswer : {[`${VHKey}`]: currentAnswer}),
        ...clearAnswers,
      },
    });
  };

  const onSave = () => {
    const idNumber = id.replace(/[^0-9]/g, '');
    const nextDependencyIds = Object.keys(currentQuestionSet)
      .filter((question) => {
        const currentItem = currentQuestionSet[question];
        if (!dependency) {
          return (
            currentItem.dependency &&
            currentItem.dependency === idNumber &&
            currentItem.dependencyTriggerValue[0].trim() === currentAnswer.trim()
          );
        }
        const currentItemNumber = currentItem.id.replace(/[^0-9]/g, '');
        return (
          currentItem.dependency === dependency &&
          parseInt(idNumber, 10) < parseInt(currentItemNumber, 10)
        );
      })
      .sort((a, b) => {
        const questionA = parseInt(a.replace(/[^0-9]/g, ''), 10);
        const questionB = parseInt(b.replace(/[^0-9]/g, ''), 10);
        return questionA - questionB;
      });
    const nextDependencyQuestions = nextDependencyIds.map((depId) => currentQuestionSet[depId]);
    const idsForDeletion = Object.keys(currentQuestionSet).filter((question) => {
      const currentItem = currentQuestionSet[question];
      return currentItem.dependency && currentItem.dependency === id.replace(/[^0-9]/g, '');
    });
    const questionsForDeletion = idsForDeletion.map((depId) => currentQuestionSet[depId]);
    const nextQualifiedQuestion = nextDependencyQuestions.find((depQ) => {
      const depQIdNumber = depQ.id.replace(/[^0-9]/g, '');
      const compareAnswer =
        parseInt(depQIdNumber - depQ.dependency, 10) === 1 || idNumber === depQ.dependency
          ? currentAnswer
          : onlineAssessmentsUserAnswers[userLanguage][
              currentQuestionSet[`${currentQuestionPrefix}${depQ.dependency}`].identifiers[0]
            ];
      const dependencyTrigger = depQ.dependencyTriggerValue && depQ.dependencyTriggerValue[0];
      return (
        dependencyTrigger && compareAnswer && dependencyTrigger.trim() === compareAnswer.trim()
      );
    });
    const clearAnswers = questionsForDeletion.reduce((acc, val) => {
      val.identifiers.forEach((idntfr) => {
        acc[idntfr] = '';
      });
      return acc;
    }, {});

    if (nextQualifiedQuestion) {
      saveAnswers(clearAnswers);
      setAssessmentCurrentPage({
        ...assessmentCurrentPage,
        [assessmentId]: {
          assessmentQuestion: nextQualifiedQuestion,
          page: nextQualifiedQuestion.id.replace(/[^0-9]/g, ''),
        },
      });
      navigation.navigate('AssessmentEdit');
    } else {
      saveAnswers(clearAnswers);
      navigation.navigate('AssessmentReview');
    }
  };

  const handleBackButton = () => {
    setShowBackButton(false);
    const previousPageNumber = id && parseInt(id.replace(/[^0-9]/g, ''), 10) - 1;
    const previousQuestion =
      previousPageNumber &&
      currentQuestionSet[`${id.replace(/[0-9]{1,}/g, previousPageNumber.toString())}`];
    setAssessmentCurrentPage({
      ...assessmentCurrentPage,
      [assessmentId]: {
        assessmentQuestion: previousQuestion,
        page: previousPageNumber,
      },
    });
  };

  const buttonDisabled = () => {
    if (typeof currentAnswer === 'object' && currentAnswer !== null) {
      if (answerType === INPUT) {
        const emptyIdentifier = Object.keys(currentAnswer).find(
          (curAnsId) => currentAnswer[curAnsId] === ''
        );
        return !!emptyIdentifier;
      }
      return false;
    }
    return !currentAnswer;
  };

  const renderButtonGroup = () => {
    return (
      <View style={[AppStyles.row, AssessmentFlowStyles.buttonContainerSimple]}>
        <View style={[{display: showBackButton ? 'flex' : 'none'}]}>
          <Button
            variation="outlined"
            accessibilityLabel="Assessment Flow back button"
            onPress={() => handleBackButton()}
            buttonStyle={[AppStyles.generalButton, AppStyles.generalOutlinedButton]}>
            <VIcon name="arrow-back" size={15} color={COLOR_AIA_RED} />
          </Button>
        </View>
        <View style={[!showBackButton ? AppStyles.fullWidthButton : AppStyles.buttonGroupPrimary]}>
          <Button
            variation="contained"
            accessibilityLabel="Save Edited Answer"
            onPress={() => onSave()}
            disabled={buttonDisabled()}
            buttonStyle={AppStyles.generalButton}>
            {t('general.save-edit')}
          </Button>
        </View>
      </View>
    );
  };

  return (
    <Journey
      onPress={() =>
        dependency && buttonDisabled()
          ? setMsgSnackbarProps('Dependent question cannot be closed.')
          : navigation.navigate('AssessmentReview')
      }
      hasProgressBar={false}
      headerContainer={selectHeader()}
      flowJourney
      questionContainer={renderQuestion()}
      buttonContainer={renderButtonGroup()}
      containsInput={containsInput}
    />
  );
};

export default AssessmentEdit;
