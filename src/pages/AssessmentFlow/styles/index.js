/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import Color from 'color';
import {RFValue} from 'react-native-responsive-fontsize';

import {projectFontFamily} from '@core/config';
import {DEVICE_WIDTH} from '@molecules/Camera/config';

import {
  BLACK,
  WHITE,
  COLOR_AIA_GREY,
  COLOR_AIA_GREY_500,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREY_50,
  COLOR_AIA_BLUE,
} from '@colors';

const AssessmentFlowStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  editableItem: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#C1D5E4',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  answersContainer: {
    marginTop: 10,
    marginBottom: 100,
    backgroundColor: WHITE,
  },
  cameraContainer: {
    backgroundColor: WHITE,
    marginTop: -165,
    height: DEVICE_WIDTH,
  },
  // Assessment Detail
  infoContainer: {
    backgroundColor: COLOR_AIA_GREY_50,
  },
  titleText: {
    fontSize: RFValue(18),
    color: COLOR_AIA_DARK_GREY,
    fontWeight: '700',
    fontFamily: projectFontFamily,
  },
  descriptionText: {
    fontSize: RFValue(14),
    fontFamily: projectFontFamily,
    color: COLOR_AIA_GREY_500,
  },
  chipsContainer: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 1,
    backgroundColor: WHITE,
    padding: 20,
    shadowColor: BLACK,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 1,
      width: 0,
    },
    width: '100%',
    justifyContent: 'space-between',
  },
  buttonContainerSimple: {
    position: 'absolute',
    bottom: 1,
    padding: 20,
    width: '100%',
    justifyContent: 'space-between',
  },
  cardContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: WHITE,
    borderBottomWidth: 0.3,
    borderBottomColor: COLOR_AIA_GREY,
  },
  cardItemLabel: {
    fontSize: RFValue(14),
    color: COLOR_AIA_DARK_GREY,
    fontFamily: projectFontFamily,
  },
  cardItemAnswer: {
    flexWrap: 'wrap',
    fontSize: RFValue(14),
    color: COLOR_AIA_DARK_GREY,
    fontFamily: projectFontFamily,
  },
  questionAndAnswersContainer: {
    marginBottom: 100,
  },
  selectedChip: {
    backgroundColor: Color(COLOR_AIA_BLUE).alpha(0.5).lighten(0.5),
  },
  unSelectedChip: {
    backgroundColor: WHITE,
  },
  selectedChipText: {
    color: WHITE,
  },
  unSelectedChipText: {
    color: COLOR_AIA_DARK_GREY,
  },
  // Assessment Questions Styles
  chipSpacing: {
    padding: 5,
  },
  headerTopSection: {
    justifyContent: 'space-between',
    height: 50,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  // Assessment Review Page
  questionReviewHeader: {
    width: '60%',
  },
  imageContainer: {
    height: 80,
    top: -80,
  },
  answerReviewContainer: {
    width: '85%',
  },
  // Assessment Done
  doneBoxStyles: {
    marginHorizontal: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 100,
    width: '90%',
    paddingTop: 20,
    marginTop: 40,
    shadowColor: '#000',
    shadowOffset: {
      width: 1,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 0.5,
    elevation: 5,
  },
  unitsStyle: {
    flexDirection: 'column',
    flex: 1,
  },
  toggleYesNo: {paddingRight: 20, flexDirection: 'column', flex: 1},
  inputAnswer: {
    width: '100%',
  },
  doneAssessment: {
    fontSize: RFValue(24),
    fontFamily: projectFontFamily,
    fontWeight: '600',
    marginTop: 40,
  },
  seeWhatYouGot: {
    fontSize: RFValue(16),
    fontFamily: projectFontFamily,
    fontWeight: 'normal',
  },
});

export default AssessmentFlowStyles;
