/**
 *
 * Assessment Flow
 * @format
 *
 */

// @flow

import React, {useContext, useState, useEffect, useRef} from 'react';
import {View, Alert} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {Dynatrace} from '@dynatrace/react-native-plugin';

import VIcon from 'react-native-vector-icons/MaterialIcons';
import {useTranslation} from 'react-i18next';

import {AssessmentsContext} from '@contexts/Assessments';
import {OnlineAssessmentsContext} from '@contexts/OnlineAssessments';

import AppStyles from '@styles';

import Button from '@atoms/Button';
import Checkbox from '@molecules/Checkbox';
import Chip from '@atoms/Chip';
import Input from '@atoms/Input';
import Selection from '@atoms/Selection';
import Toggle from '@molecules/Toggle';
import {RadioOne} from '@molecules/Radio';
import Camera from '@molecules/Camera';

import {COLOR_AIA_GREY, COLOR_AIA_RED} from '@colors';
import {ASSESSMENT_DETAIL, ASSESSMENT_QUESTION} from '@core/handler/AEMResponseParser/config';

import Journey from '@layouts/Journey';
import HorizontalHeader from '@organisms/HorizontalHeader';
import VerticalHeader from '@organisms/VerticalHeader';

import type {PropsType} from './types';
import AssessmentFlowStyles from './styles';

import {
  IdentifierQuestionIdMapping,
  ANSWER_TYPE,
  QUESTION_KEY_MAP,
  AnswerIdentifierMap,
} from './config';

const AssessmentFlow = (props: PropsType) => {
  const {i18n} = useTranslation();
  const userLanguage = i18n.language;

  const route = useRoute();
  const {params} = route;
  const {navigation} = props;
  const {
    onlineAssessmentsUserData,
    setOnlineAssessmentsUserData,
    onlineAssessmentsUserAnswers,
    setOnlineAssessmentsUserAnswers,
    completedOnlineAssessments,
    answeredOnlineAssessments,
  } = useContext(OnlineAssessmentsContext);

  const {
    currentAssessment,
    assessmentCurrentPage,
    setAssessmentCurrentPage,
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
  } = useContext(AssessmentsContext);

  const ALL_ASSESSMENTS = [
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
  ];

  const assessmentId = currentAssessment;
  const assessmentSet = ALL_ASSESSMENTS.find(
    (question) => question[ASSESSMENT_DETAIL] && question[ASSESSMENT_DETAIL].id === assessmentId
  );
  const currentQuestionSet = assessmentSet ? assessmentSet[ASSESSMENT_QUESTION] : {};
  const currentQuestionPrefix =
    Object.keys(currentQuestionSet).length > 0
      ? Object.keys(currentQuestionSet)[0].replace(/[0-9]{1,2}$/, '')
      : QUESTION_KEY_MAP[assessmentId];
  const currentAssessmentState = assessmentCurrentPage[assessmentId];
  const assessmentQuestion = currentAssessmentState
    ? currentAssessmentState.assessmentQuestion
    : undefined;
  const page = currentAssessmentState ? currentAssessmentState.page : undefined;
  const [currentPage, setCurrentPage] = useState(page || 1);
  const [currentQuestion, setCurrentQuestion] = useState(
    assessmentQuestion || currentQuestionSet[`${currentQuestionPrefix}${currentPage}`]
  );
  const [currentAnswer, setCurrentAnswer] = useState({});
  const [currentAnswerForSubmit, setCurrentAnswerForSubmit] = useState({});
  const [currentProgress, setCurrentProgress] = useState(0);
  const [dependencyMap, setDependencyMap] = useState(
    Object.keys(currentQuestionSet || {}).reduce((acc, val) => {
      if (currentQuestionSet[val].dependency) {
        acc[`${currentQuestionPrefix}${currentQuestionSet[val].dependency}`] = '';
      }
      return acc;
    }, {})
  );
  const [clearIdentifiers, setClearIdentifiers] = useState({});
  const [containsInput, setContainsInput] = useState(false);
  const {t} = useTranslation();
  const [isCameraReady, setIsCameraReady] = useState(false);
  const [isTakingPic, setIsTakingPic] = useState(false);

  const {RADIOS, CHECKBOXS, INPUT, TOGGLE, CHIPS, SELECTION, CAMERAVIEW} = ANSWER_TYPE;
  const {
    id,
    answers,
    answerType,
    identifiers,
    title,
    description,
    unit,
    identifiersValue,
    imageName,
    imageGroup,
  } = currentQuestion || {};
  const isChips = answerType === CHIPS;
  const isRadios = answerType === RADIOS;
  const isCheckbox = answerType === CHECKBOXS;
  const refCamera = useRef(null);

  const handleBackButtonInstrumentation = () => {
    const assessmentBackButtonAction = Dynatrace.enterAction('Assessment Back');
    assessmentBackButtonAction.reportStringValue('Tap Assessment Back');
    assessmentBackButtonAction.leaveAction();
  };

  const capturePhoto = async () => {
    if (refCamera) {
      if (isTakingPic || !isCameraReady) {
        return;
      }

      // if we have a non original quality, skip processing and compression.
      // we will use JPEG compression on resize.
      const options = {
        quality: 0.85,
        fixOrientation: true,
        forceUpOrientation: true,
        writeExif: true,
      };
      setIsTakingPic(true);
      let data = null;
      try {
        if (refCamera.current && refCamera.current.takePictureAsync) {
          data = await refCamera.current.takePictureAsync(options);
        }
      } catch (err) {
        Alert.alert('Error', `Failed to take picture: ${err.message || err}`);
        return;
      }
      Alert.alert('Picture Taken!', JSON.stringify(data, null, 2));
    }
  };

  const handlePrevious = () => {
    if (currentPage > 1) {
      let tempPage = currentPage - 1;
      let question = currentQuestionSet[`${currentQuestionPrefix}${tempPage}`];
      let proceed: boolean = true;

      while (
        question.dependency &&
        question.identifiers &&
        onlineAssessmentsUserAnswers[userLanguage] &&
        question.dependencyTriggerValue &&
        proceed
      ) {
        const prevAns =
          onlineAssessmentsUserAnswers[userLanguage][
            currentQuestionSet[`${currentQuestionPrefix}${question.dependency}`].identifiers[0]
          ];
        const dpdcAns = question.dependencyTriggerValue[0];
        if (prevAns && dpdcAns && prevAns.trim() !== dpdcAns.trim()) {
          tempPage -= 1;
          question = currentQuestionSet[`${currentQuestionPrefix}${tempPage}`];
        } else {
          proceed = false;
        }
      }

      setCurrentQuestion(question);
      setCurrentPage(tempPage);
      setAssessmentCurrentPage({
        ...assessmentCurrentPage,
        [assessmentId]: {
          assessmentQuestion: question,
          page: tempPage,
        },
      });

      handleBackButtonInstrumentation();
    } else {
      navigation.navigate('AssessmentDetail');
      handleBackButtonInstrumentation();
    }
  };

  const handleContinue = () => {
    const VHKey =
      identifiers && identifiers.length > 0 ? identifiers[0] : IdentifierQuestionIdMapping[id];

    setOnlineAssessmentsUserData({
      ...onlineAssessmentsUserData,
      [assessmentId]: {
        ...onlineAssessmentsUserData[assessmentId],
        ...(typeof currentAnswerForSubmit === 'object'
          ? currentAnswerForSubmit
          : {[`${VHKey}`]: currentAnswerForSubmit}),
        ...clearIdentifiers,
      },
    });
    setOnlineAssessmentsUserAnswers({
      ...onlineAssessmentsUserAnswers,
      [userLanguage]: {
        ...onlineAssessmentsUserAnswers[userLanguage],
        ...(typeof currentAnswer === 'object' ? currentAnswer : {[`${VHKey}`]: currentAnswer}),
        ...clearIdentifiers,
      },
    });

    setClearIdentifiers({});

    const depMap = {...dependencyMap};
    if (Object.keys(depMap || {}).indexOf(id) !== -1) {
      depMap[id] = currentAnswer;
      setDependencyMap({...dependencyMap, [id]: currentAnswer});
    }

    const questionsLength = Object.keys(currentQuestionSet).length;
    if (currentPage < questionsLength) {
      let tempPage = currentPage + 1;
      let question = currentQuestionSet[`${currentQuestionPrefix}${tempPage}`];
      let proceed: boolean = true;
      let dependencyNumber = null;
      let questionNumber = null;
      let prevAns;
      const delIdentifiers = [];

      while (
        question.dependency &&
        question.identifiers &&
        question.dependencyTriggerValue &&
        proceed
      ) {
        dependencyNumber = parseInt(question.dependency, 10);
        questionNumber = question.id && parseInt(question.id.replace(/[^0-9]/g, ''), 10) - 1;
        if (dependencyNumber !== questionNumber) {
          prevAns = depMap[`${currentQuestionPrefix}${question.dependency}`];
        } else {
          prevAns = currentAnswer.toString();
        }
        const dpdcAns = question.dependencyTriggerValue[0];
        if (prevAns && dpdcAns && prevAns.trim() !== dpdcAns.trim()) {
          question.identifiers.forEach((idntfr) => delIdentifiers.push(idntfr));
          tempPage += 1;
          question = currentQuestionSet[`${currentQuestionPrefix}${tempPage}`];
        } else {
          proceed = false;
        }
      }

      setCurrentQuestion(question);
      setCurrentPage(tempPage);
      setAssessmentCurrentPage({
        ...assessmentCurrentPage,
        [assessmentId]: {
          assessmentQuestion: question,
          page: tempPage,
        },
      });

      setClearIdentifiers(
        delIdentifiers.reduce((acc, val) => {
          acc[val] = '';
          return acc;
        }, {})
      );

      const assessmentContinueButtonAction = Dynatrace.enterAction('Assessment Continue');
      assessmentContinueButtonAction.reportStringValue('Tap Assessment Continue');
      assessmentContinueButtonAction.leaveAction();
    } else if (identifiers.includes('UploadSelection') || identifiers.includes('CameraView')) {
      // This condition will be removed once the offline assessment component will be in place
      capturePhoto();
    } else {
      navigation.navigate('AssessmentReview');
    }
  };

  /* eslint-disable complexity */
  const initNewPage = () => {
    const assessmentUserData = onlineAssessmentsUserData[assessmentId] || {};
    setCurrentQuestion(
      assessmentQuestion || currentQuestionSet[`${currentQuestionPrefix}${currentPage}`]
    );
    let VHKey = null;

    const questionNumber = id && id.replace(/[^0-9]{1,}/g, '');
    if (questionNumber && currentQuestionSet) {
      if (
        completedOnlineAssessments &&
        assessmentId &&
        (completedOnlineAssessments.indexOf(assessmentId) !== -1 ||
          answeredOnlineAssessments.indexOf(assessmentId) !== -1)
      ) {
        setCurrentProgress(100);
      } else {
        const questionsLength = Object.keys(currentQuestionSet || {}).length || 1;
        const progessPercentage = Math.ceil(((questionNumber - 1) / questionsLength) * 100);
        if (currentProgress < progessPercentage) {
          setCurrentProgress(progessPercentage);
        }
      }
    }

    if (identifiers) {
      switch (answerType) {
        case RADIOS: {
          setContainsInput(false);
          [VHKey] = identifiers;
          const assessmentUserAnswer =
            onlineAssessmentsUserAnswers[userLanguage] &&
            onlineAssessmentsUserAnswers[userLanguage][VHKey];
          setCurrentAnswer(assessmentUserAnswer || null);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
          break;
        }

        case INPUT: {
          setContainsInput(true);
          const forCurrentAnswer = identifiers.reduce((acc, val) => {
            const userAnswerVal =
              onlineAssessmentsUserAnswers[userLanguage] &&
              onlineAssessmentsUserAnswers[userLanguage][val];
            acc[val] = userAnswerVal || '';
            return acc;
          }, {});
          setCurrentAnswer(forCurrentAnswer);
          const forCurrentSubmitAnswer = identifiers.reduce((acc, val) => {
            acc[val] = assessmentUserData[val] || '';
            return acc;
          }, {});
          setCurrentAnswerForSubmit(forCurrentSubmitAnswer);
          break;
        }

        case CHECKBOXS:
        case CHIPS: {
          setContainsInput(false);
          const forCurrentAnswer = identifiers.reduce((acc, val) => {
            const userChipsVal =
              onlineAssessmentsUserAnswers[userLanguage] &&
              onlineAssessmentsUserAnswers[userLanguage][val];
            acc[val] = userChipsVal || '';
            return acc;
          }, {});
          setCurrentAnswer(forCurrentAnswer);
          const forCurrentSubmitAnswer = identifiers.reduce((acc, val) => {
            acc[val] = assessmentUserData[val] || 'No';
            return acc;
          }, {});
          setCurrentAnswerForSubmit(forCurrentSubmitAnswer);
          break;
        }

        case SELECTION: {
          setContainsInput(false);
          [VHKey] = identifiers;
          const userSelKey =
            onlineAssessmentsUserAnswers[userLanguage] &&
            onlineAssessmentsUserAnswers[userLanguage][VHKey];
          setCurrentAnswer(userSelKey || answers[Math.floor(answers.length / 2)]);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || answers[0]);
          break;
        }

        case TOGGLE: {
          setContainsInput(false);
          [VHKey] = identifiers;
          const userToggleKey =
            onlineAssessmentsUserAnswers[userLanguage] &&
            onlineAssessmentsUserAnswers[userLanguage][VHKey];
          setCurrentAnswer(userToggleKey || 'Yes');
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || 'Yes');
          break;
        }

        // This will be refactored once the offline assessment component will be in place
        case CAMERAVIEW: {
          [VHKey] = identifiers;
          const userCameraViewKey =
            onlineAssessmentsUserAnswers[userLanguage] &&
            onlineAssessmentsUserAnswers[userLanguage][VHKey];
          setCurrentAnswer(userCameraViewKey || 'Yes');
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || 'Yes');
          break;
        }

        default: {
          setContainsInput(false);
          [VHKey] = identifiers;
          const defaultUserKey =
            onlineAssessmentsUserAnswers[userLanguage] &&
            onlineAssessmentsUserAnswers[userLanguage][VHKey];
          setCurrentAnswer(defaultUserKey || null);
          setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
          break;
        }
      }
    } else {
      VHKey = IdentifierQuestionIdMapping[id];
      const userAnswerLang =
        onlineAssessmentsUserAnswers[userLanguage] &&
        onlineAssessmentsUserAnswers[userLanguage][VHKey];
      setCurrentAnswer(userAnswerLang || null);
      setCurrentAnswerForSubmit(assessmentUserData[VHKey] || null);
    }
  };

  useEffect(() => {
    initNewPage();
  }, [assessmentCurrentPage]);

  const selectHeader = () => {
    if (isChips || isRadios || isCheckbox) {
      return (
        <HorizontalHeader
          title={title}
          description={description}
          imageName={imageName}
          imageGroup={imageGroup}
          imageWidth={200}
          imageHeight={200}
        />
      );
    }
    return (
      <VerticalHeader
        title={title}
        description={description}
        imageName={imageName}
        imageGroup={imageGroup}
      />
    );
  };

  const handleRadioPress = (answer) => {
    setCurrentAnswer(answer);
    const indexOfAnswer: number = currentQuestion.answers.indexOf(answer);
    const idValuesLength: number = currentQuestion.identifiersValue
      ? currentQuestion.identifiersValue.length
      : 0;
    if (
      idValuesLength &&
      currentQuestion.answers[indexOfAnswer] === currentQuestion.identifiersValue[indexOfAnswer]
    ) {
      setCurrentAnswerForSubmit(answer);
    } else {
      const idntfr =
        currentQuestion.identifiers && currentQuestion.identifiers.length > 0
          ? currentQuestion.identifiers[0]
          : 'NoIdentifiersAvailable';
      const idAnswer = AnswerIdentifierMap[`${idntfr} ${answer}`];
      const answerForSubmit =
        idAnswer !== undefined
          ? AnswerIdentifierMap[`${idntfr} ${answer}`]
          : AnswerIdentifierMap[answer];
      setCurrentAnswerForSubmit(answerForSubmit);
    }
  };

  const renderRadiosAnswers = () => {
    return (
      <View style={[AppStyles.column, AssessmentFlowStyles.answersContainer]}>
        <View style={[AppStyles.marginHorizontal_20, AppStyles.marginVertical_20]}>
          {answers.map((answer, index) => {
            const newKey = index.toString();
            return (
              <RadioOne
                key={newKey}
                value={answer}
                selected={currentAnswer === answer}
                onPress={() => handleRadioPress(answer)}
                itemSelectable
              />
            );
          })}
        </View>
      </View>
    );
  };

  const handleCheckBoxPress = (answer, index) => {
    if (!identifiers) {
      setCurrentAnswer(answer);
      setCurrentAnswerForSubmit(answer);
    } else if (identifiers[index]) {
      setCurrentAnswer({
        ...currentAnswer,
        [identifiers[index]]: currentAnswer[identifiers[index]] === '' ? answer : '',
      });
      setCurrentAnswerForSubmit({
        ...currentAnswerForSubmit,
        [identifiers[index]]: currentAnswerForSubmit[identifiers[index]] === 'No' ? 'Yes' : 'No',
      });
    }
  };

  const renderCheckBoxAnswers = () => {
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.column,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        {answers.map((answer, index) => {
          const newKey = index.toString();
          return (
            <Checkbox
              variation={2}
              key={newKey}
              text={answer}
              checked={
                !identifiers
                  ? currentAnswer === answer
                  : currentAnswer[identifiers[index]] === answer
              }
              onPress={() => handleCheckBoxPress(answer, index)}
            />
          );
        })}
      </View>
    ) : (
      <View style={[AppStyles.column, AssessmentFlowStyles.answersContainer]}>
        <View style={[AppStyles.marginHorizontal_20, AppStyles.marginVertical_20]}>
          {answers.map((answer, index) => {
            const newKey = index.toString();
            return (
              <Checkbox
                variation={2}
                key={newKey}
                text={answer}
                checked={currentAnswer === answer}
                onPress={() => setCurrentAnswer(answer)}
              />
            );
          })}
        </View>
      </View>
    );
  };

  const renderInputAnswers = () => {
    const theme = {colors: {primary: COLOR_AIA_GREY}};
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.column,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        <Input
          customStyle={[AssessmentFlowStyles.inputAnswer]}
          theme={theme}
          underlineColor="transparent"
          placeholder={title}
          label={title}
          numbersOnly
          keyboardType="number-pad"
          value={currentAnswer[identifiers[0]]}
          changeText={(text) => {
            setCurrentAnswer({...currentAnswer, [identifiers[0]]: text});
            setCurrentAnswerForSubmit({...currentAnswerForSubmit, [identifiers[0]]: text});
          }}
        />
        {unit === undefined ? (
          <View />
        ) : (
          <View>
            {Object.keys(currentAnswer).length > 1 && unit.length > 1 ? (
              <View style={[AppStyles.row, AppStyles.flex_1]}>
                <View style={[AppStyles.alignItemCenter, AppStyles.paddingVertical_10]}>
                  <Toggle
                    variationNumber={2}
                    buttonLabel1={unit[0] ? unit[0].toString().toLowerCase() : ''}
                    buttonLabel2={unit[1] ? unit[1].toString().toLowerCase() : ''}
                    buttonValue1={identifiersValue[0]}
                    buttonValue2={identifiersValue[1]}
                    selectedValue={currentAnswer[identifiers[1]] || identifiersValue[0]}
                    onPress={(value) => {
                      setCurrentAnswer({
                        ...currentAnswer,
                        [identifiers[1]]: value,
                      });
                      setCurrentAnswerForSubmit({
                        ...currentAnswerForSubmit,
                        [identifiers[1]]: value,
                      });
                    }}
                  />
                </View>
              </View>
            ) : (
              <View>
                {Object.keys(currentAnswer).length > 1 && unit.length === 1 && (
                  <View style={[AppStyles.marginTop_20]}>
                    <Input
                      customStyle={[AssessmentFlowStyles.inputAnswer]}
                      theme={theme}
                      underlineColor="transparent"
                      placeholder={identifiers[1]}
                      label={title}
                      numbersOnly
                      keyboardType="number-pad"
                      value={currentAnswer[identifiers[1]]}
                      changeText={(text) => {
                        setCurrentAnswer({
                          ...currentAnswer,
                          [identifiers[1]]: text,
                        });
                        setCurrentAnswerForSubmit({
                          ...currentAnswer,
                          [identifiers[1]]: text,
                        });
                      }}
                    />
                  </View>
                )}
              </View>
            )}
          </View>
        )}
      </View>
    ) : (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AppStyles.marginHorizontal_20,
        ]}>
        <Input
          customStyle={AppStyles.fullWidthInput}
          theme={theme}
          underlineColor="transparent"
          placeholder={title}
          label={title}
          numbersOnly
          keyboardType="number-pad"
          value={currentAnswer}
          changeText={(text) => {
            setCurrentAnswer(text);
            setCurrentAnswerForSubmit(text);
          }}
        />
      </View>
    );
  };

  const handleChipsClick = (answer, index) => {
    if (!identifiers) {
      setCurrentAnswer(answer);
      setCurrentAnswerForSubmit(answer);
    } else if (identifiers[index]) {
      setCurrentAnswer({
        ...currentAnswer,
        [identifiers[index]]: currentAnswer[identifiers[index]] === '' ? answer : '',
      });
      setCurrentAnswerForSubmit({
        ...currentAnswerForSubmit,
        [identifiers[index]]: currentAnswerForSubmit[identifiers[index]] === 'No' ? 'Yes' : 'No',
      });
    }
  };

  const renderChipsAnswers = () => {
    const selectChip = currentAnswer;
    return currentAnswer !== null && typeof currentAnswer !== 'string' ? (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AssessmentFlowStyles.chipsContainer,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        {answers.map((answer, index) => {
          const selectedChipStyle =
            currentAnswer[identifiers[index]] === answer
              ? AssessmentFlowStyles.selectedChip
              : AssessmentFlowStyles.unSelectedChip;
          const selectedChipTextStyle =
            currentAnswer[identifiers[index]] === answer
              ? AssessmentFlowStyles.selectedChipText
              : AssessmentFlowStyles.unSelectedChipText;
          const newKey = index.toString();
          return (
            <View key={newKey} style={AssessmentFlowStyles.chipSpacing}>
              <Chip
                text={answer}
                style={[selectedChipStyle]}
                textStyle={[selectedChipTextStyle]}
                onPress={() => handleChipsClick(answer, index)}
              />
            </View>
          );
        })}
      </View>
    ) : (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AssessmentFlowStyles.chipsContainer,
        ]}>
        <View
          style={[
            AppStyles.row,
            AssessmentFlowStyles.answersContainer,
            AssessmentFlowStyles.chipsContainer,
            AppStyles.paddingHorizontal_20,
            AppStyles.paddingVertical_20,
          ]}>
          {answers.map((answer, index) => {
            const selectedChipStyle =
              selectChip === answer
                ? AssessmentFlowStyles.selectedChip
                : AssessmentFlowStyles.unSelectedChip;
            const selectedChipTextStyle =
              selectChip === answer
                ? AssessmentFlowStyles.selectedChipText
                : AssessmentFlowStyles.unSelectedChipText;
            const newKey = index.toString();
            return (
              <View key={newKey} style={AssessmentFlowStyles.chipSpacing}>
                <Chip
                  text={answer}
                  style={[selectedChipStyle]}
                  textStyle={selectedChipTextStyle}
                  onPress={() => setCurrentAnswer(answer)}
                />
              </View>
            );
          })}
        </View>
      </View>
    );
  };

  const renderToggleAnswers = () => {
    const label1 =
      currentQuestion.answers && currentQuestion.answers.length > 1 && currentQuestion.answers[0];
    const label2 =
      currentQuestion.answers && currentQuestion.answers.length > 1 && currentQuestion.answers[1];
    const labelYes = label1 === 'Yes' || label1 === 'Ya' ? label1 : label2;
    const labelNo = label1 === 'Yes' || label1 === 'Ya' ? label2 : label1;
    return (
      <View
        style={[
          AppStyles.row,
          AssessmentFlowStyles.answersContainer,
          AppStyles.paddingVertical_20,
          AppStyles.paddingLeft_20,
        ]}>
        <Toggle
          variationNumber={1}
          buttonLabel1={labelYes || t('general.yes')}
          buttonLabel2={labelNo || t('general.no')}
          buttonValue1={labelYes || 'Yes'}
          buttonValue2={labelNo || 'No'}
          selectedValue={currentAnswer}
          onPress={(value) => {
            setCurrentAnswer(value);
            setCurrentAnswerForSubmit(
              currentQuestion.identifiersValue &&
                currentQuestion.identifiersValue[currentQuestion.answers.indexOf(value)]
            );
          }}
        />
      </View>
    );
  };

  const renderSelection = () => {
    return typeof currentAnswer === 'string' && !Number.isNaN(parseInt(currentAnswer, 10)) ? (
      <Selection
        answers={answers}
        unit={unit && unit[0]}
        selected={currentAnswer}
        onPress={(newAnswer) => {
          setCurrentAnswer(newAnswer);
          setCurrentAnswerForSubmit(newAnswer);
        }}
      />
    ) : (
      <View />
    );
  };

  const onPictureTaken = () => {
    setIsTakingPic(false);
  };

  const onCameraReady = () => {
    setIsCameraReady(true);
  };

  const renderCameraPreview = () => {
    return (
      <View
        style={[AppStyles.row, AssessmentFlowStyles.cameraContainer, AppStyles.paddingVertical_5]}>
        <Camera
          refView={refCamera}
          onPictureTaken={onPictureTaken}
          onCameraReady={onCameraReady}
          unauthorizedMessage={`${'We need your permission to use your camera.'}`}
        />
      </View>
    );
  };

  const renderQuestion = () => {
    switch (answerType) {
      case RADIOS:
        return renderRadiosAnswers();
      case CHECKBOXS:
        return renderCheckBoxAnswers();
      case INPUT:
        return renderInputAnswers();
      case TOGGLE:
        return renderToggleAnswers();
      case CHIPS:
        return renderChipsAnswers();
      case SELECTION:
        return renderSelection();
      case CAMERAVIEW:
        return renderCameraPreview();
      default:
        return renderInputAnswers();
    }
  };

  const getButtonText = () => {
    if (params) {
      return params.flow === 'Flow' ? 'Continue' : 'Submit';
    }
    /* This will be refactored once the offline assessment component will be in place */
    if (identifiers.includes('CameraView')) {
      return 'Take Photo';
    }
    return 'Continue';
  };

  const buttonDisabled = () => {
    if (typeof currentAnswer === 'object' && currentAnswer !== null) {
      if (answerType === INPUT) {
        const emptyIdentifier = Object.keys(currentAnswer).find(
          (curAnsId) => currentAnswer[curAnsId] === ''
        );
        return !!emptyIdentifier;
      }
      return false;
    }
    return !currentAnswer;
  };

  const renderButtonGroup = () => {
    return (
      <View style={[AppStyles.row, AssessmentFlowStyles.buttonContainerSimple]}>
        <Button
          variation="outlined"
          accessibilityLabel="Assessment Flow back button"
          onPress={() => handlePrevious()}
          buttonStyle={[AppStyles.generalButton, AppStyles.generalOutlinedButton]}>
          <VIcon name="arrow-back" size={15} color={COLOR_AIA_RED} />
        </Button>
        <View style={AppStyles.buttonGroupPrimary}>
          <Button
            variation="contained"
            accessibilityLabel="Submit assessment"
            onPress={() => handleContinue()}
            disabled={buttonDisabled()}
            buttonStyle={AppStyles.generalButton}
            buttonIcon={
              // this will be refactored once the offline assessment component will be in place
              identifiers.includes('CameraView') ? 'camera' : ''
            }>
            {getButtonText()}
          </Button>
        </View>
      </View>
    );
  };

  return (
    <Journey
      onPress={() => navigation.navigate('AssessmentDetail')}
      hasProgressBar
      progressBarProgress={currentProgress}
      progressBarLabel={`${currentProgress}% completed`}
      headerContainer={selectHeader()}
      flowJourney
      questionContainer={renderQuestion()}
      buttonContainer={renderButtonGroup()}
      containsInput={containsInput}
    />
  );
};

export default AssessmentFlow;
