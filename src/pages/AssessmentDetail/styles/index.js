/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet, Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {WHITE, COLOR_AIA_GD_ACTIVE_1, COLOR_AIA_DARK_GREY} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const {width} = Dimensions.get('window');

const AssessmentDetailStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  infoContainer: {
    backgroundColor: WHITE,
  },
  textWithStatusContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleText: {
    flex: 1,
    fontSize: RFValue(30),
    fontWeight: '700',
    flexWrap: 'wrap',
  },
  statusText: {
    color: COLOR_AIA_GD_ACTIVE_1,
    fontSize: RFValue(18),
  },
  statusStyle: {
    borderColor: COLOR_AIA_GD_ACTIVE_1,
    borderWidth: 1.5,
  },
  assessmentDescription: {
    fontSize: RFValue(17),
    color: COLOR_AIA_DARK_GREY,
  },
  iconTextsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    overflow: 'hidden',
  },
  iconTextContainer: {
    alignItems: 'flex-start',
    width: '50%',
  },
  termsAndConditionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tandcContainer: {
    flexWrap: 'wrap',
  },
  body: {
    marginTop: isIOS ? -55 : -80,
  },
  assessmentImageLazyContainer: {
    height: 150,
    borderRadius: 20,
    width,
  },
  assessmentInfoLazyContainer: {
    height: 100,
    borderRadius: 20,
    width: width - 40,
  },
});

export default AssessmentDetailStyles;
