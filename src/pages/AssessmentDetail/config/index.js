/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const RETAKE_FOR_POINTS = 'Retake for points on';

export const ICON_TEXTS = [
  {
    iconGroup: 'assessmentDetail',
    iconName: 'assessment_input',
    label: 'Spend approx.',
    primaryText: '5',
    secondaryText: 'minutes',
  },
  {
    iconGroup: 'assessmentDetail',
    iconName: 'earn_points',
    label: 'Get',
    primaryText: '500',
    secondaryText: 'points',
  },
  {
    iconGroup: 'assessmentDetail',
    iconName: 'refresh',
    label: RETAKE_FOR_POINTS,
    primaryText: 'Oct 29,',
    secondaryText: '2020',
  },
];

export const HEALTH_RELATED_REPORTS = [
  {
    iconGroup: 'general',
    iconName: 'thumbnailOne',
    name: 'Nutition',
    description: 'Your diet needs work. See your full report.',
  },
  {
    iconGroup: 'general',
    iconName: 'thumbnailTwo',
    name: 'Nutition',
    description: 'Your diet needs work. See your full report.',
  },
  {
    iconGroup: 'general',
    iconName: 'thumbnailOne',
    name: 'Nutition',
    description: 'Your diet needs work. See your full report.',
  },
];

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
export const ONLINE = 'online';
export const OFFLINE = 'offline';
export const FACE_TO_FACE = 'face to face';
