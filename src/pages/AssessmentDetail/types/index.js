/**
 *
 * @format
 *
 */

// @flow
type SliderItemCoreType = {|
  title?: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points?: number,
|};

export type PropsType = {
  navigation: {
    navigate: (path: string) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
  },
};

export type ChallengeItemType = SliderItemCoreType & {
  variationNumber: number,
  endDate?: Date,
  minPoints?: number,
  maxPoints?: number,
  voucher?: number,
  highlightText?: string,
  description?: string,
};

export type IconTextType = {
  iconGroup?: string,
  iconName?: string,
  label?: string,
  primaryText?: string,
  secondaryText?: string,
};

export type HealthReportItemKey = {
  imageSrc?: string,
  iconGroup?: string,
  iconName?: string,
  name?: string,
  description?: string,
};
