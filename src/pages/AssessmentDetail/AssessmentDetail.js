/**
 *
 * AssessmentDetail Page
 * @format
 *
 */

// @flow

import React, {useState, useEffect, useContext} from 'react';
import {View, StatusBar, ScrollView} from 'react-native';
import {useTranslation} from 'react-i18next';
import {Chip} from 'react-native-paper';
import type {Element} from 'react';

import {ApplicationContext} from '@contexts/Application';
import {AssessmentsContext} from '@contexts/Assessments';
import {OnlineAssessmentsContext} from '@contexts/OnlineAssessments';
import {GlobalContext} from '@contexts/Global';

import {ASSESSMENT_DETAIL, ASSESSMENT_QUESTION} from '@core/handler/AEMResponseParser/config';

import Text from '@atoms/Text';
import Icon from '@atoms/Icon';
import Button from '@atoms/Button';
import Slider from '@atoms/Slider';
import {LazyLoad} from '@atoms/LazyLoad';

import Navigator from '@molecules/Navigator';
import IconText from '@molecules/IconText';
import Checkbox from '@molecules/Checkbox';
import Card from '@molecules/Card';
import Header from '@molecules/Header';

import {COLOR_AIA_GREY, COLOR_AIA_RED, WHITE} from '@colors';
import AppStyles from '@styles';
import AssessmentFlowStyles from './styles';
import {IdentifierQuestionIdMapping} from '../AssessmentFlow/config';
import {
  ICON_TEXTS,
  HEALTH_RELATED_REPORTS,
  HEADER_HEIGHT,
  RETAKE_FOR_POINTS,
  ONLINE,
  OFFLINE,
  FACE_TO_FACE,
} from './config';

import type {PropsType, IconTextType, HealthReportItemKey, ChallengeItemType} from './types';

const AssessmentDetail = (props: PropsType) => {
  const {navigation} = props;
  const {t, i18n} = useTranslation();
  const userLanguage = i18n.language;
  const [checked, setChecked] = useState(false);
  const [hideBackground, setHideBackground] = useState(true);
  const {setMsgSnackbarProps} = useContext(GlobalContext);
  const {selectedAssessment} = useContext(ApplicationContext);
  const {
    assessmentCurrentPage,
    setAssessmentCurrentPage,
    setCurrentAssessment,
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
    assessmentDetail,
    membershipId,
  } = useContext(AssessmentsContext);
  const ALL_ASSESSMENTS = [
    howActiveAreYou,
    howHealthyAreYou,
    howStressedAreYou,
    howWellAreYouEating,
    howWellDoYouSleep,
    nonSmokerDeclaration,
  ];

  const {
    onlineAssessmentsUserAnswers,
    completedOnlineAssessments,
    answeredOnlineAssessments,
  } = useContext(OnlineAssessmentsContext);
  const assessmentId = selectedAssessment.id || 'HowHealthyAreYou';
  const assessmentSet = ALL_ASSESSMENTS.find(
    (question) => question[ASSESSMENT_DETAIL] && question[ASSESSMENT_DETAIL].id === assessmentId
  );
  const assessmentQuestions = assessmentSet ? assessmentSet[ASSESSMENT_QUESTION] : {};
  let highestAnsweredQuestion = 0;
  const answeredQuestions = Object.keys(assessmentQuestions).reduce((acc, val) => {
    const series = parseInt(val.replace(/[^0-9]/g, ''), 10);
    const {identifiers} = assessmentQuestions[val];
    if (identifiers && identifiers.length > 0 && onlineAssessmentsUserAnswers[userLanguage]) {
      identifiers.forEach((identifier) => {
        if (onlineAssessmentsUserAnswers[userLanguage][identifier]) {
          if (highestAnsweredQuestion < series) {
            highestAnsweredQuestion = series;
          }
          acc[val] = onlineAssessmentsUserAnswers[userLanguage][identifier];
        }
      });
    } else if (
      IdentifierQuestionIdMapping[val] &&
      onlineAssessmentsUserAnswers[userLanguage] &&
      onlineAssessmentsUserAnswers[userLanguage][IdentifierQuestionIdMapping[val]]
    ) {
      if (highestAnsweredQuestion < series) {
        highestAnsweredQuestion = series;
      }
      acc[val] = onlineAssessmentsUserAnswers[userLanguage][IdentifierQuestionIdMapping[val]];
    }
    return acc;
  }, {});
  const questionsLength = assessmentQuestions ? Object.keys(assessmentQuestions).length : 0;

  const onPressBack = () => {
    navigation.goBack();
  };

  const renderImage = () => {
    const {imageGroup, imageName} = assessmentDetail;
    const content = (
      <View>
        <Icon group={imageGroup} name={imageName} />
      </View>
    );
    const lazyContent = (
      <LazyLoad>
        <View style={[AppStyles.row]}>
          <View style={[AssessmentFlowStyles.assessmentImageLazyContainer]} />
        </View>
      </LazyLoad>
    );
    return imageName && imageGroup ? content : lazyContent;
  };

  const handleScroll = (event: Object) => {
    const scrollY = event.nativeEvent.contentOffset.y;
    if (hideBackground) {
      if (scrollY > HEADER_HEIGHT + 20) {
        setHideBackground(false);
      }
    } else if (scrollY < HEADER_HEIGHT - 20) {
      setHideBackground(true);
    }
  };

  useEffect(() => {
    if (Object.keys(answeredQuestions).length > 0 && Object.keys(assessmentQuestions).length > 0) {
      setChecked(true);
    } else {
      setChecked(false);
    }
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  const renderIconText = (item: IconTextType, index: number) => {
    const {iconGroup, label, primaryText, secondaryText} = item;
    return (
      <View
        key={`key${index}`}
        style={[AppStyles.marginBottom_20, AssessmentFlowStyles.iconTextContainer]}>
        <IconText
          key={`key${index}`}
          iconGroup={iconGroup}
          label={label}
          primaryText={primaryText}
          secondaryText={secondaryText}
        />
      </View>
    );
  };

  const renderIconTexts = () => {
    return (
      <View style={[AppStyles.marginHorizontal_20, AssessmentFlowStyles.iconTextsContainer]}>
        {ICON_TEXTS.map((item: IconTextType, index: number) => {
          const {label} = item;
          const mapKey: string = `key${index}`;
          if (label === RETAKE_FOR_POINTS) {
            if (completedOnlineAssessments.indexOf(assessmentId) === -1) {
              return <View key={mapKey} />;
            }
            return renderIconText(item, index);
          }
          return renderIconText(item, index);
        })}
      </View>
    );
  };

  const onCheckTandC = () => {
    if (assessmentQuestions) {
      const questionsLoaded: boolean = Object.keys(assessmentQuestions).length > 0;
      if (questionsLoaded && !checked) {
        setChecked(true);
      } else {
        if (!questionsLoaded) {
          setMsgSnackbarProps('The questions are not yet available.');
        }
        setChecked(false);
      }
    } else {
      setMsgSnackbarProps('The questions are not yet available.');
    }
  };

  const renderTermsAndCondition = () => {
    return (
      <View
        style={[
          AppStyles.marginHorizontal_20,
          AppStyles.marginBottom_20,
          AssessmentFlowStyles.termsAndConditionContainer,
        ]}>
        <Checkbox checked={checked} onPress={() => onCheckTandC()} />
        <View style={[AppStyles.row, AssessmentFlowStyles.tandcContainer]}>
          <Text customStyle={{color: COLOR_AIA_GREY}}>{t('assessment.tcText')}</Text>
          <Text customStyle={{color: COLOR_AIA_RED}}> {t('assessment.tcLink')}</Text>
        </View>
      </View>
    );
  };

  const startAssessment = (id: string) => {
    const nextPage = highestAnsweredQuestion + 1;
    setCurrentAssessment(id);
    if (
      nextPage > questionsLength ||
      answeredOnlineAssessments.indexOf(assessmentId) !== -1 ||
      completedOnlineAssessments.indexOf(assessmentId) !== -1
    ) {
      navigation.navigate('AssessmentReview');
    } else {
      setAssessmentCurrentPage({
        ...assessmentCurrentPage,
        [assessmentId]: {
          page: nextPage,
        },
      });
      navigation.navigate('AssessmentFlow');
    }
  };

  const getButtonText = () => {
    if (
      checked &&
      (answeredOnlineAssessments.indexOf(assessmentId) !== -1 ||
        completedOnlineAssessments.indexOf(assessmentId) !== -1)
    ) {
      return `Review ${t('general.assessmentTitle').toLowerCase().replace(/s$/, '')}`;
    }
    if (
      checked &&
      Object.keys(answeredQuestions).length > 0 &&
      highestAnsweredQuestion < questionsLength
    ) {
      return `${t('general.continue')} ${t('general.assessmentTitle')
        .toLowerCase()
        .replace(/s$/, '')}`;
    }
    if (checked && highestAnsweredQuestion >= questionsLength) {
      return `Review ${t('general.assessmentTitle').toLowerCase().replace(/s$/, '')}`;
    }
    return t('assessment.startAssessment');
  };

  const renderAssessmentButton = () => {
    return (
      <View style={[AppStyles.marginHorizontal_20, AppStyles.marginBottom_20]}>
        <Button
          variation="contained"
          accessibilityLabel="Start Assessment Button"
          disabled={!checked}
          onPress={() => startAssessment(selectedAssessment.id)}
          buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
          {getButtonText()}
        </Button>
      </View>
    );
  };

  // This will be removed once the offline assessment will be in place
  const renderCameraButton = () => {
    return (
      <View style={[AppStyles.marginHorizontal_20, AppStyles.marginBottom_20]}>
        <Button
          variation="contained"
          disabled={false}
          onPress={() => startAssessment(selectedAssessment.id)}
          buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
          {t('general.open-camera')}
        </Button>
      </View>
    );
  };

  const getChipText = (assessmentType: string) => {
    switch (assessmentType) {
      case ONLINE:
        return t('general.online');
      case OFFLINE:
        return t('general.faceToFace');
      case FACE_TO_FACE:
        return t('general.faceToFace');
      default:
        return '';
    }
  };

  const renderAssessmentInfo = () => {
    const {description, title, type} = assessmentDetail;
    const content = (
      <View>
        <View
          style={[
            AppStyles.marginVertical_20,
            AppStyles.marginHorizontal_20,
            AppStyles.row,
            AssessmentFlowStyles.textWithStatusContainer,
          ]}>
          <Text customStyle={AssessmentFlowStyles.titleText}>{title}</Text>
          <Chip
            textStyle={AssessmentFlowStyles.statusText}
            style={AssessmentFlowStyles.statusStyle}
            mode="outlined">
            {getChipText(type)}
          </Chip>
        </View>
        {description !== ' ' && description !== '' && (
          <View style={[AppStyles.marginHorizontal_20, AppStyles.marginBottom_20]}>
            <Text customStyle={AssessmentFlowStyles.assessmentDescription}>{description}</Text>
          </View>
        )}
      </View>
    );
    const lazyContent = (
      <LazyLoad style={[AppStyles.marginVertical_20, AppStyles.marginHorizontal_20]}>
        <View style={[AppStyles.row]}>
          <View style={[AssessmentFlowStyles.assessmentInfoLazyContainer]} />
        </View>
      </LazyLoad>
    );

    return title ? content : lazyContent;
  };

  const renderBookAppointedButton = () => {
    return (
      <View style={[AppStyles.marginTop_20, AppStyles.marginHorizontal_20]}>
        <Button
          variation="outlined"
          accessibilityLabel={t('general.bookAppointment')}
          onPress={() => {}}
          buttonStyle={[
            AppStyles.generalButton,
            AppStyles.generalOutlinedButton,
            AppStyles.fullWidthButton,
          ]}>
          {t('general.bookAppointment')}
        </Button>
      </View>
    );
  };

  const renderSubmitResultsButton = () => {
    return (
      <View style={[AppStyles.marginVertical_20, AppStyles.marginHorizontal_20]}>
        <Button
          variation="contained"
          accessibilityLabel={t('general.submitResults')}
          onPress={() => {}}
          buttonStyle={[AppStyles.generalButton, AppStyles.fullWidthButton]}>
          {t('general.submitResults')}
        </Button>
      </View>
    );
  };

  const renderAssessmentDetailsView = () => {
    const {type, id} = assessmentDetail;
    return (
      <View style={[AssessmentFlowStyles.infoContainer]}>
        {renderAssessmentInfo()}
        {renderIconTexts()}
        {type === ONLINE && renderTermsAndCondition()}
        {type === ONLINE && renderAssessmentButton()}
        {/* This will be removed once the offline assessment will be in place */}
        {checked === true &&
          type === ONLINE &&
          membershipId !== '' &&
          membershipId === 'V000200059' &&
          id === 'HowHealthyAreYou' &&
          renderCameraButton()}
        {(type === OFFLINE || type === FACE_TO_FACE) && renderBookAppointedButton()}
        {(type === OFFLINE || type === FACE_TO_FACE) && renderSubmitResultsButton()}
      </View>
    );
  };

  const renderHealthRelatedReportsSlider = () => {
    const itemWidthPercentage = 93;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: HealthReportItemKey, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {name, iconName, description} = item;

      return (
        <Card
          key={index}
          variationNumber={1}
          name={name}
          iconName={iconName}
          description={description}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_20}>
        {HEALTH_RELATED_REPORTS && (
          <Slider
            title={t('assessment.healthReports')}
            linkText={t('general.seeAllTitle')}
            data={[]}
            renderItem={renderItem}
            itemWidthPercentage={itemWidthPercentage}
            slideAlign={slideAlign}
            containerCustomStyle={AppStyles.paddingLeft_20}
            onPressSeeAll={() => {}}
          />
        )}
      </View>
    );
  };

  const renderChallengeRelatedSlider = () => {
    const itemWidthPercentage = 42;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: ChallengeItemType, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {
        variationNumber,
        title,
        iconName,
        points,
        endDate,
        minPoints,
        maxPoints,
        voucher,
        highlightText,
        description,
      } = item;

      if (variationNumber === 2) {
        return (
          <Card
            key={index}
            variationNumber={2}
            title={title}
            iconName={iconName}
            points={points}
            minPoints={minPoints}
            maxPoints={maxPoints}
            voucher={voucher}
            endDate={endDate}
            onPressItem={() => {}}
          />
        );
      }
      return (
        <Card
          key={index}
          variationNumber={3}
          title={title}
          iconName={iconName}
          highlightText={highlightText}
          description={description}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={[AppStyles.marginVertical_20]}>
        <Slider
          title={t('assessment.relatedChallenges')}
          linkText={t('general.seeAllTitle')}
          data={[]}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={{paddingLeft: 20}}
          onPressSeeAll={() => {}}
        />
      </View>
    );
  };

  const renderHeader = () => {
    const {title} = assessmentDetail;
    const content = () => {
      return (
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={title}
          showTitle={!hideBackground}
        />
      );
    };
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={hideBackground}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  const renderRelatedCoupons = () => {
    const itemWidthPercentage = 42;
    const slideAlign = 'start';

    const renderItem = () => {
      return <View />;
    };

    return (
      <View style={AppStyles.marginTop_20}>
        <Slider
          title={t('general.relatedCoupons')}
          linkText={t('general.seeAllTitle')}
          data={[]}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={AppStyles.paddingLeft_20}
          onPressSeeAll={() => {}}
        />
      </View>
    );
  };

  const renderBody = () => {
    const {type} = assessmentDetail;
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
        style={[AssessmentFlowStyles.body]}
        scrollEventThrottle={16}
        onScroll={handleScroll}>
        {renderImage()}
        {renderAssessmentDetailsView()}
        {type === ONLINE && renderHealthRelatedReportsSlider()}
        {type === ONLINE && renderChallengeRelatedSlider()}
        {(type === OFFLINE || type === FACE_TO_FACE) && renderRelatedCoupons()}
        <View style={{height: 400}} />
      </ScrollView>
    );
  };
  return (
    <View style={AssessmentFlowStyles.container}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderBody()}
    </View>
  );
};

const AssessmentDetailWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <AssessmentDetail navigation={navigation} />;
};

export default AssessmentDetailWrapper;
