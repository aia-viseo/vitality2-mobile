/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const RewardDetailStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
  },
});

export default RewardDetailStyles;
