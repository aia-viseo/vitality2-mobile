/**
 *
 * Reward Page
 * @format
 *
 */

// @flow

import React from 'react';
import {SafeAreaView, Text} from 'react-native';

import Navigator from '@molecules/Navigator';

import type {PropsType} from './types';
import RewardStyles from './styles';

const RewardDetail = (props: PropsType) => {
  const {navigation} = props;

  return (
    <SafeAreaView style={RewardStyles.container}>
      <Navigator type="back" onPressItem={() => navigation.goBack()} title="RewardDetail" />
      <Text>Reward Detail Page</Text>
    </SafeAreaView>
  );
};

const RewardDetailWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <RewardDetail navigation={navigation} />;
};

export default RewardDetailWrapper;
