/**
 *
 * Rewards Page
 * @format
 *
 */

// @flow

import React, {useContext, useState, useRef} from 'react';
import {StatusBar, View, FlatList} from 'react-native';
import Animated from 'react-native-reanimated';
import {useTranslation} from 'react-i18next';

import {RewardsProvider} from '@contexts/Rewards';
import {ApplicationContext} from '@contexts/Application';

import AppStyles from '@styles';
import Card from '@molecules/Card';
import ChipFilter from '@molecules/ChipFilter';
import Header from '@molecules/Header';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';
import GetOS from '@platform';

import type {PropsType} from './types';
import {FILTERS, HIDE_BACKGROUND_LIMIT} from './config';

const Rewards = (props: PropsType) => {
  const {rewards} = useContext(ApplicationContext);
  const [scrollY] = useState(new Animated.Value(0));
  const [hideBackground, setHideBackground] = useState(true);
  const [selectedFilterIndex, setSelectedFilterIndex] = useState(0);
  const scrollViewRef = useRef();
  const {t} = useTranslation();
  const isIOS = GetOS.getInstance() === 'ios';
  const {navigation} = props;

  const headerHeightDiff = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

  const renderHeaderCard = () => {
    return <View style={[AppStyles.flex_1, AppStyles.padding_20]} />;
  };

  const showReward = () => {
    navigation.navigate('RewardsTab', {screen: 'RewardDetail'});
  };

  const renderRewardsList = () => {
    return (
      <View style={[isIOS ? AppStyles.iosBodyContent : AppStyles.androidBodyContent]}>
        <FlatList
          data={rewards}
          renderItem={({item}) => {
            const {name, iconName, iconGroup, discount, quantity, targetDate, terms} = item;
            return (
              <View style={[AppStyles.marginLeft_20, AppStyles.marginRight_20]}>
                <Card
                  variationNumber={4}
                  name={name}
                  iconName={iconName}
                  iconGroup={iconGroup}
                  discount={discount}
                  quantity={quantity}
                  targetDate={targetDate}
                  terms={terms}
                  onPressItem={showReward}
                />
              </View>
            );
          }}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <Header
        setTop
        scrollY={scrollY}
        scaleHeight={Animated.interpolate(scrollY, {
          inputRange: [0, headerHeightDiff * 2 - 1, headerHeightDiff * 2],
          outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
        })}
        cardContent={renderHeaderCard}
        title={t('rewards.mainTitle')}
      />
    );
  };

  const renderChipFilter = () => {
    return (
      <ChipFilter
        data={FILTERS}
        selectedIndex={selectedFilterIndex}
        onPressChip={(index) => {
          setSelectedFilterIndex(index);
        }}
        hideBackground={hideBackground}
        scrollY={scrollY}
        headerHeightDiff={headerHeightDiff}
        withContainer
      />
    );
  };

  const renderStatusBar = () => {
    return <StatusBar barStyle="light-content" />;
  };

  const renderAnimatedCode = () => {
    return (
      <Animated.Code>
        {() =>
          Animated.call([scrollY], ([val]: number[]) => {
            if (hideBackground) {
              if (val >= HIDE_BACKGROUND_LIMIT) {
                setHideBackground(false);
              }
            } else if (val <= HIDE_BACKGROUND_LIMIT) {
              setHideBackground(true);
            }
          })
        }
      </Animated.Code>
    );
  };

  const renderBody = () => {
    return (
      <Animated.ScrollView
        ref={scrollViewRef}
        bounces={false}
        style={[AppStyles.marginTop_minus_100]}
        scrollEventThrottle={16}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {y: scrollY}}}], {
          useNativeDriver: true,
        })}>
        {renderRewardsList()}
        {renderAnimatedCode()}
      </Animated.ScrollView>
    );
  };

  return (
    <View style={AppStyles.flex_1}>
      {renderStatusBar()}
      {renderHeader()}
      {renderChipFilter()}
      {renderBody()}
    </View>
  );
};

const RewardsWrapper = (props: PropsType) => {
  const {navigation} = props;

  return (
    <RewardsProvider>
      <Rewards navigation={navigation} />
    </RewardsProvider>
  );
};

export default RewardsWrapper;
