/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  WHITE,
  COLOR_AIA_GD_ACTIVE_1,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREY_50,
  COLOR_AIA_GREY_500,
  COLOR_AIA_GREEN,
  COLOR_AIA_ICON_GRAY,
} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const PointsHistoryStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  progressContainer: {
    backgroundColor: WHITE,
  },
  tableHeader: {
    padding: 20,
    backgroundColor: WHITE,
  },
  historyCell: {
    padding: 20,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  cellDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR_AIA_GREY_50,
  },
  lazyloadHeaderContainer: {
    height: 200,
    borderRadius: 10,
    width: '100%',
    marginBottom: 20,
  },
  lazyloadContainer: {
    height: 50,
    borderRadius: 10,
    width: '100%',
    marginBottom: 20,
  },
  date: {
    fontSize: RFValue(12),
    color: COLOR_AIA_GREY_500,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  rangeBarRow: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  rangeBarContainner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rangeBarName: {
    fontSize: RFValue(12),
    color: COLOR_AIA_GREY_500,
  },
  rangeBarPoints: {
    fontSize: RFValue(15),
    color: COLOR_AIA_DARK_GREY,
    fontWeight: '700',
  },
  title: {
    fontSize: RFValue(14),
    fontWeight: '700',
    color: COLOR_AIA_DARK_GREY,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  headerPoints: {
    flexDirection: 'column',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerPointsLabel: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREEN,
  },
  points: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREEN,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  figures: {
    fontSize: RFValue(20),
    fontWeight: '700',
  },
  eventText: {
    color: COLOR_AIA_GD_ACTIVE_1,
    fontSize: RFValue(18),
  },
  eventStyle: {
    borderColor: COLOR_AIA_GD_ACTIVE_1,
    borderWidth: 1.5,
    marginRight: 10,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: COLOR_AIA_ICON_GRAY,
  },
  infoContainer: {
    backgroundColor: WHITE,
  },
  textWithStatusContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleText: {
    flex: 1,
    fontSize: RFValue(30),
    fontWeight: '700',
    flexWrap: 'wrap',
  },
  statusText: {
    color: COLOR_AIA_GD_ACTIVE_1,
    fontSize: RFValue(18),
  },
  statusStyle: {
    borderColor: COLOR_AIA_GD_ACTIVE_1,
    borderWidth: 1.5,
  },
  assessmentDescription: {
    fontSize: RFValue(17),
    color: COLOR_AIA_DARK_GREY,
  },
  iconTextsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    overflow: 'hidden',
  },
  iconTextContainer: {
    alignItems: 'flex-start',
    width: '50%',
  },
  termsAndConditionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tandcContainer: {
    flexWrap: 'wrap',
  },
  body: {
    marginTop: isIOS ? -55 : -80,
  },
});

export default PointsHistoryStyles;
