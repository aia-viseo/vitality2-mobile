/**
 *
 * Points History Page
 * @format
 * @flow
 *
 */

import React, {useEffect, useContext} from 'react';
import {FlatList, StatusBar, View, RefreshControl} from 'react-native';
import {useTranslation} from 'react-i18next';

import {PointsContext, PointsProvider} from '@contexts/Points';
import DateTime from '@core/handler/DateTime';
import NumberString from '@core/handler/NumberString';

import AppStyles from '@styles';
import Icon from '@atoms/Icon';
import {LazyLoad} from '@atoms/LazyLoad';
import Text from '@atoms/Text';
import Navigator from '@molecules/Navigator';
import Header from '@molecules/Header';
import DateListItem from '@molecules/DateListItem';
import CircularProgressBar from '@molecules/CircularProgressBar';
import {WHITE} from '@colors';

import {HEADER_HEIGHT} from './config';
import Styles from './styles';
import type {PropsType} from './types';

const PointsHistory = (props: PropsType) => {
  const {t} = useTranslation();
  const {
    historyItems,
    rangeBarItems,
    refresh,
    refreshing,
    totalPoints,
    totalPointsThisMonth,
  } = useContext(PointsContext);
  const {navigation} = props;

  const onPressBack = () => {
    navigation.goBack();
  };

  const onRefresh = React.useCallback(() => {
    refresh(true);
  }, [refresh]);

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  const renderHeader = () => {
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={false}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={t('points-history.pointsHistoryTitle')}
          rightButtonType="refresh"
          onPressRightButton={refresh}
          showTitle
        />
      </Header>
    );
  };

  const TableHeader = () => {
    /* eslint-disable no-prototype-builtins */
    if (rangeBarItems.hasOwnProperty('error')) return <View />;
    /* eslint-enable no-prototype-builtins */

    return (
      <View style={Styles.tableHeader}>
        <View style={[AppStyles.row, AppStyles.justifyContentSpaceAround]}>
          <View style={Styles.headerPoints}>
            <Text customStyle={Styles.headerPointsLabel}>
              {t('points-history.totalPointsLabel')}
            </Text>
            <Text customStyle={Styles.figures}>{NumberString.toLocaleString(totalPoints)}</Text>
          </View>
          <View style={Styles.headerPoints}>
            <Text customStyle={Styles.headerPointsLabel}>{t('points-history.thisMonthLabel')}</Text>
            <Text customStyle={Styles.figures}>
              {NumberString.toLocaleString(totalPointsThisMonth)}
            </Text>
          </View>
        </View>
        <View style={[AppStyles.row, AppStyles.justifyContentSpaceAround, AppStyles.marginTop_20]}>
          <View style={Styles.rangeBarRow}>
            {rangeBarItems.assessments && (
              <View style={Styles.rangeBarContainner}>
                <CircularProgressBar
                  progress={Math.floor(
                    parseInt(
                      (rangeBarItems.assessments.currentPoints /
                        rangeBarItems.assessments.maximumPoints) *
                        100,
                      10
                    )
                  )}
                  side={40}
                  iconSize={24}
                  icon={<Icon group="medals" name="platinum" width={24} />}
                />
                <View style={AppStyles.marginLeft_10}>
                  <Text customStyle={Styles.rangeBarName}>
                    {t(rangeBarItems.assessments.rangeBarName)}
                  </Text>
                  <Text customStyle={Styles.rangeBarPoints}>
                    {NumberString.toLocaleString(rangeBarItems.assessments.currentPoints)} /{' '}
                    {NumberString.toLocaleString(rangeBarItems.assessments.maximumPoints)}
                  </Text>
                </View>
              </View>
            )}
            {rangeBarItems.challenges && (
              <View style={Styles.rangeBarContainner}>
                <CircularProgressBar
                  progress={Math.floor(
                    parseInt(
                      (rangeBarItems.challenges.currentPoints /
                        rangeBarItems.challenges.maximumPoints) *
                        100,
                      10
                    )
                  )}
                  side={40}
                  iconSize={24}
                  icon={<Icon group="medals" name="platinum" width={24} />}
                />
                <View style={AppStyles.marginLeft_10}>
                  <Text customStyle={Styles.rangeBarName}>
                    {t(rangeBarItems.challenges.rangeBarName)}
                  </Text>
                  <Text customStyle={Styles.rangeBarPoints}>
                    {NumberString.toLocaleString(rangeBarItems.challenges.currentPoints)} /{' '}
                    {NumberString.toLocaleString(rangeBarItems.challenges.maximumPoints)}
                  </Text>
                </View>
              </View>
            )}
          </View>
        </View>
        <View style={[AppStyles.row, AppStyles.justifyContentSpaceAround, AppStyles.marginTop_40]}>
          {rangeBarItems.activity && (
            <>
              <View style={[Styles.rangeBarContainner, AppStyles.alignItemCenter]}>
                <CircularProgressBar
                  progress={Math.floor(
                    parseInt(
                      (rangeBarItems.activity.currentPoints /
                        rangeBarItems.activity.maximumPoints) *
                        100,
                      10
                    )
                  )}
                  side={40}
                  iconSize={24}
                  icon={<Icon group="medals" name="platinum" width={24} />}
                />
                <View style={AppStyles.marginLeft_10}>
                  <Text customStyle={Styles.rangeBarName}>
                    {t(rangeBarItems.activity.rangeBarName)}
                  </Text>
                  <Text customStyle={Styles.rangeBarPoints}>
                    {NumberString.toLocaleString(rangeBarItems.activity.currentPoints)} /{' '}
                    {NumberString.toLocaleString(rangeBarItems.activity.maximumPoints)}
                  </Text>
                </View>
              </View>
            </>
          )}
        </View>
      </View>
    );
  };

  const separator = () => {
    return <View style={Styles.separator} />;
  };

  /* eslint-disable extra-rules/no-commented-out-code */
  const navigateNoNewPoints = () => {
    if (!refreshing) {
      // showModal: IDMPV-289
    }
  };
  /* eslint-enable extra-rules/no-commented-out-code */

  const renderBody = () => {
    const lazyContent = (
      <LazyLoad style={[AppStyles.marginVertical_20, AppStyles.marginHorizontal_20]}>
        <View style={[AppStyles.column]}>
          <View style={[Styles.lazyloadHeaderContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
        </View>
      </LazyLoad>
    );

    return !refreshing ? (
      <FlatList
        data={historyItems}
        renderItem={({item}) => {
          const date = DateTime.format(item.awardedDate, 'MMM d');
          return (
            <DateListItem
              dateStr={date.toString()}
              title={item.pointsHistoryTitle}
              points={item.points}
            />
          );
        }}
        ItemSeparatorComponent={separator}
        ListHeaderComponent={TableHeader}
        keyExtractor={(item, index) => `${index}`}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} progressViewOffset={190} />
        }
      />
    ) : (
      lazyContent
    );
  };

  return (
    <View style={Styles.container}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderBody()}
      {navigateNoNewPoints()}
    </View>
  );
};

const PointsHistoryWrapper = (props: PropsType) => {
  const {navigation} = props;

  return (
    <PointsProvider>
      <PointsHistory navigation={navigation} />
    </PointsProvider>
  );
};

export default PointsHistoryWrapper;
