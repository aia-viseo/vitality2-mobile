/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
export const EVENTS = [
  {id: 1, key: 'assessment.Assessments'},
  {id: 2, key: 'challenge.Challenges'},
  {id: 3, key: 'points.Activity tracking'},
];

export default {};
