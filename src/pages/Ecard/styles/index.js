/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const EcardStyles = StyleSheet.create({
  backButton: {position: 'absolute', left: 10, top: 20, zIndex: 5},
});

export default EcardStyles;
