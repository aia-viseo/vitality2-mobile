/**
 *
 * Sedentary
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import {View} from 'react-native';

import EcardComponent from '../../components/molecules/Ecard';
import Navigator from '../../components/molecules/Navigator';

import {GlobalContext} from '../../contexts/Global';

import type {PropsType} from './types';
import EcardStyles from './styles';

const Ecard = (props: PropsType) => {
  const {navigation} = props;
  const {userInfo} = useContext(GlobalContext);
  const {firstname, lastname, membershipNumber, currentPolicyPeriodToDate} = userInfo;

  const capsFirstLetter = (name: string) => {
    return `${name[0].toUpperCase()}${name.slice(1)}`;
  };

  const formatDate = (date: string) => {
    if (Number.isNaN(Date.parse(date))) {
      return '';
    }
    const dateArray = new Date(date).toDateString().split(' ').slice(1);
    dateArray[1] = dateArray[1].replace(/$/, ',');
    return dateArray.join(' ');
  };

  return (
    <View>
      <EcardComponent
        name={
          firstname && lastname
            ? `${capsFirstLetter(firstname.toLowerCase())} ${capsFirstLetter(
                lastname.toLowerCase()
              )}`
            : ''
        }
        membershipNumber={membershipNumber || ''}
        expirationDate={
          currentPolicyPeriodToDate ? formatDate(currentPolicyPeriodToDate.split('+')[0]) : ''
        }
        barCodeData={
          firstname && lastname && currentPolicyPeriodToDate && membershipNumber
            ? `${firstname}${lastname}${currentPolicyPeriodToDate}`
            : ''
        }
      />
      <View style={[EcardStyles.backButton]}>
        <Navigator type="back" onPressItem={() => navigation.goBack()} />
      </View>
    </View>
  );
};

export default Ecard;
