/**
 *
 * ChallengeComplete Page
 * @format
 *
 */

// @flow

import React, {useEffect} from 'react';
import {View, SafeAreaView, TouchableWithoutFeedback} from 'react-native';
import {useTranslation} from 'react-i18next';
import VIcon from 'react-native-vector-icons/MaterialIcons';

import Icon from '@atoms/Icon';
import Text from '@atoms/Text';
import Button from '@atoms/Button';

import {COLOR_AIA_GREY_500} from '@colors';

import AppStyles from '@styles';
import HeaderFlowStyles from '@organisms/HeaderFlow/styles';
import ChallengeCompleteStyles from './styles';

import type {PropsType} from './types';

const ChallengeComplete = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  const handleClose = () => {
    navigation.navigate('Challenges');
  };

  const renderControlButton = () => {
    return (
      <View style={[AppStyles.row, ChallengeCompleteStyles.buttonContainer]}>
        <View style={[AppStyles.fullWidthButton]}>
          <Button
            variation="contained"
            accessibilityLabel={t('general.backToChallengeDetails')}
            onPress={() => {}}
            buttonStyle={AppStyles.generalButton}>
            {t('general.backToChallengeDetails')}
          </Button>
        </View>
      </View>
    );
  };

  const renderVoucherCard = () => {
    return <View style={ChallengeCompleteStyles.voucherCard} />;
  };

  return (
    <SafeAreaView style={ChallengeCompleteStyles.container}>
      <View style={[AppStyles.paddingVertical_20]}>
        <View
          style={[
            AppStyles.row,
            AppStyles.paddingHorizontal_20,
            ChallengeCompleteStyles.headerTopSection,
          ]}>
          <TouchableWithoutFeedback onPress={() => handleClose()}>
            <View style={HeaderFlowStyles.closeButton}>
              <VIcon size={24} name="close" color={COLOR_AIA_GREY_500} />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
      <View style={[ChallengeCompleteStyles.bodyContainer]}>
        <Icon group="general" name="manRunning" />
        <Text customStyle={ChallengeCompleteStyles.challengeComplete}>
          {t('general.yourChallengeIsDone')}
        </Text>
        <Text customStyle={ChallengeCompleteStyles.seeWhatYouGot}>
          {t('general.letsSeeWhatYouveGot')}
        </Text>
      </View>
      {renderVoucherCard()}
      {renderControlButton()}
    </SafeAreaView>
  );
};

const ChallengeCompleteWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <ChallengeComplete navigation={navigation} />;
};

export default ChallengeCompleteWrapper;
