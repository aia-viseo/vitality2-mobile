/**
 *
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {BLACK, WHITE} from '@colors';

import {projectFontFamily} from '@core/config';

const ChallengeCompleteStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerTopSection: {
    justifyContent: 'space-between',
    height: 50,
  },
  bodyContainer: {
    alignItems: 'center',
  },
  challengeComplete: {
    fontSize: RFValue(16),
    fontFamily: projectFontFamily,
    fontWeight: '600',
    marginTop: 40,
  },
  seeWhatYouGot: {
    fontSize: RFValue(16),
    fontFamily: projectFontFamily,
    fontWeight: 'normal',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 1,
    padding: 20,
    width: '100%',
    justifyContent: 'space-between',
  },
  voucherCard: {
    marginHorizontal: 20,
    borderRadius: 10,
    backgroundColor: WHITE,
    height: 100,
    paddingTop: 20,
    marginTop: 40,
    shadowColor: BLACK,
    shadowOffset: {
      width: 1,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 0.5,
    elevation: 5,
  },
});

export default ChallengeCompleteStyles;
