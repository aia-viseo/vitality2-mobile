/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;

export const FILTERS = [
  {id: 1, key: 'workoutTracker.All', name: 'all'},
  {id: 2, key: 'workoutTracker.Steps', name: 'steps'},
  {id: 3, key: 'workoutTracker.Heart rate', name: 'heart rate'},
  {id: 4, key: 'workoutTracker.Sleep', name: 'sleep'},
];
