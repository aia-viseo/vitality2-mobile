/**
 *
 * Workout Tracker Detail Page
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import type {Node} from 'react';
import {View, StatusBar, FlatList, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import Icon from '@atoms/Icon';
import Text from '@atoms/Text';
import {LazyLoad} from '@atoms/LazyLoad';
import IconText from '@molecules/IconText';
import ChipFilter from '@molecules/ChipFilter';
import Navigator from '@molecules/Navigator';
import Header from '@molecules/Header';
import DateListItem from '@molecules/DateListItem';

import {WorkoutTrackerContext} from '@contexts/WorkoutTracker';

import {WHITE} from '@colors';
import AppStyles from '@styles';

import NumberString from '@core/handler/NumberString';
import DateTime from '@core/handler/DateTime';

import {HEADER_HEIGHT, FILTERS} from './config';

import type {PropsType} from './types';
import Styles from './styles';

const WorkoutTrackerSummary = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();
  const {
    selectedWorkoutData,
    filteredFitnessList,
    fitnessListLoading,
    setAllLoading,
    allLoading,
    selectedFilter,
    setSelectedFilter,
    syncTime,
  } = useContext(WorkoutTrackerContext);
  const {title, totalSteps, heatRate, sleepHours} = selectedWorkoutData || {};

  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressRefresh = () => {
    setAllLoading();
  };

  const renderHeader = () => {
    const content = () => {
      return (
        <View
          style={[
            AppStyles.row,
            AppStyles.justifyContentSpaceBetween,
            AppStyles.alignItemCenter,
            AppStyles.marginRight_10,
          ]}>
          <Navigator
            type="back"
            onPressItem={onPressBack}
            title="Workout Tracker Detail"
            showTitle={false}
          />
          <Navigator
            type="refresh"
            onPressItem={onPressRefresh}
            title="Workout Tracker Detail"
            showTitle={false}
          />
        </View>
      );
    };
    return (
      <Header fixHeight={HEADER_HEIGHT} hideBackground setTop colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  const renderIconText = (iconName: string, label: string, primaryText: string | Node) => {
    return (
      <View style={[AppStyles.flex_1]}>
        <IconText iconGroup="general" iconName={iconName} label={label} primaryText={primaryText} />
      </View>
    );
  };

  const renderSteps = () => {
    const value = totalSteps ? NumberString.toLocaleString(totalSteps) : '-';
    return renderIconText('steps_small', t('workoutTracker.Step'), value);
  };

  const renderHeartRate = () => {
    const value = heatRate ? (
      <Text customStyle={[Styles.iconText]}>
        <Trans
          defaults={t('workoutTracker.bpm', {
            bpm: NumberString.toLocaleString(heatRate),
          })}
          parent={RnText}
          components={[<RnText style={[Styles.bold]} />]}
        />
      </Text>
    ) : (
      '-'
    );
    return renderIconText('heart_rate_small', t('workoutTracker.Avg heart rate'), value);
  };

  const renderSleepTaken = () => {
    const value = sleepHours || '-';
    return renderIconText('sleep', t('workoutTracker.Sleep taken'), value);
  };

  const renderIconTexts = () => {
    return (
      <View style={[AppStyles.marginVertical_20]}>
        <Text customStyle={[Styles.today]}>{t('workoutTracker.Today')}</Text>
        <View
          style={[
            AppStyles.row,
            AppStyles.alignItemCenter,
            AppStyles.justifyContentSpaceBetween,
            AppStyles.marginVertical_10,
          ]}>
          {renderSteps()}
          {renderHeartRate()}
        </View>
        <View
          style={[
            AppStyles.row,
            AppStyles.alignItemCenter,
            AppStyles.justifyContentSpaceBetween,
            AppStyles.marginTop_10,
          ]}>
          {renderSleepTaken()}
        </View>
      </View>
    );
  };

  const renderTitleGroup = () => {
    return (
      <View
        style={[AppStyles.row, AppStyles.alignItemCenter, AppStyles.justifyContentSpaceBetween]}>
        {title && <Icon group="workTrackers" name={title.toLowerCase()} />}
        <View style={[AppStyles.column, AppStyles.flex_1, AppStyles.marginLeft_20]}>
          <Text customStyle={[Styles.title]}>{title}</Text>
          {syncTime !== null && (
            <Text customStyle={[Styles.subTitle]}>
              <Trans
                defaults={t('workoutTracker.Last synced', {minutes: syncTime})}
                parent={RnText}
                components={[<RnText style={[Styles.bold]} />]}
              />
            </Text>
          )}
        </View>
      </View>
    );
  };

  const renderWorkoutTrackerDetail = () => {
    return allLoading ? (
      <LazyLoad style={[AppStyles.marginVertical_20, AppStyles.marginHorizontal_20]}>
        <View style={[AppStyles.column]}>
          <View style={[Styles.lazyloadHeaderContainer]} />
        </View>
      </LazyLoad>
    ) : (
      <View style={[AppStyles.marginHorizontal_20, AppStyles.marginTop_20]}>
        {renderTitleGroup()}
        {renderIconTexts()}
      </View>
    );
  };

  const renderFilter = () => {
    const chipFilter = () => {
      return (
        <ChipFilter
          data={FILTERS}
          selectedName={selectedFilter}
          onPressChip={(item) => {
            const {name} = item;
            setSelectedFilter(name);
          }}
        />
      );
    };
    return <View style={[AppStyles.paddingTop_20]}>{chipFilter()}</View>;
  };

  const separator = () => {
    return <View style={Styles.separator} />;
  };

  const renderList = () => {
    return fitnessListLoading ? (
      <LazyLoad style={[AppStyles.marginVertical_20, AppStyles.marginHorizontal_20]}>
        <View style={[AppStyles.column]}>
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
          <View style={[Styles.lazyloadContainer]} />
        </View>
      </LazyLoad>
    ) : (
      <FlatList
        bounces={false}
        data={filteredFitnessList}
        renderItem={({item}) => {
          const {activityId, awardedDate, points} = item;
          const targetDate = awardedDate.split('+');
          const date = DateTime.format(new Date(targetDate[0]), 'MMM d, yyyy');
          return <DateListItem dateStr={date} title={activityId} points={points} />;
        }}
        ItemSeparatorComponent={separator}
        keyExtractor={(item, index) => `${index}`}
      />
    );
  };

  const renderBody = () => {
    return (
      <View style={[AppStyles.flex_1]}>
        {renderWorkoutTrackerDetail()}
        <View style={[AppStyles.flex_1, Styles.listGroup]}>
          {renderFilter()}
          {renderList()}
        </View>
      </View>
    );
  };

  return (
    <View style={Styles.container}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderBody()}
    </View>
  );
};

export default WorkoutTrackerSummary;
