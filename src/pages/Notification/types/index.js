/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  navigation: {
    navigate: (path: string) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
  },
};
