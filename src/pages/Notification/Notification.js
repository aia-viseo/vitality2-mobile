/**
 *
 * Notification
 * @format
 *
 */

// @flow

import React from 'react';
import {View, StatusBar} from 'react-native';
import {useTranslation} from 'react-i18next';

import Navigator from '@molecules/Navigator';
import Header from '@molecules/Header';

import AppStyles from '@styles';
import {WHITE} from '@colors';

import {HEADER_HEIGHT} from './config';

import type {PropsType} from './types';

const Notification = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();
  const onPressBack = () => {
    navigation.goBack();
  };

  const renderHeader = () => {
    const content = () => {
      return (
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={t('notification.notification')}
          showTitle
        />
      );
    };
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={false}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  return (
    <View style={[AppStyles.flex_1]}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
    </View>
  );
};

export default Notification;
