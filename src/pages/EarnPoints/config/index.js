/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
export const FILTERS = [
  {id: 1, key: 'earnpoints.Recommended', field: 'status', name: 'take'},
  {id: 2, key: 'earnpoints.Points', field: 'status', name: 'points'},
  {id: 3, key: 'earnpoints.Duration', field: 'status', name: 'duration'},
];
