/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const EarnPointsStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default EarnPointsStyles;
