/**
 *
 * @format
 *
 */

// @flow

type NavigationSubpath = {
  screen: string,
  params?: any,
};

export type PropsType = {
  navigation: {
    navigate: (path: string, subpath?: NavigationSubpath) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
  },
};
