/**
 *
 * EarnPoints Page
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import {View, StatusBar, FlatList} from 'react-native';
import {useTranslation} from 'react-i18next';

import Navigator from '@molecules/Navigator';
import Header from '@molecules/Header';
import ChipFilter from '@molecules/ChipFilter';
import Card from '@molecules/Card';

import {ApplicationContext} from '@contexts/Application';
import {EarnPointContext} from '@contexts/EarnPoint';
import {WHITE} from '@colors';

import AppStyles from '@styles';
import EarnPointsStyles from './styles';

import {HEADER_HEIGHT, FILTERS} from './config';

import type {PropsType} from './types';

const EarnPoints = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();
  const {selectAssessment, selectChallenge} = useContext(ApplicationContext);
  const {earnPointList, selectedFilter, selectFilter} = useContext(EarnPointContext);

  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressItem = (item) => {
    const {title, subTitle} = item;
    const data = {
      ...item,
      title: subTitle,
    };

    if (title === 'Assessment') {
      selectAssessment(data);
      navigation.navigate('Dashboard');
      navigation.navigate('AssessmentsTab', {screen: 'AssessmentDetail', initial: false});
    } else {
      selectChallenge(data);
      navigation.navigate('Dashboard');
      navigation.navigate('ChallengesTab', {screen: 'ChallengeDetail', initial: false});
    }
  };

  const renderHeader = () => {
    const content = () => {
      return (
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={t('earnpoints.EarnPoints')}
          showTitle
        />
      );
    };
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={false}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  const renderChipFilter = () => {
    return (
      <View style={[AppStyles.marginTop_20]}>
        <ChipFilter
          data={FILTERS}
          selectedName={selectedFilter}
          onPressChip={(item) => {
            const {name} = item;
            selectFilter(name);
          }}
          hideBackground
          withContainer={false}
        />
      </View>
    );
  };

  const renderEarnPointsCard = (item: any) => {
    const {title, iconName, points, subTitle, isNew, duration} = item;
    return (
      <View style={AppStyles.marginLeft_20}>
        <Card
          variationNumber={8}
          title={title}
          subTitle={subTitle}
          points={points}
          isNew={isNew}
          iconName={iconName}
          duration={duration}
          onPressItem={() => {
            onPressItem(item);
          }}
        />
      </View>
    );
  };

  const renderEarnPoints = () => {
    return (
      <View style={[AppStyles.marginTop_10, AppStyles.flex_1]}>
        <FlatList
          data={earnPointList}
          renderItem={({item}) => renderEarnPointsCard(item)}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  };

  return (
    <View style={[EarnPointsStyles.container]}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderChipFilter()}
      {renderEarnPoints()}
    </View>
  );
};

export default EarnPoints;
