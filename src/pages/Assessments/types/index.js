/**
 *
 * @format
 *
 */

// @flow

type NavigationSubpath = {
  screen: string,
  params?: any,
};

export type PropsType = {
  navigation: {
    navigate: (path: string, subpath?: NavigationSubpath) => void,
    goBack: (path?: string) => void,
  },
};

export type AssessmentType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points: number,
  description?: string,
  keyName?: string,
  assessmentName?: string,
  healthHabitCategory?: string,
  status?: string,
  duration?: string,
  rewardType?: string,
  rewardValue?: string,
  order?: string,
  challengeDesc?: string,
  assessmentType?: string,
  awardedDate?: string,
  id?: string,
};
