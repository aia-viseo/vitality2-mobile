/**
 *
 * Assessment Page
 * @format
 *
 */

// @flow

import React, {useContext, useState, useRef, useEffect} from 'react';
import {StatusBar, View, FlatList} from 'react-native';
import Animated from 'react-native-reanimated';
import {useTranslation} from 'react-i18next';
import lodash from 'lodash';

import {ApplicationContext} from '@contexts/Application';
import {AssessmentsContext} from '@contexts/Assessments';
import {OnlineAssessmentsContext} from '@contexts/OnlineAssessments';

import AppStyles from '@styles';
import Card from '@molecules/Card';
import VitalityAgeCard from '@molecules/VitalityAgeCard';
import ChipFilter from '@molecules/ChipFilter';
import Header from '@molecules/Header';
import GetOS from '@platform';
import {HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT} from '@constants/Header';

import type {PropsType, AssessmentType} from './types';
import {FILTERS, HIDE_BACKGROUND_LIMIT} from './config';

const isIOS = GetOS.getInstance() === 'ios';

const Assessments = (props: PropsType) => {
  const {assessments, vitalityAge, refreshVitalityAge, selectAssessment} = useContext(
    ApplicationContext
  );
  const {completedOnlineAssessments} = useContext(OnlineAssessmentsContext);
  const {clearAssessmentDetail} = useContext(AssessmentsContext);
  const [scrollY] = useState(new Animated.Value(0));
  const [hideBackground, setHideBackground] = useState(true);
  const [selectedFilter, setSelectedFilter] = useState('take');
  const scrollViewRef = useRef();
  const {t} = useTranslation();
  const {navigation} = props;

  const headerHeightDiff = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
  const isAgeCardEmpty = lodash.isEmpty(vitalityAge);

  const renderVitalityAgeCard = () => {
    const {age, diffYears} = vitalityAge;
    return <VitalityAgeCard age={age} diffYears={diffYears} unRead />;
  };

  const navigateAssessment = (data: any) => {
    clearAssessmentDetail(data.data.keyName);
    selectAssessment(data.data);
    navigation.navigate('AssessmentsTab', {
      screen: 'AssessmentDetail',
      initial: false,
      params: data,
    });
  };

  useEffect(() => {
    refreshVitalityAge();
  }, []);

  const renderAssessmentListItem = (item: AssessmentType) => {
    const {title, points, iconName, id} = item;
    const listItem = (
      <View style={AppStyles.marginLeft_20}>
        <Card
          variationNumber={1}
          title={title}
          iconName={iconName}
          points={points}
          onPressItem={() => navigateAssessment({data: item})}
        />
      </View>
    );

    let returnItem = null;
    FILTERS.forEach((filter) => {
      const {field, name} = filter;
      if (name === selectedFilter) {
        switch (name) {
          case 'take': {
            if (completedOnlineAssessments.indexOf(id) === -1) {
              returnItem = listItem;
            }
            break;
          }
          case 'update': {
            if (completedOnlineAssessments.indexOf(id) !== -1) {
              returnItem = listItem;
            }
            break;
          }
          default: {
            if (item[field] === selectedFilter) {
              returnItem = listItem;
            }
            break;
          }
        }
      }
    });
    return returnItem;
  };

  const renderAssessmentList = () => {
    return (
      <FlatList
        data={assessments}
        renderItem={({item}) => renderAssessmentListItem(item)}
        keyExtractor={(item, index) => `${index}`}
      />
    );
  };

  const renderHeader = () => {
    return (
      <Header
        setTop
        fixHeight={isAgeCardEmpty ? HEADER_MIN_HEIGHT : HEADER_MAX_HEIGHT}
        cardContent={!isAgeCardEmpty && renderVitalityAgeCard}
        title={t('general.assessmentTitle')}
      />
    );
  };

  const renderChipFilter = () => {
    const chipFilter = () => {
      return (
        <ChipFilter
          data={FILTERS}
          selectedName={selectedFilter}
          onPressChip={(item) => {
            const {name} = item;
            setSelectedFilter(name);
          }}
          headerHeightDiff={headerHeightDiff}
          withContainer
        />
      );
    };
    if (isAgeCardEmpty) {
      return <View style={{marginTop: isIOS ? 50 : 85}}>{chipFilter()}</View>;
    }
    return <View style={{marginTop: isIOS ? 0 : 30}}>{chipFilter()}</View>;
  };

  const renderStatusBar = () => {
    return <StatusBar barStyle="light-content" />;
  };

  const renderAnimatedCode = () => {
    return (
      <Animated.Code>
        {() =>
          Animated.call([scrollY], ([val]: number[]) => {
            if (hideBackground) {
              if (val >= HIDE_BACKGROUND_LIMIT) {
                setHideBackground(false);
              }
            } else if (val <= HIDE_BACKGROUND_LIMIT) {
              setHideBackground(true);
            }
          })
        }
      </Animated.Code>
    );
  };

  const renderBody = () => {
    return (
      <Animated.ScrollView
        ref={scrollViewRef}
        bounces={false}
        style={[AppStyles.marginTop_minus_100]}
        scrollEventThrottle={16}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {y: scrollY}}}], {
          useNativeDriver: true,
        })}>
        {renderAssessmentList()}
        {renderAnimatedCode()}
      </Animated.ScrollView>
    );
  };

  return (
    <View style={AppStyles.flex_1}>
      {renderStatusBar()}
      {renderHeader()}
      {renderChipFilter()}
      {renderBody()}
    </View>
  );
};

export default Assessments;
