/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  COLOR_AIA_GREEN,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREY_500,
  COLOR_AIA_ICON_GRAY,
  COLOR_AIA_GREY_50,
} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';
const styles = StyleSheet.create({
  title: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(22),
    fontWeight: isIOS ? '600' : 'bold',
    flexWrap: 'wrap',
  },
  subTitle: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(14),
    flexWrap: 'wrap',
  },
  summaryContainer: {
    backgroundColor: COLOR_AIA_GREY_50,
  },
  summaryTitle: {
    fontSize: RFValue(12),
    color: COLOR_AIA_GREEN,
    fontWeight: isIOS ? '600' : 'bold',
  },
  summaryValue: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(16),
  },
  summaryValuePoint: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(20),
    fontWeight: isIOS ? '600' : 'bold',
  },
  sectionBorder: {
    borderBottomWidth: 1,
    borderBottomColor: COLOR_AIA_ICON_GRAY,
  },
  sectionTitle: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(16),
    fontWeight: isIOS ? '600' : 'bold',
  },
  sectionDescription: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
    flexWrap: 'wrap',
  },
  itemTitle: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(14),
  },
  green: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREEN,
  },
  bold: {
    fontWeight: isIOS ? '600' : 'bold',
  },
  rightLabel: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(14),
  },
});

export default styles;
