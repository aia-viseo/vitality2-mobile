/**
 *
 * @format
 *
 */

// @flow

type NavigationSubPath = {
  screen: string,
};

export type PropsType = {
  navigation: {
    navigate: (path: string, subPath?: NavigationSubPath) => void,
    goBack: (path?: string) => void,
  },
};
