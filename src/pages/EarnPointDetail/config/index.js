/**
 *
 * @format
 *
 */

// @flow

export const IN_PROGRESS = 'INPROGRESS';
export const COMPLETED_IN_TIME = 'COMPLETEDINTIME';
export const COMPLETED_OUT_OF_TIME = 'COMPLETEDOUTOFTIME';
export const NO_ACHIEVED = 'NOACHIEVED';

export const STATUS = {
  INPROGRESS: 'In progress',
  COMPLETEDINTIME: 'Goal exceeded',
  COMPLETEDOUTOFTIME: 'Goal exceeded',
  NO_ACHIEVED: 'Goal missed',
};
