/**
 *
 * Challenge Weekly Progress
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import {SafeAreaView, StatusBar, View, TouchableWithoutFeedback, ScrollView} from 'react-native';
import {useTranslation} from 'react-i18next';

import AppStyles from '@styles';
import Text from '@atoms/Text';
import Icon from '@atoms/Icon';
import {ChallengesContext} from '@contexts/Challenges';

import HeaderFlowStyles from '@organisms/HeaderFlow/styles';
import type {PropsType} from './types';
import Styles from './styles';

const EarnPointDetail = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();
  const {steps, heartRates, earnedToday, dailyMax} = useContext(ChallengesContext);
  const onPressBack = () => {
    navigation.goBack();
  };

  const renderBackButton = () => {
    return (
      <View
        style={[
          AppStyles.row,
          AppStyles.marginHorizontal_10,
          AppStyles.marginTop_15,
          AppStyles.firstTopLayer,
        ]}>
        <TouchableWithoutFeedback onPress={onPressBack}>
          <View style={HeaderFlowStyles.closeButton}>
            <Icon group="general" name="cross" width={24} height={24} />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <View
        style={[
          AppStyles.column,
          AppStyles.alignItemCenter,
          AppStyles.marginTop_60,
          AppStyles.marginHorizontal_20,
        ]}>
        <Text customStyle={[Styles.title, AppStyles.textAlignCenter]}>
          {t('challenge.Earn points as you workout')}
        </Text>
        <Text customStyle={[Styles.subTitle, AppStyles.textAlignCenter]}>
          {t(`challenge.We'll take the highest points you earn each day`)}
        </Text>
      </View>
    );
  };

  const renderSummary = () => {
    return (
      <View
        style={[
          AppStyles.row,
          AppStyles.justifyContentSpaceBetween,
          AppStyles.paddingVertical_20,
          AppStyles.paddingHorizontal_60,
          AppStyles.marginTop_20,
          Styles.summaryContainer,
        ]}>
        <View style={[AppStyles.column, AppStyles.alignItemCenter]}>
          <Text customStyle={[Styles.summaryTitle]}>{t('challenge.Earned today')}</Text>
          <Text customStyle={[Styles.summaryValue]}>
            <Text customStyle={[Styles.summaryValuePoint]}>{earnedToday}</Text> pts
          </Text>
        </View>
        <View style={[AppStyles.column, AppStyles.alignItemCenter]}>
          <Text customStyle={[Styles.summaryTitle, AppStyles.textAlignCenter]}>
            {t('challenge.Daily maximum')}
          </Text>
          <Text customStyle={[Styles.summaryValue]}>
            <Text customStyle={[Styles.summaryValuePoint]}>{dailyMax}</Text> pts
          </Text>
        </View>
      </View>
    );
  };

  const renderStepItem = (step: {title: string, value: string}, index: number) => {
    const {title, value} = step;

    return (
      <View
        key={index}
        style={[
          AppStyles.row,
          AppStyles.alignItemCenter,
          AppStyles.justifyContentSpaceBetween,
          AppStyles.marginTop_10,
        ]}>
        <Text customStyle={[Styles.itemTitle]}>{title}</Text>
        <Text customStyle={[Styles.green]}>
          <Text customStyle={[Styles.green, Styles.bold]}>{value}</Text> pts
        </Text>
      </View>
    );
  };

  const renderStepsSection = () => {
    return (
      <View style={[AppStyles.paddingBottom_20, Styles.sectionBorder]}>
        <View style={[AppStyles.row, AppStyles.alignItemCenter]}>
          <Icon group="general" name="steps" />
          <View style={[AppStyles.marginLeft_15, AppStyles.flex_1]}>
            <Text customStyle={[Styles.sectionTitle]}>{t('challenge.Steps')}</Text>
            <Text customStyle={[Styles.sectionDescription]}>
              {t('challenge.Take steps to earn points')}
            </Text>
          </View>
        </View>
        {steps.map((step, index) => {
          return renderStepItem(step, index);
        })}
      </View>
    );
  };

  const renderHeartRateItem = (
    step: {title: string, subTitle: string, value: string},
    index: number
  ) => {
    const {title, subTitle, value} = step;

    return (
      <View
        key={index}
        style={[
          AppStyles.row,
          AppStyles.alignItemCenter,
          AppStyles.justifyContentSpaceBetween,
          AppStyles.marginTop_10,
        ]}>
        <View style={[AppStyles.column]}>
          <Text customStyle={[Styles.itemTitle]}>{title}</Text>
          <Text customStyle={[Styles.sectionDescription]}>{subTitle}</Text>
        </View>
        <Text customStyle={[Styles.green]}>
          <Text customStyle={[Styles.green, Styles.bold]}>{value}</Text> pts
        </Text>
      </View>
    );
  };

  const renderHeartRateSection = () => {
    return (
      <View style={[AppStyles.paddingBottom_20, Styles.sectionBorder, AppStyles.marginTop_20]}>
        <View style={[AppStyles.row, AppStyles.alignItemCenter]}>
          <Icon group="general" name="heart_rate" />
          <View style={[AppStyles.marginLeft_15, AppStyles.flex_1]}>
            <Text customStyle={[Styles.sectionTitle]}>{t('challenge.Heart rate')}</Text>
            <Text customStyle={[Styles.sectionDescription]}>
              {t('challenge.Keep a % of the max')}
            </Text>
          </View>
        </View>
        {heartRates.map((step, index) => {
          return renderHeartRateItem(step, index);
        })}
      </View>
    );
  };

  const renderEarnDetail = () => {
    return (
      <ScrollView bounces={false} style={[AppStyles.flex_1, AppStyles.padding_20]}>
        {renderStepsSection()}
        {renderHeartRateSection()}
      </ScrollView>
    );
  };

  const renderBody = () => {
    return (
      <View style={[AppStyles.flex_1, AppStyles.marginTop_minus_40]}>
        {renderHeader()}
        {renderSummary()}
        {renderEarnDetail()}
      </View>
    );
  };

  return (
    <SafeAreaView style={[AppStyles.flex_1]}>
      <StatusBar barStyle="dark-content" />
      {renderBackButton()}
      {renderBody()}
    </SafeAreaView>
  );
};

export default EarnPointDetail;
