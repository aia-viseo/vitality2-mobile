/**
 *
 *
 * @format
 *
 */

// @flow

type NavigationType = {
  navigate: (path: string) => void,
  goBack: (path?: string) => void,
  dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
};

export type PropsType = {
  navigation: {
    navigate: (path: string) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => NavigationType,
  },
};
