/**
 *
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
export const UPLOAD_DATE_TIME = 'UPLOAD_DATE_TIME';
