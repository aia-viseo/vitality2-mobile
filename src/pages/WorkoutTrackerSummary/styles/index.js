/**
 *
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_DARK_GREY, WHITE, COLOR_AIA_GREY_50, COLOR_AIA_GREY_500} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const WorkoutTrackerStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: RFValue(18),
    fontWeight: isIOS ? '600' : 'bold',
    color: COLOR_AIA_DARK_GREY,
  },
  subTitle: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREY_500,
  },
  bold: {
    fontWeight: isIOS ? '600' : 'bold',
  },
  label: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREY_500,
  },
  value: {
    fontSize: RFValue(22),
    fontWeight: isIOS ? '600' : 'bold',
    color: COLOR_AIA_DARK_GREY,
  },
  cardContainer: {
    backgroundColor: WHITE,
  },
  cardBottom: {
    backgroundColor: COLOR_AIA_GREY_50,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
});

export default WorkoutTrackerStyles;
