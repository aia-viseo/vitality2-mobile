/**
 *
 * Workout Tracker Page
 * @format
 *
 */

// @flow

import React, {useContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {View, StatusBar, Text as RnText, TouchableWithoutFeedback} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import Text from '@atoms/Text';
import Icon from '@atoms/Icon';
import Navigator from '@molecules/Navigator';
import Header from '@molecules/Header';
import Card from '@molecules/Card/Card';

import {ApplicationContext} from '@contexts/Application';
import {WorkoutTrackerContext} from '@contexts/WorkoutTracker';

import {WHITE} from '@colors';
import AppStyles from '@styles';

import NumberString from '@core/handler/NumberString';
import DateTime from '@core/handler/DateTime';
import GetOS from '@platform';

import {HEADER_HEIGHT, UPLOAD_DATE_TIME} from './config';

import type {PropsType} from './types';
import WorkoutTrackerStyles from './styles';

const isIOS = GetOS.getInstance() === 'ios';
const WorkoutTrackerSummary = (props: PropsType) => {
  const {navigation} = props;
  const [syncTime, setSyncTime] = useState(null);
  const {syncHealthData} = useContext(ApplicationContext);
  const {workoutTrackerDataList, setSelectedWorkoutData} = useContext(WorkoutTrackerContext);
  const {totalSteps, avgHeartRate, sleepTaken} = syncHealthData || {};

  const {t} = useTranslation();

  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressCard = (workoutData) => {
    setSelectedWorkoutData(workoutData);
    navigation.navigate('WorkoutTrackerDetail');
  };

  const getUploadDateTime = () => {
    AsyncStorage.getItem(UPLOAD_DATE_TIME).then((uploadDateTime: string) => {
      if (uploadDateTime) {
        const timeDifference = DateTime.differenceInMilliseconds(new Date(uploadDateTime));
        const diffMins = (timeDifference / (1000 * 60)) * -1;
        setSyncTime(parseInt(diffMins, 10));
      }
    });
  };

  useEffect(() => {
    getUploadDateTime();
  }, []);

  const renderHeader = () => {
    const content = () => {
      return (
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={t('workoutTracker.workoutTracker')}
          showTitle
        />
      );
    };
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={false}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  const renderItem = (
    title: string,
    time?: number | null,
    steps: string,
    heartRate: number,
    sleepHours?: number
  ) => {
    return (
      <Card customPadding={AppStyles.padding_0}>
        <View style={[AppStyles.flex_1]}>
          <View
            style={[
              AppStyles.row,
              AppStyles.alignItemCenter,
              AppStyles.justifyContentSpaceBetween,
              AppStyles.padding_20,
            ]}>
            {title && <Icon group="workTrackers" name={title.toLowerCase()} />}
            <View style={[AppStyles.column, AppStyles.flex_1, AppStyles.marginLeft_20]}>
              <Text customStyle={[WorkoutTrackerStyles.title]}>{title}</Text>
              {syncTime !== null && (
                <Text customStyle={[WorkoutTrackerStyles.subTitle]}>
                  <Trans
                    defaults={t('workoutTracker.Last synced', {minutes: time})}
                    parent={RnText}
                    components={[<RnText style={[WorkoutTrackerStyles.bold]} />]}
                  />
                </Text>
              )}
            </View>
            <Icon group="general" name="arrow_right" />
          </View>
          <View
            style={[
              AppStyles.row,
              AppStyles.alignItemCenter,
              AppStyles.paddingVertical_20,
              WorkoutTrackerStyles.cardBottom,
            ]}>
            <View style={[AppStyles.flex_1]}>
              <Text customStyle={[WorkoutTrackerStyles.label, AppStyles.textAlignCenter]}>
                {t('workoutTracker.Step')}
              </Text>
              <Text customStyle={[WorkoutTrackerStyles.value, AppStyles.textAlignCenter]}>
                {steps ? NumberString.toLocaleString(steps) : '-'}
              </Text>
            </View>
            <View style={[AppStyles.flex_1]}>
              <Text customStyle={[WorkoutTrackerStyles.label, AppStyles.textAlignCenter]}>
                {t('workoutTracker.Avg heart rate')}
              </Text>
              <Text customStyle={[WorkoutTrackerStyles.value, AppStyles.textAlignCenter]}>
                {heartRate ? NumberString.toLocaleString(heartRate) : '-'}
              </Text>
            </View>
            {sleepHours !== undefined && sleepHours !== null && (
              <View style={[AppStyles.flex_1]}>
                <Text customStyle={[WorkoutTrackerStyles.label, AppStyles.textAlignCenter]}>
                  {t('workoutTracker.Sleep taken')}
                </Text>
                <Text customStyle={[WorkoutTrackerStyles.value, AppStyles.textAlignCenter]}>
                  {sleepHours ? NumberString.toLocaleString(sleepHours) : '-'}
                </Text>
              </View>
            )}
          </View>
        </View>
      </Card>
    );
  };

  const renderLinkedDevices = () => {
    return (
      <View style={[AppStyles.marginTop_20, AppStyles.paddingHorizontal_20]}>
        <TouchableWithoutFeedback
          onPress={() =>
            onPressCard({
              title: isIOS ? 'Apple Health' : 'Samsung Health',
              syncTime,
              totalSteps,
              avgHeartRate,
              sleepTaken,
            })
          }>
          <View>
            {renderItem(
              isIOS ? 'Apple Health' : 'Samsung Health',
              syncTime,
              totalSteps,
              avgHeartRate,
              sleepTaken
            )}
          </View>
        </TouchableWithoutFeedback>

        <View style={[AppStyles.marginTop_20]}>
          {workoutTrackerDataList.map((workoutTrackerDevice, index) => {
            const {title, steps, heatRate, sleepHours} = workoutTrackerDevice;
            const key = index;
            return (
              <TouchableWithoutFeedback
                onPress={() => onPressCard({title, syncTime, steps, heatRate, sleepHours})}
                key={key}>
                <View>{renderItem(title, syncTime, steps, heatRate, sleepHours)}</View>
              </TouchableWithoutFeedback>
            );
          })}
        </View>
      </View>
    );
  };

  return (
    <View style={[WorkoutTrackerStyles.container]}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderLinkedDevices()}
    </View>
  );
};

export default WorkoutTrackerSummary;
