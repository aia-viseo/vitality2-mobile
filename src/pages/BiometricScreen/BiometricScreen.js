/**
 *
 * BiometricSetup
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import {SafeAreaView, View, Linking, TouchableOpacity} from 'react-native';
import {useTranslation} from 'react-i18next';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {AuthContext} from '@contexts/Auth';
import {GlobalContext} from '@contexts/Global';

import Icon from '@atoms/Icon';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import BiometricValidator from '@atoms/Biometric/BiometricValidator';

import AppStyles from '@styles';

import BiometricScreenStyles from './styles';
import type {PropsType} from './types';

const BiometricSetup = (props: PropsType) => {
  const {membershipNumber, setUsesBiometric} = useContext(AuthContext);
  const {setShowActivityIndicator, setMsgSnackbarProps} = useContext(GlobalContext);
  const {t} = useTranslation();
  const {navigation} = props;

  const bioIcon2 = <Icon group="general" name="face_recognition" height={24} width={24} />;

  const createBioKey = () => {
    BiometricValidator.createBioKey(
      () => {
        setUsesBiometric(true);
        setShowActivityIndicator(false);
        setMsgSnackbarProps('Public key successfully created.');
        navigation.navigate('Application');
      },
      () => {
        setMsgSnackbarProps('Error in creating a key.');
      }
    );
  };

  const handleBioSetup = () => {
    BiometricValidator.isBiometricsAvailable().then((isAvailable: boolean) => {
      if (isAvailable) {
        setShowActivityIndicator(true);
        BiometricValidator.checkBioKeyExist(
          (bioKeyExists: boolean) => {
            if (bioKeyExists) {
              setShowActivityIndicator(false);
              navigation.navigate('Application');
            }
          },
          () => {
            createBioKey();
          }
        );
      } else {
        Linking.openSettings();
      }
    });
  };

  const navigateToApplication = () => {
    navigation.navigate('Application');
  };

  const handleClear = () => {
    BiometricValidator.deleteKeys(
      () => {
        setMsgSnackbarProps('Successfully deleted Biometric keys.');
        setUsesBiometric(false);
        navigateToApplication();
      },
      () => {
        setMsgSnackbarProps('No more keys to delete');
        setUsesBiometric(false);
        navigateToApplication();
      }
    );
  };

  const goBack = () => {
    navigation.goBack();
  };

  const renderSetup = () => {
    return (
      <SafeAreaView style={{height: '100%'}}>
        <View style={BiometricScreenStyles.closeContainer}>
          <TouchableOpacity
            onPress={() => {
              goBack();
            }}>
            <View style={BiometricScreenStyles.closeButton}>
              <MaterialIcon size={24} name="close" />
            </View>
          </TouchableOpacity>
        </View>

        <Input
          label={t('login.membershipNumber')}
          placeholder={t('login.membershipNumber')}
          underlineColor="transparent"
          customStyle={{margin: 20}}
          defaultValue={membershipNumber}
          value={membershipNumber}
        />
        <View style={[BiometricScreenStyles.buttons]}>
          <Button
            variation="contained"
            accessibilityLabel="Handle Bio Setup"
            onPress={handleBioSetup}
            buttonIcon={bioIcon2}
            buttonStyle={[AppStyles.generalButton, AppStyles.marginBottom_20]}>
            Set up now
          </Button>
          <Button
            variation="outlined"
            accessibilityLabel="Handle Clear"
            onPress={handleClear}
            buttonIcon={bioIcon2}
            buttonStyle={[
              AppStyles.generalButton,
              AppStyles.generalOutlinedButton,
              AppStyles.fullWidthButton,
            ]}>
            Skip
          </Button>
        </View>
      </SafeAreaView>
    );
  };

  return <SafeAreaView>{renderSetup()}</SafeAreaView>;
};

export default BiometricSetup;
