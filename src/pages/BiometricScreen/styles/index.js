/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const BiometricScreenStyles = StyleSheet.create({
  closeContainer: {
    padding: 20,
  },
  closeButton: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    borderRadius: 45,
    padding: 10,
    backgroundColor: '#FFF',
    width: 45,
    elevation: 5,
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
  buttons: {
    margin: 20,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '90%',
  },
});

export default BiometricScreenStyles;
