/**
 *
 * @format
 *
 */

// @flow

export const BIOMETRIC_PAYLOAD_KEY = 'BIOMETRIC_PAYLOAD_KEY';

export const USES_BIOMETRIC_KEY = 'USES_BIOMETRIC_KEY';
