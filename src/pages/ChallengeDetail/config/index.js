/**
 *
 * @format
 *
 */

// @flow

import {isIphoneX} from 'react-native-iphone-x-helper';

export const HEADER_HEIGHT = isIphoneX() ? 105 : 80;
export const ICON_TEXTS = [
  {
    iconGroup: 'linkSection',
    iconName: 'health_report',
    label: 'Spend approx.',
    primaryText: '5',
    secondaryText: 'minutes',
  },
  {
    iconGroup: 'linkSection',
    iconName: 'health_report',
    label: 'Get',
    primaryText: '500',
    secondaryText: 'points',
  },
  {
    iconGroup: 'linkSection',
    iconName: 'health_report',
    label: 'Retake for points on',
    primaryText: 'Oct 29,',
    secondaryText: '2020',
  },
];
export const LinearLocations: number[] = [0, 0.62, 1];
export {LinearLocations as default};
export const NO_ACHIEVED = 'NOACHIEVED';
