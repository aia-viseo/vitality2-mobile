/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  WHITE,
  COLOR_AIA_GD_ACTIVE_1,
  COLOR_AIA_GREEN,
  COLOR_AIA_DARK_GREY,
  COLOR_AIA_GREY,
  COLOR_AIA_GD_ACTIVE_2,
  COLOR_AIA_GD_RED_1,
} from '@colors';
import GetOS from '@platform';

export const LinearColors = [COLOR_AIA_GREEN, COLOR_AIA_GD_ACTIVE_1, COLOR_AIA_GD_ACTIVE_2];

const isIOS = GetOS.getInstance() === 'ios';

const ChallengeStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    marginTop: isIOS ? -55 : -80,
  },
  iconTextContainer: {
    alignItems: 'flex-start',
    width: '50%',
  },
  iconTextsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    overflow: 'hidden',
  },
  infoContainer: {
    backgroundColor: WHITE,
  },
  textWithStatusContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleText: {
    flex: 1,
    fontSize: RFValue(30),
    fontWeight: '700',
    flexWrap: 'wrap',
  },
  statusText: {
    color: COLOR_AIA_GD_ACTIVE_1,
    fontSize: RFValue(18),
  },
  statusStyle: {
    borderColor: COLOR_AIA_GD_ACTIVE_1,
    borderWidth: 1.5,
  },
  assessmentDescription: {
    fontSize: RFValue(17),
    color: COLOR_AIA_DARK_GREY,
  },
  progressBarTitleContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  minPts: {
    color: COLOR_AIA_GREEN,
    fontSize: RFValue(16),
  },
  maxPts: {
    color: COLOR_AIA_GREY,
    fontSize: RFValue(16),
  },
  weeksProgress: {
    fontSize: RFValue(16),
    fontWeight: '700',
    color: COLOR_AIA_DARK_GREY,
  },
  daysLeft: {
    color: COLOR_AIA_GREY,
    fontSize: RFValue(16),
  },
  details: {
    color: COLOR_AIA_GD_RED_1,
    fontSize: RFValue(16),
  },
});

export default ChallengeStyles;
