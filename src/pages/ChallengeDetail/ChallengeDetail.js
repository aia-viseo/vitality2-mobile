/**
 *
 * Challenge Detail Page
 * @format
 *
 */

// @flow

import React, {useEffect, useState, useContext} from 'react';
import {View, ScrollView, StatusBar, TouchableWithoutFeedback} from 'react-native';
import {useTranslation} from 'react-i18next';
import {Chip} from 'react-native-paper';
import type {Element} from 'react';
import {differenceInDays} from 'date-fns';

import {GlobalContext} from '@contexts/Global';
import {ApplicationContext} from '@contexts/Application';
import {ChallengesContext} from '@contexts/Challenges';
import Text from '@atoms/Text';
import Icon from '@atoms/Icon';
import Slider from '@atoms/Slider';
import Header from '@molecules/Header';
import Navigator from '@molecules/Navigator';
import IconText from '@molecules/IconText';
import ProgressBar from '@molecules/ProgressBar';
import Card from '@molecules/Card';
import AppStyles from '@styles';
import {WHITE} from '@colors';

import {
  greenLinearLocations,
  redLinearLocations,
  redLinearColors,
  greenLinearColors,
} from '@core/config';

import {HEADER_HEIGHT, ICON_TEXTS, NO_ACHIEVED} from './config';
import type {PropsType, IconTextType} from './types';
import ChallengeStyles from './styles';

const ChallengeDetail = (props: PropsType) => {
  const {navigation} = props;
  const {t} = useTranslation();
  const [hideBackground, setHideBackground] = useState(true);
  const [selectedWeeklyIndex, setSelectedWeeklyIndex] = useState(0);

  const {setMsgSnackbarProps} = useContext(GlobalContext);
  const {selectedChallenge} = useContext(ApplicationContext);
  const {challengePeriodList} = useContext(ChallengesContext);

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  useEffect(() => {
    setSelectedWeeklyIndex(challengePeriodList.length - 1);
  }, [challengePeriodList]);

  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressDetail = () => {
    if (challengePeriodList && challengePeriodList.length > 0) {
      navigation.navigate('ChallengeWeeklyProgress');
    } else {
      setMsgSnackbarProps('Error: No Data');
    }
  };

  const onPressEarn = () => {
    navigation.navigate('EarnPointDetail');
  };

  const renderHeader = () => {
    const {title} = selectedChallenge;
    const content = () => {
      return (
        <Navigator
          type="back"
          onPressItem={onPressBack}
          title={title}
          showTitle={!hideBackground}
        />
      );
    };
    return (
      <Header
        fixHeight={HEADER_HEIGHT}
        hideBackground={hideBackground}
        setTop
        colors={[WHITE, WHITE, WHITE]}>
        {content()}
      </Header>
    );
  };

  const renderImage = () => {
    const {imageName} = selectedChallenge;
    return (
      <View>
        <Icon group="general" name={imageName} />
      </View>
    );
  };

  const renderIconText = (item: IconTextType, index: number) => {
    const {iconGroup, iconName, label, primaryText, secondaryText} = item;
    return (
      <View
        key={`key${index}`}
        style={[AppStyles.marginBottom_20, ChallengeStyles.iconTextContainer]}>
        <IconText
          key={`key${index}`}
          iconGroup={iconGroup}
          iconName={iconName}
          label={label}
          primaryText={primaryText}
          secondaryText={secondaryText}
        />
      </View>
    );
  };

  const renderIconTexts = () => {
    const {challengeIconTexts} = selectedChallenge;
    const iconTexts = challengeIconTexts || ICON_TEXTS;

    return (
      <TouchableWithoutFeedback onPress={onPressEarn}>
        <View style={[AppStyles.marginHorizontal_20, ChallengeStyles.iconTextsContainer]}>
          {iconTexts.map((item: IconTextType, index: number) => {
            return renderIconText(item, index);
          })}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderProgressBarInfo = () => {
    const challengePeriod = challengePeriodList ? challengePeriodList[selectedWeeklyIndex] : null;
    const {periodActualValue, periodTargetValue, periodStatus} = challengePeriod || {};
    const isMissed = periodStatus === NO_ACHIEVED;
    const customLinearLocations = isMissed ? redLinearLocations : greenLinearLocations;
    const customLinearColors = isMissed ? redLinearColors : greenLinearColors;
    const {endDate} = selectedChallenge;
    const today = new Date();
    const remainingDay = differenceInDays(endDate, today);

    const actualValue = challengePeriod ? parseInt(periodActualValue, 10) : null;
    const targetValue = challengePeriod ? parseInt(periodTargetValue, 10) : null;
    const progress = actualValue && targetValue ? (actualValue / targetValue) * 100 : null;

    return (
      <View style={[AppStyles.marginHorizontal_20, AppStyles.marginBottom_20]}>
        <View style={[AppStyles.row, ChallengeStyles.progressBarTitleContainer]}>
          <Text customStyle={[ChallengeStyles.weeksProgress]}>
            {t('general.thisWeeksProgress')}
          </Text>
          <TouchableWithoutFeedback onPress={onPressDetail}>
            <View>
              <Text customStyle={[ChallengeStyles.details]}>{t('general.details')}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={[AppStyles.row, AppStyles.marginBottom_10]}>
          <Text customStyle={[ChallengeStyles.daysLeft]}>
            {remainingDay}
            {t('general.daysLeft')}
          </Text>
        </View>
        {actualValue !== null && targetValue !== null && (
          <ProgressBar
            leftLabel={`${t('general.earned')} ${actualValue} ${t('general.pointsTitle')}`}
            rightLabel={`${t('general.goal')} ${targetValue} ${t('general.pointsTitle')}`}
            progress={progress}
            customLinearLocations={customLinearLocations}
            customLinearColors={customLinearColors}
            barHeight={10}
            leftLabelStyle={ChallengeStyles.minPts}
            rightLabelStyle={ChallengeStyles.maxPts}
          />
        )}
      </View>
    );
  };

  const renderChallengeInfo = () => {
    const {description, title} = selectedChallenge;
    return (
      <View style={[ChallengeStyles.infoContainer]}>
        <View
          style={[
            AppStyles.marginVertical_20,
            AppStyles.marginHorizontal_20,
            AppStyles.row,
            ChallengeStyles.textWithStatusContainer,
          ]}>
          <Text customStyle={ChallengeStyles.titleText}>{title}</Text>
          <Chip
            textStyle={ChallengeStyles.statusText}
            style={ChallengeStyles.statusStyle}
            mode="outlined">
            {t('general.fitness')}
          </Chip>
        </View>
        {description !== ' ' && description !== '' && (
          <View style={[AppStyles.marginHorizontal_20, AppStyles.marginBottom_20]}>
            <Text customStyle={ChallengeStyles.assessmentDescription}>{description}</Text>
          </View>
        )}
        {renderIconTexts()}
        {renderProgressBarInfo()}
      </View>
    );
  };

  const handleScroll = (event: Object) => {
    const scrollY = event.nativeEvent.contentOffset.y;
    if (hideBackground) {
      if (scrollY > HEADER_HEIGHT + 20) {
        setHideBackground(false);
      }
    } else if (scrollY < HEADER_HEIGHT - 20) {
      setHideBackground(true);
    }
  };

  const renderHealthRelatedReportsSlider = () => {
    const itemWidthPercentage = 93;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: Object, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {name, iconName, description} = item;

      return (
        <Card
          key={index}
          variationNumber={1}
          name={name}
          iconName={iconName}
          description={description}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_20}>
        <Slider
          title={t('general.relatedHealthMetrics')}
          linkText={t('general.seeAllTitle')}
          data={[]}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={AppStyles.paddingLeft_20}
          onPressSeeAll={() => {}}
        />
      </View>
    );
  };

  const renderAssessmentSlider = () => {
    const itemWidthPercentage = 75;
    const slideAlign = 'start';

    const renderItem = (itemData: {item: Object, index: number}): Element<*> => {
      const {item, index} = itemData;
      const {title, points, iconName} = item;

      return (
        <Card
          key={index}
          variationNumber={1}
          title={title}
          iconName={iconName}
          points={points}
          onPressItem={() => {}}
        />
      );
    };

    return (
      <View style={AppStyles.marginTop_20}>
        <Slider
          isLoading={false}
          title={t('general.relatedAssessments')}
          linkText={t('general.seeAllTitle')}
          data={[]}
          renderItem={renderItem}
          itemWidthPercentage={itemWidthPercentage}
          slideAlign={slideAlign}
          containerCustomStyle={{paddingLeft: 20}}
          onPressSeeAll={() => {}}
        />
      </View>
    );
  };

  const renderBody = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
        style={[ChallengeStyles.body]}
        scrollEventThrottle={16}
        onScroll={handleScroll}>
        {renderImage()}
        {renderChallengeInfo()}
        {renderHealthRelatedReportsSlider()}
        {renderAssessmentSlider()}
        <View style={{height: 400}} />
      </ScrollView>
    );
  };

  return (
    <View style={ChallengeStyles.container}>
      <StatusBar barStyle="dark-content" />
      {renderHeader()}
      {renderBody()}
    </View>
  );
};

const ChallengeDetailWrapper = (props: PropsType) => {
  const {navigation} = props;

  return <ChallengeDetail navigation={navigation} />;
};

export default ChallengeDetailWrapper;
