/**
 *
 * @format
 *
 */

// @flow
export type PropsType = {
  navigation: {
    navigate: (path: string) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
  },
};

export type IconTextType = {
  iconGroup?: string,
  iconName?: string,
  label?: string,
  primaryText?: string,
  secondaryText?: string,
};
