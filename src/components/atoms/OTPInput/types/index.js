/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  containerStyle?: {},
  code?: string,
  pinCount: number,
  onCodeChanged?: (code: string) => void,
  onCodeFilled?: (code: string) => void,
  autoFocusOnLoad?: boolean,
  secureTextEntry?: boolean,
  clearInputs?: boolean,
  codeInputFieldStyle?: {},
  codeInputHighlightStyle?: {},
  placeholderCharacter?: string,
  placeholderTextColor?: string,
};
