/**
 *
 *  Test OTPInput
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import OTPInput from '../OTPInput';

test('Test OTPInput ', () => {
  const tree = renderer.create(<OTPInput pinCount={4} />).toJSON();
  expect(tree).toMatchSnapshot();
});
