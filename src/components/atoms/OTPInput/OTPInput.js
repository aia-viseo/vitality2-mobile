/**
 *
 * OtpInput
 * @format
 *
 */

// @flow

import React from 'react';
import OtpInputView from '@twotalltotems/react-native-otp-input';

import type {PropsType} from './types';
import OtpInputStyles from './styles';

const OtpInput = (props: PropsType) => {
  const {
    containerStyle,
    code,
    pinCount,
    onCodeChanged,
    onCodeFilled,
    autoFocusOnLoad,
    secureTextEntry,
    clearInputs,
    codeInputFieldStyle,
    codeInputHighlightStyle,
    placeholderCharacter,
    placeholderTextColor,
  } = props;

  return (
    <OtpInputView
      style={[OtpInputStyles.inputView, containerStyle]}
      code={code}
      pinCount={pinCount}
      onCodeChanged={onCodeChanged}
      onCodeFilled={onCodeFilled}
      autoFocusOnLoad={autoFocusOnLoad}
      secureTextEntry={secureTextEntry}
      clearInputs={clearInputs}
      codeInputFieldStyle={[OtpInputStyles.underlineStyleBase, codeInputFieldStyle]}
      codeInputHighlightStyle={codeInputHighlightStyle}
      placeholderCharacter={placeholderCharacter}
      placeholderTextColor={placeholderTextColor}
    />
  );
};

OtpInput.defaultProps = {
  containerStyle: {},
  code: '',
  pinCount: 6,
  onCodeChanged: () => {},
  onCodeFilled: () => {},
  autoFocusOnLoad: true,
  secureTextEntry: false,
  clearInputs: false,
  codeInputFieldStyle: {},
  codeInputHighlightStyle: {},
  placeholderCharacter: '',
  placeholderTextColor: '',
};

export default OtpInput;
