## OtpInput
A Input Box Component to receive code from sms

# Usage

```js
import OtpInput from 'atom/OtpInput';
<OtpInput
  containerStyle
  code
  pinCount
  onCodeChanged
  onCodeFilled
  autoFocusOnLoad
  secureTextEntry
  clearInputs
  codeInputFieldStyle
  codeInputHighlightStyle
  placeholderCharacter
  placeholderTextColor
/>
```
Key                     | Type        | Required | Description
--                      | --          | --       | --
containerStyle          | `ViewStyle` | `False`  | Style of input container.
code                    | `string`    | `False`  | Initial pin code.
pinCount                | `number`    | `True`   | Digits of pins in the OTP.
onCodeChanged           | `function`  | `False`  | Trigger when a field of the OTP is changed.
onCodeFilled            | `function`  | `False`  | Trigger when all fields of the OTP has been filled.
autoFocusOnLoad         | `boolean`   | `False`  | If keyboard is automatically brought up when OTP is loaded.
secureTextEntry         | `boolean`   | `False`  | Secure input text.
clearInputs             | `boolean`   | `False`  | If inputs are automatically cleared.
codeInputFieldStyle     | `TextStyle` | `False`  | Style of the input fields.
codeInputHighlightStyle | `TextStyle` | `False`  | Style of highlighted status for input fields.
placeholderCharacter    | `string`    | `False`  | Placeholder character to fill all inputs when the OTP is empty.
placeholderTextColor    | `string`    | `False`  | Placeholder text color of inputs.
