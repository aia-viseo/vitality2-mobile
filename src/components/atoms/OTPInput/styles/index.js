/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const OtpInputStyles = StyleSheet.create({
  inputView: {
    borderWidth: 1,
    height: 50,
  },

  underlineStyleBase: {
    color: '#000000',
    borderWidth: 0,
  },
});

export default OtpInputStyles;
