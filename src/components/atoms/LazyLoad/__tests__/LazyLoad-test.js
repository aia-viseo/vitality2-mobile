/**
 *
 * LazyLoad Test
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import LazyLoad from '../LazyLoad';

jest.useFakeTimers();

test('LazyLoad', () => {
  const child = <View />;
  const tree = renderer.create(<LazyLoad style={{margin: 20}}>{child}</LazyLoad>).toJSON();
  expect(tree).toMatchSnapshot();
});
