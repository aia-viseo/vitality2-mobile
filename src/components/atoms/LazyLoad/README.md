## Lazyload

Temporary component to be shown while data is not yet available.

### Usage

There are two ways to use this package:
with LazyLoad and View:

```javascript
import React from "react";
import {View} from "react-native";
import {LazyLoad} from "@atoms/LazyLoad";

const renderHeader = () => {
  const content = (
    <View
      style={[
        DashboardStyles.headerContent,
        AppStyles.alignItemFlexStart,
        AppStyles.paddingVertical_10,
      ]}>
      <TouchableWithoutFeedback onPress={showProfileTab}>
        <View>
          <FlexibleText row scalableText={t('header.greet')} flexibleText={userInfo.username} />
        </View>
      </TouchableWithoutFeedback>
      <MembershipBadge showLevel={showLevel} points={userInfo.points} level={userInfo.level} />
    </View>
  );
  const lazyContent = (
    <LazyLoad style={{marginTop: 20, marginHorizontal: 20}}>
      <View style={{flexDirection: 'row'}}>
        <View style={{width: 200, height: 30, borderRadius: 50}} />
        <View style={{flexDirection: 'row-reverse', width: 175}}>
          <View style={{width: 100, height: 30, borderRadius: 50}} />
        </View>
      </View>
    </LazyLoad>
  );
  return (
    <Header
      scrollY={scrollY}
      setTop
      scaleHeight={Animated.interpolate(scrollY, {
        inputRange: [0, heightDiff * 2 - 1, heightDiff * 2],
        outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
      })}>
      {Object.entries(userInfo).length > 0 ? content : lazyContent}
    </Header>
  );
};
```

or with LazyLoad and LazyLoadView

```javascript
import React from "react";
import {LazyLoad, LazyLoadView} from "@atoms/LazyLoad";

const renderHeader = () => {
  const content = (
    <View
      style={[
        DashboardStyles.headerContent,
        AppStyles.alignItemFlexStart,
        AppStyles.paddingVertical_10,
      ]}>
      <TouchableWithoutFeedback onPress={showProfileTab}>
        <View>
          <FlexibleText row scalableText={t('header.greet')} flexibleText={userInfo.username} />
        </View>
      </TouchableWithoutFeedback>
      <MembershipBadge showLevel={showLevel} points={userInfo.points} level={userInfo.level} />
    </View>
  );
  const lazyContent = (
    <LazyLoad style={{marginTop: 20, marginHorizontal: 20}}>
      <LazyLoadView style={{flexDirection: 'row'}}>
        <LazyLoadView style={{width: 200, height: 30, borderRadius: 50}} />
        <LazyLoadView style={{flexDirection: 'row-reverse', width: 175}}>
          <LazyLoadView style={{width: 100, height: 30, borderRadius: 50}} />
        </LazyLoadView>
      </LazyLoadView>
    </LazyLoad>
  );
  return (
    <Header
      scrollY={scrollY}
      setTop
      scaleHeight={Animated.interpolate(scrollY, {
        inputRange: [0, heightDiff * 2 - 1, heightDiff * 2],
        outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT, HEADER_MIN_HEIGHT],
      })}>
      {Object.entries(userInfo).length > 0 ? content : lazyContent}
    </Header>
  );
};
```

### Properties

#### LazyLoad

|      Prop       |                  Description                   |  Type  |  Default  |
| :-------------: | :--------------------------------------------: | :----: | :-------: |
| style           |      ViewStyleProps to style the layout        | object | {}        |
| backgroundColor |      Determines the color of placeholder       | string | _#E1E9EE_ |
| highlightColor  | Determines the highlight color of placeholder  | string | _#F2F8FC_ |
| speed           | Determines the animation speed in milliseconds | number |   _800_   |

#### LazyLoadView

| Prop |            Description            | Type | Default |
| :--: | :-------------------------------: | :--: | :-----: |
| any  | Any view style props was accepted | any  |
