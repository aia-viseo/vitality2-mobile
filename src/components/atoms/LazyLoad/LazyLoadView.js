/**
 *
 * LazyLoadView
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import type {PropsType} from './types';

const LazyLoadView = (props: PropsType) => {
  const {style} = props;

  return <View style={style} />;
};

export default LazyLoadView;
