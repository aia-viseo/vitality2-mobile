/**
 *
 * LazyLoad
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import type {PropsType} from './types';

const LazyLoad = (props: PropsType) => {
  const {style, children, backgroundColor, highlightColor, speed} = props;

  return (
    <View style={style}>
      <SkeletonPlaceholder
        backgroundColor={backgroundColor || '#E1E9EE'}
        highlightColor={highlightColor || '#F2F8FC'}
        speed={speed || 800}>
        {children}
      </SkeletonPlaceholder>
    </View>
  );
};

export default LazyLoad;
