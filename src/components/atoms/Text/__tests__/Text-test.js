/**
 *
 *  Test Text
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Text from '../Text';

test('Test Text ', () => {
  const tree = renderer.create(<Text customStyle={{fontSize: 20}}>Hi Test</Text>).toJSON();
  expect(tree).toMatchSnapshot();
});
