/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const TextStyles = StyleSheet.create({
  text: {
    fontSize: RFValue(16),
  },
});

export default TextStyles;
