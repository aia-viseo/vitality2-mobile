/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  children: Node,
  customStyle?: TextStyleProp,
  numberOfLines?: number,
  color?: string,
  ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip',
};
