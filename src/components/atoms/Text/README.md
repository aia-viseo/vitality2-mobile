## Text
Atom Component to render text with default config
https://reactnative.dev/docs/text
All props in Rn Text also can be used in here
# Usage
```js
import Text from '/atoms/Text';

<Text
  text
  customStyle
/>
```

# Example
```js
import Text from '/atoms/Text';

<Text
  text={"Hi Jack"}
  customStyle={{color: BLACK}}
/>
```

Key           | Type              | Required | Description                           |
------------- | ----------------- | -------- | ------------------------------------- |
text          | `string`          | `True`   | Text string to display.               |
customStyle   | `TextStyleProp`   | `False`  | Optional extra styling for the image. |
