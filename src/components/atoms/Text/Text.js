/**
 *
 * Text
 * @format
 *
 */

// @flow

import React from 'react';
import {Text} from 'react-native-paper';

import type {PropsType} from './types';
import TextStyles from './styles';

const CustomText = (props: PropsType) => {
  const {children, customStyle, numberOfLines, ellipsizeMode} = props;

  return (
    <Text
      style={[TextStyles.text, customStyle]}
      numberOfLines={numberOfLines}
      ellipsizeMode={ellipsizeMode}>
      {children}
    </Text>
  );
};

export default CustomText;
