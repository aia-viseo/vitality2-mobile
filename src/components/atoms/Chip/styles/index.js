/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_GREEN, WHITE} from '@colors';

const ChipStyles = StyleSheet.create({
  flatText: {
    color: WHITE,
  },
  outlinedText: {
    color: COLOR_AIA_GREEN,
  },
  text: {
    fontSize: RFValue(14),
    marginVertical: 0,
  },
  flatContainer: {
    backgroundColor: 'transparent',
  },
  container: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    margin: 0,
  },
  linearGradientStyle: {
    padding: 0.5,
  },
});

export default ChipStyles;
