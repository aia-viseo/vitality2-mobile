## Chip
Atom Component to render Chip

# Usage
```js
import Chip from '/atoms/Chip';

<Chip
  text
  onPress
  mode
  customLinearColors
  customLinearLocations
  chipHeight
  textStyle
  style
/>
```

# Example
```js
import Chip from '/atoms/Chip';

<Chip
  text={"Two"}
  onPress={() => {}}
  mode={'outlined'}
  customLinearColors={[#FFF, #DDD, #000]}
  customLinearLocations={[0, 0.25, 1]}
  chipHeight={20}
  textStyle={{fontSize: 20}}
  style={{height: 100}}
/>
```

Key                   | Type              | Required | Description                                                                     |
--------------------- | ----------------- | -------- | ------------------------------------------------------------------------------- |
text                  | `string`          | `True`   | Text string to display.                                                         |
onPress               | `function`        | `False`  | Function to execute on press.                                                   |
mode                  | `outlined | flat` | `False`  | Mode of the chip.                                                               |
customLinearLocations | `Array<number>`   | `False`  | An optional array of numbers defining the location of each gradient color stop. |
customLinearColors    | `Array<string>`   | `False`  | An optional array of at least two color values that represent gradient colors.  |
chipHeight            | `number`          | `False`  | Optional number to control the height of the chip.                              |
style                 | `ViewStyleProp`   | `False`  | Optional View style.                                                            |
textStyle             | `TextStyleProp`   | `False`  | Optional Text style.                                                            |
