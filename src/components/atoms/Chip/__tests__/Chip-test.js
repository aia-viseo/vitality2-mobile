/**
 *
 *  Test Chip
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Chip from '../Chip';

test('Test Chip ', () => {
  const tree = renderer.create(<Chip text="Hi Jack" />).toJSON();
  expect(tree).toMatchSnapshot();
});
