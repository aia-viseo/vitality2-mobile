/**
 *
 * @format
 *
 */

// @flow

import {COLOR_AIA_GREEN, COLOR_AIA_CYAN, COLOR_AIA_BLUE} from '@colors';

export const LinearLocations = [0, 0.6235, 1];
export const LinearColors = [COLOR_AIA_GREEN, COLOR_AIA_CYAN, COLOR_AIA_BLUE];
export const defaultHeight = 26;
