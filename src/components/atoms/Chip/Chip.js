/**
 *
 * Chip
 * @format
 *
 */

// @flow

import React from 'react';
import {Chip as FrameworkChip} from 'react-native-paper';

import {LinearLocations, defaultHeight, LinearColors} from './config';

import type {PropsType} from './types';
import ChipStyles from './styles';

import LinearGradient from '../LinearGradient';

const Chip = (props: PropsType) => {
  const {
    text,
    onPress,
    mode,
    customLinearColors,
    customLinearLocations,
    chipHeight,
    textStyle,
    style,
  } = props;

  const isFlat = mode === 'flat';
  const height = chipHeight || defaultHeight;
  const radius = height / 2;

  const linearGradientStyle = {height, borderRadius: radius};
  const chipStyle = {height: isFlat ? height : height - 1, borderRadius: radius};

  return (
    <LinearGradient
      customLinearLocations={customLinearLocations || LinearLocations}
      customLinearColors={customLinearColors || LinearColors}
      extraStyle={[linearGradientStyle, !isFlat && ChipStyles.linearGradientStyle]}>
      <FrameworkChip
        mode={mode || 'outlined'}
        onPress={onPress}
        style={[ChipStyles.container, isFlat && ChipStyles.flatContainer, chipStyle, style]}
        textStyle={[
          isFlat ? ChipStyles.flatText : ChipStyles.outlinedText,
          ChipStyles.text,
          textStyle,
        ]}>
        {text}
      </FrameworkChip>
    </LinearGradient>
  );
};

export default Chip;
