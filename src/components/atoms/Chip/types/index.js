/**
 *
 * @format
 *
 */

// @flow

import type {TextStyleProp, ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

type ModeVariations = 'flat' | 'outlined';

export type PropsType = {
  text: string,
  style?: ViewStyleProp,
  textStyle?: TextStyleProp,
  onPress?: () => void,
  mode?: ModeVariations,
  customLinearColors?: Array<string>,
  customLinearLocations?: Array<number>,
  chipHeight?: number,
};
