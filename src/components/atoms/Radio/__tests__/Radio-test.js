/**
 *
 * Radio Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Radio from '../Radio';

test('Radio', () => {
  const tree = renderer.create(<Radio value="Option 1" onPress={() => {}} selected />).toJSON();
  expect(tree).toMatchSnapshot();
});
