/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  value?: string,
  selected?: boolean,
  onPress?: () => void,
  color?: string,
  uncheckedColor?: string,
  disabled?: boolean,
  theme?: {},
};
