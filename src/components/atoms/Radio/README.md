## Radio
An atomic Radio component

# Usage & Example
```js
import Checkbox from '@atoms/Radio';

<RadioButton
  value={value}
  selected={selected}
  onPress={onPress}
  color={color || '#00A4C9'}
  uncheckedColor={uncheckedColor || '#00A4C9'}
  disabled={disabled}
  theme={theme}
/>
```
Key                             | Type           | Required  | Description                             |
------------------------------- | -------------- | --------- | --------------------------------------  |
value                           | `string`       | `False`   | The radio button value                  |
selected                        | `boolean`      | `false`   | Selects the radio button                |
onPress                         | `callback `    | `False`   | Callback function when radio is clicked |
color                           | `string`       | `False`   | The color of the radio button           |
uncheckedColor                  | `string`       | `False`   | The color of the unchecked radio button |
disabled                        | `boolean `     | `False`   | Disables the radio button               |
theme                           | `object`       | `False`   | react native paper theme                |


# Viewing the buttons.
Click the 'Open modal' button from the Dashboard to see the 2 variations.
