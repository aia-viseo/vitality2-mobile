/**
 *
 * Radio
 * @format
 *
 */

// @flow

import React from 'react';
import {RadioButton} from 'react-native-paper';

import type {PropsType} from './types';

const Radio = (props: PropsType) => {
  const {value, onPress, color, uncheckedColor, theme, disabled, selected} = props;

  return (
    <RadioButton.Android
      value={value}
      status={selected ? 'checked' : 'unchecked'}
      onPress={onPress}
      color={color || '#00A4C9'}
      uncheckedColor={uncheckedColor || '#00A4C9'}
      disabled={disabled}
      theme={theme}
    />
  );
};

Radio.defaultProps = {};

export default Radio;
