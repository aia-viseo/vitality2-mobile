## LinearGradient
Atom Component to render LinearGradient with default config

# Usage
```js
import LinearGradient from '/atoms/LinearGradient';

<LinearGradient
 startX
 startY
 endX
 endY
 customLinearLocations
 customLinearColors
 extraStyle
 children
>
  <View/>
</LinearGradient>
```

# Example
```js
import LinearGradient from '/atoms/LinearGradient';

<LinearGradient
 startX={0}
 startY={0}
 endX={1}
 endY={0}
 customLinearLocations={[0, 0.22, 1]}
 customLinearColors={[#fff, #ddd, #000]}
 extraStyle={{height: 100}}
>
  <View/>
</LinearGradient>
```

Key                   | Type              | Required | Description                                                                     |
--------------------- | ----------------- | -------- | ------------------------------------------------------------------------------- |
startX                | `number`          | `False`  | Optional number to control the start point X axis.                              |
startY                | `number`          | `False`  | Optional number to control the start point Y axis.                              |
endX                  | `number`          | `False`  | Optional number to control the end point X axis.                                |
endY                  | `number`          | `False`  | Optional number to control the end point Y axis.                                |
customLinearLocations | `Array<number>`   | `False`  | An optional array of numbers defining the location of each gradient color stop. |
customLinearColors    | `Array<string>`   | `False`  | An optional array of at least two color values that represent gradient colors.  |
extraStyle            | `ViewStyleProp`   | `False`  | Optional style.                                                                 |
