/**
 *
 * @format
 *
 */

// @flow

import * as React from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  startX?: 0 | 1,
  startY?: 0 | 1,
  endX?: 0 | 1,
  endY?: 0 | 1,
  customLinearLocations?: Array<number>,
  customLinearColors?: Array<string>,
  extraStyle?: ViewStyleProp,
  children?: React.Node,
};
