/**
 *
 * Test LinearGradient
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import LinearGradient from '../LinearGradient';

test('Test LinearGradient', () => {
  const tree = renderer.create(<LinearGradient />).toJSON();

  expect(tree).toMatchSnapshot();
});
