/**
 *
 * ProgressBar
 * @format
 *
 */

import React from 'react';
import FrameWorkLinearGradient from 'react-native-linear-gradient';

import {PropsType} from './types';
import {LinearLocations, LinearColors, TransparentColors} from './configs';

const LinearGradient = (props) => {
  const {
    startX,
    startY,
    endX,
    endY,
    customLinearLocations,
    customLinearColors,
    extraStyle,
    children,
    hideBackground,
  } = props;

  const colors = customLinearColors || LinearColors;
  return (
    <FrameWorkLinearGradient
      start={{x: startX || 0, y: startY || 0}}
      end={{x: endX || 1, y: endY || 0}}
      locations={customLinearLocations || LinearLocations}
      colors={hideBackground ? TransparentColors : colors}
      style={extraStyle}>
      {children}
    </FrameWorkLinearGradient>
  );
};
LinearGradient.propTypes = PropsType;
export default LinearGradient;
