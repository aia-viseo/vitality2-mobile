/**
 *
 * @format
 *
 */

// @flow

import {COLOR_AIA_GD_RED_1, COLOR_AIA_RED, COLOR_AIA_GD_RED_2, TRANSPARENT} from '@colors';

export const LinearLocations = [0, 0.5, 1];
export const LinearColors = [COLOR_AIA_GD_RED_1, COLOR_AIA_RED, COLOR_AIA_GD_RED_2];
export const TransparentColors = [TRANSPARENT, TRANSPARENT, TRANSPARENT];
