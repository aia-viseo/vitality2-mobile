## Slider
A Generic Carousel functional component

# Usage
```js
import Slider from 'atom/Slider';

<Slider
  data
  renderItem
  itemWidthPercentage
  slideAlign
  containerCustomStyle
  title
  onPressSeeAll
/>
```

# Example
```js
import Slider from 'atom/Slider';

renderComponent = ({item, index}) => {
  return (
    <Text key={index}>
      {item.title}
    </Text>
  )
}

<Slider
  data={[
    {title: 'Boy'}
    {title: 'Girl'}
  ]}
  renderItem={renderComponent}
  itemWidthPercentage={60}
  slideAlign={'start'}
  containerCustomStyle={{marginTop: 20}}
  title={"Hi Jack"}
  onPressSeeAll={() => {
    //do sth
  }}
/>
```

Key                   | Type                    | Required | Description                                               |
--------------------- | ----------------------- | -------- | --------------------------------------------------------- |
data                  | `array`                 | `True`   | Array of content to be one of the renderItem params.      |
renderItem            | `function`              | `True`   | Function to render each item of the data array.           |
itemWidthPercentage   | `number`                | `False`  | Number to control the precentage of Item container width. |
slideAlign            | `center/ start / end`   | `False`  | Indicate the item is flex start/ center/ end.             |
containerCustomStyle  | `Object`                | `False`  | Optional styles for Scrollview's global wrapper.	         |
title                 | `string`                | `False`  | Text to display on the top of the slider.                 |
onPressSeeAll         | `function`              | `False`  | Callback function of the see all button.                  |
