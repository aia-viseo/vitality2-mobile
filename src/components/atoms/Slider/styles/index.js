/**
 *
 * @format
 *
 */

// @flow

import {Dimensions, StyleSheet} from 'react-native';

const {width: viewportWidth} = Dimensions.get('window');

const widthPercentage = (percentage: number) => {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
};

const itemHorizontalMargin = widthPercentage(1);

export const sliderWidth = viewportWidth;
export const itemWidth = (percentage: number) =>
  widthPercentage(percentage) + itemHorizontalMargin * 2;

const SliderStyles = StyleSheet.create({
  firstItem: {
    width: 200,
  },
  secondItem: {
    width: 100,
  },
  headerItem: {
    height: 20,
    borderRadius: 10,
  },
  bodyItem: {
    height: 50,
    borderRadius: 25,
  },
  secondItemContainer: {
    flexDirection: 'row-reverse',
    width: 175,
  },
});

export default SliderStyles;
