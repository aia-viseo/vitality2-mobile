/**
 *
 * @format
 *
 */

// @flow

import type {Element} from 'react';

type SliderSlideAlign = 'center' | 'start' | 'end';

export type PropsType = {
  data: Array<{[key: string]: any}>,
  renderItem: (itemData: {item: {title: string}, index: number}) => Element<*>,
  itemWidthPercentage: number,
  slideAlign: SliderSlideAlign,
  containerCustomStyle?: {},
  title?: string,
  linkText?: string,
  onPressSeeAll?: () => void,
  isLoading?: boolean,
};
