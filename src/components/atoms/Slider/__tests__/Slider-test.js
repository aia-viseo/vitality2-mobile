/**
 *
 *  Test Slider
 *  @format
 *
 */

// @flow

import React from 'react';
import {Text} from 'react-native';
import type {Element} from 'react';
import renderer from 'react-test-renderer';

import Slider from '../Slider';

jest.useFakeTimers();
test('Test Slider ', () => {
  const itemWidthPercentage = 75;
  const slideAlign = 'start';
  const data = [{title: 'testing1'}, {title: 'testing2'}];

  const renderItem = (itemData: {item: {title: string}, index: number}): Element<*> => {
    const {item, index} = itemData;
    const {title} = item;

    return <Text key={index}>{title}</Text>;
  };
  const tree = renderer
    .create(
      <Slider
        data={data}
        renderItem={renderItem}
        itemWidthPercentage={itemWidthPercentage}
        slideAlign={slideAlign}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
