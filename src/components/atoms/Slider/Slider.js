/**
 *
 * Slider
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import AppStyles from '@styles';
import SectionHeader from '@molecules/SectionHeader';

import type {PropsType} from './types';
import SliderStyles, {sliderWidth, itemWidth} from './styles';

import {LazyLoad} from '../LazyLoad';

const Slider = (props: PropsType) => {
  const {
    data,
    renderItem,
    itemWidthPercentage,
    slideAlign,
    containerCustomStyle,
    title,
    linkText,
    onPressSeeAll,
    isLoading,
  } = props;

  const lazyHeader = () => (
    <LazyLoad style={[AppStyles.marginTop_20, AppStyles.marginHorizontal_20]}>
      <View style={[AppStyles.row]}>
        <View style={[SliderStyles.firstItem, SliderStyles.headerItem]} />
        <View style={[SliderStyles.secondItemContainer]}>
          <View style={[SliderStyles.secondItem, SliderStyles.headerItem]} />
        </View>
      </View>
    </LazyLoad>
  );

  const lazyBody = () => (
    <LazyLoad style={[AppStyles.marginTop_20, AppStyles.marginHorizontal_20]}>
      <View style={[AppStyles.row]}>
        <View style={[SliderStyles.firstItem, SliderStyles.bodyItem]} />
        <View style={[SliderStyles.secondItemContainer]}>
          <View style={[SliderStyles.secondItem, SliderStyles.bodyItem]} />
        </View>
      </View>
    </LazyLoad>
  );

  return (
    <View>
      {isLoading ? (
        <>
          {lazyHeader()}
          {lazyBody()}
        </>
      ) : (
        <>
          <SectionHeader title={title} linkText={linkText} onPressSeeAll={onPressSeeAll} />
          <Carousel
            data={data}
            renderItem={renderItem}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth(itemWidthPercentage)}
            activeSlideAlignment={slideAlign}
            layout="default"
            containerCustomStyle={containerCustomStyle}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
          />
        </>
      )}
    </View>
  );
};

Slider.defaultProps = {
  data: [],
  renderItem: {},
  itemWidthPercentage: 50,
  slideAlign: 'center',
};

export default Slider;
