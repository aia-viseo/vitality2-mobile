/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {OVERLAY} from '@colors';

const ActivityIndicatorStyle = StyleSheet.create({
  indicator: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: OVERLAY,
  },
});

export default ActivityIndicatorStyle;
