/**
 *
 * ActivityIndicator test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import ActivityIndicator from '../ActivityIndicator';

jest.useFakeTimers();
test('ActivityIndicator', () => {
  const tree = renderer.create(<ActivityIndicator />).toJSON();
  expect(tree).toMatchSnapshot();
});
