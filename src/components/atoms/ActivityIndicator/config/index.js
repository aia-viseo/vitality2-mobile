/**
 *
 * @format
 *
 */

// @flow

import {COLOR_AIA_RED} from '@colors';

export const defaultColor = COLOR_AIA_RED;
export const defaultSize = 'large';
