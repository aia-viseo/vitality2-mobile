/**
 *
 * ActivityIndicator
 * @format
 *
 */

// @flow

import React from 'react';
import {ActivityIndicator as PaperActivityIndicator} from 'react-native-paper';

import {defaultColor, defaultSize} from './config';

import type {PropsType} from './types';
import ActivityIndicatorStyle from './styles';

const ActivityIndicator = (props: PropsType) => {
  const {animating, color, size, style} = props;

  return (
    <PaperActivityIndicator
      size={size || defaultSize}
      color={color || defaultColor}
      style={style || ActivityIndicatorStyle.indicator}
      animating={animating || true}
    />
  );
};

export default ActivityIndicator;
