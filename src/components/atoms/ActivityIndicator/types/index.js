/**
 *
 * @format
 *
 */

// @flow

import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

type Size = 'small' | 'large' | number;
export type PropsType = {
  animating?: boolean,
  color?: string,
  size?: Size,
  style?: ViewStyleProp,
};
