/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp, TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

type Variation = 'text' | 'outlined' | 'contained';

export type PropsType = {
  accessibilityLabel?: string,
  buttonIcon?: string,
  buttonIconSize?: string,
  buttonIconColor?: string,
  buttonStyle?: ViewStyleProp,
  buttonTextStyle?: TextStyleProp,
  buttonWidth?: number | string,
  children: Node,
  color?: string,
  compact?: boolean,
  disabled?: boolean,
  onPress: () => any,
  uppercase?: boolean,
  variation?: Variation,
};
