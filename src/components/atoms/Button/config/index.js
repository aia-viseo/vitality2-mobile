/**
 *
 * @format
 *
 */

// @flow

import {COLOR_AIA_GD_RED_1, COLOR_AIA_RED, COLOR_AIA_GD_RED_2, WHITE} from '@colors';

export const buttonOneDefaultGradient: string[] = [
  COLOR_AIA_GD_RED_1,
  COLOR_AIA_RED,
  COLOR_AIA_GD_RED_2,
];

export const buttonOneTextFontColor = WHITE;
