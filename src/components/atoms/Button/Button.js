/**
 *
 * Button
 * @format
 *
 */

// @flow

import * as React from 'react';
import {Button as RnButton} from 'react-native-paper';
import AppStyles from '@styles';
import {COLOR_AIA_RED} from '@colors';

import type {PropsType} from './types';

const Button = (props: PropsType) => {
  const {
    accessibilityLabel,
    buttonIcon,
    buttonWidth,
    buttonStyle,
    buttonTextStyle,
    children,
    compact,
    disabled,
    onPress,
    uppercase,
    variation,
  } = props;

  return (
    <RnButton
      accessibilityLabel={accessibilityLabel}
      buttonWidth={buttonWidth}
      icon={buttonIcon}
      color={COLOR_AIA_RED}
      compact={!!compact}
      contentStyle={AppStyles.generalButtonContentStyle}
      disabled={disabled}
      labelStyle={buttonTextStyle}
      mode={variation}
      onPress={onPress}
      style={buttonStyle}
      uppercase={!!uppercase}>
      {children}
    </RnButton>
  );
};

Button.defaultProps = {
  active: true,
};

export default Button;
