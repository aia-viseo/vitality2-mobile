## Button
A button component

# Usage & Example
```js
import Button from '@atoms/Button';

<Button
  buttonVariationNumber={3}
  onPress={() => Alert.alert('Button clicked')}
/>
```
Key                             | Type           | Required  | Description                         |
------------------------------- | -------------- | --------- | ----------------------------------- |
buttonText                      | `string`       | `False    | Determines the initial drawer state |
buttonVariationNumber           | `number`       | `True`    | Delay in ms when drawer opens       |
onPress                         | `callback`     | `True`    | onPress callback function           |
buttonIcon                      | `React.Node`   | `False`   | Icon component                      |
buttonOneGradientColors         | `string[]`     | `False`   | Callback function hide is clicked   |
buttonOneTextColor              | `string`       | `Fals`    | Styles the text inside the button   |
buttonWidth                     | `number`       | `True`    | Sets the width of the button        |
buttonStyle                     | `ViewStyle`    | `False    | Sets the custom style of the button |
buttonTextStyle                 | `ViewStyle`    | `False`   | Styles the text inside the button   |


# Using and testing the Button Variation 3
# Test this temporarily in the Empty assessment screen.

```js
/**
 *
 * Assessment Page
 * @format
 *
 */

// @flow

import React from 'react';
import {SafeAreaView, Text, Alert} from 'react-native';

import {AssessmentProvider} from '@contexts/Assessment';
import Button from '@atoms/Button';

import type {PropsType} from './types';
import AssessmentStyles from './styles';

const Assessment = () => {
  return (
    <SafeAreaView style={AssessmentStyles.container}>
      <Text>Assessment Page</Text>
      <Button
        buttonVariationNumber={3}
        buttonText="See All"
        onPress={() => Alert.alert('Clicked')}
      />
      <Button buttonVariationNumber={2} onPress={() => Alert.alert('Something')} />
      <Button
        buttonVariationNumber={1}
        onPress={() => Alert.alert('Something')}
        buttonText="Proceed Now"
      />
    </SafeAreaView>
  );
};

const AssessmentWrapper = (props: PropsType) => {
  const {navigation} = props;

  return (
    <AssessmentProvider>
      <Assessment navigation={navigation} />
    </AssessmentProvider>
  );
};

export default AssessmentWrapper;
```
