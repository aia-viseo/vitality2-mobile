## Image
Atom Component to render image with default config

# Usage
```js
import Image from '/atoms/Image';

<Image
  source
  customStyle
/>
```

# Example
```js
import Image from '/atoms/Image';

<Image
  source={{
    uri: 'https://reactnative.dev/img/tiny_logo.png',
  }}
  customStyle={{width: 100}}
/>
```

Key           | Type                   | Required | Description                           |
------------- | ---------------------- | -------- | ------------------------------------- |
source        | `ImageSourcePropType`  | `True`   | The image source .                    |
customStyle   | `ImageStyleProp`       | `False`  | Optional extra styling for the image. |

