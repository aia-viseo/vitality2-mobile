/**
 *
 * Image
 * @format
 *
 */

// @flow

import React from 'react';
import {Image as RnImage} from 'react-native';

import type {PropsType} from './types';
import ImageStyles from './styles';

const Image = (props: PropsType) => {
  const {source, customStyle} = props;

  return <RnImage style={[ImageStyles.image, customStyle]} source={source} />;
};

export default Image;
