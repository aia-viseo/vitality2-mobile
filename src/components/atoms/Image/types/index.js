/**
 *
 * @format
 *
 */

// @flow

import type {ImageStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  source: {
    uri: string,
    body?: string,
    bundle?: string,
    cache?: 'default' | 'reload' | 'force-cache' | 'only-if-cached',
    headers?: {},
    height?: number,
    width?: number,
    method?: string,
    scale?: number,
    body?: string,
  },
  customStyle?: ImageStyleProp,
};
