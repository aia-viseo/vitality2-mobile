/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const ImageStyles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
  },
});

export default ImageStyles;
