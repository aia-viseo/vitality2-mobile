/**
 *
 *  Test Image
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Image from '../Image';

test('Test Image ', () => {
  const tree = renderer
    .create(
      <Image
        source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}
        customStyle={{width: 200, height: 200}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
