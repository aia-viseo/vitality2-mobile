/**
 *
 * @format
 *
 */

// @flow

import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  group: string,
  name: string,
  extraStyle?: ViewStyleProp,
  withBorder?: boolean,
  unRead?: boolean,
  height?: number,
  width?: number,
};
