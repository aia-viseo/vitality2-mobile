## Icon
Component to import the SVG icon from assets/icon to render

# Usage
```js
  import Icon from 'atom/Icon';

  <Icon
    group
    name
  />
```

# Example
```js
  import Icon from 'atom/Icon';

  <Icon
    group="general"
    name="manRunning"
  />
```

Key   | Type     | Required | Description
--    | --       | --       | --
group | `string` | `True`   | Key to map the group of the icon folder
name  | `string` | `True`   | Key to map the icon name

# Step to add SVG to use

1. Add SVG in icons/*/
2. import and export in icons/*/index.js
