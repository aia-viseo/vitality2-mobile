/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

import {COLOR_AIA_RED, COLOR_AIA_ICON_GRAY} from '@colors';

const IconStyle = StyleSheet.create({
  iconContainer: {
    position: 'relative',
  },
  iconBorder: {
    borderColor: COLOR_AIA_ICON_GRAY,
    borderWidth: 1,
    padding: 10,
    overflow: 'hidden',
    borderRadius: 5,
  },
  redDot: {
    position: 'absolute',
    backgroundColor: COLOR_AIA_RED,
    width: 5,
    height: 5,
    borderRadius: 5,
    right: 5,
    top: 5,
  },
});

export default IconStyle;
