/**
 *
 *  Test Icon
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Icon from '../Icon';

test('Test Icon ', () => {
  const tree = renderer.create(<Icon group="general" name="arrow_right" />).toJSON();
  expect(tree).toMatchSnapshot();
});
