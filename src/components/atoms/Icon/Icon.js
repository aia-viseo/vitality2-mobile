/**
 *
 * Icon
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import AppStyles from '@styles';

import type {PropsType} from './types';
import IconStyle from './styles';

import Icons from '../../../assets/icons';

const Icon = (props: PropsType) => {
  const {group, name, extraStyle, withBorder, unRead, height, width} = props;

  const icon = Icons[group](name, extraStyle, width, height);

  return (
    <View
      style={[
        unRead || withBorder ? IconStyle.iconContainer : {},
        unRead || withBorder ? AppStyles.padding_10 : {},
        withBorder ? IconStyle.iconBorder : {},
      ]}>
      {unRead && <View style={IconStyle.redDot} />}
      {icon}
    </View>
  );
};

export default Icon;
