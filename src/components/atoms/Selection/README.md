## Selection
Selection component

# Usage & Example
```js
import Selection from '@atoms/Selection';

const [selectionAnswer, setSelectionAnswer] = useState('3');

<Selection
  answers={['1', '2', '3', '4', '5']}
  selected={selectionAnswer}
  onPress={(newAnswer) => {
    setSelectionAnswer(newAnswer);
  }}
/>
```
Key                             | Type            | Required  | Description                           |
------------------------------- | --------------- | --------- | ------------------------------------- |
answers                         | `string[]`      | `False`   | Array of string answers               |
selected                        | `string`        | `False`   | The current selected answer           |
textStyle                       | `TextStyleProps`| `False`   | Style for the text                    |
customStyle                     | `ViewStyleProps`| `False`   | Style for the component               |
componentBackgroundColor        | `string`        | `False`   | Color of the component background     |
useGradient                     | `boolean`       | `False`   | Selects on using a gradient           |
onPress                         | `callback`      | `False`   | Function to call when item is pressed |
offsetLeftPosition              | `number  `      | `False`   | Left position offset                  |

# Note about offsetLeftPosition
When the main screen component elements has a container marginLeft of ex: 20, set the offsetLeftPosition to 20 to center the Selection component.
