/**
 *
 * Selection Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Selection from '../SelectionCarousel';

test('Selection', () => {
  const tree = renderer.create(<Selection />).toJSON();
  expect(tree).toMatchSnapshot();
});
