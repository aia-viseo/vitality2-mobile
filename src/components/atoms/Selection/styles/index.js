/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {BLACK, COLOR_AIA_SELECTION_BORDER_SOLID, COLOR_AIA_GREY_500} from '@colors';

const SelectionStyles = StyleSheet.create({
  componentContainer: {
    position: 'relative',
  },
  carouselContainer: {
    zIndex: 5,
    position: 'relative',
  },
  centerBoxContainer: {
    position: 'relative',
    top: -62,
    flexDirection: 'column',
    alignItems: 'center',
  },
  centerBox: {
    borderWidth: 1,
    width: 60,
    height: 60,
    borderRadius: 5,
    borderColor: COLOR_AIA_SELECTION_BORDER_SOLID,
  },
  carouselSelectedText: {
    fontSize: RFValue(28),
    fontWeight: '600',
    color: BLACK,
    paddingVertical: 15,
    alignItems: 'center',
    textAlign: 'center',
  },
  unit: {
    color: COLOR_AIA_GREY_500,
  },
});

export default SelectionStyles;
