/**
 *
 * @format
 *
 */

// @flow

import type {TextStyleProp, ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  answers?: string[],
  selected?: string,
  textStyle?: TextStyleProp,
  customStyle?: ViewStyleProp,
  componentBackgroundColor?: string,
  useGradient?: boolean,
  onPress?: (value: string) => void,
  offsetLeftPosition?: number,
  unit?: string,
};
