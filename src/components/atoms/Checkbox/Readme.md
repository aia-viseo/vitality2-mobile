## Checkbox
A Checkbox component implementing react-native-paper checkbox

# Usage & Example
```js
import Checkbox from '@atoms/Checkbox';

<View style={{flexDirection: 'row', alignItems: 'center'}}>
  <Checkbox
    checkboxVariationNumber={2}
    checked={checkedTwo}
    onPress={handleCheckboxTwoClick}
  />
  <Text>Button Variation 2</Text>
</View>
<View style={{flexDirection: 'row', alignItems: 'center'}}>
  <Checkbox
    checkboxVariationNumber={3}
    checked={checkedThree}
    onPress={handleCheckboxThreeClick}
  />
  <Text>Button Variation 3</Text>
</View>
```
Key                             | Type           | Required  | Description                            |
------------------------------- | -------------- | --------- | -------------------------------------- |
onPress                         | `callback`     | `True`    | The function to be called when clicked |
checkboxVariationNumber         | `number`       | `False`   | Determines what variation to render    |
checked                         | `boolean `     | `True`    | Determines if button is checked        |
color                           | `String`       | `False`   | The color of the checkbox              |



# Viewing the buttons.
Click the 'Open modal' button from the Dashboard to see the 2 variations.
