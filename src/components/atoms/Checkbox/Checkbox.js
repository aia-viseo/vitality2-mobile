/**
 *
 * CheckboxOne
 * @format
 *
 */

// @flow

import React from 'react';
import {Checkbox as FrameworkCheckbox} from 'react-native-paper';

import type {PropsType} from './types';

const Checkbox = (props: PropsType) => {
  const {checked, onPress, color, uncheckedColor, disabled} = props;

  return (
    <FrameworkCheckbox.Android
      status={checked ? 'checked' : 'unchecked'}
      onPress={onPress}
      color={color || '#00A4C9'}
      uncheckedColor={uncheckedColor || '#00A4C9'}
      disabled={disabled}
    />
  );
};

Checkbox.defaultProps = {};

export default Checkbox;
