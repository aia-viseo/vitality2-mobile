/**
 *
 * Checkbox Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Checkbox from '../Checkbox';

test('Checkbox', () => {
  const cb = <Checkbox onPress={() => {}} checked />;
  const tree = renderer.create(cb).toJSON();
  expect(tree).toMatchSnapshot();
});
