/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  checked: boolean,
  onPress: (boolean) => void,
  color?: string,
  uncheckedColor?: string,
  disabled?: boolean,
};
