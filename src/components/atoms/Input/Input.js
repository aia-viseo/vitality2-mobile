/**
 *
 * Input
 * @format
 *
 */

// @flow

import React from 'react';
import {TextInput} from 'react-native-paper';

import type {PropsType} from './types';

import InputStyles from './styles';

const Input = (props: PropsType) => {
  const {
    value,
    placeholder,
    editable,
    changeText,
    numberOfLines,
    customStyle,
    mode,
    label,
    isError,
    underlineColor,
    theme,
    secureTextEntry,
    defaultValue,
    numbersOnly,
    keyboardType,
  } = props;

  return (
    <TextInput
      style={[InputStyles.input, customStyle]}
      numberOfLines={numberOfLines}
      mode={mode}
      label={label}
      error={isError}
      onChangeText={(text) =>
        changeText && changeText(!numbersOnly ? text : text.replace(/[^0-9]{1,}/g, ''))
      }
      disabled={editable}
      secureTextEntry={secureTextEntry}
      underlineColor={underlineColor}
      placeholder={placeholder}
      theme={theme}
      value={value}
      defaultValue={defaultValue}
      keyboardType={keyboardType || 'default'}
    />
  );
};

export default Input;
