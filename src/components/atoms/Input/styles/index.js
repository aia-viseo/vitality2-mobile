/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_GREY, WHITE} from '@colors';

const InputStyles = StyleSheet.create({
  input: {
    borderColor: COLOR_AIA_GREY,
    fontSize: RFValue(16),
    borderWidth: 0.3,
    borderRadius: 5,
    backgroundColor: WHITE,
  },
});

export default InputStyles;
