/**
 *
 * Input Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Input from '../Input';

test('Test Input', () => {
  const tree = renderer.create(<Input value="Test value" customStyle={{fontSize: 20}} />).toJSON();
  expect(tree).toMatchSnapshot();
});
