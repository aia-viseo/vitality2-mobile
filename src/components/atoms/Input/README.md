## Input
Atom Component to render input with default config
https://callstack.github.io/react-native-paper/text-input.html
All props in React Native Paper TextInput also can be used in here
# Usage
```js
import Input from '/atoms/Input';

<Input
  value
  defaultValue
  placeholder
  editable
  changeText
  numberOfLines
  customStyle
  mode
  error
  label
  secureTextEntry
/>
```

# Example
```js
import Input from '/atoms/Input';

<Input
  value={"username"}
  placeholder={"Username"}
  editable={true}
  customStyle={{color: BLACK}}
/>
```

Key             | Type              | Required | Description                                            |
--------------- | ----------------- | -------- | ------------------------------------------------------ |
value           | `string`          | `False`  | Optional Value of input string.                        |
defaultValue    | `string`          | `False`  | Optional default value of input string.                |
placeholder     | `string`          | `False`  | Optional placeholder for the input.                    |
underlineColor  | `string`          | `False`  | Optional underline color of the input.                 |
editable        | `boolean`         | `False`  | Optional if text input editable or not.                |
secureTextEntry | `boolean`         | `False`  | Optional secure text entry when true.                  |
changeText      | `function`        | `False`  | Optional void function when text change.               |
numberOfLines   | `number`          | `False`  | Optional number of lines.                              |
customStyle     | `TextStyleProp`   | `False`  | Optional extra styling for the image.                  |
mode            | `string`          | `False`  | Optional either `flat` or `outlined`.                  |
error           | `boolean`         | `False`  | Optional wether to style text input with error style.  |
label           | `string`          | `False`  | Optional text to use for the floating label.           |
