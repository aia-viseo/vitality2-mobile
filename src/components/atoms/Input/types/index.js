/**
 *
 * @format
 *
 */

// @flow

import type {TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  value?: string,
  defaultValue?: string,
  placeholder?: string,
  customStyle?: TextStyleProp,
  numberOfLines?: number,
  editable?: boolean,
  mode?: 'flat' | 'outlined',
  label?: string,
  isError?: boolean,
  secureTextEntry?: boolean,
  underlineColor?: string,
  theme?: {},
  changeText?: (text: any) => void,
  numbersOnly?: boolean,
  keyboardType?: string,
};
