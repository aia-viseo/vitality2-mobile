## FlexibleText
Atom Component to display Texts with scale and flex function

# Usage
```js
import FlexibleText from '/atoms/FlexibleText';

<FlexibleText
  scalableText
  flexibleText
/>
```

# Example
```js
import FlexibleText from '/atoms/FlexibleText';

const scrollY = new Animated.Value(0);
<FlexibleText
  scalableText={"Hi"}
  flexibleText={"Jack"}
/>
```

Key            | Type      | Required | Description                                                  |
-------------- | --------  | -------- | ------------------------------------------------------------ |
scalableText   | `string`  | `True`   | Text string apply for apply scale effect.                    |
flexibleText   | `string`  | `True`   | Text string apply for apply flex effect.                     |

