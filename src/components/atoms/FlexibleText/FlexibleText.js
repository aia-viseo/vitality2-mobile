/**
 *
 * FlexibleText
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import type {PropsType} from './types';
import FlexibleTextStyles from './styles';

import Text from '../Text';

const FlexibleText = (props: PropsType) => {
  const {flexibleText, scalableText, row} = props;

  return (
    <View style={[FlexibleTextStyles.container, {flexDirection: row ? 'row' : 'column'}]}>
      <Text numberOfLines={1} ellipsizeMode="tail" customStyle={FlexibleTextStyles.scalableText}>
        {scalableText}
      </Text>
      <Text numberOfLines={1} ellipsizeMode="tail" customStyle={FlexibleTextStyles.flexibleText}>
        {flexibleText}
      </Text>
    </View>
  );
};

export default FlexibleText;
