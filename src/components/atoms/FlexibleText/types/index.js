/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  flexibleText: string,
  scalableText: string,
  row?: boolean,
};
