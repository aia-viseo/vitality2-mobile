/**
 *
 *  Test FlexText
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import FlexibleText from '../FlexibleText';

test('Test FlexibleText ', () => {
  const tree = renderer
    .create(<FlexibleText row scalableText="Hi" flexibleText="Testing" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
