/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import GetOS from '@platform';
import {WHITE} from '@colors';

const isIOS = GetOS.getInstance() === 'ios';
const FlexibleTextStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: isIOS ? 0 : 5,
  },
  scalableText: {
    color: WHITE,
    fontSize: RFValue(24),
    fontWeight: isIOS ? '300' : 'normal',
  },
  flexibleText: {
    color: WHITE,
    fontSize: RFValue(24),
    fontWeight: isIOS ? '900' : 'bold',
  },
});

export default FlexibleTextStyles;
