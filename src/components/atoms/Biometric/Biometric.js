/**
 *
 * Biometrics
 * @format
 *
 */

// @flow

import React, {useEffect, useState} from 'react';

import type {PropsType} from './types';

import BiometricValidator from './BiometricValidator';

const Biometric = (props: PropsType) => {
  const {biometricTrigger} = props;
  const [isAvailable, setIsAvailable] = useState(false);

  useEffect(() => {
    BiometricValidator.isBiometricsAvailable().then((result: boolean) => {
      setIsAvailable(result);
    });
  }, []);

  useEffect(() => {
    if (isAvailable && biometricTrigger) {
      BiometricValidator.createBioKey(
        () => {
          BiometricValidator.createSignature(
            () => {},
            () => {}
          );
        },
        () => {}
      );
    }
  }, [biometricTrigger]);

  return <></>;
};

export default Biometric;
