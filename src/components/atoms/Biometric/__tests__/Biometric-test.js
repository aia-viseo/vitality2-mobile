/**
 *
 *  Test Biometric
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Biometric from '../Biometric';

test('Test Biometric', () => {
  const tree = renderer.create(<Biometric />).toJSON();
  expect(tree).toMatchSnapshot();
});
