/**
 *
 * BiometricsValidator
 * @format
 *
 */

// @flow

import ReactNativeBiometrics from 'react-native-biometrics';

// Function to return result from the Trigger
function returnResult<T>(callBack: (result: T) => void, error: () => void, result: T) {
  if (result) {
    callBack(result);
  } else {
    error();
  }
}

// ReactNativeBiometrics function to check is biometrics available
const isBiometricsAvailable = () => {
  let isAvailable = false;

  return new Promise<boolean>((resolve: (isAvailable?: boolean) => void) => {
    ReactNativeBiometrics.isSensorAvailable().then((resultObject: {available: boolean}) => {
      const {available} = resultObject;
      isAvailable = available;
      resolve(isAvailable);
    });
  });
};

// ReactNativeBiometrics function to create biometrics key
const createBioKey = (callBack: (result: string) => void, error: () => void) => {
  ReactNativeBiometrics.createKeys('Confirm fingerprint').then((result: {publicKey: string}) => {
    returnResult<string>(callBack, error, result.publicKey);
  });
};

// ReactNativeBiometrics function to check biometrics key exist
const checkBioKeyExist = (callBack: (result: boolean) => void, error: () => void) => {
  ReactNativeBiometrics.biometricKeysExist().then((result: {keysExist: boolean}) => {
    returnResult<boolean>(callBack, error, result.keysExist);
  });
};

// ReactNativeBiometrics function to delete keys
const deleteKeys = (callBack: (result: boolean) => void, error: () => void) => {
  ReactNativeBiometrics.deleteKeys().then((result: {keysDeleted: boolean}) => {
    returnResult<boolean>(callBack, error, result.keysDeleted);
  });
};

// ReactNativeBiometrics function to create signature
const createSignature = (callBack: (result: string) => void, error: () => void) => {
  const epochTimeSeconds = Math.round(new Date().getTime() / 1000).toString();
  const payload = `${epochTimeSeconds}`;

  ReactNativeBiometrics.createSignature({
    promptMessage: 'Sign in',
    payload,
  }).then((result: {signature: string}) => {
    returnResult<string>(callBack, error, result.signature);
  });
};

const BiometricValidator = {
  isBiometricsAvailable,
  checkBioKeyExist,
  createSignature,
  deleteKeys,
  createBioKey,
};

export default BiometricValidator;
