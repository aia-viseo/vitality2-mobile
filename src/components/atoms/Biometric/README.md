## Biometric
Component to call biometric function

# Usage

```js
  import Biometric from 'atom/Biometric';

  <Biometric
    biometricTrigger
  />
```
Key                     | Type   | Required | Description
--                      | --     | --       | --
biometricTrigger        | `bool` | `False`  | Trigger to fire biometric
