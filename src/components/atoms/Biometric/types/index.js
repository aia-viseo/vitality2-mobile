/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  biometricTrigger?: boolean,
};
