/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  iconGroup?: string,
  iconName?: string,
  label?: string,
  primaryText?: string,
  secondaryText?: string,
};
