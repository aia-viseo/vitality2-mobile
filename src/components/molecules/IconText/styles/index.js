/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_GREY} from '@colors';

import {projectFontFamily} from '@core/config';

const IconTextStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textLabel: {
    fontSize: RFValue(12),
    fontFamily: projectFontFamily,
    color: COLOR_AIA_GREY,
  },
  infoTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  primaryText: {
    fontSize: RFValue(20),
    fontFamily: projectFontFamily,
    fontWeight: '600',
  },
  secondaryText: {
    fontSize: RFValue(16),
    fontFamily: projectFontFamily,
  },
});

export default IconTextStyles;
