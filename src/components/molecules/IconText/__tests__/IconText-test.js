/**
 *
 * IconText Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import IconText from '../IconText';

test('Text IconText', () => {
  const iconGroup = 'linkSection';
  const iconName = 'health_report';
  const label = 'Spend approx.';
  const primaryText = '5';
  const secondaryText = 'minutes';
  const tree = renderer
    .create(
      <IconText
        iconGroup={iconGroup}
        iconName={iconName}
        label={label}
        primaryText={primaryText}
        secondaryText={secondaryText}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
