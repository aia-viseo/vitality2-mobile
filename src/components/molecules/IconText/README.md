## IconText
An Icon Text Component

# Usage
```js
import IconText from 'molecules/IconText';

<IconText
  iconGroup
  iconName
  label
  primaryText
  secondaryText
/>
```

# Example
```js
import IconText from 'molecules/IconText';

<IconText
  iconGroup={"linkSection"}
  iconName={"health_report"}
  label={"Spend approx."}
  primaryText={"5"}
  secondaryText={"minutes"}
/>
```

Key           | Type           | Required  | Description                        |
------------- | -------------- | --------- | ---------------------------------- |
iconGroup     | `string`       | `False`   | String for specific group of icon  |
iconName      | `string`       | `False`   | String for specific icon.          |
label         | `string`       | `False`   | String label or description.       |
primaryText   | `string`       | `False`   | String primaryText or value.       |
secondaryText | `string`       | `False`   | String secondaryText or unit.      |
