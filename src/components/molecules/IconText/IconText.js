/**
 *
 * IconText
 * @format
 *
 */

// @flow

import React from 'react';

import {View} from 'react-native';
import AppStyles from '@styles';
import Icon from '@atoms/Icon';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import IconTextStyles from './styles';

const IconText = (props: PropsType) => {
  const {iconGroup, iconName, label, primaryText, secondaryText} = props;
  return (
    <View style={IconTextStyles.container}>
      <Icon group={iconGroup} name={iconName} />
      <View style={AppStyles.marginLeft_10}>
        <Text customStyle={IconTextStyles.textLabel}>{label}</Text>
        <View style={IconTextStyles.infoTextContainer}>
          <Text customStyle={IconTextStyles.primaryText}>{primaryText}</Text>
          <Text customStyle={IconTextStyles.secondaryText}> {secondaryText}</Text>
        </View>
      </View>
    </View>
  );
};

export default IconText;
