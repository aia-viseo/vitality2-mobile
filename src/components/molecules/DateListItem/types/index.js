/**
 *
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  dateStr: string,
  title: string,
  points: string,
};
