## DateListItem
Containe Date Title and point

# Example
```js
<DateListItem
  dateStr={dateStr}
  title={title}
  points={points}
  />
```

# Props
Key      | Type      | Required  | Description                      |
-------- | --------- | --------- | -------------------------------- |
dateStr  | `string`  | `True`    | Value to show in date area.      |
title    | `string`  | `True`    | Value to show in title area.     |
points   | `string`  | `True`    | Value to show in points area.    |
