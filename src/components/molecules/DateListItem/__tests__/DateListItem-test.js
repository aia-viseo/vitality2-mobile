/**
 *
 *  Test DateListItem
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import DateListItem from '../DateListItem';

test('Test DateListItem ', () => {
  const tree = renderer
    .create(<DateListItem dateStr="2020-02-20" title="Title" points="300" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
