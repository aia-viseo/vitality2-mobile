/**
 *
 * DateListItem
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import {useTranslation} from 'react-i18next';

import Text from '@atoms/Text';

import NumberString from '@core/handler/NumberString';

import AppStyles from '@styles';

import type {PropsType} from './types';
import Styles from './styles';

const DateListItem = (props: PropsType) => {
  const {dateStr, title, points} = props;
  const {t} = useTranslation();

  return (
    <View style={[AppStyles.padding_20]}>
      <View
        style={[AppStyles.row, AppStyles.alignItemCenter, AppStyles.justifyContentSpaceBetween]}>
        <View style={[AppStyles.column, AppStyles.flex_1]}>
          <Text customStyle={Styles.date}>{dateStr}</Text>
          <Text customStyle={Styles.title}>{title}</Text>
        </View>
        <Text customStyle={Styles.points}>
          {NumberString.toLocaleString(points)} {t('general.points-suffix')}
        </Text>
      </View>
    </View>
  );
};

export default DateListItem;
