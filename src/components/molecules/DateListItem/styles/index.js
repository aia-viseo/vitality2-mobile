/**
 *
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_DARK_GREY, COLOR_AIA_GREY_500, COLOR_AIA_GREEN} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const Styles = StyleSheet.create({
  points: {
    fontSize: RFValue(14),
    color: COLOR_AIA_GREEN,
  },
  title: {
    fontSize: RFValue(14),
    fontWeight: isIOS ? '600' : 'bold',
    color: COLOR_AIA_DARK_GREY,
  },
  date: {
    fontSize: RFValue(12),
    fontWeight: isIOS ? '300' : 'normal',
    color: COLOR_AIA_GREY_500,
  },
  cellDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Styles;
