import {StyleSheet} from 'react-native';

import {COLOR_AIA_RED} from '@colors';
import {DEVICE_WIDTH, DEVICE_HEIGHT, SIZE_SCALING, ASPECT_RATIO, INSET_BOTTOM, MINUS_HEIGHT} from '../config';

const height = DEVICE_WIDTH * ASPECT_RATIO;
const heightOffset = DEVICE_HEIGHT - MINUS_HEIGHT - INSET_BOTTOM;

const CameraStyles = StyleSheet.create({
  viewWrapper: {
    flex: 1,
    width: DEVICE_WIDTH,
    height: heightOffset,
  },
  preview: {
    position: 'absolute',
    justifyContent: 'center',
    top: Math.max(0, (heightOffset - height) / 2),
    left: 0,
    width: DEVICE_WIDTH,
    height,
    overflow: 'hidden',
    borderRadius: 20,
  },
  loading: {
    flex: 1, 
    alignSelf: 'center',
  },
  textContainer: {
    flexDirection: 'column',
    flex: 1,
    width: DEVICE_WIDTH,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  notAuthorized: {
    padding: 20 * SIZE_SCALING,
    color: COLOR_AIA_RED,
  },
});

export default CameraStyles;
