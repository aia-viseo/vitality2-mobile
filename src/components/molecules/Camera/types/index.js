/**
 *
 * @format
 *
 */

// @flow

export type CameraPropsType = {
  refView?: Object,
  onPictureTaken?: () => void,
  onStatusChange?: (status: string) => void,
  onCameraReady?: () => void,
  onCameraMountError?: () => void,
  unauthorizedMessage?: string,
};
