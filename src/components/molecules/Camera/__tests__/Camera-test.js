/**
 *
 * Camera Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Camera from '../Camera';

test('Camera', () => {
  const tree = renderer
    .create(<Camera onPictureTaken={() => {}} onCameraReady={() => {}} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
