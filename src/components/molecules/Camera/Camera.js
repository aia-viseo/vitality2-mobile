/**
 *
 * Camera
 * @format
 *
 */

// @flow

import React from 'react';

import {View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Text from '@atoms/Text';
import {BACK_TYPE, ASPECT_RATIO_STR, WHITE_BALANCE} from './config';

import CameraStyles from './styles';
import type {CameraPropsType} from './types';

const Camera = (props: CameraPropsType) => {
  const {
    refView,
    onPictureTaken,
    onStatusChange,
    onCameraReady,
    onCameraMountError,
    unauthorizedMessage,
  } = props;

  return (
    <View style={CameraStyles.viewWrapper}>
      <RNCamera
        ref={refView}
        style={CameraStyles.preview}
        type={BACK_TYPE}
        onPictureTaken={onPictureTaken}
        ratio={ASPECT_RATIO_STR}
        flashMode="off"
        zoom={0}
        whiteBalance={WHITE_BALANCE}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        captureAudio={false}
        onStatusChange={onStatusChange}
        onCameraReady={onCameraReady}
        onMountError={onCameraMountError}
        notAuthorizedView={
          <View style={CameraStyles.textContainer}>
            <Text customStyle={CameraStyles.notAuthorized}>{unauthorizedMessage}</Text>
          </View>
        }
      />
    </View>
  );
};

Camera.defaultProps = {};

export default Camera;
