/**
 *
 * @format
 *
 */

// @flow

import {Dimensions, StatusBar} from 'react-native';
import {RNCamera} from 'react-native-camera';
import GetOS from '@platform';

const parseRatio = (str) => {
  let [p1, p2] = str.split(':');
  p1 = parseInt(p1, 10);
  p2 = parseInt(p2, 10);
  return p1 / p2;
};

let minusHeight;
let insetTop;
let insetBottom;

const devHeight = Dimensions.get('window').height;
const devWidth = Dimensions.get('window').width;

const isIOS = GetOS.getInstance() === 'ios';
const isIPhoneX =
  isIOS && (devHeight === 812 || devWidth === 812 || devHeight === 896 || devWidth === 896);

if (!isIOS) {
  // on Android, we always have a fixed status bar throught the app
  minusHeight = StatusBar.currentHeight;
  insetTop = 0;
  insetBottom = 0;
} else if (isIPhoneX) {
  minusHeight = 0;
  insetTop = 32;
  insetBottom = 20;
} else {
  minusHeight = 0;
  insetTop = 32;
  insetBottom = 0;
}

const recommendedScaling = () => {
  try {
    // our guideline will be 375 width, based on iphone 7,8 and Xs
    // ideas taken from https://blog.solutotlv.com/size-matters/
    const baseWidth = 375;
    const factor = 0.3; // resize factor

    // Tested Widths in DPI
    // 375 -> iphone 7, 8, X, 11 [Pro] (4.7 to 5.8 inches) --> normal, no prompt
    // 414 -> iPhone X/Xs/11 Max (6.5 inches) --> normal, no prompt
    let res = '1.0'; // our normal sizing

    const {width, height} = Dimensions.get('window');
    const value = Math.min(width, height); // just in case the device starts up rotated.

    let scale;
    let moderateScale;
    if (value !== 0) {
      scale = value / baseWidth;
      moderateScale = 1 + (scale - 1) * factor;

      // map our ratio to our internal scaling values
      if (moderateScale >= 2.5) {
        res = '2.5';
      } else if (moderateScale <= 0.7) {
        // should never happen unless using a tiny phone
        res = '0.7';
      } else {
        res = (Math.round(moderateScale * 10) / 10).toFixed(1);
      }
    } else {
      console.warn(
        'Warning, failed to get device screen ratio for scaling calculations, it was 0.'
      );
    }
    return res;
  } catch (err) {
    return 1;
  }
};

export const MINUS_HEIGHT = minusHeight;
export const INSET_TOP = insetTop;
export const INSET_BOTTOM = insetBottom;
export const DEVICE_WIDTH = devWidth;
export const DEVICE_HEIGHT = devHeight;
export const ASPECT_RATIO_STR = '5:4';
export const SIZE_SCALING = parseFloat(recommendedScaling()) || 1;
export const ASPECT_RATIO = parseRatio(ASPECT_RATIO_STR);
export const BACK_TYPE = RNCamera.Constants.Type.back;
export const WHITE_BALANCE = RNCamera.Constants.WhiteBalance.auto;
