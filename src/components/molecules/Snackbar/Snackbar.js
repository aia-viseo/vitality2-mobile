/**
 *
 *
 * Snackbar
 * @format
 *
 */

// @flow

import React from 'react';
import {Snackbar as PaperSnackbar} from 'react-native-paper';

import type {PropsType} from './types';

const Snackbar = (props: PropsType) => {
  const {
    style,
    wrapperStyle,
    title,
    actionLabel,
    duration,
    visible,
    dismissCallback,
    onPressAction,
  } = props;

  return (
    <PaperSnackbar
      wrapperStyle={wrapperStyle}
      duration={duration}
      style={[style]}
      visible={visible}
      onDismiss={dismissCallback}
      action={
        actionLabel && {
          label: actionLabel,
          onPress: onPressAction,
        }
      }>
      {title}
    </PaperSnackbar>
  );
};

Snackbar.defaultProps = {
  duration: 1000,
  style: {},
  title: 'Snackbar',
  titleStyle: {},
  visible: false,
  dismissCallback: () => {},
  wrapperStyle: {},
};
export default Snackbar;
