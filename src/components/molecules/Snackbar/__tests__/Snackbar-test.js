/**
 *
 *  Test Snackbar
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Snackbar from '../Snackbar';

jest.useFakeTimers();
test('Test Snackbar', () => {
  const tree = renderer.create(<Snackbar />).toJSON();
  expect(tree).toMatchSnapshot();
});
