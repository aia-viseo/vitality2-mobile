## Snackbar
Contain Label and a Button

# Usage & Example

```js
import Snackbar from 'molecules/Snackbar';

<Snackbar
  style={{}}
  title="Title"
  duration={5000}
  titleStyle={{}}
  visible={true}
  dismissCallback={() => {}}
  actionLabel="Action"
  onPressAction={() => {}}
  wrapperStyle={() => {}}
/>
```

Key             | Type            | Required  | Description                                                                                                 |
--------------- | --------------- | --------- | ----------------------------------------------------------------------------------------------------------- |
style           | `ViewStyle`     | `False`   | String on display at the top.                                                                               |
title           | `string | Node` | `False`   | Text content of the Snackbar.                                                                               |
duration        | `number`        | `False`   | The duration for which the Snackbar is shown.                                                               |
titleStyle      | `TextStyle`     | `False`   | Title Style.                                                                                                |
visible         | `boolean`       | `False`   | Whether the Snackbar is currently visible.                                                                  |
dismissCallback | `callback`      | `False`   | Callback called when Snackbar is dismissed. The visible prop needs to be updated when this is called.       |
actionLabel     | `string`        | `False`   | Label of the action button.                                                                                 |
onPressAction   | `callback`      | `False`   | Callback that is called when action button is pressed.                                                      |
wrapperStyle    | `ViewStyle`     | `False`   | Style for the wrapper of the snackbar.                                                                      |
