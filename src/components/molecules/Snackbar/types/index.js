/**
 *
 *  @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  duration?: number,
  style?: ViewStyleProp,
  title?: string | Node,
  visible?: boolean,
  dismissCallback?: () => void,
  actionLabel?: string,
  onPressAction?: () => void,
  wrapperStyle?: ViewStyleProp,
};
