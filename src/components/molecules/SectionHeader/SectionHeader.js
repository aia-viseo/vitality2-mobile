/**
 *
 * Section Header
 * @format
 *
 */

// @flow

import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';

import type {PropsType} from './types';
import SectionHeaderStyles from './styles';

const SectionHeader = (props: PropsType) => {
  const {title, linkText, onPressSeeAll} = props;

  return (
    <View style={SectionHeaderStyles.titleContainer}>
      {title && <Text style={SectionHeaderStyles.title}>{title}</Text>}
      {onPressSeeAll && (
        <TouchableWithoutFeedback onPress={onPressSeeAll}>
          <Text style={SectionHeaderStyles.seeAll}>{linkText}</Text>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};

export default SectionHeader;
