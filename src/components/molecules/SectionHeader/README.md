## SectionHeader
Contain Title and a Button

# Usage & Example
```js
import SectionHeader from 'molecules/SectionHeader';

<SectionHeader
  title={"Assessment"}
  onPressSeeAll={() => {}}
/>
```

Key         | Type         | Required  | Description                       |
----------- | ------------ | --------- | --------------------------------- |
title       | `string`     | `False`   | String on display at the top.     |
onPressItem | `callback`   | `False`   | Trigger when press see all.       |
