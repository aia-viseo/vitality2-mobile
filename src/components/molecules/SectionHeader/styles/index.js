/**
 *
 *  @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import GetOS from '@platform';
import {COLOR_AIA_DARK_GREY, COLOR_AIA_RED} from '@colors';

const isIOS = GetOS.getInstance() === 'ios';

const SectionHeaderStyles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginBottom: 9,
  },
  title: {
    fontSize: RFValue(18),
    color: COLOR_AIA_DARK_GREY,
    fontWeight: isIOS ? '600' : 'bold',
  },
  seeAll: {
    fontSize: RFValue(14),
    color: COLOR_AIA_RED,
  },
});
export default SectionHeaderStyles;
