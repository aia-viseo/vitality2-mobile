/**
 *
 *  Test SectionHeader
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import SectionHeader from '../SectionHeader';

jest.useFakeTimers();
test('Test SectionHeader', () => {
  const tree = renderer.create(<SectionHeader />).toJSON();
  expect(tree).toMatchSnapshot();
});
