/**
 *
 *  @format
 *
 */

// @flow

export type PropsType = {
  title?: string,
  linkText?: string,
  onPressSeeAll?: () => void,
};
