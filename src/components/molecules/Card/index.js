/**
 *
 * Card
 * @format
 *
 */

// @flow

import React from 'react';

import type {CardPropsType} from './types';

import VariationOne from './VariationOne';
import VariationTwo from './VariationTwo';
import VariationThree from './VariationThree';
import VariationFour from './VariationFour';
import VariationFive from './VariationFive';
import VariationSix from './VariationSix';
import VariationEight from './VariationEight';

const Card = (props: CardPropsType) => {
  const {
    variationNumber,
    title,
    imageSrc,
    onPressItem,
    points,
    endDate,
    minPoints,
    maxPoints,
    voucher,
    highlightText,
    description,
    iconName,
    iconGroup,
    name,
    discount,
    quantity,
    targetDate,
    terms,
    subTitle,
    isNew,
    duration,
  } = props;

  switch (variationNumber) {
    case 1:
      return (
        <VariationOne
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          onPressItem={onPressItem}
          points={points}
          title={title}
          variationNumber={variationNumber}
        />
      );

    case 2:
      return (
        <VariationTwo
          endDate={endDate}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          maxPoints={maxPoints}
          minPoints={minPoints}
          onPressItem={onPressItem}
          points={points}
          title={title}
          variationNumber={variationNumber}
          voucher={voucher}
        />
      );

    case 3:
      return (
        <VariationThree
          description={description}
          highlightText={highlightText}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          onPressItem={onPressItem}
          title={title}
          variationNumber={variationNumber}
        />
      );

    case 4:
      return (
        <VariationFour
          discount={discount}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          name={name}
          onPressItem={onPressItem}
          quantity={quantity}
          targetDate={targetDate}
          terms={terms}
          variationNumber={variationNumber}
        />
      );

    case 5:
      return (
        <VariationFive
          description={description}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          onPressItem={onPressItem}
          title={title}
          variationNumber={variationNumber}
        />
      );

    case 6:
      return (
        <VariationSix
          endDate={endDate}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          maxPoints={maxPoints}
          minPoints={minPoints}
          onPressItem={onPressItem}
          points={points}
          title={title}
          variationNumber={variationNumber}
          voucher={voucher}
        />
      );

    case 8:
      return (
        <VariationEight
          duration={duration}
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          isNew={isNew}
          onPressItem={onPressItem}
          points={points}
          subTitle={subTitle}
          title={title}
          variationNumber={variationNumber}
        />
      );
    default:
      return (
        <VariationOne
          iconGroup={iconGroup}
          iconName={iconName}
          imageSrc={imageSrc}
          onPressItem={onPressItem}
          points={points}
          title={title}
          variationNumber={variationNumber}
        />
      );
  }
};

export default Card;
