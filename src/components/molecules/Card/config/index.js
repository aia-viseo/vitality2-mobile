/**
 *
 * @format
 *
 */

// @flow

export const VariationOneImageSize = {height: 110, width: 100};
export const VariationTwoThreeImageSize = {height: 83, width: 150};
export const VariationFiveImageSize = {height: 110, width: 100};
export const VariationEightImageSize = {height: 140, width: 100};
