/**
 *
 * Card Variation Two
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';
import {differenceInDays} from 'date-fns';
import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Text from '@atoms/Text';

import {LinearLocations, greenLinearColors} from '@core/config';
import {VariationTwoThreeImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

import ProgressBar from '../ProgressBar';

const isIOS = GetOS.getInstance() === 'ios';

const VariationSix = (props: CardPropsType) => {
  const {title, points, endDate, minPoints, maxPoints, voucher, onPressItem, iconName} = props;
  const {height, width} = VariationTwoThreeImageSize;
  const {t} = useTranslation();
  const today = new Date();
  let dayDiff = 0;
  if (endDate) {
    dayDiff = differenceInDays(endDate, today);
  }
  let progress = 0;
  if (points !== null && minPoints !== null && maxPoints !== null) {
    progress = (((points || 0) - (minPoints || 0)) / ((maxPoints || 0) - (minPoints || 0))) * 100;
  }
  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.column,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        <View style={[AppStyles.marginTop_20, CardStyles.variationSixTopRow]}>
          {iconName && <Icon group="general" name={iconName} height={height} width={width} />}
          <View
            style={[
              CardStyles.variationSixTitleContainer,
              isIOS && AppStyles.flex_1,
              (iconName && AppStyles.marginLeft_10) || AppStyles.marginLeft_20,
            ]}>
            <View style={AppStyles.justifyContentFlexStart}>
              <Text customStyle={CardStyles.title}>{title}</Text>
            </View>
            <View style={CardStyles.variationSixVoucherContainer}>
              <View style={AppStyles.justifyContentFlexStart}>
                <Text customStyle={CardStyles.description}>
                  <Trans
                    defaults={t('card.Get voucher', {voucher})}
                    parent={RnText}
                    components={[<RnText style={CardStyles.highlight} />]}
                  />
                </Text>
              </View>
              <View style={AppStyles.justifyContentFlexEnd}>
                <Icon group="general" name="arrow_right" />
              </View>
            </View>
          </View>
        </View>

        <View style={CardStyles.variationSixProgressContainer}>
          <View style={AppStyles.row}>
            <Text customStyle={[CardStyles.title, AppStyles.flex_1, {marginBottom: 10}]}>
              {t('challenge.Your Progress')}
            </Text>
            <Text
              customStyle={[
                CardStyles.description,
                AppStyles.alignItemFlexEnd,
                {marginBottom: 10},
              ]}>
              {t('card.days left', {days: dayDiff.toString()})}
            </Text>
          </View>
          {points !== null && maxPoints !== null && (
            <ProgressBar
              leftLabel={`${t('header.earned')}: ${points || 0} ${t('header.pts')}`}
              rightLabel={`${t('challenge.Goal')}: ${maxPoints || 0} ${t('header.pts')}`}
              progress={progress}
              customLinearLocations={LinearLocations}
              customLinearColors={greenLinearColors}
              barHeight={15}
              leftLabelStyle={[AppStyles.marginTop_5, CardStyles.minPts]}
              rightLabelStyle={[AppStyles.marginTop_5, CardStyles.maxPts]}
            />
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationSix;
