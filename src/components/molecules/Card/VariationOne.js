/**
 *
 * Card Variation One
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';

import {VariationOneImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

import NumberString from '../../../core/handler/NumberString';

const isIOS = GetOS.getInstance() === 'ios';

const VariationOne = (props: CardPropsType) => {
  const {title, imageSrc, points, onPressItem, iconName} = props;
  const {height, width} = VariationOneImageSize;
  const {t} = useTranslation();

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.row,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={{width, height}}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={{width, height}}>
            <Icon group="general" name={iconName} height={height} width={width} />
          </View>
        )}
        <View style={[CardStyles.variationOneContent, AppStyles.flex_1]}>
          <Text customStyle={CardStyles.title}>{title}</Text>
          <View style={[CardStyles.descriptionRow, AppStyles.row, AppStyles.flex_1]}>
            {points && points > 0 ? (
              <Text customStyle={[CardStyles.description, AppStyles.flex_1]}>
                <Trans
                  defaults={t('card.Earn up to', {points: NumberString.toLocaleString(points)})}
                  parent={RnText}
                  components={[<RnText style={CardStyles.highlight} />]}
                />
              </Text>
            ) : (
              <Text customStyle={[CardStyles.description, AppStyles.flex_1]}>
                {t('card.No points available')}
              </Text>
            )}
            <View>
              <Icon group="general" name="arrow_right" />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationOne;
