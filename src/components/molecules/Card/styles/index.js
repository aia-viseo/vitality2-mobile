/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {
  COLOR_AIA_DARK_GREY,
  WHITE,
  COLOR_AIA_GREEN,
  COLOR_AIA_GREY_500,
  COLOR_AIA_GREY_200,
  COLOR_AIA_RED,
  COLOR_AIA_GREY_50,
} from '@colors';
import GetOS from '@platform';

const isIOS = GetOS.getInstance() === 'ios';

const CardStyles = StyleSheet.create({
  card: {
    backgroundColor: WHITE,
    borderRadius: 10,
    shadowColor: COLOR_AIA_DARK_GREY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    overflow: 'visible',
    marginLeft: isIOS ? 0 : 2,
    marginVertical: 5,
  },
  image: {
    width: 100,
    height: 110,
  },
  descriptionRow: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: RFValue(14),
    color: COLOR_AIA_DARK_GREY,
    fontWeight: isIOS ? '600' : 'bold',
  },
  grey: {
    color: COLOR_AIA_GREY_500,
  },
  description: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
    flexWrap: 'wrap',
  },
  highlight: {
    color: COLOR_AIA_GREEN,
    fontSize: RFValue(14),
    fontWeight: isIOS ? '600' : 'bold',
  },
  variationOneContent: {
    paddingVertical: 20,
    paddingLeft: 5,
    paddingRight: 20,
    justifyContent: 'space-between',
  },
  variationTwoContent: {
    paddingHorizontal: 20,
    paddingTop: 15,
    paddingBottom: 20,
  },
  variationFourContent: {
    flex: 1,
  },
  variationFiveContent: {
    flex: 1,
    padding: 15,
    justifyContent: 'space-between',
  },
  variationEightContent: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  variationTwoImage: {
    width: 150,
    height: 83,
  },
  variationFourImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    overflow: 'hidden',
  },
  minPts: {
    color: COLOR_AIA_GREEN,
    fontSize: RFValue(12),
  },
  maxPts: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(12),
  },
  progressBar: {
    padding: 3,
    borderRadius: 6,
  },
  progressBarBackground: {
    width: '100%',
    borderRadius: 6,
    backgroundColor: COLOR_AIA_GREY_200,
  },
  progressBarContainer: {
    marginTop: 10,
  },
  cardBorder: {
    borderWidth: 1,
    borderColor: COLOR_AIA_GREY_200,
  },
  name: {
    fontSize: RFValue(12),
    color: COLOR_AIA_DARK_GREY,
  },
  targetDate: {
    color: COLOR_AIA_RED,
  },
  moreInfoContainer: {
    flexDirection: 'row-reverse',
    marginTop: 20,
  },
  fixHeight: {
    height: 200,
  },
  variationSixTopRow: {
    flexDirection: 'row',
  },
  variationSixTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginRight: 20,
  },
  variationSixVoucherContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  variationSixProgressContainer: {
    backgroundColor: COLOR_AIA_GREY_50,
    paddingHorizontal: 20,
    marginTop: 20,
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
});

export default CardStyles;
