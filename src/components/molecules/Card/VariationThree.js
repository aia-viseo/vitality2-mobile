/**
 *
 * Card Variation Three
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';

import {VariationTwoThreeImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

const isIOS = GetOS.getInstance() === 'ios';

const VariationThree = (props: CardPropsType) => {
  const {title, imageSrc, highlightText, description, onPressItem, iconName} = props;
  const {height, width} = VariationTwoThreeImageSize;

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.column,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={{width, height}}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={{width, height}}>
            <Icon group="general" name={iconName} width={width} height={height} />
          </View>
        )}
        <View
          style={[
            CardStyles.variationTwoContent,
            AppStyles.justifyContentSpaceBetween,
            isIOS ? AppStyles.flex_1 : CardStyles.fixHeight,
          ]}>
          <Text customStyle={CardStyles.title}>{title}</Text>
          <View
            style={[CardStyles.descriptionRow, AppStyles.marginTop_20, isIOS && AppStyles.flex_1]}>
            <View style={[AppStyles.flex_1, AppStyles.column]}>
              <View>
                <Text
                  customStyle={[CardStyles.description, isIOS && AppStyles.flex_1]}
                  numberOfLines={3}
                  ellipsizeMode="tail">
                  {description}
                </Text>
              </View>
              <View
                style={[
                  AppStyles.row,
                  AppStyles.alignItemFlexEnd,
                  AppStyles.justifyContentSpaceBetween,
                ]}>
                <Text customStyle={[CardStyles.highlight, AppStyles.flex_1]}>{highlightText}</Text>
                <Icon group="general" name="arrow_right" />
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationThree;
