/**
 *
 *  Test Card Variation Four
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import VariationFour from '../VariationFour';

jest.useFakeTimers();
test('Test Card Variation Four', () => {
  const data = {
    name: 'Weekly fitness challenge',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    discount: '50%',
    iconName: 'arrow',
    iconGroup: 'general',
    quantity: 2,
    targetDate: new Date(),
    terms: 'Terms and conditions',
    onPressItem: () => {},
  };
  const tree = renderer
    .create(
      <VariationFour
        variationNumber={4}
        name={data.name}
        imageSrc={data.imageSrc}
        discount={data.discount}
        iconName={data.iconName}
        iconGroup={data.iconGroup}
        quantity={data.quantity}
        targetDate={data.targetDate}
        terms={data.terms}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
