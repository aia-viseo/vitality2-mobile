/**
 *
 *  Test Card Variation One
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import VariationOne from '../VariationOne';

jest.useFakeTimers();
test('Test Card Variation One', () => {
  const data = {
    title: 'Lorem ipsum dolor sit amet',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    points: 1000,
    onPressItem: () => {},
  };
  const tree = renderer
    .create(
      <VariationOne
        variationNumber={1}
        title={data.title}
        imageSrc={data.imageSrc}
        points={data.points}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
