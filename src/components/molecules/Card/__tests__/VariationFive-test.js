/**
 *
 *  Test Card Variation Five
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import VariationFive from '../VariationFive';

jest.useFakeTimers();
test('Test Card Variation Five', () => {
  const data = {
    title: 'Weekly fitness challenge',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    iconName: 'arrow',
    description: 'Terms and conditions',
    onPressItem: () => {},
  };
  const tree = renderer
    .create(
      <VariationFive
        variationNumber={5}
        title={data.title}
        imageSrc={data.imageSrc}
        description={data.description}
        iconName={data.iconName}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
