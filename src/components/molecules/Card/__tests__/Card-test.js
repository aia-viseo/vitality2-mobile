/**
 *
 *  Test Card
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import Card from '../index';

jest.useFakeTimers();
test('Test Card', () => {
  const data = {
    title: 'Weekly fitness challenge',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    iconName: 'arrow',
    description: 'Terms and conditions',
    onPressItem: () => {},
  };
  const tree = renderer
    .create(
      <Card
        variationNumber={5}
        title={data.title}
        imageSrc={data.imageSrc}
        description={data.description}
        iconName={data.iconName}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
