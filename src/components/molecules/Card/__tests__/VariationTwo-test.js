/**
 *
 *  Test Card Variation Two
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import VariationTwo from '../VariationTwo';

jest.useFakeTimers();
test('Test Card Variation Two', () => {
  const data = {
    title: 'Weekly fitness challenge',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    points: 210,
    endDate: new Date(),
    minPoints: 150,
    maxPoints: 250,
    voucher: 1,
    onPressItem: () => {},
  };

  const tree = renderer
    .create(
      <VariationTwo
        variationNumber={2}
        title={data.title}
        imageSrc={data.imageSrc}
        points={data.points}
        endDate={data.endDate}
        minPoints={data.minPoints}
        maxPoints={data.maxPoints}
        voucher={data.voucher}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
