/**
 *
 *  Test Card Variation One
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import '@clients/i18n';

import VariationThree from '../VariationThree';

jest.useFakeTimers();
test('Test Card Variation Three', () => {
  const data = {
    title: 'Weekly fitness challenge',
    imageSrc: 'https://reactnative.dev/img/tiny_logo.png',
    highlightText: 'Apple',
    description: 'I am a boy',
    iconName: 'arrow',
    onPressItem: () => {},
  };
  const tree = renderer
    .create(
      <VariationThree
        variationNumber={3}
        title={data.title}
        imageSrc={data.imageSrc}
        highlightText={data.highlightText}
        description={data.description}
        iconName={data.iconName}
        onPressItem={data.onPressItem}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
