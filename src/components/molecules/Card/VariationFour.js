/**
 *
 * Card Variation Four
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import AppStyles from '@styles';
import GetOS from '@platform';

import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';
import Chip from '@atoms/Chip';

import CardStyles from './styles';
import type {CardPropsType} from './types';

const isIOS = GetOS.getInstance() === 'ios';
const WEEKDAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const VariationFour = (props: CardPropsType) => {
  const {
    name,
    imageSrc,
    discount,
    onPressItem,
    iconName,
    iconGroup,
    quantity,
    targetDate,
    terms,
  } = props;
  const {t} = useTranslation();
  let targetDay = 0;

  if (targetDate) {
    targetDay = new Date(targetDate).getDay();
  }

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.row,
          AppStyles.padding_20,
          AppStyles.alignItemCenter,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={CardStyles.variationFourImage}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={CardStyles.variationFourImage}>
            <Icon group={iconGroup} name={iconName} />
          </View>
        )}
        <View
          style={[
            AppStyles.row,
            CardStyles.variationFourContent,
            AppStyles.justifyContentSpaceBetween,
            AppStyles.marginHorizontal_20,
          ]}>
          <View style={[AppStyles.column, {flex: 1}]}>
            <Text customStyle={CardStyles.name}>{name}</Text>
            <Text customStyle={CardStyles.description}>
              <Trans
                defaults={t('card.Discount off', {discount})}
                parent={RnText}
                components={[<RnText style={[CardStyles.highlight, AppStyles.fontSize_20]} />]}
              />
            </Text>
            {targetDate && (
              <Text customStyle={CardStyles.description}>
                <Trans
                  defaults={t('card.Use one by', {targetDate: WEEKDAYS[targetDay]})}
                  parent={RnText}
                  components={[<RnText style={CardStyles.targetDate} />]}
                />
              </Text>
            )}
            {terms && <Text customStyle={CardStyles.description}>{terms}</Text>}
          </View>
          {quantity && quantity > 1 && (
            <View>
              <Chip text={`X${quantity}`} />
            </View>
          )}
        </View>

        <Icon group="general" name="arrow_right" />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationFour;
