/**
 *
 * Card
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import AppStyles from '@styles';

import type {CardContainerPropType} from './types';

const Card = (props: CardContainerPropType) => {
  const {children, customPadding} = props;

  return (
    <View
      style={[
        AppStyles.mainCard,
        customPadding || AppStyles.padding_20,
        AppStyles.row,
        AppStyles.alignItemCenter,
        AppStyles.justifyContentSpaceBetween,
      ]}>
      {children}
    </View>
  );
};

export default Card;
