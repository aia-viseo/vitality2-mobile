/**
 *
 * Inner Card
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import AppStyles from '@styles';
import type {CardContainerPropType} from './types';

const InnerCard = (props: CardContainerPropType) => {
  const {children} = props;

  return (
    <View style={[AppStyles.row, AppStyles.alignItemCenter, AppStyles.justifyContentFlexStart]}>
      {children}
    </View>
  );
};

export default InnerCard;
