/**
 *
 * Card Variation Five
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';
import Chip from '@atoms/Chip';

import {VariationEightImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

const isIOS = GetOS.getInstance() === 'ios';

const VariationEight = (props: CardPropsType) => {
  const {height, width} = VariationEightImageSize;
  const {
    title,
    subTitle,
    points,
    isNew,
    duration,
    durationUnit,
    imageSrc,
    onPressItem,
    iconName,
  } = props;
  const {t} = useTranslation();

  const renderPoint = () => {
    return (
      <View style={[AppStyles.row, AppStyles.alignItemCenter, AppStyles.marginBottom_10]}>
        <Icon group="general" name="earn_points" />
        <View style={[AppStyles.marginLeft_5]}>
          <Trans
            defaults={t('card.Up to points', {points})}
            parent={RnText}
            components={[<RnText style={[CardStyles.highlight]} />]}
          />
        </View>
      </View>
    );
  };

  const renderDuration = () => {
    return (
      <View style={[AppStyles.row, AppStyles.alignItemCenter]}>
        <Icon group="general" name="hourglass" />
        <View style={[AppStyles.marginLeft_5]}>
          <Trans
            defaults={t('card.Takes', {duration}, {durationUnit})}
            parent={RnText}
            components={[<RnText style={[CardStyles.highlight]} />]}
          />
        </View>
      </View>
    );
  };

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.row,
          CardStyles.cardBorder,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={{width, height}}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={{width, height}}>
            <Icon group="general" name={iconName} height={height} width={width} />
          </View>
        )}
        <View style={[CardStyles.variationEightContent, AppStyles.justifyContentSpaceBetween]}>
          <View>
            <Text customStyle={[CardStyles.title, CardStyles.grey]}>{title}</Text>
            <View style={[AppStyles.row, AppStyles.alignItemCenter]}>
              <Text customStyle={[CardStyles.title, AppStyles.flex_1]}>{subTitle}</Text>
              {isNew && (
                <View>
                  <Chip text={t('card.new')} mode="flat" />
                </View>
              )}
            </View>
          </View>

          <View
            style={[
              AppStyles.row,
              AppStyles.alignItemFlexEnd,
              AppStyles.justifyContentSpaceBetween,
            ]}>
            <View>
              {renderPoint()}
              {renderDuration()}
            </View>
            <Icon group="general" name="arrow_right" />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationEight;
