/**
 *
 * Card Variation Two
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback, Text as RnText} from 'react-native';
import {useTranslation, Trans} from 'react-i18next';

import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';

import {LinearLocations, greenLinearColors} from '@core/config';
import {VariationTwoThreeImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

import ProgressBar from '../ProgressBar';

const isIOS = GetOS.getInstance() === 'ios';

const VariationTwo = (props: CardPropsType) => {
  const {
    title,
    imageSrc,
    points,
    endDate,
    minPoints,
    maxPoints,
    voucher,
    onPressItem,
    iconName,
  } = props;
  const {height, width} = VariationTwoThreeImageSize;
  const {t} = useTranslation();
  const today = new Date();
  let dayDiff = 0;
  if (endDate) {
    const endDateObj = new Date(endDate);
    dayDiff = endDateObj.getDate() - today.getDate();
  }
  let progress = 0;
  if (points && minPoints && maxPoints) {
    progress = ((points - minPoints) / (maxPoints - minPoints)) * 100;
  }

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.column,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={{width, height}}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={{width, height}}>
            <Icon group="general" name={iconName} height={height} width={width} />
          </View>
        )}
        <View style={CardStyles.variationTwoContent}>
          <Text customStyle={CardStyles.title}>{title}</Text>
          <Text customStyle={CardStyles.description}>
            {t('card.days left', {days: dayDiff.toString()})}
          </Text>
          {minPoints && maxPoints && (
            <ProgressBar
              leftLabel={`${minPoints} pts`}
              rightLabel={`${maxPoints} pts`}
              progress={progress}
              customLinearLocations={LinearLocations}
              customLinearColors={greenLinearColors}
              barHeight={6}
              leftLabelStyle={AppStyles.minPts}
              rightLabelStyle={AppStyles.maxPts}
            />
          )}
          <View
            style={[CardStyles.descriptionRow, AppStyles.marginTop_20, isIOS && AppStyles.flex_1]}>
            <Text customStyle={CardStyles.description}>
              <Trans
                defaults={t('card.Get voucher', {voucher})}
                parent={RnText}
                components={[<RnText style={CardStyles.highlight} />]}
              />
            </Text>
            <View>
              <Icon group="general" name="arrow_right" />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationTwo;
