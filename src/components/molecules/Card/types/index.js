/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type CardPropsType = {
  variationNumber: number,
  title?: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points?: number | null,
  endDate?: Date,
  minPoints?: number | null,
  maxPoints?: number | null,
  voucher?: number,
  highlightText?: string,
  description?: string,
  name?: string,
  discount?: string,
  quantity?: number,
  targetDate?: Date,
  terms?: string,
  onPressItem?: () => void,
  subTitle?: string,
  isNew?: boolean,
  duration?: number,
  durationUnit?: string,
};

export type CardContainerPropType = {
  children?: Node,
  customPadding?: ViewStyleProp,
};
