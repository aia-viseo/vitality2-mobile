## Card
A General molecules component for different variations

# CardVariation1
# Usage
```js
import Card from '/molecules/Card';

<Card
  variationNumber={1}
  title
  imageSrc
  points
  onPressItem
/>
```

# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={1}
  title={'Jack'}
  imageSrc={'https://reactnative.dev/img/tiny_logo.png'}
  points={10000}
  onPressItem={() => {}}
/>
```

Key              | Type       | Required | Description                                                |
---------------- | ---------- | -------- | ---------------------------------------------------------- |
variationNumber  | `number`   | `True`   | Number to define which variation for the card.             |
title            | `string`   | `True`   | String to display on the title area for variation 1.       |
imageSrc         | `string`   | `True`   | Image url to display on the image area for variation 1.    |
points           | `number`   | `True`   | Number to display on the description area for variation 1. |
onPressItem      | `function` | `False`   | Function to trigger when press the item.                   |

# CardVariation2
# Usage
```js
import Card from '/molecules/Card';

<Card
  variationNumber
  title
  imageSrc
  points
  endDate
  minPoints
  maxPoints
  voucher
  onPressItem
/>
```

# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={2}
  title={'Jack'}
  imageSrc={'https://reactnative.dev/img/tiny_logo.png'}
  points={10000}
  endDate={new Date()}
  minPoints={0}
  maxPoints={10000000}
  voucher={1}
  onPressItem={() => {}}
/>
```

Key              | Type       | Required | Description                                                |
---------------- | ---------- | -------- | ---------------------------------------------------------- |
variationNumber  | `number`   | `True`   | Number to define which variation for the card.             |
title            | `string`   | `True`   | String to display on the title area for variation 2.       |
imageSrc         | `string`   | `True`   | Image url to display on the image area for variation 2.    |
points           | `number`   | `True`   | Number of progress on the progress bar.                    |
endDate          | `Date`     | `True`   | Date to calculate how many day left.                       |
minPoints        | `number`   | `True`   | Number of min points on the progress bar.                  |
maxPoints        | `number`   | `True`   | Number of max points on the progress bar.                  |
voucher          | `number`   | `True`   | Number to display on the description area for variation 2. |
onPressItem      | `function` | `False`   | Function to trigger when press the item.                   |

# CardVariation3
# Usage
```js
import Card from '/molecules/Card';

<Card
  variationNumber
  title
  imageSrc
  highlightText
  description
  onPressItem
/>
```

# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={3}
  title={'Jack'}
  imageSrc={'https://reactnative.dev/img/tiny_logo.png'}
  highlightText={"Hi"}
  description={"Apple"}
  onPressItem={() => {}}
/>
```

Key              | Type       | Required | Description                                                               |
---------------- | ---------- | -------- | ------------------------------------------------------------------------  |
variationNumber  | `number`   | `True`   | Number to define which variation for the card.                            |
title            | `string`   | `True`   | String to display on the title area for variation 3.                      |
highlightText    | `string`   | `True`   | String to display at the bottom area in highlighted for variation 3.      |
description      | `string`   | `True`   | String to display at the bottom area  for variation 3.                    |
onPressItem      | `function` | `False`   | Function to trigger when press the item.                                  |


# CardVariation4
# Usage
```js
import Card from '/molecules/Card';

<Card
  variationNumber
  name
  iconName
  iconGroup
  discount
  quantity
  targetDate
  terms
  onPressItem
/>
```

# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={4}
  name={"Apple"}
  iconName={"arrow"}
  iconGroup="general"
  discount={"50%"}
  quantity={2}
  targetDate={new Date()}
  terms={"Terms Terms"}
  onPressItem={() => {}}
/>
```

Key              | Type       | Required | Description                                             |
---------------- | ---------- | -------- | ------------------------------------------------------  |
variationNumber  | `number`   | `True`   | Number to define which variation for the card.          |
name             | `string`   | `True`   | String to display on the name area for variation 4.     |
iconName         | `string`   | `True`   | String to render icon on image area for variation 4.    |
iconGroup        | `string`   | `True`   | String to render icon group for image                   |
discount         | `string`   | `True`   | String to display on discount area  for variation 4.    |
quantity         | `number`   | `False`  | Number to render Chip on quantity for variation 4.      |
targetDate       | `Date`     | `False`  | String to display on target date area for variation 4.  |
terms            | `string`   | `False`  | String to display on terms area for variation 4.        |
onPressItem      | `function` | `False`  | Function to trigger when press the item.                |


# CardVariation5
# Usage
```js
import Card from '/molecules/Card';

<Card
  variationNumber
  title
  imageSrc
  description
  onPressItem
/>
```

# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={5}
  title={"Jck"}
  imageSrc={'https://reactnative.dev/img/tiny_logo.png'}
  description={"Apple"}
  onPressItem={() => {}}
/>
```

Key              | Type        | Required | Description                                                 |
---------------- | ----------- | -------- | ----------------------------------------------------------- |
variationNumber  | `number`    | `True`   | Number to define which variation for the card.              |
title            | `string`    | `True`   | String to display on the title area for variation 5.        |
description      | `string`    | `True`   | String to display on the description area for variation 5.  |
imageSrc         | `string`    | `True`   | Image url to display on the image area for variation 5.     |
onPressItem      | `function`  | `False`  | Number of progress on the progress bar.                     |


# Example
```js
import Card from '/molecules/Card';

<Card
  variationNumber={6}
  name={"Apple"}
  iconName={"arrow"}
  iconGroup="general"
  imageSrc={"arrow"}
  description={"Terms Terms"}
  onPressItem={() => {}}
/>
```

Key              | Type       | Required | Description                                             |
---------------- | ---------- | -------- | ------------------------------------------------------  |
variationNumber  | `number`   | `True`   | Number to define which variation for the card.          |
name             | `string`   | `True`   | String to display on the name area for variation 6.     |
iconName         | `string`   | `True`   | String to render icon on image area for variation 6.    |
iconGroup        | `string`   | `True`   | String to render icon group for image                   |
imageSrc         | `string`   | `True`   | Image url to display on the image area for variation 6. |
description      | `string`   | `False`  | String to display on description area for variation 6.  |
onPressItem      | `function` | `False`  | Function to trigger when press the item.                |
