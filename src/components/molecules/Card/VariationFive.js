/**
 *
 * Card Variation Five
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import AppStyles from '@styles';
import GetOS from '@platform';
import Icon from '@atoms/Icon';
import Image from '@atoms/Image';
import Text from '@atoms/Text';

import {VariationFiveImageSize} from './config';
import type {CardPropsType} from './types';
import CardStyles from './styles';

const isIOS = GetOS.getInstance() === 'ios';

const VariationFive = (props: CardPropsType) => {
  const {title, imageSrc, description, onPressItem, iconName} = props;
  const {height, width} = VariationFiveImageSize;

  return (
    <TouchableWithoutFeedback onPress={onPressItem}>
      <View
        style={[
          CardStyles.card,
          AppStyles.row,
          CardStyles.cardBorder,
          AppStyles.marginRight_20,
          isIOS && AppStyles.flex_1,
        ]}>
        {imageSrc && (
          <Image
            customStyle={{width, height}}
            source={{
              uri: imageSrc,
            }}
          />
        )}
        {iconName && (
          <View style={{width, height}}>
            <Icon group="general" name={iconName} height={height} width={width} />
          </View>
        )}
        <View style={[CardStyles.variationFiveContent, AppStyles.justifyContentSpaceBetween]}>
          <View
            style={[
              AppStyles.row,
              AppStyles.alignItemCenter,
              AppStyles.justifyContentSpaceBetween,
            ]}>
            <Text customStyle={[CardStyles.title, AppStyles.flex_1]}>{title}</Text>
            <Icon group="general" name="arrow_right" />
          </View>
          <View>
            <Text numberOfLines={3} ellipsizeMode="tail" customStyle={CardStyles.description}>
              {description}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default VariationFive;
