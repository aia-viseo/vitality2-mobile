/**
 *
 * Card Container
 * @format
 *
 */

// @flow

import React from 'react';

import type {CardContainerPropType} from './types';

import InnerCard from './InnerCard';
import Card from './Card';

const CardContainer = (props: CardContainerPropType) => {
  const {children, customPadding} = props;

  return (
    <InnerCard>
      <Card customPadding={customPadding}>{children}</Card>
    </InnerCard>
  );
};

export default CardContainer;
