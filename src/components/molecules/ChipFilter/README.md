## ChipFilter
Contain horizontal scrollable chips list

# Usage
```js
import ChipFilter from 'molecules/ChipFilter';

<ChipFilter
  data
  selectedIndex
  onPressChip
/>
```

# Example
```js
import ChipFilter from 'molecules/ChipFilter';

<ChipFilter
  data={[
    {id: 1, key: 'Recommended'},
    {id: 2, key: 'Completed'}
  ]}
  selectedIndex={0}
  onPressChip={() => {}}
/>
```

Key           | Type           | Required  | Description               |
------------- | -------------- | --------- | ------------------------- |
data          | `callback`     | `True`    | Array of chips data.      |
selectedIndex | `number`       | `False`   | Id of selected item.      |
onPressItem   | `callback`     | `False`   | Trigger when press chip.  |
