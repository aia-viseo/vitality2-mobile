/**
 *
 * Header
 * @format
 *
 */

// @flow

import React from 'react';
import Animated from 'react-native-reanimated';
import {FlatList, View, TouchableWithoutFeedback} from 'react-native';
import {useTranslation} from 'react-i18next';

import AppStyles from '@styles';
import {WHITE} from '@colors';
import GetOS from '@platform';
import Chip from '@atoms/Chip';

import type {PropsType} from './types';

const isIOS = GetOS.getInstance() === 'ios';
const ChipFilter = (props: PropsType) => {
  const {
    data,
    onPressChip,
    selectedIndex,
    selectedName,
    withContainer,
    hideBackground,
    scrollY,
    headerHeightDiff,
  } = props;
  const {t} = useTranslation();

  const renderChipFilter = () => {
    let items = [];
    items = data;
    return (
      <FlatList
        bounces={false}
        style={[AppStyles.paddingHorizontal_20]}
        showsHorizontalScrollIndicator={false}
        horizontal
        data={items}
        renderItem={({item, index}) => {
          const {key, name} = item;
          const isSelected = selectedIndex === index || selectedName === name;
          const isEnd = index === items.length - 1;

          return (
            <TouchableWithoutFeedback onPress={() => onPressChip && onPressChip(item)}>
              <View style={[isEnd ? AppStyles.marginRight_40 : AppStyles.marginRight_10]}>
                <Chip text={t(key)} mode={isSelected ? 'flat' : 'outlined'} />
              </View>
            </TouchableWithoutFeedback>
          );
        }}
        keyExtractor={(item) => `${item.id}`}
      />
    );
  };

  const renderChipFilterWithContainer = () => {
    const inputPrefix = headerHeightDiff || 0;

    return (
      <Animated.View
        style={[
          AppStyles.mainFilterContainer,
          {
            transform: [{translateY: isIOS ? '-100%' : -110}],
            backgroundColor: hideBackground ? 'transparent' : WHITE,
            borderBottomLeftRadius: 25,
          },
        ]}>
        <Animated.View
          style={[
            {
              paddingTop: Animated.interpolate(scrollY, {
                inputRange: [0, inputPrefix * 2 - 1, inputPrefix * 2],
                outputRange: [70, 40, 40],
                extrapolate: 'clamp',
              }),
              paddingBottom: 20,
            },
          ]}>
          {renderChipFilter()}
        </Animated.View>
      </Animated.View>
    );
  };

  return <>{withContainer ? renderChipFilterWithContainer() : renderChipFilter()}</>;
};

export default ChipFilter;
