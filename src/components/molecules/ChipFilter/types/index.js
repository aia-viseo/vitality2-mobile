/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  data: Array<{[key: string]: any}>,
  onPressChip?: (item: {[key: string]: any}) => void,
  selectedIndex?: number,
  selectedName?: string,
  withContainer?: boolean,
  hideBackground?: boolean,
  scrollY?: number,
  headerHeightDiff?: number,
};
