/**
 *
 *  Test ChipFilter
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import ChipFilter from '../ChipFilter';

test('Test ChipFilter ', () => {
  const data = [
    {id: 1, key: 'Recommended'},
    {id: 2, key: 'Completed'},
  ];
  const tree = renderer.create(<ChipFilter data={data} />).toJSON();
  expect(tree).toMatchSnapshot();
});
