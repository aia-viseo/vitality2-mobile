/**
 *
 * ProgressBar
 * @format
 *
 */

// @flow

import React from 'react';
import {Text, View} from 'react-native';

import LinearGradient from '@atoms/LinearGradient';

import {LinearLocations, greenLinearColors} from '@core/config';
import {defaultHeight} from './config';
import type {PropsType} from './types';
import ProgressBarStyles from './styles';

const ProgressBar = (props: PropsType) => {
  const {
    leftLabel,
    rightLabel,
    progress,
    customLinearLocations,
    customLinearColors,
    barHeight,
    leftLabelStyle,
    rightLabelStyle,
  } = props;

  const radius = (barHeight || defaultHeight) / 2;
  let widthBase = progress;
  if (progress && progress > 100) {
    widthBase = 100;
  }
  return (
    <View>
      <View style={[ProgressBarStyles.background, {borderRadius: radius}]}>
        <LinearGradient
          startX={0}
          startY={0}
          endX={1}
          endY={0}
          customLinearLocations={customLinearLocations || LinearLocations}
          customLinearColors={customLinearColors || greenLinearColors}
          extraStyle={{width: `${widthBase || 0}%`, padding: radius, borderRadius: radius}}
        />
      </View>
      <View style={ProgressBarStyles.labelContainer}>
        <Text style={[ProgressBarStyles.leftLabel, leftLabelStyle]}>{leftLabel || ''}</Text>
        <Text style={[ProgressBarStyles.rightLabel, rightLabelStyle]}>{rightLabel || ''}</Text>
      </View>
    </View>
  );
};

export default ProgressBar;
