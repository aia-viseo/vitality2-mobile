## Progress Bar
Atom Component to render progress bar with default config

# Usage
```js
import ProgressBar from '/atoms/ProgressBar';

<ProgressBar
  leftLabel
  rightLabel
  progress
  customLinearLocations
  customLinearColors
  barHeight
  leftLabelStyle
  rightLabelStyle
/>
```

# Example
```js
import ProgressBar from '/atoms/ProgressBar';

<ProgressBar
  leftLabel={"0 pts"}
  rightLabel={"100 pts"}
  progress={35}
  customLinearLocations={[0, 0.62, 1]}
  customLinearColors={['#000', '#ddd', '#fff']}
  barHeight={6}
  leftLabelStyle={{fontSize: 10}}
  rightLabelStyle={{fontSize: 10}}
/>
```

Key                      | Type              | Required | Description                                                                     |
------------------------ | ----------------- | -------- | ------------------------------------------------------------------------------- |
leftLabel                | `string`          | `False`  | Text string to display bottom left of the progress bar.                         |
rightLabel               | `string`          | `False`  | Text string to display bottom right of the progress bar.                        |
progress                 | `number`          | `False`  | Number for the progress.                                                        |
customLinearLocations    | `Array<number>`   | `False`  | An optional array of numbers defining the location of each gradient color stop. |
customLinearColors       | `Array<string>`   | `False`  | An array of at least two color values that represent gradient colors.           |
barHeight                | `number`          | `False`  | Number for the progress bar height.                                             |
leftLabelStyle           | `Object`          | `False`  | Optional Style for left label.                                                  |
rightLabelStyle          | `Object`          | `False`  | Optional Style for right label.                                                 |
