/**
 *
 *  Test Progress bar
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import ProgressBar from '../ProgressBar';

test('Test Progress bar ', () => {
  const tree = renderer
    .create(
      <ProgressBar
        leftLabel="0 pts"
        rightLabel="100 pts"
        progress={35}
        customLinearLocations={[0, 0.62, 1]}
        customLinearColors={['#000', '#ddd', '#fff']}
        barHeight={6}
        leftLabelStyle={{fontSize: 10}}
        rightLabelStyle={{fontSize: 10}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
