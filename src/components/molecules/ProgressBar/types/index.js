/**
 *
 * @format
 *
 */

// @flow

import type {TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  leftLabel?: string,
  rightLabel?: string,
  progress?: number,
  customLinearLocations?: Array<number>,
  customLinearColors?: Array<string>,
  barHeight?: number,
  leftLabelStyle?: TextStyleProp,
  rightLabelStyle?: TextStyleProp,
};
