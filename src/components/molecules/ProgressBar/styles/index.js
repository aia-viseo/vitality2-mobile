/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_GREEN, COLOR_AIA_GREY_200, COLOR_AIA_DARK_GREY} from '@colors';

const ProgressBarStyles = StyleSheet.create({
  background: {
    width: '100%',
    backgroundColor: COLOR_AIA_GREY_200,
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftLabel: {
    color: COLOR_AIA_GREEN,
    fontSize: RFValue(12),
  },
  rightLabel: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(12),
  },
});

export default ProgressBarStyles;
