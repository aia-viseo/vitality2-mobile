/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {BLACK, WHITE, COLOR_AIA_GREY_500, COLOR_AIA_ICON_GRAY} from '@colors';

const MembershipProfileStyle = StyleSheet.create({
  container: {
    backgroundColor: WHITE,
    flexDirection: 'column',
    borderRadius: 10,
    shadowColor: BLACK,
  },
  memberInfo: {
    flexDirection: 'row',
    height: 50,
  },
  memberBadge: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: COLOR_AIA_ICON_GRAY,
  },
  username: {
    fontSize: RFValue(16),
    fontWeight: '600',
  },
  memberDetailsContainer: {
    flexDirection: 'column',
    marginLeft: 10,
  },
  defaultText: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
  },
  boldText: {
    fontWeight: '600',
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
  },
  pointsDetailsContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 20,
    justifyContent: 'space-between',
  },
  remainingPointsContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  nextLevel: {
    color: COLOR_AIA_GREY_500,
    fontSize: RFValue(12),
  },
});

export default MembershipProfileStyle;
