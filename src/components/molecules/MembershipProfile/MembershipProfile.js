/**
 *
 * MembershipProfile
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import {useTranslation} from 'react-i18next';

import Icon from '@atoms/Icon';
import Text from '@atoms/Text';
import DateTime from '@core/handler/DateTime';
import NumberString from '@core/handler/NumberString';
import PointsProgressBar from '../PointsProgressBar';
import LEVEL_CONFIG from '../MembershipBadge/config';

import type {PropsType} from './types';
import Styles from './styles';

const MembershipProfile = (props: PropsType) => {
  const {t} = useTranslation();
  const {
    user: {
      level,
      nextLevel,
      membershipNumber,
      membershipEndDate,
      points,
      pointsToNextStatus,
      username,
    },
    pointsInfoCallback,
  } = props;
  const config = LEVEL_CONFIG[level];
  const configNext = LEVEL_CONFIG[nextLevel];
  const medal = (level || '').toLowerCase();

  const MemberInfo = () => {
    const formattedEndDate = DateTime.format(new Date(membershipEndDate), 'MMM dd, yyyy');

    return (
      <View style={Styles.memberInfo}>
        <View style={Styles.memberBadge}>
          <Icon group="medals" name={medal} width={25} />
        </View>
        <View style={Styles.memberDetailsContainer}>
          <Text customStyle={Styles.username}>{username}</Text>
          <Text customStyle={Styles.defaultText}>
            {t('header.membershipNum')}: {membershipNumber}
          </Text>
          <Text customStyle={Styles.defaultText}>
            {t('header.membershipExpiry')}: {formattedEndDate}
          </Text>
        </View>
      </View>
    );
  };

  const PointsDetails = () => {
    const currentPoints = NumberString.toLocaleString(points);
    const nextLevelPoints = NumberString.toLocaleString(pointsToNextStatus);
    return (
      <View style={Styles.pointsDetailsContainer}>
        <Text customStyle={Styles.defaultText}>
          <Text customStyle={Styles.boldText}>{t('header.earned')}: </Text>
          {currentPoints} {t('header.pts')}
        </Text>
        <View style={Styles.remainingPointsContainer}>
          <Text customStyle={Styles.defaultText}>
            {nextLevelPoints} {t('header.ptsTo')}{' '}
            <Text customStyle={[Styles.boldText, {color: configNext.color}]}>{nextLevel}</Text>
          </Text>
        </View>
      </View>
    );
  };

  const progress = () => {
    const actualProgress = points / (points + pointsToNextStatus);
    return actualProgress < 0.1 ? 0.085 : actualProgress; // we set a minimum to make it look nicer
  };

  return (
    <View style={Styles.container}>
      <MemberInfo />
      <PointsProgressBar
        label={t(config.labelKey)}
        color={config.color}
        progress={progress()}
        infoCallback={pointsInfoCallback}
      />
      <PointsDetails />
    </View>
  );
};
export default MembershipProfile;
