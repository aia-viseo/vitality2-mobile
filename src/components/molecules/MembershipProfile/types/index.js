/**
 *
 * @format
 *
 */

// @flow

import type {UserInfoType} from '@contexts/Global/types';

export type PropsType = {
  user: UserInfoType,
  pointsInfoCallback: () => void,
};
