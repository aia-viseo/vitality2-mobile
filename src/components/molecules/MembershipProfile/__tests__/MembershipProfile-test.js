/**
 *
 *  Test MembershipProfile
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import type {UserInfoType} from '@contexts/Global/types';
import MembershipProfile from '../MembershipProfile';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({t: (key) => key}),
}));

test('Test MembershipProfile ', () => {
  const userInfo: UserInfoType = {
    points: 100,
    level: 'Bronze',
    membershipEndDate: '2021-05-21',
    membershipNumber: 'ABC123',
    nextLevel: 'Silver',
    pointsToNextStatus: 9900,
    pointsToMaintainStatus: 10000,
    username: 'ABC123',
  };

  const tree = renderer
    .create(<MembershipProfile user={userInfo} pointsInfoCallback={() => {}} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
