## RadioOne
A Radio component variation 1

# Usage & Example
```js
import RadioOne from '@molecules/RadioOne';

const [choiceSelected, setChoiceSelected] = useState('Choice 1');

<RadioOne
  value="Choice 2"
  selected={choiceSelected === 'Choice 2'}
  onPress={() => setChoiceSelected('Choice 2')}
/>
```
Key                             | Type           | Required  | Description                             |
------------------------------- | -------------- | --------- | --------------------------------------  |
value                           | `string`       | `False`   | The radio button value                  |
selected                        | `boolean`      | `false`   | Selects the radio button                |
onPress                         | `callback `    | `False`   | Callback function when radio is clicked |
color                           | `string`       | `False`   | The color of the radio button           |
uncheckedColor                  | `string`       | `False`   | The color of the unchecked radio button |
disabled                        | `boolean `     | `False`   | Disables the radio button               |
theme                           | `object`       | `False`   | react native paper theme                |
extraStyle                      | `object`       | `False`   | Style to be applied to the component    |
textStyle                       | `object `      | `False`   | Style to be applied to the text         |


# Viewing the buttons.
Click the 'Open modal' button from the Dashboard to see the 2 variations.

## RadioTwo
A Radio component variation 2

# Usage & Example
```js
import RadioTwo from '@molecules/RadioTwo';

const [choiceSelected, setChoiceSelected] = useState('Choice 1');

const image = <Icon group="general" name="assessment_thumbnail" />;

<RadioTwo
  value="Take a photo"
  textBottom="Use for printed results"
  selected={choiceSelected === 'Choice 2'}
  onPress={() => setChoiceSelected('Choice 2')}
  image={image}
/>
```
Key                             | Type           | Required  | Description                             |
------------------------------- | -------------- | --------- | --------------------------------------  |
value                           | `string`       | `False`   | The radio button value                  |
selected                        | `boolean`      | `false`   | Selects the radio button                |
onPress                         | `callback `    | `False`   | Callback function when radio is clicked |
color                           | `string`       | `False`   | The color of the radio button           |
uncheckedColor                  | `string`       | `False`   | The color of the unchecked radio button |
disabled                        | `boolean `     | `False`   | Disables the radio button               |
theme                           | `object`       | `False`   | react native paper theme                |
extraStyle                      | `object`       | `False`   | Style to be applied to the component    |
textBottom                      | `string `      | `False`   | Bottom text of the component            |
image                           | `React.Node`   | `False`   | The image component to be displaed      |


# Viewing the buttons.
Click the 'Open modal' button from the Dashboard to see the 2 variations.

## RadioGroup
Groups radio components

# Usage & Example
```js
import {RadioOne, RadioTwo, RadioGroup} from '@molecules/Radio';

const [radioSelected, setRadioSelected] = useState('Radio2');

const image = <Icon group="general" name="assessment_thumbnail" />;

<RadioGroup onValueChange={setRadioSelected} value={radioSelected}>
  <RadioOne value="Radio1" />
  <RadioOne value="Radio2" />
  <RadioTwo value="Take a photo" image={image} textBottom="Use for printed results" />
</RadioGroup>
```
Key                             | Type           | Required  | Description                               |
------------------------- | -------------- | --------- | ----------------------------------------------- |
onValueChange             | `callback`     | `True`    | Function to be called when any button is clicke |
radioSelected             | `string`       | `True`    | The currently selected value                    |
children                  | `React.Node`   | `True`    | The Radio component children                    |


# Viewing the buttons.
Click the 'Open modal' button from the Dashboard to see the 2 variations.
