/**
 *
 * CheckboxThree
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import RadioButton from '@atoms/Radio';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import {RadioTwoStyles} from './styles';

const RadioTwo = (props: PropsType) => {
  const {
    extraStyle,
    value,
    onPress,
    color,
    uncheckedColor,
    theme,
    disabled,
    textBottom,
    image,
    selected,
  } = props;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[RadioTwoStyles.container, extraStyle]}>
        <RadioButton
          value={value}
          selected={selected}
          onPress={onPress}
          color={color || '#00A4C9'}
          uncheckedColor={uncheckedColor || '#00A4C9'}
          disabled={disabled}
          theme={theme}
        />
        <View>
          <Text customStyle={RadioTwoStyles.textUp}>{value}</Text>
          <Text customStyle={RadioTwoStyles.textBottom}>{textBottom}</Text>
        </View>
        <View style={RadioTwoStyles.imageContainer}>{image}</View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default RadioTwo;
