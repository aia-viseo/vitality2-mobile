/**
 *
 * Radio
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import RadioButton from '@atoms/Radio';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import {RadioOneStyles} from './styles';

const RadioOne = (props: PropsType) => {
  const {
    extraStyle,
    value,
    onPress,
    color,
    uncheckedColor,
    theme,
    disabled,
    textStyle,
    selected,
  } = props;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[RadioOneStyles.container, extraStyle]}>
        <RadioButton
          value={value}
          selected={selected}
          onPress={onPress}
          color={color || '#00A4C9'}
          uncheckedColor={uncheckedColor || '#00A4C9'}
          disabled={disabled}
          theme={theme}
        />
        <Text customStyle={([RadioOneStyles.text], textStyle)}>{value}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default RadioOne;
