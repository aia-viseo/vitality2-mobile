/**
 *
 * RadioTwo Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import RadioTwo from '../RadioTwo';

test('RadioTwo', () => {
  const tree = renderer.create(<RadioTwo value="Option2" onPress={() => {}} selected />).toJSON();
  expect(tree).toMatchSnapshot();
});
