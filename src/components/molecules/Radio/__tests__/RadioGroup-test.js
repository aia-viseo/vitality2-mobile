/**
 *
 * RadioGroup Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import RadioOne from '../RadioOne';
import RadioTwo from '../RadioTwo';
import RadioGroup from '../RadioGroup';

test('RadioGroup', () => {
  const tree = renderer
    .create(
      <RadioGroup value="Option2" onValueChange={() => {}} selected>
        <RadioOne value="Option1" />
        <RadioTwo value="Option2" />
      </RadioGroup>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
