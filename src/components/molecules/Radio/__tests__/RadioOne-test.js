/**
 *
 * RadioOne Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import RadioOne from '../RadioOne';

test('RadioOne', () => {
  const tree = renderer
    .create(<RadioOne value="Option1" onPress={() => {}} selected={false} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
