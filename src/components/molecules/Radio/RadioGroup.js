/**
 *
 * RadioGroup
 * @format
 *
 */

// @flow

import React from 'react';
import {RadioButton} from 'react-native-paper';

import type {GroupPropsType} from './types';

const RadioGroup = (props: GroupPropsType) => {
  const {value, children, onValueChange} = props;

  return (
    <RadioButton.Group onValueChange={(val) => onValueChange(val)} value={value}>
      {children}
    </RadioButton.Group>
  );
};

export default RadioGroup;
