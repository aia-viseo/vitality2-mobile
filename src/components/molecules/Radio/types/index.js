/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  extraStyle?: ViewStyleProp,
  value?: string,
  selected?: boolean,
  onPress?: () => void,
  color?: string,
  uncheckedColor?: string,
  disabled?: boolean,
  theme?: {},
  textStyle?: ViewStyleProp,
  textBottom?: string,
  image?: any,
};

export type GroupPropsType = {
  value: string,
  children: Node,
  onValueChange: (value: string) => void,
};
