/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export const RadioOneStyles = StyleSheet.create({
  container: {
    marginBottom: 15,
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: '#C3D5E3',
    alignItems: 'center',
    borderWidth: 1,
    padding: 20,
  },
  text: {
    color: '#767B7D',
    flexWrap: 'wrap',
  },
});

export const RadioTwoStyles = StyleSheet.create({
  container: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CDDDE8',
  },
  textStyle: {
    color: '#3B4243',
  },
  textUp: {
    color: '#3E4546',
    fontSize: RFValue(16),
    marginBottom: 5,
  },
  textBottom: {
    color: '#99A5B0',
    fontSize: RFValue(14),
  },
  imageContainer: {
    flexDirection: 'row-reverse',
    flex: 1,
    alignItems: 'center',
    height: 80,
    overflow: 'hidden',
  },
});
