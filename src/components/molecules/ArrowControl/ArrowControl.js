/**
 *
 * Arrow Control
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import AppStyles from '@styles';
import Icon from '@atoms/Icon';

import type {PropsType} from './types';
import Styles from './styles';

const ArrowControl = (props: PropsType) => {
  const {children, disableLeft, disableRight, onPress} = props;

  return (
    <View style={[AppStyles.row, AppStyles.alignItemCenter, AppStyles.justifyContentSpaceBetween]}>
      <TouchableWithoutFeedback disabled={disableLeft} onPress={() => onPress && onPress(false)}>
        <View style={[disableLeft && Styles.disable, AppStyles.padding_10]}>
          <Icon group="general" name="arrow_left_no_tail" />
        </View>
      </TouchableWithoutFeedback>
      {children}
      <TouchableWithoutFeedback disabled={disableRight} onPress={() => onPress && onPress(true)}>
        <View style={[disableRight && Styles.disable, AppStyles.padding_10]}>
          <Icon
            group="general"
            name="arrow_left_no_tail"
            extraStyle={{transform: [{rotate: '180deg'}]}}
          />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default ArrowControl;
