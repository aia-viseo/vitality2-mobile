/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children?: Node,
  disableLeft?: boolean,
  disableRight?: boolean,
  onPress?: (isPressRight?: boolean) => void,
};
