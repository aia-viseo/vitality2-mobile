/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
  disable: {
    opacity: 0.5,
  },
});

export default Styles;
