## ArrowControl
A component with left arrow and right arrow

# Usage & Example ArrowControl
```js
import ArrowControl from '@molecules/ArrowControl';

  <ArrowControl>
    {children}
  </ArrowControl>
```

Key            | Type       | Required  | Description                                 |
-------------- | ---------- | --------- | ------------------------------------------- |
children       | `Node`     | `False`   | children will be render between two arrow   |
disableLeft    | `boolean`  | `False`   | Boolean to disable the left arrow button    |
disableRight   | `boolean`  | `False`   | Boolean to disable the right arrow button   |
onPress        | `callback` | `False`   | callback function when arrow button pressed |
