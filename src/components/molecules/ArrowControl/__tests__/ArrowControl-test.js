/**
 *
 * Arrow Control Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import ArrowControl from '../ArrowControl';

test('ArrowControl', () => {
  const cb = <ArrowControl />;
  const tree = renderer.create(cb).toJSON();
  expect(tree).toMatchSnapshot();
});
