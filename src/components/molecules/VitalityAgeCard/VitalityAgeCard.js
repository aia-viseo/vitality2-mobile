/**
 *
 * Vitality Age Card
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {useTranslation} from 'react-i18next';

import AppStyles from '@styles';
import Icon from '@atoms/Icon';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import CardStyles from './styles';

const VitalityAgeCard = (props: PropsType) => {
  const {age, onPress, diffYears, unRead} = props;
  const {t} = useTranslation();

  const showIcon = diffYears !== 0;
  const increase = diffYears > 0;

  const renderAge = () => {
    return (
      <View style={CardStyles.ageContainer}>
        {unRead && <View style={CardStyles.redDot} />}
        <Text customStyle={CardStyles.age}>{age}</Text>
      </View>
    );
  };
  const renderContent = () => {
    return (
      <View style={[CardStyles.contentContainer, AppStyles.paddingHorizontal_10]}>
        <Text customStyle={CardStyles.title}>{t('vitalityAgeCard.Your Vitality Age')}</Text>
        <View
          style={[
            AppStyles.row,
            AppStyles.alignItemCenter,
            AppStyles.marginTop_5,
            CardStyles.descriptionContainer,
          ]}>
          {showIcon && (
            <Icon
              group="general"
              name={increase ? 'increase' : 'decrease'}
              extraStyle={CardStyles.icon}
            />
          )}
          <Text
            customStyle={[
              CardStyles.highlight,
              increase ? CardStyles.increase : CardStyles.decrease,
            ]}>
            {t('vitalityAgeCard.years', {diffYears})}
          </Text>
          <Text customStyle={CardStyles.description}>
            {t('vitalityAgeCard.compared to your age')}
          </Text>
        </View>
      </View>
    );
  };

  const renderButton = () => {
    return (
      <View style={CardStyles.button}>
        <Icon group="general" name="arrow_right" />
      </View>
    );
  };

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <>
        {renderAge()}
        {renderContent()}
        {renderButton()}
      </>
    </TouchableWithoutFeedback>
  );
};

export default VitalityAgeCard;
