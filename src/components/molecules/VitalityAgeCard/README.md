## VitalityAgeCard
Molecules component contain age data with press function button

# Usage
```js
import VitalityAgeCard from 'molecules/VitalityAgeCard';

<VitalityAgeCard
  age
  diffYears
  onPress
  unRead
/>
```

# Example
```js
import VitalityAgeCard from 'molecules/VitalityAgeCard';

<VitalityAgeCard
  age={40}
  diffYears={10}
  onPress={() => {}}
  unRead={true}
/>
```

Key         | Type         | Required  | Description                             |
----------- | ------------ | --------- | --------------------------------------- |
age         | `number`     | `True`    | Number on display on the left.          |
diffYears   | `number`     | `True`    | Number on display in highlighted.       |
unRead      | `boolean`    | `False`   | Boolean on control display the red dot. |
onPress     | `function`   | `False`   | Trigger when press see all.             |
