/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  age: number,
  onPress?: () => void,
  diffYears: number,
  unRead?: boolean,
};
