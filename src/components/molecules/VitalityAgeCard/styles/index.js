/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import GetOS from '@platform';
import {
  COLOR_AIA_DARK_GREY,
  WHITE,
  COLOR_AIA_GREY_500,
  COLOR_AIA_RED,
  COLOR_AIA_GREEN,
  COLOR_AIA_CYAN,
} from '@colors';

import {AGE_CARD_HEIGHT} from '@constants/Header';

const isIOS = GetOS.getInstance() === 'ios';

const CardStyles = StyleSheet.create({
  card: {
    backgroundColor: WHITE,
    borderRadius: 10,
    shadowColor: COLOR_AIA_DARK_GREY,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 10,
    height: AGE_CARD_HEIGHT,
  },
  title: {
    fontSize: RFValue(16),
    color: COLOR_AIA_DARK_GREY,
    fontWeight: isIOS ? '600' : 'bold',
  },
  description: {
    fontSize: RFValue(12),
    color: COLOR_AIA_GREY_500,
    fontWeight: isIOS ? '300' : 'normal',
  },
  highlight: {
    fontSize: RFValue(12),

    fontWeight: isIOS ? '600' : 'bold',
  },
  increase: {
    color: COLOR_AIA_RED,
  },
  decrease: {
    color: COLOR_AIA_CYAN,
  },
  descriptionContainer: {
    flexWrap: 'wrap',
  },
  contentContainer: {
    flex: 1,
  },
  ageContainer: {
    position: 'relative',
    padding: 5,
  },
  redDot: {
    position: 'absolute',
    backgroundColor: COLOR_AIA_RED,
    width: 5,
    height: 5,
    borderRadius: 5,
    right: 0,
  },
  age: {
    fontSize: RFValue(26),
    fontWeight: isIOS ? '600' : 'bold',
    color: COLOR_AIA_GREEN,
  },
  button: {},
  icon: {
    height: 15,
    width: 15,
  },
});

export default CardStyles;
