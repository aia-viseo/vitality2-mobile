/**
 *
 *  Test Vitality Age Card
 *  @format
 *
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import VitalityAgeCard from '../VitalityAgeCard';

jest.useFakeTimers();
test('Test Vitality Age Card', () => {
  const tree = renderer.create(<VitalityAgeCard age={38} diffYears={20} />).toJSON();
  expect(tree).toMatchSnapshot();
});
