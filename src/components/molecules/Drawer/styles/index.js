/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {WHITE, BLACK} from '@colors';

const DrawerSectionStyles = StyleSheet.create({
  drawerContainer: {
    backgroundColor: WHITE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.4,
    shadowRadius: 10,
    elevation: 5,
  },
  arrowContainer: {
    flexDirection: 'row',
    position: 'absolute',
    top: 15,
    alignSelf: 'center',
  },
});

export default DrawerSectionStyles;
