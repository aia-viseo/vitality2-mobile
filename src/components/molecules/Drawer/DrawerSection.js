/**
 *
 * Drawer Component
 * @format
 *
 */

// @flow

import React, {useRef, useEffect} from 'react';
import {View, TouchableWithoutFeedback, Animated} from 'react-native';
import {PanGestureHandler} from 'react-native-gesture-handler';

import Icon from '@atoms/Icon';

import type {PropsType} from './types';
import DrawerSectionStyles from './styles';

const DrawerSection = (props: PropsType) => {
  const heightAnim = useRef(new Animated.Value(0)).current;
  const {children, isDrawerOpen, changeIsDrawerOpenValueFunction, delayBeforeDrawerOpens} = props;

  const toggleDrawer = () => {
    if (changeIsDrawerOpenValueFunction) {
      changeIsDrawerOpenValueFunction(!isDrawerOpen);
    }
  };

  const toggleOpen = () => {
    if (isDrawerOpen) {
      Animated.timing(heightAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start((status) => {
        if (status.finished) {
          toggleDrawer();
        }
      });
    } else {
      Animated.timing(heightAnim, {
        toValue: 120,
        duration: 500,
        useNativeDriver: false,
      }).start((status) => {
        if (status.finished) {
          toggleDrawer();
        }
      });
    }
  };

  const initDrawer = async () => {
    if (isDrawerOpen) {
      Animated.timing(heightAnim, {
        toValue: 200,
        duration: 500,
        useNativeDriver: false,
        delay: delayBeforeDrawerOpens || 0,
      }).start();
    }
  };

  useEffect(() => {
    initDrawer();
  }, []);

  const arrowSVG = (
    <Icon
      group="linkSection"
      name="links_section_arrow"
      extraStyle={{transform: [{rotate: !isDrawerOpen ? '180deg' : '0deg'}]}}
    />
  );

  const onPanGenstureEvent = (e) => {
    const {nativeEvent} = e;
    if (nativeEvent.translationY > 0) {
      toggleOpen();
    }
  };

  return (
    <PanGestureHandler onGestureEvent={(e) => onPanGenstureEvent(e)}>
      <Animated.View style={[DrawerSectionStyles.drawerContainer, {height: heightAnim}]}>
        <View>{children}</View>
        <TouchableWithoutFeedback onPress={() => toggleOpen()}>
          <View style={[DrawerSectionStyles.arrowContainer]}>{arrowSVG}</View>
        </TouchableWithoutFeedback>
      </Animated.View>
    </PanGestureHandler>
  );
};

export default DrawerSection;
