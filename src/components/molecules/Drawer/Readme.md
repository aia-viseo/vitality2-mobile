## Drawer
A container component for displaying a notification thru a drawer.

# Usage & Example
```js
import Drawer, {DrawerContainer} from './components/molecules/Drawer';

<Drawer
  isDrawerOpen
  delayBeforeDrawerOpens={500}
  changeIsDrawerOpenValueFunction={handleDrawerOpen}>
  <Text>This is a drawer</Text>
</Drawer>
```
Key                             | Type           | Required  | Description                         |
------------------------------- | -------------- | --------- | ----------------------------------- |
isDrawerOpen                    | `boolean`      | `True`    | Determines the initial drawer state |
delayBeforeDrawerOpens          | `number`       | `False`   | Delay in ms when drawer opens       |
changeIsDrawerOpenValueFunction | `callback`     | `False`   | Callback function hide is clicked   |


# Handling the Drawer implementation in React Hooks.
```js
// Create a state
const [isDrawerOpen, setIsDrawerOpen] = useState(true);
const handleDrawerOpen = (drawerStatus: boolean) => {
  setIsDrawerOpen(drawerStatus)
}
<Drawer
  isDrawerOpen
  delayBeforeDrawerOpens={500}
  changeIsDrawerOpenValueFunction={handleDrawerOpen}>
  <Text>This is a drawer</Text>
</Drawer>
```