/**
 *
 * @format
 *
 */

// @flow

import DrawerSection from './DrawerSection';

const DrawerContainer = {
  zIndex: 98,
  elevation: 99,
  position: 'absolute',
  left: 0,
  bottom: 0,
  width: '100%',
};

export {DrawerSection as default};
export {DrawerContainer};
