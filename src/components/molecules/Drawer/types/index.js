/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
  isDrawerOpen: boolean,
  changeIsDrawerOpenValueFunction?: (nextState: boolean) => void,
  delayBeforeDrawerOpens?: number,
};
