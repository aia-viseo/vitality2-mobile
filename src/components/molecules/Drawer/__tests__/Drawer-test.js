/**
 *
 * Test case for Drawer component
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import {Text} from 'react-native';
import DrawerSection from '../DrawerSection';

jest.useFakeTimers();

test('renders correctly', () => {
  const tree = renderer
    .create(
      <DrawerSection isDrawerOpen>
        <Text>For Testing</Text>
      </DrawerSection>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
