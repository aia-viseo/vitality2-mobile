/**
 *
 * CheckboxOne Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Checkbox from '../Checkbox';

test('CheckboxOne', () => {
  const cb = <Checkbox variation="1" onPress={() => {}} checked />;
  const tree = renderer.create(cb).toJSON();
  expect(tree).toMatchSnapshot();
});

test('CheckboxTwo', () => {
  const cb = <Checkbox variation="2" text="Checkbox" onPress={() => {}} checked />;
  const tree = renderer.create(cb).toJSON();
  expect(tree).toMatchSnapshot();
});

test('CheckboxTree', () => {
  const cb = <Checkbox variation="3" text="Checkbox" onPress={() => {}} checked />;
  const tree = renderer.create(cb).toJSON();
  expect(tree).toMatchSnapshot();
});
