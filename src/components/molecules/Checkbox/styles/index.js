/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const CheckboxStyles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkboxBorderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CDDDE8',
    padding: 20,
    marginBottom: 15,
  },
  checkContainer: {
    transform: [{scale: 0.9}],
    backgroundColor: 'white',
    borderRadius: 30,
  },
  checkboxTwoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyle: {
    color: '#3B4243',
  },
  textUp: {
    color: '#3E4546',
    fontSize: RFValue(16),
    marginBottom: 5,
  },
  textBottom: {
    color: '#8B96A6',
    fontSize: RFValue(14),
  },
  imageContainer: {
    flexDirection: 'row-reverse',
    flex: 1,
    alignItems: 'center',
    height: 80,
    overflow: 'hidden',
  },
  gradient: {borderRadius: 30, padding: 0, transform: [{scale: 0.6}]},
});

export default CheckboxStyles;
