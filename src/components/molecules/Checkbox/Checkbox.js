/**
 *
 * CheckboxOne
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

import type {PropsType} from './types';
import CheckboxStyles from './styles';
import AppStyles from '../../../core/styles';

import FrCheckbox from '../../atoms/Checkbox';
import Text from '../../atoms/Text';

const Checkbox = (props: PropsType) => {
  const {
    variation,
    checked,
    onPress,
    color,
    text,
    subtext,
    image,
    uncheckedColor,
    disabled,
  } = props;

  let selectedStyle;
  switch (variation) {
    case 1: {
      selectedStyle = CheckboxStyles.checkboxContainer;
      break;
    }
    case 2: {
      selectedStyle = [CheckboxStyles.checkboxBorderContainer, AppStyles.paddingVertical_20];
      break;
    }
    case 3: {
      selectedStyle = CheckboxStyles.checkboxBorderContainer;
      break;
    }
    default: {
      selectedStyle = CheckboxStyles.checkboxContainer;
      break;
    }
  }

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[selectedStyle]}>
        <FrCheckbox
          checked={checked}
          onPress={onPress}
          color={color || '#00A4C9'}
          uncheckedColor={uncheckedColor || '#00A4C9'}
          disabled={disabled}
        />
        {text && (variation === 1 || variation === 2) && (
          <Text style={[CheckboxStyles.textStyle]}>{text}</Text>
        )}
        {text && variation === 3 && (
          <View>
            <Text customStyle={CheckboxStyles.textUp}>{text}</Text>
            <Text customStyle={CheckboxStyles.textBottom}>{subtext}</Text>
          </View>
        )}
        {image && variation === 3 && <View style={CheckboxStyles.imageContainer}>{image}</View>}
      </View>
    </TouchableWithoutFeedback>
  );
};

Checkbox.defaultProps = {};

export default Checkbox;
