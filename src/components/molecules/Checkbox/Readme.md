## Checkbox
A Checkbox component implementing react-native-paper checkbox

# Usage & Example Checkbox variation 1
```js
import Checkbox from '@molecules/Checkbox';

<Checkbox
    variation={1}
    checked={checked}
    onPress={onPress}
    color={color}
    uncheckedColor={uncheckedColor}
    disabled={disabled}
/>
```

# Usage & Example Checkbox variation 2
```js
import Checkbox from '@molecules/Checkbox';

<Checkbox
    variation={2}
    checked={checked}
    onPress={onPress}
    color={color}
    uncheckedColor={uncheckedColor}
    disabled={disabled}
    text={text}
/>
```

# Usage & Example Checkbox Variation 3
```js
import Checkbox from '@molecules/Checkbox';

<Checkbox
    variation={3}
    checked={checked}
    onPress={onPress}
    color={color}
    uncheckedColor={uncheckedColor}
    disabled={disabled}
    text={variationThreeTextUp}
    subtext={variationThreeTextBottom}
    image={image}
/>
```
Key                             | Type           | Required  | Description                            |
------------------------------- | -------------- | --------- | -------------------------------------- |
variation                       | `string`       | `True`    | Variation to select checkbox styles    |
onPress                         | `callback`     | `True`    | The function to be called when clicked |
checked                         | `boolean `     | `True`    | Determines if button is checked        |
color                           | `String`       | `False`   | The color of the checkbox              |
uncheckedColor                  | `String`       | `False`   | The color of the unchecked checkbox    |
borderColor                     | `String`       | `False`   | The color of the border                |
text                            | `String`       | `False`   | The text of the checkbox               |
subtext                         | `String`       | `False`   | The bottom text of the checkbox 3      |
image                           | `any`          | `False`   | The image of the checkbox 3            |
disabled                        | `boolean`      | `False`   | Determines if button is disabled       |
