/**
 *
 * @format
 *
 */

// @flow

type variationType = '1' | '2' | '3';

export type PropsType = {
  checked: boolean,
  onPress: () => void,
  color?: string,
  uncheckedColor?: String,
  borderColor?: string,
  text?: string,
  subtext?: string,
  variationThreeTextUp?: string,
  variationThreeTextBottom?: string,
  image?: any,
  disabled?: boolean,
  variation: variationType,
};
