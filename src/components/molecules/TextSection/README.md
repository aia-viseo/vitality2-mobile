# TextSection

This is a TextSection component that contains a boldface title and a body, both of which are centered horizontally.

## Usage & Example

```js
import TextSction from 'molecules/TextSection';

<TextSection
  title="title"
  body="body"
/>
```

Key             | Type         | Required  | Description
--------------- | ------------ | --------- | --------------------------------
title           | `string`     | `true`    | Section Title
body            | `string`     | `true`    | Section Description
backgroundColor | `string`     | `false`   | Hex color identifier. Defaults to clear.
