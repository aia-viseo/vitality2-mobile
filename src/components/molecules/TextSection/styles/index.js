/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {BLACK, COLOR_AIA_DARK_GREY_600} from '@colors';

const TextSectionStyles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  title: {
    fontWeight: '500',
    fontSize: RFValue(20),
    color: BLACK,
    paddingBottom: 10,
  },
  body: {
    fontSize: RFValue(16),
    color: COLOR_AIA_DARK_GREY_600,
    paddingBottom: 20,
  },
});

export default TextSectionStyles;
