// @format
// @flow

export type PropsType = {
  title: string,
  body: string,
  backgroundColor?: string,
};
