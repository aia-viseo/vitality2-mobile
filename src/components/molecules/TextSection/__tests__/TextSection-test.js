/**
 *
 * TextSection Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import TextSection from '../TextSection';

test('TextSection', () => {
  const tree = renderer.create(<TextSection title="title" body="body" />).toJSON();
  expect(tree).toMatchSnapshot();
});
