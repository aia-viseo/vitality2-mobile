/**
 *
 * TextSection
 * @format
 * @flow
 *
 */

import React from 'react';
import {View} from 'react-native';

import Text from '@atoms/Text';

import type {PropsType} from './types';
import Styles from './styles';

const TextSection = (props: PropsType) => {
  const {title, body, backgroundColor} = props;

  const containerStyle = backgroundColor ? [Styles.container, {backgroundColor}] : Styles.container;

  return (
    <View style={containerStyle}>
      <Text customStyle={Styles.title}>{title}</Text>
      <Text customStyle={Styles.body}>{body}</Text>
    </View>
  );
};

export default TextSection;
