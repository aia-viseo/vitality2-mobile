/**
 *
 * Test Case StackNavigation
 * @format
 *
 */

// @flow

import * as React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const ModalNavigation = () => {
  const TestScreen = () => {
    return (
      <View>
        <Text>Test</Text>
      </View>
    );
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Test" component={TestScreen} />
    </Stack.Navigator>
  );
};

export default ModalNavigation;
