/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  navigation: any,
};

export type ScreenType = {
  name: string,
  screen: Node,
};
