/**
 *
 * ModalNavigation Test
 * @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import ModalNavigation from '../Test-ModalNavigation';

jest.mock('react-native-vector-icons/AntDesign', () => ({close: 'close'}));
jest.useFakeTimers();

test('Test Modal Navigation ', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <ModalNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
