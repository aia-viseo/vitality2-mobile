/**
 *
 * ModalNavigation
 * @format
 *
 */

// @flow

import React from 'react';
import lodash from 'lodash';
import {createStackNavigator} from '@react-navigation/stack';

import SCREENS from './config';

import type {ScreenType} from './types';

const Stack = createStackNavigator();

const ModalNavigation = () => {
  return (
    <Stack.Navigator
      model="modal"
      screenOptions={{
        headerShown: false,
      }}>
      {lodash.map(SCREENS, (SCREEN: ScreenType, index: number) => {
        const {name, screen} = SCREEN;
        return <Stack.Screen key={index} name={name} component={screen} />;
      })}
    </Stack.Navigator>
  );
};

ModalNavigation.defaultProps = {};

export default ModalNavigation;
