## StackNavigation
Modal Navigator
A nested navigator screen placed in the bottom navigation.

## Usage with another navigators
```js
// Create screens to be placed inside the modal navigation.
import React from 'react';
import {Text} from 'react-native';
import Modal from '../../Modal';

import type {PropsType} from '../types';

/* Note, you do not need to place the screen here, ideally it 
should be located in another subfolder */

const SedentaryScreen = (props: PropsType) => {
  const {navigation} = props;
  return (
    <Modal navigation={navigation}>
      <Text style={{fontSize: 20}}>Place your content here</Text>
    </Modal>
  );
};

const SCREENS: {}[] = [
  {
    name: 'Sedentary',
    screen: SedentaryScreen,
  },
];

export {SCREENS as default};

// Insert the Modal Navigator with the defined screens inside the bottom navigator
import BottomNavigation from '../../BottomNavigation';
import Login from '../../../../pages/Login';
import ModalNavigation from '../../ModalNavigation';

const SCREENS: {}[] = [
  {
    name: 'Login',
    screen: Login,
  },
  {
    name: 'Application',
    screen: BottomNavigation,
  },
  {
    name: 'Modal',
    screen: ModalNavigation,
  },
];

export {SCREENS as default};
```