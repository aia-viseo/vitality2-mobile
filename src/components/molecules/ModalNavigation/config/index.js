/**
 *
 * @format
 *
 */

// @flow

import Sedentary from '../../../../pages/Sedentary';

const SCREENS: {}[] = [
  {
    name: 'Sedentary',
    screen: Sedentary,
  },
];

export {SCREENS as default};
