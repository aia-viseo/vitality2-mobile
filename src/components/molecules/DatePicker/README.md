## DatePicker
Component for date selection

# Usage
For stateless components and React hooks.
```js
import DatePicker from '../../components/molecules/DatePicker';

const [date, setDate] = useState(null);

<DatePicker onChange={(event, selectedDate) => setDate(selectedDate)} date={date} />
```

# Example
```js
/**
 *
 * ExamplePage
 * @format
 *
 */

// @flow

import React, {useState} from 'react';
import {ScrollView} from 'react-native';

import DatePicker from '../../components/molecules/DatePicker';

const ExamplePage = () => {
  const [date, setDate] = useState(null);

  return (
    <ScrollView>
      <DatePicker onChange={(selectedDate) => setDate(selectedDate)} date={date} />
    </ScrollView>
  );
};

ExamplePage.defaultProps = {};

export default ExamplePage;
```
# Props 
Key           | Type           | Required  | Description                         |
------------- | -------------- | --------- | ----------------------------------- |
onChange      | `callback`     | `False`   | Callback for the change event.      |
placeHolder   | `string`       | `False`   | Initial input placeholder.          |
date          | `Date`         | `False`   | Initial date for the picker.        |
mode          | `string`       | `False`   | Selects time or date picker.        |

# References 
Uses @react-native-community/datetimepicker
For modifications and refactoring, please see the guide at: https://github.com/react-native-community/datetimepicker