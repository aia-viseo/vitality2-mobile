/**
 *
 * DatePicker Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import DatePicker from '../DatePicker';

jest.useFakeTimers();
test('DatePicker', () => {
  const tree = renderer.create(<DatePicker />).toJSON();
  expect(tree).toMatchSnapshot();
});
