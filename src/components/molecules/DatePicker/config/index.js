/**
 *
 * @format
 *
 */

// @flow

export const ACCEPTED_DATE_FORMAT = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
export const INVALID_DATE_FORMAT = 'Invalid date format.';
