/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const DatePickerStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 5,
    borderRadius: 5,
    borderColor: '#C0D1DA',
    borderWidth: 1,
    alignItems: 'center',
  },
  dateInput: {
    width: '90%',
    padding: 15,
    fontSize: 18,
    color: '#8592A1',
  },
  datePickerContainer: {height: '100%', justifyContent: 'center'},
  calendar: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default DatePickerStyles;
