/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  onChange?: (selectedValue: Date) => void,
  placeHolder?: string,
  date?: Date,
  mode?: string,
};
