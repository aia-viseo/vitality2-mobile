/**
 *
 * DatePicker
 * @format
 *
 */

// @flow

import React, {useState, useEffect, useRef} from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import DateTimePicker from '@react-native-community/datetimepicker';
import ModalContainer from '@molecules/Modal';
import GetOS from '@platform';

import type {PropsType} from './types';
import DatePickerStyles from './styles';
import {ACCEPTED_DATE_FORMAT, INVALID_DATE_FORMAT} from './config';

const DatePicker = (props: PropsType) => {
  const {onChange, placeHolder, mode, date} = props;
  const [datePickerVisible, setDatePickerVisible] = useState(false);
  const [userDate, setUserDate] = useState('');
  const [placeholderText, setPlaceholderText] = useState('Enter a date.');
  const contentRef = useRef(null);
  const isIOS = GetOS.getInstance() === 'ios';

  useEffect(() => {
    setUserDate(date ? date.toLocaleString().split(',')[0] : '');
  }, [date]);

  const handleFinishEdit = () => {
    if (!ACCEPTED_DATE_FORMAT.test(userDate)) {
      setUserDate('');
      setPlaceholderText(INVALID_DATE_FORMAT);
    }
  };

  const handleDatePickerChange = (newDate) => {
    if (!isIOS) {
      setDatePickerVisible(false);
    }
    if (onChange) {
      onChange(newDate);
    }
  };

  const renderDatePicker = () => {
    return (
      <DateTimePicker
        testID="dateTimePicker"
        value={date || new Date()}
        mode={mode}
        is24Hour
        display="default"
        onChange={(event, selectedDate) => handleDatePickerChange(selectedDate)}
      />
    );
  };

  return (
    <View style={[DatePickerStyles.container]}>
      <TextInput
        style={[DatePickerStyles.dateInput]}
        placeholder={placeHolder || placeholderText}
        value={userDate}
        onChangeText={(text) => setUserDate(text)}
        onEndEditing={handleFinishEdit}
        ref={contentRef}
      />
      <TouchableOpacity
        onPress={() => {
          if (!date) {
            handleDatePickerChange(new Date());
          }
          setDatePickerVisible(true);
        }}
        style={[DatePickerStyles.calendar]}>
        <VIcon name="calendar-month-outline" color="#059DE0" size={30} />
      </TouchableOpacity>
      {datePickerVisible && isIOS ? (
        <ModalContainer
          modalVisible={datePickerVisible}
          setModalVisible={setDatePickerVisible}
          height={275}
          width="80%"
          margin={20}>
          {renderDatePicker()}
        </ModalContainer>
      ) : (
        <View />
      )}
      {datePickerVisible && !isIOS ? renderDatePicker() : <View />}
    </View>
  );
};

DatePicker.defaultProps = {
  mode: 'date',
  date: new Date('01/01/2020'),
};

export default DatePicker;
