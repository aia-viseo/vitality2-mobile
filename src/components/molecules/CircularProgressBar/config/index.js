/**
 *
 * @format
 *
 */

// @flow

export const CIRCULAR_PROGRESS_BAR_COLOR = '#1795DA';
export const INNER_CIRCLE_COLOR = '#D9E6EE';
