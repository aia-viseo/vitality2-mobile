## CircularProgressBar
Progress indicator with a circular layout.

# Usage
For stateless components and React hooks.
```js
import CircularProgressBar from '@molecules/CircularProgressBar';
import VIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const circularProgressIcon = <VIcon name="clipboard-text-outline" size={26} color="#1795DA" />;

<CircularProgressBar progress={50} icon={circularProgressIcon} iconSize={26} />
```

# Example
```js
/**
 *
 * Example Page
 * @format
 *
 */

// @flow

import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import CircularProgressBar from '@molecules/CircularProgressBar';
import Input from '@atoms/Input';

const ExamplePage = () => {
  const [side, setSide] = useState(50);
  const [progress, setProgress] = useState(50);
  const [iconSize] = useState(26);
  const circularProgressIcon = <VIcon name="clipboard-text-outline" size={iconSize} color="#1795DA" />;

  return (
    <ScrollView>
      <View style={{paddingTop: 10}}>
        <Input
          label="Progress (0 - 100)"
          numbersOnly
          changeText={(text) => {
            const intText = parseInt(text, 10);
            if (intText >= 0 && intText <= 100) {
              setProgress(text);
            } else {
              setProgress(50);
            }
          }}
          value={progress}
        />
        <Input
          label="Component size"
          numbersOnly
          changeText={(text) => {
            setSide(text);
          }}
          value={side}
          customStyle={{marginTop: 10}}
        />
        <CircularProgressBar
          progress={progress}
          icon={circularProgressIcon}
          iconSize={iconSize}
          side={side}
          customStyle={{marginTop: 10}}
        />
      </View>
    </ScrollView>
  );
};

ExamplePage.defaultProps = {};

export default ExamplePage;

```
# Props 
Key                 | Type           | Required  | default   | Description                             |
------------------- | -------------- | --------- | --------- | --------------------------------------- |
progress            | `number`       | `True`    | None      | The progress value from 0 to 100        |
side                | `number`       | `False`   | 50        | The legth of the square container       |
icon                | `Node`         | `False`   | None      | The icon in the center of the component |
progressStrokeWidth | `number`       | `False`   | 2.5       | Width of the progress indicator         |
circleStrokeWidth   | `number`       | `False`   | 1.5       | Width of the inner gray cirle           |
iconSize            | `number`       | `False`   | 26        | Used to center the icon. Should be equal to the size prop of the icon component |
customStyle         | `ViewStyleProp`| `False`   | None      | Styling of the View component container |
progressStrokeColor | `String`       | `False    | #1795DA   | The stroke color of the progress indicator |



# References 
Uses react-native-svg
For modifications and refactoring, please see the guide at: https://www.npmjs.com/package/react-native-svg

# Bugs
There is currently a clipping for progress value anywhere from 96 - 99.