/**
 *
 * CircularProgressBar Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import CircularProgressBar from '../CircularProgressBar';

test('CircularProgressBar', () => {
  const tree = renderer.create(<CircularProgressBar progress={50} />).toJSON();
  expect(tree).toMatchSnapshot();
});
