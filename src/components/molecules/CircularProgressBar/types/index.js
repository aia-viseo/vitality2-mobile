/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  progress: number,
  side?: number,
  icon?: Node,
  progressStrokeWidth?: number,
  circleStrokeWidth?: number,
  iconSize?: number,
  customStyle?: ViewStyleProp,
  progressStrokeColor?: string,
};
