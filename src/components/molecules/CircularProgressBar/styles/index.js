/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const CircularProgressBarStyles = StyleSheet.create({
  iconContainer: {
    position: 'absolute',
  },
});

export default CircularProgressBarStyles;
