/**
 *
 * CircularProgressBar
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import Svg, {Circle, Path} from 'react-native-svg';

import {CIRCULAR_PROGRESS_BAR_COLOR, INNER_CIRCLE_COLOR} from './config';
import CircularProgressBarStyles from './styles';
import type {PropsType} from './types';

const CircularProgressBar = (props: PropsType) => {
  const {
    side,
    progress,
    circleStrokeWidth,
    progressStrokeWidth,
    icon,
    iconSize,
    customStyle,
    progressStrokeColor,
  } = props;
  const svgSide = side || 50;
  const origin = svgSide / 2;
  const arcRadius = origin - 2;
  const largeArc = progress > 50 ? 1 : 0;
  const iconOffset = svgSide / 2 - (iconSize || 26) / 2;

  const arcTo = (): number[] => {
    const progressToRad = (progress / 100) * Math.PI * 2;
    const offsetX = Math.sin(progressToRad) * arcRadius;
    const offsetY = Math.cos(progressToRad) * arcRadius;
    return [Math.round(origin + offsetX), Math.round(origin - offsetY)];
  };

  const toPoints = arcTo();

  return (
    <View style={customStyle}>
      <Svg height={svgSide || 50} width={svgSide + 2}>
        <Circle
          cx={origin}
          cy={origin}
          r={arcRadius}
          stroke={INNER_CIRCLE_COLOR}
          strokeWidth={circleStrokeWidth || 1.5}
        />
        {progress < 100 ? (
          <Path
            d={`M ${origin + 1} 2 A${arcRadius} ${arcRadius}, 0 ${largeArc} 1, ${toPoints[0]} ${
              toPoints[1]
            }`}
            fill="none"
            stroke={progressStrokeColor || CIRCULAR_PROGRESS_BAR_COLOR}
            strokeWidth={progressStrokeWidth || 2.5}
          />
        ) : (
          <Circle
            cx={origin}
            cy={origin}
            r={arcRadius}
            stroke={progressStrokeColor || CIRCULAR_PROGRESS_BAR_COLOR}
            strokeWidth={progressStrokeWidth || 2.5}
          />
        )}
      </Svg>
      <View style={[CircularProgressBarStyles.iconContainer, {top: iconOffset, left: iconOffset}]}>
        {icon}
      </View>
    </View>
  );
};

CircularProgressBar.defaultProps = {
  side: 50,
};

export default CircularProgressBar;
