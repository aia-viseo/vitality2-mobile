/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {WHITE} from '@colors';

const ModalStyles = StyleSheet.create({
  container: {},
  closeButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 15,
    borderRadius: 45,
    padding: 8,
    backgroundColor: WHITE,
    elevation: 5,
    shadowOffset: {width: 2, height: 3},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    position: 'absolute',
    top: 20,
  },
  contentContainer: {
    padding: 15,
  },
});

export default ModalStyles;
