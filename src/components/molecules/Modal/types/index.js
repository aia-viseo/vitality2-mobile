/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  modalVisible?: boolean,
  closeIcon?: Node,
  children?: Node,
  setModalVisible?: (boolean) => void,
  height?: number,
  margin?: number,
  rightClose?: boolean,
};
