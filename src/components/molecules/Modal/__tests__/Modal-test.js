/**
 *
 * Modal Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import {Text} from 'react-native';

import Modal from '../Modal';

jest.mock('react-native-vector-icons/AntDesign', () => ({close: 'close'}));

test('Modal', () => {
  const closeIcon = <Text>X</Text>;
  const tree = renderer
    .create(<Modal navigation={{goBack: () => {}}} closeIcon={closeIcon} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
