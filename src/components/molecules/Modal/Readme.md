## Modal Container component
This is a wrapper for a modal screen, it contains a close icon in the upper left corder.
If there is an implementation of closing the modal, the content may not need to be wrapped.
Pass the global navigation object down for proper navigation handling.

## Using the modal wrapper in a Screen component
```js
import React from 'react';
import {Text} from 'react-native';
import Modal from '../../Modal';

import type {PropsType} from '../types';

const SedentaryScreen = (props: PropsType) => {
  const {navigation} = props;
  return (
    <Modal navigation={navigation}>
      <Text style={{fontSize: 20}}>Place your content here</Text>
    </Modal>
  );
};

const SCREENS: {}[] = [
  {
    name: 'Sedentary',
    screen: SedentaryScreen,
  },
];

export {SCREENS as default};
```

Key         | Type           | Required  | Description                       |
----------- | -------------- | --------- | --------------------------------- |
navigation  | `object`       | `True`    | The react navigation object.      |