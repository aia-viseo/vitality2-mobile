/**
 *
 * Modal
 * @format
 *
 */

// @flow

import React from 'react';
import {View, Dimensions} from 'react-native';

import {Portal, Modal} from 'react-native-paper';
import {COLOR_AIA_BG_1} from '@colors';
import Effect from './Effect';

import type {PropsType} from './types';

const ModalContainer = (props: PropsType) => {
  const {children, closeIcon, modalVisible, setModalVisible, height, margin} = props;

  return (
    <Portal>
      <Modal
        visible={modalVisible}
        dismissable={false}
        contentContainerStyle={{
          backgroundColor: COLOR_AIA_BG_1,
          height: height || Dimensions.get('window').height + 10,
          justifyContent: 'flex-start',
          paddingTop: 25,
          margin,
          borderRadius: margin && margin > 0 ? 10 : 0,
        }}>
        <View>
          <Effect
            closeIcon={closeIcon}
            setModalVisible={setModalVisible}
            rightClose={margin !== undefined && margin > 0}>
            {children}
          </Effect>
        </View>
      </Modal>
    </Portal>
  );
};

ModalContainer.defaultProps = {
  margin: 0,
};

export default ModalContainer;
