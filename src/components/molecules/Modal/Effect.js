/**
 *
 * Effect
 * @format
 *
 */

// @flow

import React, {useEffect, useRef} from 'react';
import {SafeAreaView, Animated, View, TouchableWithoutFeedback} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialIcons';

import type {PropsType} from './types';
import ModalStyles from './styles';

const Effect = (props: PropsType) => {
  const effect = useRef(new Animated.Value(0)).current;
  const {children, closeIcon, setModalVisible, rightClose} = props;

  const closeModal = () => {
    Animated.timing(effect, {
      toValue: 0,
      duration: 250,
      useNativeDriver: false,
    }).start((status) => {
      if (status.finished && setModalVisible) {
        setModalVisible(false);
      }
    });
  };

  useEffect(() => {
    Animated.timing(effect, {
      toValue: 1,
      duration: 250,
      useNativeDriver: false,
    }).start();
  }, []);

  return (
    <SafeAreaView>
      <Animated.View
        style={[
          ModalStyles.contentContainer,
          {
            transform: [{scale: effect.interpolate({inputRange: [0, 1], outputRange: [1.1, 1]})}],
            opacity: effect.interpolate({inputRange: [0, 0.2, 1], outputRange: [0, 1, 1]}),
          },
        ]}>
        {children}
      </Animated.View>
      <View style={[ModalStyles.closeButton, rightClose ? {right: 0, top: -25} : {left: 0}]}>
        <TouchableWithoutFeedback onPress={closeModal}>
          <View>{closeIcon || <VIcon size={24} name="close" color="#059DE0" />}</View>
        </TouchableWithoutFeedback>
      </View>
    </SafeAreaView>
  );
};

Effect.defaultProps = {};

export default Effect;
