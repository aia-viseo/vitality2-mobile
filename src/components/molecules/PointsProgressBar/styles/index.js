/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {WHITE, COLOR_AIA_ICON_GRAY} from '@colors';

const HEIGHT = 26;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
    height: HEIGHT,
    backgroundColor: COLOR_AIA_ICON_GRAY,
    borderRadius: HEIGHT / 2,
  },
  progressContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    height: HEIGHT,
    borderRadius: HEIGHT / 2,
  },
  progress: {
    alignItems: 'center',
    height: HEIGHT,
    borderRadius: HEIGHT / 2,
  },
  labelContainer: {
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'center',
    marginLeft: HEIGHT / 2,
  },
  label: {
    marginRight: 7,
    fontSize: RFValue(14),
    color: WHITE,
    fontWeight: '600',
  },
  icon: {
    width: 40,
    height: 40,
  },
});

export default styles;
