/**
 *
 * PointsProgressBar
 * @format
 *
 */

// @flow

import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import Icon from '@atoms/Icon';

import type {PropsType} from './types';
import Styles from './styles';

const PointsProgressBar = (props: PropsType) => {
  const {label, color, progress, infoCallback} = props;

  return (
    <View style={Styles.container}>
      <TouchableWithoutFeedback onPress={infoCallback}>
        <View style={Styles.progressContainer}>
          <View style={[Styles.progress, {flex: progress, backgroundColor: color}]} />

          <View style={Styles.labelContainer}>
            <Text style={Styles.label}>{label}</Text>
            <Icon style={Styles.icon} group="general" name="information" />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default PointsProgressBar;
