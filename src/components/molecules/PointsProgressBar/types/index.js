/**
 * @format
 * @flow
 */

export type PropsType = {
  color: string,
  label: string,
  progress: number,
  infoCallback: () => void,
};
