/**
 *
 * PointsProgressBar Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import PointsProgressBar from '../PointsProgressBar';

test('PointsProgressBar', () => {
  const tree = renderer
    .create(
      <PointsProgressBar infoCallback={() => {}} label="Bronze" color="#ffffff" progress={0.5} />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
