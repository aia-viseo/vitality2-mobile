/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

type QuickLinkItemType = {
  id: string,
  iconName: string,
  iconGroup: string,
  title: string,
  type: string,
};

type QuickLinksType = {
  ecard: QuickLinkItemType,
  hReport: QuickLinkItemType,
  help: QuickLinkItemType,
  notify: QuickLinkItemType,
  points: QuickLinkItemType,
  voucher: QuickLinkItemType,
  wkTrack: QuickLinkItemType,
};

export type ButtonItemType = {
  labelKey: string,
  icon: Node,
  path: string,
};

export type HeaderMenuType = {
  menuIcon: string,
  menuTitle: string,
  order: string,
};

export type PropsType = {
  onPressItem?: (path: string) => void,
  data?: QuickLinksType,
  max?: number,
};
