/**
 *
 * LinkSection
 * @format
 *
 */

// @flow

import React, {useState, useRef, useEffect} from 'react';
import {View, TouchableWithoutFeedback, Animated} from 'react-native';
import {useTranslation} from 'react-i18next';

import Icon from '@atoms/Icon';
import Text from '@atoms/Text';

import type {PropsType, ButtonItemType} from './types';
import LinkSectionStyles from './styles';
import {QUICK_LINKS} from './config';

const LinkSection = (props: PropsType) => {
  const [isOpen, setIsOpen] = useState(false);
  const [links, setLinks] = useState([]);
  const heightAnim = useRef(new Animated.Value(75)).current;
  const {t} = useTranslation();
  const {onPressItem, data, max} = props;

  const toggleOpen = () => {
    if (isOpen) {
      Animated.timing(heightAnim, {
        toValue: 75,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(heightAnim, {
        toValue: 135,
        duration: 500,
        useNativeDriver: false,
      }).start();
    }
    setIsOpen(!isOpen);
  };

  const processLinks = () => {
    const keys = data ? Object.keys(data) : [];
    const quickLinkContents = [];
    keys.forEach((key) => {
      const {title} = (data || {})[key];
      const quickLink = QUICK_LINKS[key];
      quickLinkContents.push(Object.assign(quickLink, {labelKey: title}));
    });
    const missingItem = (max || 8) - quickLinkContents.length;
    const missingItemArr = new Array(missingItem);
    missingItemArr.fill({});
    missingItemArr.forEach(() => {
      quickLinkContents.push({});
    });
    setLinks(quickLinkContents);
  };

  useEffect(() => {
    processLinks();
  }, [data]);

  const renderLink = (buttonItem: ButtonItemType, index: number) => {
    const {labelKey, icon, path} = buttonItem;
    return (
      <TouchableWithoutFeedback key={index} onPress={() => onPressItem && onPressItem(path)}>
        <View style={LinkSectionStyles.itemContainer}>
          <View style={LinkSectionStyles.icon}>{icon}</View>
          <Text customStyle={LinkSectionStyles.label}>{t(labelKey)}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const arrowSVG = (
    <Icon
      group="linkSection"
      name="links_section_arrow"
      extraStyle={{transform: [{rotate: isOpen ? '180deg' : '0deg'}]}}
    />
  );

  const renderToggle = () => {
    return (
      <TouchableWithoutFeedback onPress={() => toggleOpen()}>
        <View style={[LinkSectionStyles.arrowContainer]}>{arrowSVG}</View>
      </TouchableWithoutFeedback>
    );
  };
  return (
    <View style={LinkSectionStyles.container}>
      <Animated.View style={[LinkSectionStyles.itemsContainer, {height: heightAnim}]}>
        {links.map((quickLink, index) => {
          return renderLink(quickLink, index);
        })}
      </Animated.View>
      {links.length > 4 && renderToggle()}
    </View>
  );
};

export default LinkSection;
