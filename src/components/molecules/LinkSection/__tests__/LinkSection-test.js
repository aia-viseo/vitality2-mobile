/**
 *
 *  Test Link Section
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';
import LinkSection from '../LinkSection';

jest.useFakeTimers();
test('Test Bottom Navigation ', () => {
  const tree = renderer.create(<LinkSection />).toJSON();
  expect(tree).toMatchSnapshot();
});
