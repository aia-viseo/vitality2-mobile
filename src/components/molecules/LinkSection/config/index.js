/**
 *
 * @format
 *
 */

// @flow

import React from 'react';

import Icon from '@atoms/Icon';

export const QUICK_LINKS = {
  wkTrack: {
    labelKey: `Workout tracker`,
    icon: <Icon group="linkSection" name="workout_tracker" />,
    path: 'WorkoutTracker',
  },
  points: {
    labelKey: 'Earn points',
    icon: <Icon group="linkSection" name="earn_points" />,
    path: 'EarnPoints',
  },
  ecard: {
    labelKey: `eCard`,
    icon: <Icon group="linkSection" name="e_card" />,
    path: 'ECard',
  },
  voucher: {
    labelKey: 'Use coupons',
    icon: <Icon group="linkSection" name="use_coupons" />,
    path: 'Rewards',
  },
  help: {
    labelKey: `Help`,
    icon: <Icon group="linkSection" name="help" />,
    path: 'Help',
  },
  hReport: {
    labelKey: `Health report`,
    icon: <Icon group="linkSection" name="health_report" />,
    path: 'HealthReport',
  },
  notify: {
    labelKey: 'Notifications',
    icon: <Icon group="linkSection" name="notification" />,
    path: 'Notification',
  },
};

export const BUTTON_LIST = [
  {
    labelKey: 'Notifications',
    icon: <Icon group="linkSection" name="notification" />,
    path: 'Notification',
  },
  {
    labelKey: 'Earn points',
    icon: <Icon group="linkSection" name="earn_points" />,
    path: 'EarnPoints',
  },
  {
    labelKey: 'Use coupons',
    icon: <Icon group="linkSection" name="use_coupons" />,
    path: 'UseCoupon',
  },
  {
    labelKey: `What's on`,
    icon: <Icon group="linkSection" name="whats_on" />,
    path: 'WhatsOn',
  },
  {
    labelKey: `Health report`,
    icon: <Icon group="linkSection" name="health_report" />,
    path: 'Health',
  },
  {
    labelKey: `Workout tracker`,
    icon: <Icon group="linkSection" name="workout_tracker" />,
    path: 'Workout',
  },
  {
    labelKey: `eCard`,
    icon: <Icon group="linkSection" name="e_card" />,
    path: 'ECard',
  },
  {
    labelKey: `Help`,
    icon: <Icon group="linkSection" name="help" />,
    path: 'Help',
  },
];
