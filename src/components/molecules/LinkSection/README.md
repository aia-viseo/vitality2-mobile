## LinkSection
Contain link buttons for redirect user to different screen

# Usage & Example
```js
import LinkSection from 'molecules/LinkSection';

<LinkSection
  onPressItem={(path) => {
    //do sth with the path
  }}
/>
```
Key         | Type           | Required  | Description                       |
----------- | -------------- | --------- | --------------------------------- |
onPressItem | `callback`     | `False`   | Trigger when press link item. |

# Step to add new links
in /LinkSection/config/index.js
```js

export const BUTTON_LIST = [
  {
    labelKey: 'Notifications',
    icon: <Notification />,
    path: 'Notification',
  },
  //2. Add here
];
```

Key         | Type      | Required | Description                       |
-------- | ------------ | -------- | --------------------------------- |
labelKey | `string`     | `True`   | Key to map the translation label. |
icon     | `React.Node` | `True`   | Node for link icon.               |
path     | `string`     | `True`   | Linking Path.                     |
