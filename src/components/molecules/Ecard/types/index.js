/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  name?: string,
  membershipNumber?: string,
  expirationDate?: string,
  barCodeData?: string,
  barCodeFormat?: string,
};
