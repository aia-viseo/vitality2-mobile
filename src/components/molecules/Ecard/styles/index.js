/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {COLOR_AIA_RED, WHITE} from '@colors';

const EcardStyles = StyleSheet.create({
  container: {
    height: '95%',
    justifyContent: 'space-evenly',
    paddingTop: 90,
  },
  eCard: {
    backgroundColor: COLOR_AIA_RED,
    borderRadius: 10,
    height: 230,
    justifyContent: 'space-between',
  },
  logo: {
    color: WHITE,
  },
  name: {
    color: WHITE,
    fontWeight: 'bold',
  },
  barcodeName: {
    color: '#363E3F',
    fontSize: 24,
    fontWeight: '500',
    padding: 10,
  },
  expire: {
    color: WHITE,
    fontSize: 14,
  },
  barcodeExpire: {
    color: '#3E4647',
    fontSize: 16,
    fontWeight: '300',
  },
  barcodeImage: {alignItems: 'center', paddingTop: 20},
  logoImage: {height: '65%', width: '65%'},
});

export default EcardStyles;
