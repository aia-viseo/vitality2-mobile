/**
 *
 * Ecard Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Ecard from '../Ecard';

jest.mock('react-native-barcode-builder', () => 'Barcode');
test('Ecard', () => {
  const tree = renderer.create(<Ecard />).toJSON();
  expect(tree).toMatchSnapshot();
});
