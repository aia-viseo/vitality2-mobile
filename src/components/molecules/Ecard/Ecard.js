/**
 *
 * Ecard
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import Barcode from 'react-native-barcode-builder';

import Text from '../../atoms/Text';
import LinearGradient from '../../atoms/LinearGradient';
import Icon from '../../atoms/Icon';

import AppStyles from '../../../core/styles';

import type {PropsType} from './types';
import EcardStyles from './styles';

import {LinearLocations, LinearColors} from '../../atoms/LinearGradient/configs';

const Ecard = (props: PropsType) => {
  const {name, membershipNumber, expirationDate, barCodeData, barCodeFormat} = props;
  const membershipText: string = membershipNumber || '';
  const expirationText: string = expirationDate || '';

  const aiaLogo = (
    <Icon
      group="general"
      name="aia_vitality_white"
      extraStyle={[EcardStyles.logoImage, {transform: [{translateX: -55}, {translateY: -10}]}]}
    />
  );

  return (
    <View style={[AppStyles.paddingHorizontal_20, EcardStyles.container]}>
      <LinearGradient
        startX={0}
        startY={0}
        endX={1}
        endY={0}
        customLinearLocations={LinearLocations}
        customLinearColors={LinearColors}
        extraStyle={[
          EcardStyles.eCard,
          AppStyles.paddingHorizontal_20,
          AppStyles.paddingVertical_20,
        ]}>
        <View>{aiaLogo}</View>
        <View>
          <Text customStyle={[EcardStyles.name]}>{name}</Text>
          <Text customStyle={[EcardStyles.expire]}>{`Membership Number: ${membershipText}`}</Text>
          <Text customStyle={[EcardStyles.expire]}>{`Expires on ${expirationText}`}</Text>
        </View>
      </LinearGradient>
      <View style={[AppStyles.paddingTop_20]}>
        <View style={{alignItems: 'center'}}>
          <Text customStyle={[EcardStyles.barcodeName]}>{name}</Text>
          <Text customStyle={[EcardStyles.barcodeExpire]}>
            {`Membership Number: ${membershipText}`}
          </Text>
          <Text customStyle={[EcardStyles.barcodeExpire]}>{`Expires on ${expirationText}`}</Text>
        </View>
        <View style={[EcardStyles.barcodeImage]}>
          {barCodeData ? (
            <Barcode
              value={barCodeData}
              format={barCodeFormat || 'CODE128'}
              height={75}
              width={1}
              background="transparent"
            />
          ) : (
            <View />
          )}
        </View>
      </View>
    </View>
  );
};

Ecard.defaultProps = {};

export default Ecard;
