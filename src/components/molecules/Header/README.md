## Header
Component to contain content display at the top of the mobile application.

# Usage
```js
  import Header from 'molecules/Header';

  <Header
    fixHeight
    scrollY
    start
    end
    location
    colors
  >
    // any component here
  </Header>
```

#Animate Example
```js
import Header from 'molecules/Header';
import {
  Text,
  Animated
} from 'react-native';

const scrollY = new Animated.Value(0)

<View>
  <Header
    fixHeight={100}
    scrollY={new Animate.Value}
    start={{x: 1, y: 0}}
    end={{x: 0, y: 1}}
    location={[0, 0.5, 1]}
    colors={[#fff, #ddd, #000]}
  >
    <Text>
      Hi there
    </Text>
  </Header>
  <ScrollView
    style={{flex: 1}}
    scrollEventThrottle={16}
    onScroll={Animated.event(
      [{ nativeEvent: { contentOffset: { y: scrollY } } } ], {}
    )}
  >
  </ScrollView>
</View>
```
Key       | Type            | Required | Description                                                                |
--------- | --------------- | -------- | -------------------------------------------------------------------------- |
fixHeight | `number`        | `False`  | Fixed height of the header. scrollY will not work if input this.           |
scrollY   | `AnimatedValue` | `False`  | Animate height value of the header. fixHeight will not work if input this. |
start     | `Object`        | `False`  | Object containe x and y, indicate the start point.                         |
end       | `Object`        | `False`  | Object containe x and y, indicate the end point.                           |
location  | `Array<number>` | `False`  | Array of numbers, to control the linear gradient offset.                   |
colors    | `Array<color>`  | `False`  | Array of color, to control the linear gradient colors.                     |
children  | `Node`          | `False`  | Node will render inside the header.                                        |

# Non-animate Example
```js
import Header from 'molecules/Header';
import {
  Text
} from 'react-native';

<Header
  fixHeight={100}
  start={{x: 1, y: 0}}
  end={{x: 0, y: 1}}
  location={[0, 0.5, 1]}
  colors={[#fff, #ddd, #000]}
>
  <Text>
    Hi there
  </Text>
</Header>
```
Key       | Type            | Required | Description                                                                |
--------- | --------------- | -------- | -------------------------------------------------------------------------- |
fixHeight | `number`        | `False`  | Fixed height of the header. scrollY will not work if input this.           |
start     | `Object`        | `False`  | Object containe x and y, indicate the start point.                         |
end       | `Object`        | `False`  | Object containe x and y, indicate the end point.                           |
location  | `Array<number>` | `False`  | Array of numbers, to control the linear gradient offset.                   |
colors    | `Array<color>`  | `False`  | Array of color, to control the linear gradient colors.                     |
children  | `Node`          | `False`  | Node will render inside the header.                                        |
