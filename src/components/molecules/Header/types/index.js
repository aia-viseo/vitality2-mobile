/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  fixHeight?: number,
  scaleHeight?: number,
  startX?: 0 | 1,
  startY?: 0 | 1,
  endX?: 0 | 1,
  endY?: 0 | 1,
  locations?: Array<number>,
  colors?: Array<string>,
  children?: Node,
  innerCardChildren?: Node,
  setTop?: boolean,
  hideBackground?: boolean,
  title?: string,
  scrollY?: number,
  cardContent?: () => void,
  translateY?: number,
  customPadding?: ViewStyleProp,
};
