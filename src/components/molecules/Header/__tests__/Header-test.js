/**
 *
 *  Test Header
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Header from '../Header';

test('Test Header ', () => {
  const tree = renderer.create(<Header />).toJSON();
  expect(tree).toMatchSnapshot();
});
