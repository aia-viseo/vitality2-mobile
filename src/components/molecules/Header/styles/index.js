/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';

import GetOS from '@platform';
import {COLOR_AIA_DARK_GREY} from '@colors';

const isIOS = GetOS.getInstance() === 'ios';

const containerTop = isIphoneX() ? 25 : 10;

const HeaderStyles = StyleSheet.create({
  container: {
    shadowColor: COLOR_AIA_DARK_GREY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 98,
    zIndex: 98,
    overflow: 'visible',
  },
  linearGradient: {
    flex: 1,
    position: 'relative',
    borderBottomLeftRadius: 25,
  },
  childrenContainer: {
    position: 'relative',

    left: 0,
    right: 0,
  },
  topArea: {
    top: isIOS ? containerTop : 0,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  headerContainer: {
    shadowColor: COLOR_AIA_DARK_GREY,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 98,
    zIndex: 98,
    marginBottom: 5,
    overflow: 'visible',
  },
  generalCardFilterGroup: {
    marginTop: 100,
    position: 'relative',
    elevation: 100,
  },
});

export default HeaderStyles;
