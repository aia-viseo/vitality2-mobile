/**
 *
 * Outer Card
 * @format
 *
 */

// @flow

import React from 'react';
import Animated from 'react-native-reanimated';

import AppStyles from '@styles';
import GetOS from '@platform';

import {TRANSLATE_Y_IOS, TRANSLATE_Y_ANDROID} from './configs';
import type {PropsType} from './types';

const isIOS = GetOS.getInstance() === 'ios';

const OuterCard = (props: PropsType) => {
  const {children, scrollY, translateY} = props;

  const defaultTranslateY = isIOS ? TRANSLATE_Y_IOS : TRANSLATE_Y_ANDROID;
  return (
    <Animated.View
      style={[
        AppStyles.headerCardContainer,
        {
          transform: [{translateY: translateY || defaultTranslateY}],
          opacity: Animated.interpolate(scrollY, {
            inputRange: [0, 10],
            outputRange: [1, 0],
            extrapolate: 'clamp',
          }),
          top: Animated.interpolate(scrollY, {
            inputRange: [0, 10],
            outputRange: [0, -1000],
            extrapolate: 'clamp',
          }),
        },
      ]}>
      {children}
    </Animated.View>
  );
};

export default OuterCard;
