/**
 *
 * Header
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import Animated from 'react-native-reanimated';

import AppStyles from '@styles';
import Text from '@atoms/Text';
import LinearGradient from '@atoms/LinearGradient';

import {LinearLocations, LinearColors} from './configs';

import type {PropsType} from './types';
import HeaderStyles from './styles';

import CardContainer from '../Card/CardContainer';
import HeaderOuterCard from './HeaderOuterCard';

const Header = (props: PropsType) => {
  const {
    fixHeight,
    scaleHeight,
    hideBackground,
    children,
    colors,
    locations,
    startX,
    startY,
    endX,
    endY,
    setTop,
    title,
    scrollY,
    translateY,
    cardContent,
    customPadding,
  } = props;

  const renderCard = () => {
    return (
      <HeaderOuterCard scrollY={scrollY} translateY={translateY}>
        {cardContent && (
          <CardContainer customPadding={customPadding}>{cardContent()}</CardContainer>
        )}
      </HeaderOuterCard>
    );
  };
  return (
    <>
      <Animated.View
        style={[
          HeaderStyles.container,
          {
            height: fixHeight || scaleHeight,
          },
        ]}>
        <LinearGradient
          startX={startX || 0}
          startY={startY || 0}
          endX={endX || 1}
          endY={endY || 0}
          customLinearLocations={locations || LinearLocations}
          customLinearColors={colors || LinearColors}
          hideBackground={hideBackground}
          extraStyle={[HeaderStyles.linearGradient]}>
          <View style={[HeaderStyles.childrenContainer, setTop && HeaderStyles.topArea]}>
            {title && (
              <View style={[AppStyles.justifyContentCenter, AppStyles.marginTop_20, AppStyles.row]}>
                <Text customStyle={AppStyles.headerTitle}>{title}</Text>
              </View>
            )}
            {children}
          </View>
        </LinearGradient>
        <View />
      </Animated.View>
      {cardContent && renderCard()}
    </>
  );
};

export default Header;
