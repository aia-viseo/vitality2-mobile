/**
 *
 * Toggle
 * @format
 *
 */

// @flow

import React from 'react';
import {Dimensions} from 'react-native';
import type {PropsType} from './types';
import ToggleOne from './ToggleOne';
import ToggleTwo from './ToggleTwo';

const {width} = Dimensions.get('window');

const Toggle = (props: PropsType) => {
  const {
    variationNumber,
    buttonLabel1,
    buttonLabel2,
    buttonValue1,
    buttonValue2,
    style,
    buttonTextStyle,
    onPress,
    selectedValue,
    buttonWidth,
  } = props;

  if (variationNumber === 2) {
    return (
      <ToggleTwo
        buttonLabel1={buttonLabel1}
        buttonLabel2={buttonLabel2}
        buttonValue1={buttonValue1}
        buttonValue2={buttonValue2}
        selectedValue={selectedValue}
        style={style}
        buttonTextStyle={buttonTextStyle}
        buttonWidth={buttonWidth || width / 2 - 20}
        onPress={onPress}
      />
    );
  }

  return (
    <ToggleOne
      buttonLabel1={buttonLabel1}
      buttonLabel2={buttonLabel2}
      buttonValue1={buttonValue1}
      buttonValue2={buttonValue2}
      selectedValue={selectedValue}
      style={style}
      buttonTextStyle={buttonTextStyle}
      buttonWidth={buttonWidth || width / 2 - 20}
      onPress={onPress}
    />
  );
};

export default Toggle;
