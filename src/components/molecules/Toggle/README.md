## Toggle
A General molecules component for different variations

# ToggleVariation1
# Usage
```js
import Toggle from '/molecules/Toggle';

<Toggle
  variationNumber={1}
  selectedValue
  buttonLabel1
  buttonLabel2
  buttonValue1
  buttonValue2
  buttonTextStyle
  style
  buttonWidth
  onPress
/>
```

# Example
```js
import Toggle from '/molecules/Toggle';

<Toggle
  variationNumber={1}
  selectedValue="Yes"
  buttonLabel1="Yes"
  buttonLabel2="No"
  buttonValue1="Yes"
  buttonValue2="No"
  buttonTextStyle
  style
  buttonWidth
  onPress={() => {}}
/>
```

# ToggleVariation2
# Usage
```js
import Toggle from '/molecules/Toggle';

<Toggle
  variationNumber={2}
  selectedValue
  buttonLabel1
  buttonLabel2
  buttonValue1
  buttonValue2
  buttonTextStyle
  style
  buttonWidth
  onPress
/>
```

# Example
```js
import Toggle from '/molecules/Toggle';

<Toggle
  variationNumber={2}
  selectedValue="Yes"
  buttonLabel1="Yes"
  buttonLabel2="No"
  buttonValue1="Yes"
  buttonValue2="No"
  buttonTextStyle
  style
  buttonWidth
  onPress={() => {}}
/>
```

Key              | Type              | Required | Description                                                |
---------------- | ----------------- | -------- | ---------------------------------------------------------- |
variationNumber  | `number`          | `True`   | Number to define which variation for the toggle.           |
selectedValue    | `string`          | `True`   | String default value of toggle.                            |
buttonLabel1     | `string`          | `True`   | String label for first button.                             |
buttonLabel2     | `string`          | `True`   | String label for second button.                            |
buttonValue1     | `string`          | `True`   | String value for first button.                             |
buttonValue2     | `string`          | `True`   | String value for second button.                            |
buttonTextStyle  | `TextStyleProp`   | `False`  | Optional extra styling for the button text.                |
style            | `ViewStyleProp`   | `False`  | Optional extra styling for the toggle component.           |
buttonWidth      | `number`          | `False`  | Optional toggle button width.                              |
onPress          | `function`        | `False`  | Optional function to trigger when press the toggle button. |
