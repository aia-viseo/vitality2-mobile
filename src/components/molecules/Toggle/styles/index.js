/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

import {
  WHITE,
  BLACK,
  COLOR_AIA_GREY,
  COLOR_AIA_GD_RED_1,
  COLOR_AIA_VITALITY_BLUE,
  COLOR_AIA_GREY_50,
} from '@colors';

const ToggleStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  toggleButtonContainer: {
    justifyContent: 'center',
    borderRadius: 5,
  },
  selectedContainer: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: COLOR_AIA_VITALITY_BLUE,
  },
  selectedContainerBtnOne: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: COLOR_AIA_VITALITY_BLUE,
  },
  selectedContainerBtnTwo: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: COLOR_AIA_GD_RED_1,
  },
  notSelectedContainer: {
    backgroundColor: COLOR_AIA_GREY_50,
    borderWidth: 0,
  },
  selectedLabel: {
    color: BLACK,
    fontWeight: '700',
  },
  notSelectedLabel: {
    color: COLOR_AIA_GREY,
    fontWeight: '500',
  },
});

export default ToggleStyles;
