/**
 *
 * @format
 *
 */

// @flow

export const CHECK_CIRCLE = 'checkcircle';
export const CHECK_CIRCLE_O = 'checkcircleo';
export const CLOSE_CIRCLE = 'closecircle';
export const CLOSE_CIRCLE_O = 'closecircleo';
