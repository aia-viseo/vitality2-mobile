/**
 *
 * ToggleTwo
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity} from 'react-native';

import AppStyles from '@styles';
import Text from '@atoms/Text';

import {COLOR_AIA_GREY_50} from '@colors';
import type {PropsType} from './types';
import ToggleStyles from './styles';

const ToggleTwo = (props: PropsType) => {
  const {
    buttonLabel1,
    buttonLabel2,
    buttonValue1,
    buttonValue2,
    onPress,
    style,
    buttonTextStyle,
    selectedValue,
    buttonWidth,
  } = props;
  const [btnOne, setBtnOne] = useState({
    isSelected: selectedValue === buttonValue1,
  });
  const [btnTwo, setBtnTwo] = useState({
    isSelected: selectedValue === buttonValue2,
  });

  const handleOnPress = (value: string) => {
    setBtnOne({
      isSelected: value === buttonValue1,
    });
    setBtnTwo({
      isSelected: value === buttonValue2,
    });
    if (onPress) {
      onPress(value);
    }
  };

  useEffect(() => {
    if (selectedValue) handleOnPress(selectedValue);
  }, [selectedValue]);

  const renderToggleButton = (label: string, value: any, icon: any) => {
    const {isSelected} = icon;
    return (
      <TouchableOpacity onPress={() => handleOnPress(value)}>
        <View
          style={[
            style,
            AppStyles.paddingVertical_20,
            ToggleStyles.toggleButtonContainer,
            isSelected ? ToggleStyles.selectedContainer : ToggleStyles.notSelectedContainer,
            {width: buttonWidth, backgroundColor: COLOR_AIA_GREY_50},
          ]}>
          <View
            style={[AppStyles.column, AppStyles.alignItemCenter]}
            onLayout={() => selectedValue && handleOnPress(selectedValue)}>
            <Text
              customStyle={[
                buttonTextStyle,
                isSelected ? ToggleStyles.selectedLabel : ToggleStyles.notSelectedLabel,
              ]}>
              {label}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={[AppStyles.row, ToggleStyles.container, style]}>
      {renderToggleButton(buttonLabel1, buttonValue1, btnOne)}
      {renderToggleButton(buttonLabel2, buttonValue2, btnTwo)}
    </View>
  );
};

export default ToggleTwo;
