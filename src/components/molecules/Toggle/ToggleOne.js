/**
 *
 * ToggleOne
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import AppStyles from '@styles';
import {COLOR_AIA_VITALITY_BLUE, COLOR_AIA_GD_RED_1, COLOR_AIA_GREY} from '@colors';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import ToggleStyles from './styles';

import {CHECK_CIRCLE, CHECK_CIRCLE_O, CLOSE_CIRCLE, CLOSE_CIRCLE_O} from './config';

const ToggleOne = (props: PropsType) => {
  const {
    buttonLabel1,
    buttonLabel2,
    buttonValue1,
    buttonValue2,
    onPress,
    style,
    buttonWidth,
    buttonTextStyle,
    selectedValue,
  } = props;
  const [iconOne, setIconOne] = useState({
    iconName: selectedValue === buttonValue1 ? CHECK_CIRCLE : CHECK_CIRCLE_O,
    isSelected: selectedValue === buttonValue1,
  });
  const [iconTwo, setIconTwo] = useState({
    iconName: selectedValue === buttonValue2 ? CLOSE_CIRCLE : CLOSE_CIRCLE_O,
    isSelected: selectedValue === buttonValue2,
  });

  const updateIcon = (value?: string) => {
    setIconOne({
      iconName: value === buttonValue1 ? CHECK_CIRCLE : CHECK_CIRCLE_O,
      isSelected: value === buttonValue1,
    });
    setIconTwo({
      iconName: value === buttonValue2 ? CLOSE_CIRCLE : CLOSE_CIRCLE_O,
      isSelected: value === buttonValue2,
    });
  };

  const handleOnPress = (value: string) => {
    updateIcon(value);
    if (onPress) {
      onPress(value);
    }
  };

  useEffect(() => {
    updateIcon(selectedValue);
  }, [selectedValue]);

  const renderToggleButton = (label: string, value: any, icon: any, index: number) => {
    const {iconName, isSelected} = icon;
    const iconColor = value === buttonValue1 ? COLOR_AIA_VITALITY_BLUE : COLOR_AIA_GD_RED_1;
    const selectedContainer =
      index === 1 ? ToggleStyles.selectedContainerBtnOne : ToggleStyles.selectedContainerBtnTwo;
    return (
      <TouchableOpacity onPress={() => handleOnPress(value)}>
        <View
          style={[
            AppStyles.paddingVertical_10,
            ToggleStyles.toggleButtonContainer,
            isSelected ? selectedContainer : ToggleStyles.notSelectedContainer,
            {width: buttonWidth},
          ]}>
          <View style={[AppStyles.column, AppStyles.alignItemCenter]}>
            <Icon color={isSelected ? iconColor : COLOR_AIA_GREY} name={iconName} size={25} />
            <Text
              customStyle={[
                buttonTextStyle,
                isSelected ? ToggleStyles.selectedLabel : ToggleStyles.notSelectedLabel,
              ]}>
              {label}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={[AppStyles.row, ToggleStyles.container, style]}>
      {renderToggleButton(buttonLabel1, buttonValue1, iconOne, 1)}
      {renderToggleButton(buttonLabel2, buttonValue2, iconTwo, 2)}
    </View>
  );
};

export default ToggleOne;
