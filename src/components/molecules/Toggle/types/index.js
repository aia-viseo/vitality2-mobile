/**
 *
 * @format
 *
 */

// @flow

import type {ViewStyleProp, TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  selectedValue?: any,
  buttonLabel1: string,
  buttonLabel2: string,
  buttonValue1: any,
  buttonValue2: any,
  buttonTextStyle?: TextStyleProp,
  style?: ViewStyleProp,
  onPress?: (value: any) => any,
  variationNumber?: number,
  buttonWidth?: number,
};
