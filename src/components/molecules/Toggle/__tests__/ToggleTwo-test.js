/**
 *
 * ToggleTwo Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import ToggleTwo from '../ToggleTwo';

jest.useFakeTimers();

test('test ToggleTwo', () => {
  const tree = renderer
    .create(
      <ToggleTwo
        variationNumber={1}
        buttonLabel1="Yes"
        buttonValue1="Yes"
        buttonLabel2="No"
        buttonValue2="No"
        selectedValue="Yes"
        onPress={() => {}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
