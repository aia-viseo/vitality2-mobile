/**
 *
 * ToggleOne Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import ToggleOne from '../ToggleOne';

jest.useFakeTimers();
jest.mock('react-native-vector-icons/AntDesign', () => 'checkcirle');
test('test ToggleOne', () => {
  const tree = renderer
    .create(
      <ToggleOne
        variationNumber={1}
        buttonLabel1="Yes"
        buttonValue1="Yes"
        buttonLabel2="No"
        buttonValue2="No"
        selectedValue="Yes"
        onPress={() => {}}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
