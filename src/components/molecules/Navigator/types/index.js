/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  type?: 'back' | 'dismiss',
  title: string,
  scaleWidth?: number,
  onPressItem: () => void,
  showTitle?: boolean,
  rightButtonType?: string,
  onPressRightButton?: () => void,
};
