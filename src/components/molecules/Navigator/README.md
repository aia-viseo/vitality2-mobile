# Navigator

Replacement Navbar with Back Button and Title. This assumes that `headerShown` is set to `false`

## Usage within a StackNavigation

```js
import Navigator from 'molecules/Navigator';

const SomeScreen = (props: PropsType) => {
  const {navigation} = props;
  const goBack = () => navigation.goBack();

return (
    <Navigator title={'some title'} onPressItem={goBack} />
  );
};

```

## Parameters

Key             | Type                    | Required                      | Description
--------------- | ----------------------- | ----------------------------- | --------------------------------------------
type            | `'back'` \| `'dismiss'` | `false`, default is `'back'`  | The type of button to display
onPressItem     | `string`                | `true`                        | The function to call when the user taps on the button
title           | `string`                | `false`                       | The title to display when user scrolls down

## TODOs

1. Support expanding title bar
