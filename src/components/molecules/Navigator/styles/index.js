/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import GetOS from '@platform';
import {COLOR_AIA_GREY, WHITE} from '@colors';

const isIOS = GetOS.getInstance() === 'ios';

const containerTop = isIphoneX() ? 25 : 10;

const NavigatorStyles = StyleSheet.create({
  container: {
    elevation: 99,
    zIndex: 99,
  },
  navBar: {
    flexDirection: 'row',
    top: isIOS ? containerTop : 0,
  },
  icon: {
    shadowColor: COLOR_AIA_GREY,
    shadowOffset: {width: 2, height: 3},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    marginTop: 10,
    marginLeft: 10,
    width: 40,
    height: 40,
    backgroundColor: WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  rightIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 20,
  },
  position: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginLeft: 10,
    width: 40,
    height: 40,
  },
  titleContainer: {
    marginTop: 10,
    justifyContent: 'center',
    flexGrow: 4,
    height: 40,
  },
  title: {
    fontWeight: '600',
  },
});

export default NavigatorStyles;
