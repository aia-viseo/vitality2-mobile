/**
 *
 * Navigator Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Navigator from '../Navigator';

test('Navigator', () => {
  const tree = renderer.create(<Navigator title="hello" onPressItem={() => {}} />).toJSON();
  expect(tree).toMatchSnapshot();
});
