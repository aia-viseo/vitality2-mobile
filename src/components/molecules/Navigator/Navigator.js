/**
 *
 * Navigator
 * @format
 *
 */

// @flow

import React from 'react';
import {SafeAreaView, View, TouchableWithoutFeedback} from 'react-native';
import Animated from 'react-native-reanimated';

import Icon from '@atoms/Icon';
import Text from '@atoms/Text';

import type {PropsType} from './types';
import NavigatorStyles from './styles';

const Navigator = (props: PropsType) => {
  const {
    scaleWidth = '100%',
    type = 'back',
    title,
    onPressItem,
    onPressRightButton,
    rightButtonType,
    showTitle,
  } = props;

  return (
    <Animated.View style={NavigatorStyles.container}>
      <SafeAreaView style={[NavigatorStyles.navBar, {width: scaleWidth}]}>
        <TouchableWithoutFeedback onPress={onPressItem}>
          <View style={[!showTitle && NavigatorStyles.icon, NavigatorStyles.position]}>
            <Icon group="general" name={type} />
          </View>
        </TouchableWithoutFeedback>
        {showTitle && (
          <View style={NavigatorStyles.titleContainer}>
            <Text customStyle={NavigatorStyles.title}>{title}</Text>
          </View>
        )}
        {rightButtonType && (
          <TouchableWithoutFeedback onPress={onPressRightButton}>
            <View style={NavigatorStyles.rightIcon}>
              <Icon group="general" name={rightButtonType} />
            </View>
          </TouchableWithoutFeedback>
        )}
      </SafeAreaView>
    </Animated.View>
  );
};

export default Navigator;
