/**
 *
 * @format
 *
 */

// @flow

import {COLOR_AIA_GOLD, COLOR_AIA_BRONZE, COLOR_AIA_SILVER, COLOR_AIA_PLATINUM} from '@colors';

export const LEVEL_CONFIG_MAP = {
  Loading: {
    color: '#aaa',
    labelWidth: 60,
    labelKey: '',
  },
  Gold: {
    color: COLOR_AIA_GOLD,
    labelWidth: 60,
    labelKey: 'Gold',
  },
  Bronze: {
    color: COLOR_AIA_BRONZE,
    labelWidth: 90,
    labelKey: 'Bronze',
  },
  Silver: {
    color: COLOR_AIA_SILVER,
    labelWidth: 60,
    labelKey: 'Silver',
  },
  Platinum: {
    color: COLOR_AIA_PLATINUM,
    labelWidth: 80,
    labelKey: 'Platinum',
  },
};
export {LEVEL_CONFIG_MAP as default};
