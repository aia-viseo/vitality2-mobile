/**
 *
 * @format
 *
 */

// @flow

type BadgeLevel = 'Gold' | 'Bronze' | 'Silver' | 'Platinum';

export type PropsType = {
  level: BadgeLevel,
  points: number,
  showLevel?: boolean,
  retrievePoints?: (points: number) => void,
  retrieveLevel?: (level: string) => void,
};
