/**
 *
 *  Test MembershipBadge
 *  @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import MembershipBadge from '../MembershipBadge';

jest.useFakeTimers();
test('Test MembershipBadge ', () => {
  const level = 'Gold';
  const points = 5000;

  const tree = renderer.create(<MembershipBadge level={level} points={points} />).toJSON();
  expect(tree).toMatchSnapshot();
});
