## MembershipBadge
Molecules Component to display Membership Level and the points

# Usage
```js
import MembershipBadge from 'molecules/MembershipBadge';

<MembershipBadge
  points
  level
  showName
/>
```

# Example
```js
import MembershipBadge from 'molecules/MembershipBadge';

<MembershipBadge
  points={12321312312}
  level={'gold'}
  showName={false}
/>
```

Key            | Type     | Required | Description                                  |
-------------- | -------- | -------- | -------------------------------------------- |
points         | `number` | `True`   | Point value to display.                      |
level          | `string` | `True`   | Key to map the level config.                 |
showName       | `boolean`| `False`  | Control the display between bullet or label. |

# Step to add new level
in /MembershipBadge/config/index.js
```js
//1. Get your new color here
import {COLOR_AIA_YELLOW} from '@colors';

export const LEVEL_CONFIG_MAP = {
  gold: {
    color: COLOR_AIA_YELLOW,
    labelWidth: 60,
    labelKey: 'Gold',
  },
  //2. Add here
};
```

Key         | Type     | Required | Description                                                             |
----------- | -------- | -------- | ----------------------------------------------------------------------- |
color       | `string` | `True`   | Color for the level label and background for the level label container. |
labelWidth  | `number` | `True`   | Width of the level label container and the animation value.             |
labelKey    | `string` | `True`   | Translation key.                                                        |
