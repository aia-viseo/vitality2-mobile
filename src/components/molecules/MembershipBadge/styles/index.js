/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {WHITE, BLACK} from '@colors';

const MembershipBadgeStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 20,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: WHITE,
    shadowColor: BLACK,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 1,
    shadowRadius: 2,
  },
  pointContainer: {
    minHeight: 30,
    backgroundColor: WHITE,
    paddingVertical: 5,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  levelContainer: {
    overflow: 'hidden',
  },
  levelText: {
    paddingVertical: 3,
    paddingHorizontal: 10,
    color: WHITE,
    fontSize: RFValue(14),
    fontWeight: '600',
  },
  pointText: {
    fontSize: RFValue(14),
    fontWeight: '500',
    marginLeft: 15,
  },
  pointLabel: {
    fontSize: RFValue(14),
    fontWeight: '300',
    marginHorizontal: 5,
  },
  pointLevelText: {
    fontSize: RFValue(14),
    fontWeight: '600',
    marginRight: 15,
  },
  bulletContainer: {
    height: 16,
    width: 16,
    borderRadius: 8,
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bullet: {
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: WHITE,
  },
});

export default MembershipBadgeStyle;
