/**
 *
 * MembershipBadge
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import {useTranslation} from 'react-i18next';

import Text from '@atoms/Text';

import LEVEL_CONFIG from './config';
import type {PropsType} from './types';
import MembershipBadgeStyle from './styles';

import NumberString from '../../../core/handler/NumberString';

const MembershipBadge = (props: PropsType) => {
  const {t} = useTranslation();
  const {points, level, showLevel} = props;
  const config = LEVEL_CONFIG[level];

  const renderBullet = () => {
    return (
      <View style={[MembershipBadgeStyle.bulletContainer, {backgroundColor: config.color}]}>
        <View style={MembershipBadgeStyle.bullet} />
      </View>
    );
  };

  const renderPoints = () => {
    const pointStr = NumberString.toLocaleString(points);

    return (
      <View style={MembershipBadgeStyle.pointContainer}>
        <Text customStyle={MembershipBadgeStyle.pointText} numberOfLines={1}>
          {pointStr}
        </Text>
        <Text customStyle={MembershipBadgeStyle.pointLabel} numberOfLines={1}>
          {t('general.pointsTitle')}
        </Text>
        {showLevel ? (
          <Text
            customStyle={[MembershipBadgeStyle.pointLevelText, {color: config.color}]}
            numberOfLines={1}>
            {t(config.labelKey)}
          </Text>
        ) : (
          renderBullet()
        )}
      </View>
    );
  };

  return (
    <View style={[MembershipBadgeStyle.container, {backgroundColor: config.color}]}>
      {renderPoints()}
    </View>
  );
};
export default MembershipBadge;
