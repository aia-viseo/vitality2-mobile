/**
 *
 * Journey
 * @format
 *
 */

// @flow

import React from 'react';
import {StatusBar, ScrollView, View, FlatList, KeyboardAvoidingView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import GetOS from '@platform';
import HeaderFlow from '@organisms/HeaderFlow';

import JourneyStyles from './styles';
import type {PropsType} from './types';

const isIOS = GetOS.getInstance() === 'ios';
const Journey = (props: PropsType) => {
  const {
    onPress,
    hasProgressBar,
    headerContainer,
    ref,
    questionContainer,
    buttonContainer,
    progressBarProgress,
    progressBarLabel,
    flowJourney,
    reviewJourney,
    containsInput,
  } = props;

  const content = (
    <KeyboardAvoidingView
      behavior={isIOS ? 'padding' : 'position'}
      style={JourneyStyles.keyboardAvoidingContainer}>
      <View style={JourneyStyles.headerContainer}>
        <HeaderFlow
          onPress={onPress}
          hasProgressBar={hasProgressBar}
          progress={progressBarProgress}
          rightLabel={progressBarLabel}
        />
        {headerContainer}
      </View>
      {flowJourney && (
        <ScrollView ref={ref} showsVerticalScrollIndicator={false} bounces={false}>
          <View>{questionContainer}</View>
        </ScrollView>
      )}
      {reviewJourney && (
        <FlatList data={[questionContainer]} renderItem={({item}) => <View>{item}</View>} />
      )}
    </KeyboardAvoidingView>
  );

  return (
    <View style={JourneyStyles.container}>
      <StatusBar barStyle="dark-content" />
      {containsInput ? <KeyboardAwareScrollView>{content}</KeyboardAwareScrollView> : content}
      {buttonContainer}
    </View>
  );
};

export default Journey;
