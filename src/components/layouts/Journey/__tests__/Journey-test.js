/**
 *
 * Journey Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import Journey from '../Journey';

test('Journey', () => {
  const tree = renderer
    .create(
      <Journey
        hasProgressBar
        progressBarProgress={50}
        progressBarLabel="50% completed"
        flowJourney
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
