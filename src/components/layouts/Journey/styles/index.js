/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';

import {COLOR_AIA_GREY_50} from '@colors';

export const PADDING_TOP = isIphoneX() ? 40 : 20;

const JourneyStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_AIA_GREY_50,
  },
  keyboardAvoidingContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  headerContainer: {
    paddingTop: PADDING_TOP,
    paddingBottom: 20,
  },
});

export default JourneyStyles;
