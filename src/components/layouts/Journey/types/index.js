/**
 *
 * @format
 *
 */

// @flow
import type {Node} from 'react';

export type PropsType = {
  onPress?: () => void,
  hasProgressBar?: boolean,
  headerContainer?: Node,
  ref?: () => void,
  questionContainer?: Node,
  buttonContainer?: Node,
  progressBarProgress?: number,
  progressBarLabel?: string,
  flowJourney?: Node,
  editJourney?: Node,
  reviewJourney?: Node,
  containsInput?: boolean,
};
