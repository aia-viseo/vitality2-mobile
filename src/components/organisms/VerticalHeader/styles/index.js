/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_DARK_GREY, COLOR_AIA_GREY_500, COLOR_AIA_GREY_50} from '@colors';
import {projectFontFamily, generalHorizontalPadding} from '@core/config';

const VerticalHeaderStyles = StyleSheet.create({
  container: {
    backgroundColor: COLOR_AIA_GREY_50,
    paddingHorizontal: generalHorizontalPadding,
    flexDirection: 'column',
  },
  title: {
    color: COLOR_AIA_DARK_GREY,
    fontFamily: projectFontFamily,
    fontSize: RFValue(22),
    fontWeight: '700',
  },
  description: {
    color: COLOR_AIA_GREY_500,
    fontFamily: projectFontFamily,
    fontSize: RFValue(14),
    marginTop: 5,
  },
});

export default VerticalHeaderStyles;
