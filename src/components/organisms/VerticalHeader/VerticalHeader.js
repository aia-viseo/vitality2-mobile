/**
 *
 * VerticalHeader
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import Text from '@atoms/Text';
import Icon from '@atoms/Icon';

import VerticalHeaderStyles from './styles';
import type {PropsType} from './types';

const VerticalHeader = (props: PropsType) => {
  const {title, description, imageName, imageGroup, imageWidth, imageHeight} = props;

  return (
    <View style={VerticalHeaderStyles.container}>
      <View>
        <Text customStyle={[VerticalHeaderStyles.title]}>{title}</Text>
        <Text customStyle={VerticalHeaderStyles.description}>{description}</Text>
      </View>
      <View>
        <Icon group={imageGroup} name={imageName} width={imageWidth} height={imageHeight} />
      </View>
    </View>
  );
};

VerticalHeader.defaultProps = {
  title: 'Title',
  description: '',
  imageName: 'manRunning',
  imageGroup: 'general',
};

export default VerticalHeader;
