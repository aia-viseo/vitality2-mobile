/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  title: string,
  description: string,
  imageName: string,
  imageGroup?: string,
  imageWidth?: number,
  imageHeight?: number,
};
