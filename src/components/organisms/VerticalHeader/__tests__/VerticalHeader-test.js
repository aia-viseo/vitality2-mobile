/**
 *
 * VerticalHeader Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import VerticalHeader from '../VerticalHeader';

test('VerticalHeader', () => {
  const tree = renderer
    .create(
      <VerticalHeader
        title="Title"
        description="description"
        imageName="manRunning"
        imageGroup="general"
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
