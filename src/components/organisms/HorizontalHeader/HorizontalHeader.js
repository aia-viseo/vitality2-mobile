/**
 *
 * Horizontal Header
 * @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';

import Text from '@atoms/Text';
import Icon from '@atoms/Icon';

import HorizontalHeaderStyles from './styles';
import type {PropsType} from './types';

const HorizontalHeader = (props: PropsType) => {
  const {title, description, imageName, imageGroup, imageWidth, imageHeight} = props;

  return (
    <View style={HorizontalHeaderStyles.container}>
      <View style={HorizontalHeaderStyles.textContainer}>
        <Text customStyle={HorizontalHeaderStyles.title}>{title}</Text>
        <Text customStyle={HorizontalHeaderStyles.description}>{description}</Text>
      </View>
      <View style={HorizontalHeaderStyles.imageContainer}>
        <Icon group={imageGroup} name={imageName} width={imageWidth} height={imageHeight} />
      </View>
    </View>
  );
};

HorizontalHeader.defaultProps = {
  title: 'Details title',
  description: '',
  imageName: 'manRunning',
  imageGroup: 'general',
  imageWidth: 200,
  imageHeight: 200,
};

export default HorizontalHeader;
