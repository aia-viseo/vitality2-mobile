/**
 *
 * HorizontalHeader Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import HorizontalHeader from '../HorizontalHeader';

test('HorizontalHeader', () => {
  const tree = renderer
    .create(
      <HorizontalHeader
        title="title"
        description="description"
        imageName="manRunning"
        imageGroup="general"
        imageWidth={200}
        imageHeight={200}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
