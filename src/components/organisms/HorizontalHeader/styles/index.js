/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {COLOR_AIA_DARK_GREY, COLOR_AIA_GREY_500} from '@colors';
import {projectFontFamily, generalHorizontalPadding} from '@core/config';

const HorizontalHeaderStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: generalHorizontalPadding,
    paddingTop: 10,
  },
  textContainer: {
    width: '50%',
  },
  imageContainer: {
    height: 156,
    left: -25,
    top: -35,
    width: '49%',
  },
  title: {
    color: COLOR_AIA_DARK_GREY,
    fontFamily: projectFontFamily,
    fontSize: RFValue(22),
    fontWeight: '700',
  },
  description: {
    color: COLOR_AIA_GREY_500,
    fontFamily: projectFontFamily,
    fontSize: RFValue(14),
    marginTop: 5,
  },
});

export default HorizontalHeaderStyles;
