/**
 *
 * @format
 *
 */

// @flow
import type {ViewStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  hasProgressBar?: boolean,
  progress?: number,
  rightLabel?: string,
  customLinearLocations?: Array<number>,
  customLinearColors?: Array<string>,
  barHeight?: number,
  rightLabelStyle?: ViewStyleProp,
  onPress?: () => void,
};
