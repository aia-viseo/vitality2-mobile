/**
 *
 * HeaderFlow
 * @format
 *
 */

// @flow

import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import VIcon from 'react-native-vector-icons/MaterialIcons';

import ProgressBar from '@molecules/ProgressBar';

import {LinearLocations, greenLinearColors} from '@core/config';
import {COLOR_AIA_VITALITY_BLUE} from '@colors';
import HeaderFlowStyles from './styles';
import type {PropsType} from './types';

const HeaderFlow = (props: PropsType) => {
  const {hasProgressBar, progress, rightLabel, onPress} = props;

  return (
    <View style={HeaderFlowStyles.container}>
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={HeaderFlowStyles.closeButton}>
          <VIcon size={24} name="close" color={COLOR_AIA_VITALITY_BLUE} />
        </View>
      </TouchableWithoutFeedback>
      {hasProgressBar && (
        <View style={HeaderFlowStyles.progressBarContainer}>
          <ProgressBar
            progress={progress}
            rightLabel={rightLabel}
            customLinearLocations={LinearLocations}
            customLinearColors={greenLinearColors}
            barHeight={15}
            rightLabelStyle={HeaderFlowStyles.pointsText}
          />
        </View>
      )}
    </View>
  );
};

HeaderFlow.defaultProps = {
  hasProgressBar: true,
  progress: 50,
  rightLabel: '50% completed',
  customLinearLocations: LinearLocations,
  customLinearColors: greenLinearColors,
  barHeight: 15,
  rightLabelStyle: '',
};

export default HeaderFlow;
