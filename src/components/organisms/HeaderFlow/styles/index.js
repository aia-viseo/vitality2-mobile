/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import {generalHorizontalPadding} from '@core/config';

import {BLACK, WHITE, COLOR_AIA_DARK_GREY} from '@colors';

const HeaderFlowStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
    paddingHorizontal: generalHorizontalPadding,
  },
  pointsText: {
    color: COLOR_AIA_DARK_GREY,
    fontSize: RFValue(12),
  },
  closeButton: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 40,
    backgroundColor: WHITE,
    width: 40,
    elevation: 5,
    shadowColor: BLACK,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  },
  progressBarContainer: {
    height: 50,
    width: '60%',
    justifyContent: 'center',
  },
});

export default HeaderFlowStyles;
