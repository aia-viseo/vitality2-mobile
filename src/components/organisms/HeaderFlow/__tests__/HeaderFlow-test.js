/**
 *
 * HeaderFlow Test
 * @format
 *
 */

// @flow

import React from 'react';
import renderer from 'react-test-renderer';

import HeaderFlow from '../HeaderFlow';

test('HeaderFlow', () => {
  const tree = renderer
    .create(<HeaderFlow hasProgressBar progress={50} rightLabel="50% Completed" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
