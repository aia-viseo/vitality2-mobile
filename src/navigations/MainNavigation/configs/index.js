/**
 *
 * @format
 *
 */

// @flow

import ModalNavigation from '@molecules/ModalNavigation';
import BottomNavigation from '@navigations/BottomNavigation';
import Login from '@pages/Login';
import BiometricScreen from '@pages/BiometricScreen';

const SCREENS: {}[] = [
  {
    name: 'Login',
    screen: Login,
  },
  {
    name: 'Application',
    screen: BottomNavigation,
  },
  {
    name: 'Modal',
    screen: ModalNavigation,
  },
  {
    name: 'BiometricScreen',
    screen: BiometricScreen,
  },
];

export {SCREENS as default};
