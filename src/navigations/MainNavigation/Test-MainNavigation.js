/**
 *
 * Test Case MainNavigation
 * @format
 *
 */

// @flow

import * as React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import type {PropsType} from './types';

const Stack = createStackNavigator();

const MainNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName} = props;

  const TestScreen = () => {
    return (
      <View>
        <Text>Test</Text>
      </View>
    );
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: headerShown || false,
      }}
      initialRouteName={initialRouteName}>
      <Stack.Screen name="Test" component={TestScreen} />
    </Stack.Navigator>
  );
};

export default MainNavigation;
