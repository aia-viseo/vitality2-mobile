## MainNavigation
 Stack Navigator contain Screen

# Usage
```js
import MainNavigation from 'molecules/MainNavigation';

<MainNavigation
  initialRouteName
  headerShown
/>
```

# Example
```js
import MainNavigation from 'molecules/MainNavigation';

<MainNavigation
  initialRouteName={'Home'}
  headerShown={false}
/>
```

Key              | Type     | Required | Description                                                     |
---------------- | -------- | -------  | --------------------------------------------------------------- |
initialRouteName | `string` | `False`  | The name of the route to render on first load of the navigator. |
headerShown      | `bool`   | `False`  | Control header of stack navigation display.                     |

# Steps to add screen

In config/index.js
```js
import Dashboard from 'pages/Dashboard';

const SCREENS = [
  {
    name: 'Dashboard',
    screen: Dashboard
  }
  // Add here
];
```

Key      | Type             | Required | Description                                                                         |
-------- | ---------------- | -------  | ----------------------------------------------------------------------------------- |
name     | `string`         | `True`   | Route name.                                                                         |
screen   | `React Element`  | `True`   | Component to show navigate.                                                         |
