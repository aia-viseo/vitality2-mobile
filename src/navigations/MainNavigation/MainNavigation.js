/**
 *
 * MainNavigation
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import lodash from 'lodash';
import {createStackNavigator} from '@react-navigation/stack';

import Snackbar from '@molecules/Snackbar';
import {GlobalContext} from '@contexts/Global';

import SCREENS from './configs';

import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();

const MainNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName} = props;
  const {snackbarProps} = useContext(GlobalContext);
  const {
    style,
    titleStyle,
    wrapperStyle,
    title,
    actionLabel,
    duration,
    visible,
    dismissCallback,
    onPressAction,
  } = snackbarProps;

  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerShown: headerShown || false,
        }}
        initialRouteName={initialRouteName}>
        {lodash.map(SCREENS, (SCREEN: ScreenType, index: number) => {
          const {name, screen} = SCREEN;
          return <Stack.Screen key={index} name={name} component={screen} />;
        })}
      </Stack.Navigator>
      <Snackbar
        style={style}
        titleStyle={titleStyle}
        wrapperStyle={wrapperStyle}
        title={title}
        actionLabel={actionLabel}
        duration={duration}
        visible={visible}
        dismissCallback={dismissCallback}
        onPressAction={onPressAction}
      />
    </>
  );
};

export default MainNavigation;
