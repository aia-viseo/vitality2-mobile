/**
 *
 *  Test Stack Navigation
 *  @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import MainNavigation from '../Test-MainNavigation';

jest.useFakeTimers();
test('Test Stack Navigation ', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <MainNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
