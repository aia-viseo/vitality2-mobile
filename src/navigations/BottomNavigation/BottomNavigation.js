/**
 *
 * BottomNavigation
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ApplicationProvider} from '@contexts/Application';
import {GlobalContext} from '@contexts/Global';
import {OnlineAssessmentsProvider} from '@contexts/OnlineAssessments';
import {COLOR_AIA_GREEN, COLOR_AIA_GREY_500} from '@colors';

import BottomNavigationStyles from './styles';
import type {PropsType, ScreenType} from './types';

const Tab = createBottomTabNavigator();

const BottomNavigation = (props: PropsType) => {
  const {initialRouteName, activeColor, inactiveColor} = props;
  const {bottomNavigationList} = useContext(GlobalContext);

  return (
    <ApplicationProvider>
      <OnlineAssessmentsProvider>
        <Tab.Navigator
          initialRouteName={initialRouteName}
          activeColor={activeColor || COLOR_AIA_GREEN}
          inactiveColor={inactiveColor || COLOR_AIA_GREY_500}
          barStyle={BottomNavigationStyles.barStyle}>
          {bottomNavigationList.map((navigation: ScreenType) => {
            const {name, screen, options} = navigation;
            return <Tab.Screen key={name} name={name} component={screen} options={options} />;
          })}
        </Tab.Navigator>
      </OnlineAssessmentsProvider>
    </ApplicationProvider>
  );
};

export default BottomNavigation;
