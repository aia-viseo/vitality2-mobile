/**
 *
 * @format
 *
 */

// @flow

import type {Element} from 'react';

type OptionsType = {
  tabBarLabel: string,
  tabBarIcon: (result: {focused: boolean}) => Element<*>,
};

export type PropsType = {
  activeColor?: string,
  inactiveColor?: string,
  initialRouteName?: string,
};

export type ScreenType = {
  name: string,
  screen: Node,
  options: OptionsType,
};
