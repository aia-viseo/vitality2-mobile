/**
 *
 * @format
 *
 */

// @flow

import * as React from 'react';
import {View} from 'react-native';

import AssessmentsNavigation from '@navigations/AssessmentsNavigation';
import ChallengesNavigation from '@navigations/ChallengesNavigation';
import DashboardNavigation from '@navigations/DashboardNavigation';
import ProfileNavigation from '@navigations/ProfileNavigation';
import RewardsNavigation from '@navigations/RewardsNavigation';
import Icon from '@atoms/Icon';

export const BOTTOM_NAVIGATIONS = {
  dashbd: {
    name: 'DashboardTab',
    screen: DashboardNavigation,
    options: {
      tabBarLabel: 'Dashboard',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'dashboardActive' : 'dashboard'} />
          </View>
        );
      },
    },
  },
  assess: {
    name: 'AssessmentsTab',
    screen: AssessmentsNavigation,
    options: {
      tabBarLabel: 'Assessments',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'assessmentActive' : 'assessment'} />
          </View>
        );
      },
    },
  },
  chalenge: {
    name: 'ChallengesTab',
    screen: ChallengesNavigation,
    options: {
      tabBarLabel: 'Challenges',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'challengeActive' : 'challenge'} />
          </View>
        );
      },
    },
  },
  rewards: {
    name: 'RewardsTab',
    screen: RewardsNavigation,
    options: {
      tabBarLabel: 'Rewards',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'rewardActive' : 'reward'} />
          </View>
        );
      },
    },
  },
  profile: {
    name: 'ProfileTab',
    screen: ProfileNavigation,
    options: {
      tabBarLabel: 'Profile',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'profileActive' : 'profile'} />
          </View>
        );
      },
    },
  },
};

export const SCREENS = [
  {
    name: 'DashboardTab',
    screen: DashboardNavigation,
    options: {
      tabBarLabel: 'Dashboard',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'dashboardActive' : 'dashboard'} />
          </View>
        );
      },
    },
  },
  {
    name: 'AssessmentsTab',
    screen: AssessmentsNavigation,
    options: {
      tabBarLabel: 'Assessments',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'assessmentActive' : 'assessment'} />
          </View>
        );
      },
    },
  },
  {
    name: 'ChallengesTab',
    screen: ChallengesNavigation,
    options: {
      tabBarLabel: 'Challenges',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'challengeActive' : 'challenge'} />
          </View>
        );
      },
    },
  },
  {
    name: 'RewardsTab',
    screen: RewardsNavigation,
    options: {
      tabBarLabel: 'Rewards',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'rewardActive' : 'reward'} />
          </View>
        );
      },
    },
  },
  {
    name: 'ProfileTab',
    screen: ProfileNavigation,
    options: {
      tabBarLabel: 'Profile',
      tabBarIcon: (result: {focused: boolean}) => {
        const {focused} = result;
        return (
          <View>
            <Icon group="bottomNavigation" name={focused ? 'profileActive' : 'profile'} />
          </View>
        );
      },
    },
  },
];
