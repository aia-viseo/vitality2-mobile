## BottomNavigation
Bottom Tab Navigator contain button and icon

# Usage
```js
import BottomNavigation from '/molecules/BottomNavigation';

<BottomNavigation
  initialRouteName
  activeColor
  inactiveColor
/>
```

# Example
```js
import BottomNavigation from 'molecules/BottomNavigation';

<BottomNavigation
  initialRouteName={'Home'}
  activeColor={'#fff'}
  inactiveColor={'#ddd'}
/>
```

Key              | Type     | Required | Description                                                     |
---------------- | -------- | -------  | --------------------------------------------------------------- |
initialRouteName | `string` | `False`  | The name of the route to render on first load of the navigator. |
activeColor      | `color`  | `False`  | Custom color for icon and label in the active tab.              |
inactiveColor    | `color`  | `False`  | Custom color for icon and label in the inactive tab.            |

# Steps to add screen

In config/index.js
```js
import Dashboard from 'pages/Dashboard';

const SCREENS = [
  {
    name: 'Dashboard',
    screen: Dashboard,
    options: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ color }) => (
        <View>
          <Icon name={'md-home'} size={25} style={[{ color }]} />
        </View>
      )
    }
  }
  // Add here
];
```

Key      | Type             | Required | Description                                                                         |
-------- | ---------------- | -------  | ----------------------------------------------------------------------------------- |
name     | `string`         | `True`   | Route name.                                                                         |
screen   | `React Element`  | `True`   | Component to show navigate.                                                         |
options  | `Object`         | `False`  | The options prop can be used to configure individual screens inside the navigator.  |
