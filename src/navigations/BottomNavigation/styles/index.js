/**
 *
 * @format
 *
 */

// @flow

import {StyleSheet} from 'react-native';

const BottomNavigationStyles = StyleSheet.create({
  barStyle: {
    backgroundColor: '#fff',
  },
});

export default BottomNavigationStyles;
