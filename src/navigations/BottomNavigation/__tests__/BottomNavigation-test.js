/**
 *
 *  Test Bottom Navigation
 *  @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import BottomNavigation from '../Test-BottomNavigation';

jest.useFakeTimers();
test('Test Bottom Navigation ', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <BottomNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
