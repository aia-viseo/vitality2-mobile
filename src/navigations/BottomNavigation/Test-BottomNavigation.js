/**
 *
 * Test Case Bottom Navigation
 * @format
 *
 */

// @flow

import React from 'react';
import {View, Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {COLOR_AIA_GREEN, COLOR_AIA_GREY_500} from '@colors';

import type {PropsType} from './types';
import BottomNavigationStyles from './styles';

const Tab = createBottomTabNavigator();

const TestScreen = () => {
  return (
    <View>
      <Text>Test</Text>
    </View>
  );
};

const BottomNavigation = (props: PropsType) => {
  const {initialRouteName, activeColor, inactiveColor} = props;

  return (
    <Tab.Navigator
      initialRouteName={initialRouteName}
      activeColor={activeColor || COLOR_AIA_GREEN}
      inactiveColor={inactiveColor || COLOR_AIA_GREY_500}
      barStyle={BottomNavigationStyles.barStyle}>
      <Tab.Screen
        name="Test"
        component={TestScreen}
        options={{
          tabBarLabel: 'Test',
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNavigation;
