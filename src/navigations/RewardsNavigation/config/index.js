/**
 *
 * @format
 *
 */

// @flow

import Rewards from '../../../pages/Rewards';
import RewardDetail from '../../../pages/RewardDetail';

const SCREENS: {}[] = [
  {
    name: 'Rewards',
    screen: Rewards,
  },
  {
    name: 'RewardDetail',
    screen: RewardDetail,
  },
];

export {SCREENS as default};
