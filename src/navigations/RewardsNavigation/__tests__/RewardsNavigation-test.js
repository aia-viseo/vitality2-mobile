/**
 *
 * Rewards Navigation Test
 * @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import RewardsNavigation from '../RewardsNavigation';

test('ChallengesNavigation', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <RewardsNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
