/**
 *
 * @format
 *
 */

// @flow

export type PropsType = {
  initialRouteName?: string,
  headerShown?: boolean,
};

export type ScreenType = {
  name: string,
  screen: Node,
};
