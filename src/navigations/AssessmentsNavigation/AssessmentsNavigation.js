/**
 *
 * AssessmentsNavigation
 * @format
 *
 */

// @flow

import React from 'react';
import lodash from 'lodash';
import {createStackNavigator} from '@react-navigation/stack';

import {AssessmentsProvider} from '@contexts/Assessments';

import SCREENS from './config';
import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();

const AssessmentsNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName} = props;

  return (
    <AssessmentsProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: headerShown || false,
        }}
        initialRouteName={initialRouteName}>
        {lodash.map(SCREENS, (SCREEN: ScreenType, index: number) => {
          const {name, screen} = SCREEN;
          return <Stack.Screen key={index} name={name} component={screen} />;
        })}
      </Stack.Navigator>
    </AssessmentsProvider>
  );
};

export default AssessmentsNavigation;
