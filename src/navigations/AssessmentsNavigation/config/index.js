/**
 *
 * @format
 *
 */

// @flow
import Assessments from '../../../pages/Assessments';
import AssessmentDetail from '../../../pages/AssessmentDetail';
import {
  AssessmentFlow,
  AssessmentReview,
  AssessmentEdit,
  AssessmentDone,
} from '../../../pages/AssessmentFlow';

const SCREENS: {}[] = [
  {
    name: 'Assessments',
    screen: Assessments,
  },
  {
    name: 'AssessmentDetail',
    screen: AssessmentDetail,
  },
  {
    name: 'AssessmentFlow',
    screen: AssessmentFlow,
  },
  {
    name: 'AssessmentReview',
    screen: AssessmentReview,
  },
  {
    name: 'AssessmentEdit',
    screen: AssessmentEdit,
  },
  {
    name: 'AssessmentDone',
    screen: AssessmentDone,
  },
];

export {SCREENS as default};
