/**
 *
 * AssessmentNavigation Test
 * @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import AssessmentsNavigation from '../Test-WorkoutTrackerNavigation';

jest.useFakeTimers();
test('Assessments', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <AssessmentsNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
