/**
 *
 * WorkoutTrackerNavigation
 * @format
 *
 */

// @flow

import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {WorkoutTrackerProvider} from '@contexts/WorkoutTracker';

import SCREENS from './config';

import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();
const WorkoutTrackerNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName, navigation} = props;

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  return (
    <WorkoutTrackerProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: headerShown || false,
        }}
        initialRouteName={initialRouteName}>
        {SCREENS.map((SCREEN: ScreenType, index: number) => {
          const {name, screen} = SCREEN;
          const key = `${index}`;
          return <Stack.Screen key={key} name={name} component={screen} />;
        })}
      </Stack.Navigator>
    </WorkoutTrackerProvider>
  );
};

export default WorkoutTrackerNavigation;
