/**
 *
 * @format
 *
 */

// @flow

import WorkoutTrackerSummary from '@pages/WorkoutTrackerSummary';
import WorkoutTrackerDetail from '@pages/WorkoutTrackerDetail';

const SCREENS = [
  {
    name: 'WorkoutTrackerSummary',
    screen: WorkoutTrackerSummary,
  },
  {
    name: 'WorkoutTrackerDetail',
    screen: WorkoutTrackerDetail,
  },
];

export {SCREENS as default};
