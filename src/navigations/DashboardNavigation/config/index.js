/**
 *
 * @format
 *
 */

// @flow

import Dashboard from '@pages/Dashboard';
import ECard from '@pages/Ecard';
import EarnPointNavigation from '@navigations/EarnPointNavigation';
import WorkoutTrackerNavigation from '@navigations/WorkoutTrackerNavigation';
import HelpNavigation from '@navigations/HelpNavigation';
import HealthReportNavigation from '@navigations/HealthReportNavigation';
import NotificationNavigation from '@navigations/NotificationNavigation';

const SCREENS: {}[] = [
  {
    name: 'Dashboard',
    screen: Dashboard,
  },
  {
    name: 'EarnPoints',
    screen: EarnPointNavigation,
  },
  {
    name: 'WorkoutTracker',
    screen: WorkoutTrackerNavigation,
  },
  {
    name: 'ECard',
    screen: ECard,
  },
  {
    name: 'Help',
    screen: HelpNavigation,
  },
  {
    name: 'HealthReport',
    screen: HealthReportNavigation,
  },
  {
    name: 'Notification',
    screen: NotificationNavigation,
  },
];

export {SCREENS as default};
