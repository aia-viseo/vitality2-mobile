/**
 *
 *  DashboardNavigation Test
 *  @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import DashboardNavigation from '../Test-DashboardNavigation';

jest.useFakeTimers();
test('Test Stack Navigation ', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <DashboardNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
