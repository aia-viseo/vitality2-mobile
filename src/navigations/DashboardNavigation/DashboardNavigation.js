/**
 *
 * DashboardNavigation
 * @format
 *
 */

// @flow

import React from 'react';
import lodash from 'lodash';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-native-paper';

import SCREENS from './config';

import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();

const DashboardNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName} = props;

  return (
    <Provider>
      <Stack.Navigator
        screenOptions={{
          headerShown: headerShown || false,
        }}
        initialRouteName={initialRouteName}>
        {lodash.map(SCREENS, (SCREEN: ScreenType, index: number) => {
          const {name, screen} = SCREEN;
          return <Stack.Screen key={index} name={name} component={screen} />;
        })}
      </Stack.Navigator>
    </Provider>
  );
};

export default DashboardNavigation;
