/**
 *
 * AssessmentNavigation Test
 * @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import ChallengesNavigation from '../ChallengesNavigation';

test('ChallengesNavigation', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <ChallengesNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
