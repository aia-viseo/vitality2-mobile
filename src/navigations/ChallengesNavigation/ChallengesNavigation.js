/**
 *
 * ChallengesNavigation
 * @format
 *
 */

// @flow

import React from 'react';
import lodash from 'lodash';
import {createStackNavigator} from '@react-navigation/stack';

import {ChallengesProvider} from '@contexts/Challenges';
import SCREENS from './config';

import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();

const ChallengesNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName} = props;
  return (
    <ChallengesProvider>
      <Stack.Navigator
        screenOptions={{
          headerShown: headerShown || false,
        }}
        initialRouteName={initialRouteName}>
        {lodash.map(SCREENS, (SCREEN: ScreenType, index: number) => {
          const {name, screen} = SCREEN;
          return <Stack.Screen key={index} name={name} component={screen} />;
        })}
      </Stack.Navigator>
    </ChallengesProvider>
  );
};

export default ChallengesNavigation;
