/**
 *
 * @format
 *
 */

// @flow

import Challenges from '@pages/Challenges';
import ChallengeDetail from '@pages/ChallengeDetail';
import ChallengeWeeklyProgress from '@pages/ChallengeWeeklyProgress';
import EarnPointDetail from '@pages/EarnPointDetail';
import ChallengeComplete from '@pages/ChallengeComplete';

const SCREENS: {}[] = [
  {
    name: 'Challenges',
    screen: Challenges,
  },
  {
    name: 'ChallengeDetail',
    screen: ChallengeDetail,
  },
  {
    name: 'ChallengeWeeklyProgress',
    screen: ChallengeWeeklyProgress,
  },
  {
    name: 'EarnPointDetail',
    screen: EarnPointDetail,
  },
  {
    name: 'ChallengeComplete',
    screen: ChallengeComplete,
  },
];

export {SCREENS as default};
