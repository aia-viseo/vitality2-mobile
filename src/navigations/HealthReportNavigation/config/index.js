/**
 *
 * @format
 *
 */

// @flow

import HealthReport from '@pages/HealthReport';

const SCREENS = [
  {
    name: 'HealthReport',
    screen: HealthReport,
  },
];

export {SCREENS as default};
