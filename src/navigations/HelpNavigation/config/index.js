/**
 *
 * @format
 *
 */

// @flow

import Help from '@pages/Help';

const SCREENS = [
  {
    name: 'Help',
    screen: Help,
  },
];

export {SCREENS as default};
