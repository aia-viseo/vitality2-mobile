/**
 *
 * @format
 *
 */

// @flow

import EarnPoints from '@pages/EarnPoints';

const SCREENS = [
  {
    name: 'EarnPoints',
    screen: EarnPoints,
  },
];

export {SCREENS as default};
