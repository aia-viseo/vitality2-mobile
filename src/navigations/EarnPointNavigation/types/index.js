/**
 *
 * @format
 *
 */

// @flow

type NavigationType = {
  navigate: (path: string) => void,
  goBack: (path?: string) => void,
  dangerouslyGetParent: () => {setOptions: ({tabBarVisible?: boolean}) => void},
  setOptions: ({tabBarVisible?: boolean}) => void,
};

export type PropsType = {
  initialRouteName?: string,
  headerShown?: boolean,
  navigation: {
    navigate: (path: string) => void,
    goBack: (path?: string) => void,
    dangerouslyGetParent: () => NavigationType,
  },
};

export type ScreenType = {
  name: string,
  screen: Node,
};
