/**
 *
 * Test Case Profile Navigation
 * @format
 *
 */

// @flow

import React from 'react';
import {View, Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const TestScreen = () => {
  return (
    <View>
      <Text>Test</Text>
    </View>
  );
};

const ProfileNavigation = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Test"
        component={TestScreen}
        options={{
          tabBarLabel: 'Test',
        }}
      />
    </Tab.Navigator>
  );
};

export default ProfileNavigation;
