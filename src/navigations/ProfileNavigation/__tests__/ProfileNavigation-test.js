/**
 *
 * ProfileNavigation Test
 * @format
 *
 */

// @flow

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import renderer from 'react-test-renderer';

import ProfileNavigation from '../Test-ProfileNavigation';

jest.useFakeTimers();
test('Assessments', () => {
  const tree = renderer
    .create(
      <NavigationContainer>
        <ProfileNavigation />
      </NavigationContainer>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
