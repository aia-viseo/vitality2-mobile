/**
 *
 * @format
 *
 */

// @flow

import Profile from '@pages/Profile';
import PointsHistory from '@pages/PointsHistory';

const SCREENS: {}[] = [
  {
    name: 'Profile',
    screen: Profile,
  },
  {
    name: 'PointsHistory',
    screen: PointsHistory,
  },
];

export {SCREENS as default};
