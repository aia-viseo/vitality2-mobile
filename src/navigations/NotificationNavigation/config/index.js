/**
 *
 * @format
 *
 */

// @flow

import Notification from '@pages/Notification';

const SCREENS = [
  {
    name: 'Notification',
    screen: Notification,
  },
];

export {SCREENS as default};
