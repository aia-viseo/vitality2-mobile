/**
 *
 * NotificationNavigation
 * @format
 *
 */

// @flow

import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SCREENS from './config';

import type {PropsType, ScreenType} from './types';

const Stack = createStackNavigator();

const NotificationNavigation = (props: PropsType) => {
  const {headerShown, initialRouteName, navigation} = props;

  useEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
    return () =>
      parent.setOptions({
        tabBarVisible: true,
      });
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: headerShown || false,
      }}
      initialRouteName={initialRouteName}>
      {SCREENS.map((SCREEN: ScreenType, index: number) => {
        const {name, screen} = SCREEN;
        const key = `${index}`;
        return <Stack.Screen key={key} name={name} component={screen} />;
      })}
    </Stack.Navigator>
  );
};

export default NotificationNavigation;
