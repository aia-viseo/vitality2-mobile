/**
 *
 *  @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import AuthProvider from '../Auth';

test('Test Auth Context', () => {
  const tree = renderer
    .create(
      <AuthProvider>
        <View />
      </AuthProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
