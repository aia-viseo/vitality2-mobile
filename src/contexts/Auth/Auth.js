/**
 *
 * Auth Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';

import BiometricValidator from '@atoms/Biometric/BiometricValidator';

import LoginForm from './configs';
import type {PropsType} from './types';

export const AuthContext = React.createContext<{}>({});

const AuthProvider = (props: PropsType) => {
  const [loginForm, setLoginForm] = useState(LoginForm);
  const [membershipNumber, setMembershipNumber] = useState();
  const [usesBiometric, setUsesBiometric] = useState(false);
  const {children} = props;

  useEffect(() => {
    BiometricValidator.checkBioKeyExist(
      () => {
        setUsesBiometric(true);
      },
      () => {}
    );
  }, []);

  return (
    <AuthContext.Provider
      value={{
        loginForm,
        setLoginForm: (field: string, value: string) => {
          loginForm[field] = value;
          setLoginForm(loginForm);
        },
        membershipNumber,
        setMembershipNumber: (value: string) => {
          setMembershipNumber(value);
        },
        usesBiometric,
        setUsesBiometric: (value: boolean) => {
          setUsesBiometric(value);
        },
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
