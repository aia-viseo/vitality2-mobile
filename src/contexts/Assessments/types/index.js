/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

export type AssessmentQuestionType = {
  id: string,
  title: string,
  description: string,
  answerType: string,
  answers: Array<string>,
  unit: Array<string>,
  errorMessage: Array<string>,
  order: string,
};

export type AssessmentPageType = {
  assessmentQuestion: AssessmentQuestionType,
  page: number,
};

export type AssessmentStateAnswerType = {
  page: number,
  answer: string,
};

export type AssessmentStateType = {
  assessment: string,
  page: number,
  answers: AssessmentStateAnswerType[],
  keyName: string,
  answer: string,
};
