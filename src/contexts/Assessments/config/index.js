/**
 *
 *
 * @format
 *
 */

// @flow

export const BASIC_HEALTH_SCREENING = 'BasicHealthScreening';
export const ADVANCED_HEALTH_SCREENING = 'AdvancedHealthScreening';
export const DENTAL_SCREENING = 'DentalScreening';
export const VACCINATION_SCREENING = 'VaccinationScreening';
export const HOW_HEALTHY_ARE_YOU = 'HowHealthyAreYou';
export const HOW_WELL_ARE_YOU_EATING = 'HowWellAreYouEating';
export const NUTRITION_SCREENING = 'NutritionScreening';
export const NON_SMOKERS_DECLARATION = 'NonSmokersdecalaration';
export const HOW_STRESSED_ARE_YOU = 'HowStressedAreYou';
export const HOW_WELL_ARE_YOU_SLEEPING = 'HowWellAreYouSleeping';
export const HOW_ACTIVE_ARE_YOU = 'HowActiveAreYou';
