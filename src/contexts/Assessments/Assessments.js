/**
 *
 * Assessment Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect, useContext} from 'react';
import {AxiosResponse} from 'axios';

import AEMApiCenter from '@clients/AEMApiCenter';
import {EN, ID} from '@constants/i18n';
import AuthCenter from '@clients/AuthCenter';

import {ApplicationContext} from '@contexts/Application';

import {
  ASSESSMENT_QUESTION,
  ASSESSMENT_REVIEW,
  ASSESSMENT_DONE,
  ASSESSMENT_DETAIL,
  OFFLINE_ASSESSMENT_FIND_MORE,
} from '@core/handler/AEMResponseParser/config';
import type {PropsType, AssessmentPageType, AssessmentStateType} from './types';

import {
  HOW_HEALTHY_ARE_YOU,
  HOW_WELL_ARE_YOU_EATING,
  NON_SMOKERS_DECLARATION,
  HOW_STRESSED_ARE_YOU,
  HOW_WELL_ARE_YOU_SLEEPING,
  HOW_ACTIVE_ARE_YOU,
  BASIC_HEALTH_SCREENING,
} from './config';

export const AssessmentsContext = React.createContext<{
  isHowHealthyAreYouAssessmentLoading: boolean,
  currentAssessment: string,
  setCurrentAssessment: (assessment: string) => void,
  clearAssessmentDetail: (keyName: string) => void,
  assessmentCurrentPage: Object,
  setAssessmentCurrentPage: (currentPage: Object) => void,
  howHealthyAreYouAssessment: Object,
  howHealthyAreYouCurrentPage: AssessmentPageType,
  setHowHealthyAreYouCurrentPage: (currentPage: Object) => void,
  assessmentState: AssessmentStateType[],
  setAssessmentState: (state: AssessmentStateType[]) => void,
  howActiveAreYouAll: Object,
  howActiveAreYou: Object,
  howActiveAreYouQuestions: Object,
  howHealthyAreYouAll: Object,
  howHealthyAreYou: Object,
  howHealthyAreYouQuestions: Object,
  howStressedAreYouAll: Object,
  howStressedAreYou: Object,
  howStressedAreYouQuestions: Object,
  howWellAreYouEatingAll: Object,
  howWellAreYouEating: Object,
  howWellAreYouEatingQuestions: Object,
  howWellDoYouSleepAll: Object,
  howWellDoYouSleep: Object,
  howWellDoYouSleepQuestions: Object,
  nonSmokerDeclarationAll: Object,
  nonSmokerDeclaration: Object,
  nonSmokerDeclarationQuestions: Object,
  basicHealthScreening: Object,
  basicHealthScreeningQuestions: Object,
  assessmentDetail: Object,
  assessmentReview: Object,
  assessmentDone: Object,
  basicHealthScreeningFindMore: Object,
  membershipId: string,
}>({});

const AssessmentsProvider = (props: PropsType) => {
  const [assessmentCurrentPage, setAssessmentCurrentPage] = useState({});
  const [currentAssessment, setCurrentAssessment] = useState('');
  const [howHealthyAreYouAssessment, setHowHealthyAreYouAssessment] = useState({});
  const [howHealthyAreYouCurrentPage, setHowHealthyAreYouCurrentPage] = useState({});
  const [isHowHealthyAreYouAssessmentLoading, setIsHowHealthyAreYouAssessmentLoading] = useState(
    false
  );
  const [howActiveAreYouAll, setHowActiveAreYouAll] = useState({});
  const [howActiveAreYou, setHowActiveAreYou] = useState({});
  const [howActiveAreYouDetail, setHowActiveAreYouDetail] = useState({});
  const [howActiveAreYouReview, setHowActiveAreYouReview] = useState({});
  const [howActiveAreYouDone, setHowActiveAreYouDone] = useState({});
  const [howActiveAreYouQuestions, setHowActiveAreYouQuestions] = useState({});
  const [howHealthyAreYouAll, setHowHealthyAreYouAll] = useState({});
  const [howHealthyAreYou, setHowHealthyAreYou] = useState({});
  const [howHealthyAreYouDetail, setHowHealthyAreYouDetail] = useState({});
  const [howHealthyAreYouReview, setHowHealthyAreYouReview] = useState({});
  const [howHealthyAreYouDone, setHowHealthyAreYouDone] = useState({});
  const [howHealthyAreYouQuestions, setHowHealthyAreYouQuestions] = useState({});
  const [howStressedAreYouAll, setHowStressedAreYouAll] = useState({});
  const [howStressedAreYou, setHowStressedAreYou] = useState({});
  const [howStressedAreYouDetail, setHowStressedAreYouDetail] = useState({});
  const [howStressedAreYouReview, setHowStressedAreYouReview] = useState({});
  const [howStressedAreYouDone, setHowStressedAreYouDone] = useState({});
  const [howStressedAreYouQuestions, setHowStressedAreYouQuestions] = useState({});
  const [howWellAreYouEatingAll, setHowWellAreYouEatingAll] = useState({});
  const [howWellAreYouEating, setHowWellAreYouEating] = useState({});
  const [howWellAreYouEatingDetail, setHowWellAreYouEatingDetail] = useState({});
  const [howWellAreYouEatingReview, setHowWellAreYouEatingReview] = useState({});
  const [howWellAreYouEatingDone, setHowWellAreYouEatingDone] = useState({});
  const [howWellAreYouEatingQuestions, setHowWellAreYouEatingQuestions] = useState({});
  const [howWellDoYouSleepAll, setHowWellDoYouSleepAll] = useState({});
  const [howWellDoYouSleep, setHowWellDoYouSleep] = useState({});
  const [howWellDoYouSleepDetail, setHowWellDoYouSleepDetail] = useState({});
  const [howWellDoYouSleepReview, setHowWellDoYouSleepReview] = useState({});
  const [howWellDoYouSleepDone, setHowWellDoYouSleepDone] = useState({});
  const [howWellDoYouSleepQuestions, setHowWellDoYouSleepQuestions] = useState({});
  const [nonSmokerDeclarationAll, setNonSmokerDeclarationAll] = useState({});
  const [nonSmokerDeclaration, setNonSmokerDeclaration] = useState({});
  const [nonSmokerDeclarationDetail, setNonSmokerDeclarationDetail] = useState({});
  const [nonSmokerDeclarationReview, setNonSmokerDeclarationReview] = useState({});
  const [nonSmokerDeclarationDone, setNonSmokerDeclarationDone] = useState({});
  const [nonSmokerDeclarationQuestions, setNonSmokerDeclarationQuestions] = useState({});

  const [basicHealthScreening, setBasicHealthScreening] = useState({});
  const [basicHealthScreeningDetail, setBasicHealthScreeningDetail] = useState({});
  const [basicHealthScreeningReview, setBasicHealthScreeningReview] = useState({});
  const [basicHealthScreeningDone, setBasicHealthScreeningDone] = useState({});
  const [basicHealthScreeningQuestions, setBasicHealthScreeningQuestions] = useState({});
  const [basicHealthScreeningFindMore, setBasicHealthScreeningFindMore] = useState({});

  const [assessmentDetail, setAssessmentDetail] = useState({});
  const [assessmentReview, setAssessmentReview] = useState({});
  const [assessmentDone, setAssessmentDone] = useState({});
  const [assessmentState, setAssessmentState] = useState([]);
  const [membershipId, setMembershipId] = useState('');
  const {children} = props;

  const {userLanguage, selectedAssessment} = useContext(ApplicationContext);

  const getMembershipId = () => {
    AuthCenter.getMembershipNumber((membershipNumberResult) => {
      if (membershipNumberResult === 'V000200059') {
        setMembershipId(membershipNumberResult);
      }
    });
  };

  const getHowActiveAreYou = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getHowActiveAreYouAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getHowActiveAreYouAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setHowActiveAreYouAll({en: enLabel || {}, id: idLabel || {}});
      setHowActiveAreYou(translatedData || {});
      setHowActiveAreYouQuestions(translatedData[ASSESSMENT_QUESTION] || {});
      setHowActiveAreYouDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setHowActiveAreYouReview(translatedData[ASSESSMENT_REVIEW] || {});
      setHowActiveAreYouDone(translatedData[ASSESSMENT_DONE] || {});
    });
  };

  /* This will be removed once the offline assessment payload will be in place */
  const offlineAssessmentStub = (data: Object): Object => {
    const stub = {
      'how-healthy-are-you-q-1': {
        id: 'how-healthy-are-you-q-1',
        title: 'Show us your health check results',
        answerType: 'Radios',
        identifiers: ['UploadSelection'],
        identifiersValue: ['CameraPhoto'],
        answers: ['Take a photo'],
        imageName: 'manRunning',
        imageGroup: 'general',
        order: '1',
      },
      'how-healthy-are-you-q-2': {
        id: 'how-healthy-are-you-q-2',
        title: 'Put your report in the frame',
        answerType: 'CameraPreview',
        identifiers: ['CameraView'],
        identifiersValue: ['CameraPreviewPerception'],
        answers: ['Use 1 photo for each page'],
        imageName: 'information',
        imageGroup: 'general',
        order: '2',
      },
    };
    const finalData = data;
    finalData['Assessment Question'] = {};
    finalData['Assessment Question'] = stub;
    return finalData;
  };

  const getHowHealthyAreYou = () => {
    getMembershipId();
    setIsHowHealthyAreYouAssessmentLoading(true);
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getHowHealthyAreYouAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getHowHealthyAreYouAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      let translatedData = userLanguage === EN ? enLabel : idLabel;
      /* This will be refactored once the offline assessment payload will be in place */
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        if (membershipNumberResult === 'V000200059') {
          translatedData = offlineAssessmentStub(translatedData);
        }
      });
      setHowHealthyAreYouAll({en: enLabel || {}, id: idLabel || {}});
      setHowHealthyAreYou(translatedData || {});
      setHowHealthyAreYouAssessment(translatedData || {});
      setHowHealthyAreYouQuestions(translatedData[ASSESSMENT_QUESTION] || {});
      setHowHealthyAreYouDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setHowHealthyAreYouReview(translatedData[ASSESSMENT_REVIEW] || {});
      setHowHealthyAreYouDone(translatedData[ASSESSMENT_DONE] || {});
      setIsHowHealthyAreYouAssessmentLoading(false);
    });
  };

  const getHowStressedAreYou = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getHowStressedAreYouAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getHowStressedAreYouAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setHowStressedAreYouAll({en: enLabel || {}, id: idLabel || {}});
      setHowStressedAreYou(translatedData || {});
      setHowStressedAreYouDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setHowStressedAreYouReview(translatedData[ASSESSMENT_REVIEW] || {});
      setHowStressedAreYouDone(translatedData[ASSESSMENT_DONE] || {});
      setHowStressedAreYouQuestions(translatedData[ASSESSMENT_QUESTION] || {});
    });
  };

  const getHowWellAreYouEating = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getEatingWellAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getEatingWellAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setHowWellAreYouEatingAll({en: enLabel || {}, id: idLabel || {}});
      setHowWellAreYouEating(translatedData || {});
      setHowWellAreYouEatingDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setHowWellAreYouEatingReview(translatedData[ASSESSMENT_REVIEW] || {});
      setHowWellAreYouEatingDone(translatedData[ASSESSMENT_DONE] || {});
      setHowWellAreYouEatingQuestions(translatedData[ASSESSMENT_QUESTION] || {});
    });
  };

  const getHowWellDoYouSleep = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getHowWellDoYouSleepAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getHowWellDoYouSleepAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setHowWellDoYouSleepAll({en: enLabel || {}, id: idLabel || {}});
      setHowWellDoYouSleep(translatedData || {});
      setHowWellDoYouSleepDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setHowWellDoYouSleepReview(translatedData[ASSESSMENT_REVIEW] || {});
      setHowWellDoYouSleepDone(translatedData[ASSESSMENT_DONE] || {});
      setHowWellDoYouSleepQuestions(translatedData[ASSESSMENT_QUESTION] || {});
    });
  };

  const getNonSmokerDeclaration = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getNonSmokerDeclarationAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getNonSmokerDeclarationAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setNonSmokerDeclarationAll({en: enLabel || {}, id: idLabel || {}});
      setNonSmokerDeclaration(translatedData || {});
      setNonSmokerDeclarationDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setNonSmokerDeclarationReview(translatedData[ASSESSMENT_REVIEW] || {});
      setNonSmokerDeclarationDone(translatedData[ASSESSMENT_DONE] || {});
      setNonSmokerDeclarationQuestions(translatedData[ASSESSMENT_QUESTION] || {});
    });
  };

  const getBasicHealthScreening = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getBasicHealthScreeningAssessment(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getBasicHealthScreeningAssessment(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setBasicHealthScreening(translatedData || {});
      setBasicHealthScreeningDetail(translatedData[ASSESSMENT_DETAIL] || {});
      setBasicHealthScreeningReview(translatedData[ASSESSMENT_REVIEW] || {});
      setBasicHealthScreeningDone(translatedData[ASSESSMENT_DONE] || {});
      setBasicHealthScreeningQuestions(translatedData[ASSESSMENT_QUESTION] || {});
      setBasicHealthScreeningFindMore(translatedData[OFFLINE_ASSESSMENT_FIND_MORE] || {});
    });
  };

  const setAssessmentDetailTranslation = () => {
    const {keyName} = selectedAssessment;
    let assessmentDetailTranslation = {};
    switch (keyName) {
      case HOW_HEALTHY_ARE_YOU:
        assessmentDetailTranslation = howHealthyAreYouDetail;
        break;
      case HOW_WELL_ARE_YOU_EATING:
        assessmentDetailTranslation = howWellAreYouEatingDetail;
        break;
      case NON_SMOKERS_DECLARATION:
        assessmentDetailTranslation = nonSmokerDeclarationDetail;
        break;
      case HOW_STRESSED_ARE_YOU:
        assessmentDetailTranslation = howStressedAreYouDetail;
        break;
      case HOW_WELL_ARE_YOU_SLEEPING:
        assessmentDetailTranslation = howWellDoYouSleepDetail;
        break;
      case HOW_ACTIVE_ARE_YOU:
        assessmentDetailTranslation = howActiveAreYouDetail;
        break;
      case BASIC_HEALTH_SCREENING:
        assessmentDetailTranslation = basicHealthScreeningDetail;
        break;
      default:
        break;
    }
    setAssessmentDetail(assessmentDetailTranslation);
  };

  const setAssessmentReviewAndDone = (keyName: string) => {
    let assessmentReviewTranslation = {};
    let assessmentDoneTranslation = {};
    switch (keyName) {
      case HOW_HEALTHY_ARE_YOU:
        assessmentReviewTranslation = howHealthyAreYouReview;
        assessmentDoneTranslation = howHealthyAreYouDone;
        break;
      case HOW_WELL_ARE_YOU_EATING:
        assessmentReviewTranslation = howWellAreYouEatingReview;
        assessmentDoneTranslation = howWellAreYouEatingDone;
        break;
      case NON_SMOKERS_DECLARATION:
        assessmentReviewTranslation = nonSmokerDeclarationReview;
        assessmentDoneTranslation = nonSmokerDeclarationDone;
        break;
      case HOW_STRESSED_ARE_YOU:
        assessmentReviewTranslation = howStressedAreYouReview;
        assessmentDoneTranslation = howStressedAreYouDone;
        break;
      case HOW_WELL_ARE_YOU_SLEEPING:
        assessmentReviewTranslation = howWellDoYouSleepReview;
        assessmentDoneTranslation = howWellDoYouSleepDone;
        break;
      case HOW_ACTIVE_ARE_YOU:
        assessmentReviewTranslation = howActiveAreYouReview;
        assessmentDoneTranslation = howActiveAreYouDone;
        break;
      case BASIC_HEALTH_SCREENING:
        assessmentReviewTranslation = basicHealthScreeningReview;
        assessmentDoneTranslation = basicHealthScreeningDone;
        break;
      default:
        break;
    }
    setAssessmentReview(assessmentReviewTranslation);
    setAssessmentDone(assessmentDoneTranslation);
  };

  const initData = () => {
    getHowActiveAreYou();
    getHowHealthyAreYou();
    getHowStressedAreYou();
    getHowWellAreYouEating();
    getHowWellDoYouSleep();
    getNonSmokerDeclaration();
    getBasicHealthScreening();
  };

  useEffect(() => {
    initData();
  }, [userLanguage]);

  useEffect(() => {
    setAssessmentDetail({});
    if (
      howActiveAreYouDetail === {} ||
      howHealthyAreYouDetail === {} ||
      howStressedAreYouDetail === {} ||
      howWellAreYouEatingDetail === {} ||
      howWellDoYouSleepDetail === {} ||
      nonSmokerDeclarationDetail === {}
    ) {
      initData();
    } else {
      setAssessmentDetailTranslation();
    }
  }, [selectedAssessment]);

  useEffect(() => {
    setAssessmentDetailTranslation();
  }, [
    howActiveAreYouDetail,
    howHealthyAreYouDetail,
    howStressedAreYouDetail,
    howWellAreYouEatingDetail,
    howWellDoYouSleepDetail,
    nonSmokerDeclarationDetail,
  ]);

  return (
    <AssessmentsContext.Provider
      value={{
        assessmentCurrentPage,
        setAssessmentCurrentPage: (currentPage: Object) => {
          setAssessmentCurrentPage(currentPage);
        },
        currentAssessment,
        setCurrentAssessment: (assessment: string) => {
          setAssessmentReviewAndDone(assessment);
          setCurrentAssessment(assessment);
        },
        clearAssessmentDetail: (keyName: string) => {
          if (keyName !== selectedAssessment.keyName) {
            setAssessmentDetail({});
          }
        },
        howHealthyAreYouCurrentPage,
        setHowHealthyAreYouCurrentPage: (currentPage: Object) => {
          setHowHealthyAreYouCurrentPage(currentPage);
        },
        assessmentState,
        setAssessmentState: (state: AssessmentStateType[]) => {
          setAssessmentState(state);
        },
        isHowHealthyAreYouAssessmentLoading,
        howHealthyAreYouAssessment,
        howActiveAreYouAll,
        howActiveAreYou,
        howActiveAreYouQuestions,
        howHealthyAreYouAll,
        howHealthyAreYou,
        howHealthyAreYouQuestions,
        howStressedAreYouAll,
        howStressedAreYou,
        howStressedAreYouQuestions,
        howWellAreYouEatingAll,
        howWellAreYouEating,
        howWellAreYouEatingQuestions,
        howWellDoYouSleepAll,
        howWellDoYouSleep,
        howWellDoYouSleepQuestions,
        nonSmokerDeclarationAll,
        nonSmokerDeclaration,
        nonSmokerDeclarationQuestions,
        basicHealthScreening,
        basicHealthScreeningQuestions,
        assessmentDetail,
        assessmentReview,
        assessmentDone,
        basicHealthScreeningFindMore,
        membershipId,
      }}>
      {children}
    </AssessmentsContext.Provider>
  );
};

export default AssessmentsProvider;
