/**
 *
 * Rewards Context
 * @format
 *
 */

// @flow

import React, {useEffect, useState, useContext} from 'react';

import {ApplicationContext} from '@contexts/Application';

import type {PropsType} from './types';

export const EarnPointContext = React.createContext<{}>({});

const EarnPointProvider = (props: PropsType) => {
  const {children} = props;
  const {assessments, challenges} = useContext(ApplicationContext);
  const [selectedFilter, setSelectedFilter] = useState('take');
  const [earnPointList, setEarnPointList] = useState([]);

  const getEarnPointList = () => {
    const earnPointArr = [];
    assessments.forEach((assessment) => {
      const {status, title} = assessment;
      if (status === 'take') {
        const item = {
          ...assessment,
          title: 'Assessment',
          subTitle: title,
        };
        earnPointArr.push(item);
      }
    });
    challenges.forEach((challenge) => {
      const {status, title, points} = challenge;
      if (status === 'take' && points) {
        const item = {
          ...challenge,
          title: 'Challenge',
          subTitle: title,
        };
        earnPointArr.push(item);
      }
    });
    setEarnPointList(earnPointArr);
  };

  const selectFilter = (filterName: string) => {
    setSelectedFilter(filterName);
  };

  useEffect(() => {
    getEarnPointList();
  }, [selectedFilter]);

  return (
    <EarnPointContext.Provider
      value={{
        selectedFilter,
        selectFilter: (filterName: string) => {
          selectFilter(filterName);
        },
        earnPointList,
      }}>
      {children}
    </EarnPointContext.Provider>
  );
};

export default EarnPointProvider;
