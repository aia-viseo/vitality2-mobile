/**
 *
 * Rewards Context
 * @format
 *
 */

// @flow

import React from 'react';

import type {PropsType} from './types';

export const RewardsContext = React.createContext<{}>({});

const RewardsProvider = (props: PropsType) => {
  const {children} = props;

  return <RewardsContext.Provider value={{}}>{children}</RewardsContext.Provider>;
};

export default RewardsProvider;
