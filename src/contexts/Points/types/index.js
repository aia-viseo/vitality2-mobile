/**
 *
 * @format
 * @flow
 *
 */

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

export type RangeBarItemType = {
  rangeBarName: string,
  maximumPoints: number,
  currentPoints: number,
  progress: number,
};

export type RangeBarItemsType = {
  assessments?: RangeBarItemType,
  challenges?: RangeBarItemType,
  activity?: RangeBarItemType,
  error?: string,
};

export type HistoryItemType = {
  pointsHistoryTitle: string,
  desc: string,
  healthHabitCategory: string,
  points: number,
  awardedDate: Date,
  isInCurrentMonth: boolean,
  rejectionReason: string,
  eventType: string,
};
