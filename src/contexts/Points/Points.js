/**
 *
 * Points Provider
 * @format
 * @flow
 */

import React, {useContext, useEffect, useState} from 'react';
import {AxiosResponse} from 'axios';

import {GlobalContext} from '@contexts/Global';
import PointsAPI from '@core/clients/ApiCenter/Points';
import type {HistoryItemType, PropsType, RangeBarItemType, RangeBarItemsType} from './types';

export const PointsContext = React.createContext<{
  eventTypes: Array<string>,
  hasNewPoints: boolean,
  historyItems: Array<HistoryItemType>,
  rangeBarItems: RangeBarItemsType,
  totalPoints: number,
  totalPointsThisMonth: number,
  refreshing: boolean,
}>({});

const PointsProvider = (props: PropsType) => {
  const {userInfo} = useContext(GlobalContext);
  const [refreshing, setRefreshing] = useState(false);
  const [eventTypes, setEventTypes] = useState([]);
  const [hasNewPoints, setHasNewPoints] = useState(false);
  const [historyItems, setHistoryItems] = useState([]);
  const [rangeBarItems, setRangeBarItems] = useState({});
  const [totalPoints, setTotalPoints] = useState(0);
  const [totalPointsThisMonth, setTotalPointsThisMonth] = useState(0);
  const {children} = props;

  const processRangeBarAPIResult = (rangeBar: any): RangeBarItemsType => {
    const items: RangeBarItemsType = {};

    /* eslint-disable no-prototype-builtins */
    if (!!rangeBar && rangeBar.hasOwnProperty('rangeBarList') > 0) {
      /* eslint-enable no-prototype-builtins */
      rangeBar.rangeBarList
        .filter((item) => {
          return item.rangeBarName !== 'Sleep Health';
        })
        .forEach((item) => {
          const currentPoints = parseFloat(item.currentPoints);
          const maximumPoints = parseFloat(item.maximumPoints);
          const progress = currentPoints / maximumPoints;
          const rangeBarName = '';

          const rangeBarItem: RangeBarItemType = {
            currentPoints,
            maximumPoints,
            progress,
            rangeBarName,
          };

          switch (item.rangeBarName) {
            case 'Assessment':
              items.assessments = rangeBarItem;
              items.assessments.rangeBarName = 'points-history-filters.assessmentLabel';
              break;
            case 'Challenge':
              items.challenges = rangeBarItem;
              items.challenges.rangeBarName = 'points-history-filters.challengesLabel';
              break;
            case 'Regular Excercise':
            case 'Regular Excerise':
              items.activity = rangeBarItem;
              items.activity.rangeBarName = 'points-history-filters.activityLabel';
              break;
            default:
              break;
          }
        });

      return items;
    }

    return {error: 'error'};
  };

  const processHistoryAPIResult = (history: any): Array<HistoryItemType> => {
    const currentMonth = new Date().getMonth();
    let computedTotalPoints = 0;
    let computedTotalPointsThisMonth = 0;
    const events: Array<string> = [];
    let items: Array<HistoryItemType> = [];

    /* eslint-disable no-prototype-builtins */
    if (!!history && history.hasOwnProperty('hasNewPoints')) {
      setHasNewPoints(history.hasNewPoints);
    }

    if (!!history && history.hasOwnProperty('responseResults')) {
      /* eslint-enable no-prototype-builtins */
      const pointsHistory = history.responseResults
        .filter((item) => {
          const points = parseInt(item.points, 10);
          return points > 0;
        })
        .map((item) => {
          const points = parseInt(item.points, 10);
          const awardedDate = new Date(item.awardedDate);
          const isInCurrentMonth = awardedDate.getMonth() === currentMonth;
          if (isInCurrentMonth) {
            computedTotalPointsThisMonth += points;
          }
          computedTotalPoints += points;
          events.push(item.eventType);
          const historyItem: HistoryItemType = {
            eventType: item.eventType,
            pointsHistoryTitle: item.pointsHistoryTitle,
            points,
            desc: item.desc,
            awardedDate,
            isInCurrentMonth,
            healthHabitCategory: item.healthHabitCategory,
            rejectionReason: item.rejectionReason,
          };
          return historyItem;
        });

      setTotalPoints(computedTotalPoints);
      setTotalPointsThisMonth(computedTotalPointsThisMonth);
      setEventTypes(
        events.filter((value, index, self) => {
          return self.indexOf(value) === index;
        })
      );
      items = pointsHistory;
    }

    return items;
  };

  const getPointsData = (shouldRefresh: boolean = false) => {
    setRefreshing(true);
    const rangeBarRequest = new Promise((resolve) => {
      PointsAPI.fetchRangebarList(
        userInfo.membershipNumber,
        (result: AxiosResponse) => {
          resolve(result);
        },
        shouldRefresh
      );
    });

    const historyItemsRequest = new Promise((resolve) => {
      PointsAPI.fetchHistoryItems(
        userInfo.membershipNumber,
        (result: AxiosResponse) => {
          resolve(result);
        },
        shouldRefresh
      );
    });

    const promises = [rangeBarRequest, historyItemsRequest];
    Promise.all(promises).then((values) => {
      const [rangeBar, history] = values;
      setRangeBarItems(processRangeBarAPIResult(rangeBar));
      setHistoryItems(processHistoryAPIResult(history));
      setRefreshing(false);
    });
  };

  useEffect(() => {
    getPointsData();
  }, []);

  return (
    <PointsContext.Provider
      value={{
        eventTypes,
        hasNewPoints,
        historyItems,
        rangeBarItems,
        refresh: () => {
          getPointsData(true);
        },
        refreshing,
        totalPoints,
        totalPointsThisMonth,
      }}>
      {children}
    </PointsContext.Provider>
  );
};

export default PointsProvider;
