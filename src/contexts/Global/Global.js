/**
 *
 * Global Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';
import {AxiosResponse} from 'axios';
import {useTranslation} from 'react-i18next';

import AEMApiCenter from '@clients/AEMApiCenter';
import {addResource} from '@clients/i18n';
import {EN, ID} from '@constants/i18n';
import SAPApiCenter from '@clients/SAPApiCenter';
import AuthCenter from '@clients/AuthCenter';
import PointsAPI from '@clients/ApiCenter/Points';

import Text from '@atoms/Text';

import AppStyles from '@styles';
import {SCREENS, BOTTOM_NAVIGATIONS} from '@navigations/BottomNavigation/config';
import {
  POINTS_COMING_SOON,
  POINTS_WHY_NOT_APPEARING,
  POINTS_HISTORY_FILTERS,
  POINTS_EARN_FILTERS,
  POINTS_HISTORY,
} from './config';

import type {PropsType, UserInfoType, SnackbarPropsType} from './types';

export const GlobalContext = React.createContext<{
  userInfo: UserInfoType,
}>({});

const GlobalProvider = (props: PropsType) => {
  const {i18n} = useTranslation();
  const [isLogin, setIsLogin] = useState(false);
  const [initApp, setInitApp] = useState(true);
  const [showActivityIndicator, setShowActivityIndicator] = useState(false);
  const [bottomNavigationList, setBottomNavigationList] = useState(SCREENS);
  const [userInfo, setUserInfo] = useState({});
  const [snackbarProps, setSnackbarProps] = useState({});
  const {children} = props;

  const getGeneralTranslations = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getGeneral(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getGeneral(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      addResource(EN, {general: enLabel}, true, true);
      addResource(ID, {general: idLabel}, true, true);
    });
  };

  const getBottomNavigation = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getBottomNavigationList(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getBottomNavigationList(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = i18n.language === EN ? enLabel : idLabel;
      if (!translatedData || translatedData === {}) {
        setBottomNavigationList(SCREENS);
      }
      const keys = Object.keys(translatedData);
      let bottomNavigationItems = [];
      bottomNavigationItems = keys.map((key) => {
        const {title} = translatedData[key];
        const bottomNav = BOTTOM_NAVIGATIONS[key];
        const {options} = bottomNav;
        const modifiedOptions = Object.assign(options, {tabBarLabel: title});
        return {...bottomNav, options: modifiedOptions};
      });

      setBottomNavigationList(bottomNavigationItems.length > 0 ? bottomNavigationItems : SCREENS);
    });
  };

  const getPointsTranslations = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getPoints(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getPoints(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      addResource(EN, {[POINTS_COMING_SOON]: enLabel[POINTS_COMING_SOON]}, true, true);
      addResource(EN, {[POINTS_WHY_NOT_APPEARING]: enLabel[POINTS_WHY_NOT_APPEARING]}, true, true);
      addResource(EN, {[POINTS_HISTORY_FILTERS]: enLabel[POINTS_HISTORY_FILTERS]}, true, true);
      addResource(EN, {[POINTS_EARN_FILTERS]: enLabel[POINTS_EARN_FILTERS]}, true, true);
      addResource(EN, {[POINTS_HISTORY]: enLabel[POINTS_HISTORY]}, true, true);

      addResource(ID, {[POINTS_COMING_SOON]: idLabel[POINTS_COMING_SOON]}, true, true);
      addResource(ID, {[POINTS_WHY_NOT_APPEARING]: idLabel[POINTS_WHY_NOT_APPEARING]}, true, true);
      addResource(ID, {[POINTS_HISTORY_FILTERS]: idLabel[POINTS_HISTORY_FILTERS]}, true, true);
      addResource(ID, {[POINTS_EARN_FILTERS]: idLabel[POINTS_EARN_FILTERS]}, true, true);
      addResource(ID, {[POINTS_HISTORY]: idLabel[POINTS_HISTORY]}, true, true);
    });
  };

  const clearCache = () => {
    PointsAPI.clearCache();
  };

  const setMsgSnackbarProps = (msg) => {
    setSnackbarProps({
      visible: true,
      dismissCallback: () => {
        setSnackbarProps({visible: false});
      },
      duration: 5000,
      title: <Text customStyle={[AppStyles.snackbarTitle]}>{msg}</Text>,
    });
  };

  const refreshUserInfo = () => {
    AuthCenter.getMembershipNumber((membershipNumber: string) => {
      AuthCenter.getJWT(membershipNumber.toUpperCase().trim(), (jwt) => {
        SAPApiCenter.getMembershipInfo(
          jwt,
          membershipNumber,
          (result) => {
            if (result) {
              setUserInfo(result);
            }
          },
          (errMsg: string) => {
            setMsgSnackbarProps(errMsg);
          }
        );
      });
    });
  };

  useEffect(() => {
    getGeneralTranslations();
    getPointsTranslations();
  }, []);

  useEffect(() => {
    getBottomNavigation();
  }, [i18n.language]);

  return (
    <GlobalContext.Provider
      value={{
        userInfo,
        setUserInfo: (result: UserInfoType) => {
          setUserInfo(result);
        },
        isLogin,
        setIsLogin: (result: {}) => {
          setIsLogin(result);
        },
        showActivityIndicator,
        setShowActivityIndicator: (show: boolean) => {
          setShowActivityIndicator(show);
        },
        initApp,
        setInitApp: () => {
          setInitApp(false);
        },
        bottomNavigationList,
        snackbarProps,
        setSnackbarProps: (snackbarConfig: SnackbarPropsType) => {
          setSnackbarProps(snackbarConfig);
        },
        clearSnackbar: () => {
          setSnackbarProps({visible: false});
        },
        setMsgSnackbarProps: (msg) => {
          setMsgSnackbarProps(msg);
        },
        refreshUserInfo: () => {
          refreshUserInfo();
        },
        clearCache: () => {
          clearCache();
        },
      }}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalProvider;
