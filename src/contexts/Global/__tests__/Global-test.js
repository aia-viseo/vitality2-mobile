/**
 *
 *  @format
 *
 */

// @flow

import React from 'react';
import {View} from 'react-native';
import renderer from 'react-test-renderer';

import GlobalProvider from '../Global';

test('Test Global Context', () => {
  const tree = renderer
    .create(
      <GlobalProvider>
        <View />
      </GlobalProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
