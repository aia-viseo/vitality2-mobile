/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';
import type {ViewStyleProp, TextStyleProp} from 'react-native/Libraries/StyleSheet/StyleSheet';

export type PropsType = {
  children: Node,
};

export type UserInfoType = {
  level: string,
  membershipEndDate: string,
  membershipNumber: string,
  nextLevel: string,
  points: number,
  pointsToMaintainStatus: number,
  pointsToNextStatus: number,
  username: string,
};

export type BottomNavigationType = {
  id: string,
  title: string,
  iconName: string,
  iconGroup: string,
  order: string,
};

export type SnackbarPropsType = {
  duration?: number,
  style?: ViewStyleProp,
  title?: string | Node,
  titleStyle?: TextStyleProp,
  visible?: boolean,
  dismissCallback?: () => void,
  actionLabel?: string,
  onPressAction?: () => void,
  wrapperStyle?: ViewStyleProp,
};
