/**
 *
 *
 * @format
 *
 */

// @flow

export const POINTS_COMING_SOON = 'points-coming-soon';
export const POINTS_WHY_NOT_APPEARING = 'points-why-not-appearing';
export const POINTS_HISTORY_FILTERS = 'points-history-filters';
export const POINTS_EARN_FILTERS = 'points-earn-filters';
export const POINTS_HISTORY = 'points-history';
