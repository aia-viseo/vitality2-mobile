/**
 *
 * Dashboard Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';
import ApiCenter from '@clients/ApiCenter';

import type {PropsType, WhatsOnType, DashboardNotificationType} from './types';

export const DashboardContext = React.createContext<{
  whatsOns: Array<WhatsOnType>,
  isLoading: boolean,
  dashboardNotifications: Array<DashboardNotificationType>,
  setDashboardNotifications: (DashboardNotificationType[]) => void,
}>({});

const DashboardProvider = (props: PropsType) => {
  const [whatsOns, setWhatsOns] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [dashboardNotifications, setDashboardNotifications] = useState([]);
  const {children} = props;

  const getWhatsOns = () => {
    setWhatsOns(ApiCenter.getWhatsOns());
  };

  const getDashboardNotifications = () => {
    setDashboardNotifications(ApiCenter.getDashboardNotifications());
  };

  useEffect(() => {
    const promises = [getWhatsOns(), getDashboardNotifications()];
    Promise.all(promises).then(() => {
      setIsLoading(false);
    });
  }, []);

  return (
    <DashboardContext.Provider
      value={{
        whatsOns,
        isLoading,
        dashboardNotifications,
        setDashboardNotifications: (dbNotifications: DashboardNotificationType[]) => {
          setDashboardNotifications(dbNotifications);
        },
      }}>
      {children}
    </DashboardContext.Provider>
  );
};

export default DashboardProvider;
