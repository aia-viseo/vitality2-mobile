/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

export type ChallengeType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points: number,
  endDate: Date,
  minPoints: number,
  maxPoints: number,
  voucher: number,
};

export type AssessmentType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points: number,
};

export type WhatsOnType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  description: number,
};

export type RewardType = {
  name: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  discount: string,
  quantity: number,
  targetDate?: Date,
  terms?: string,
};

export type DashboardNotificationType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  description: String,
  show: boolean,
};
