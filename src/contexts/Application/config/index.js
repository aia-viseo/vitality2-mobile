/**
 *
 *
 * @format
 *
 */

// @flow

export const SELECTED = 'SELECTED';
export const COMPLETED_IN_TIME = 'COMPLETEDINTIME';
export const COMPLETED_OUT_OF_TIME = 'COMPLETEDOUTOFTIME';
export const COMPLETED_IN_TIME_NO_DEP = 'COMPLETEDINTIMENODEP';
export const NOT_ACHIEVED = 'NOTACHIEVED';
// keys
export const ASSESSMENT_QUESTION = 'how-healthy-are-you-assessment';
export const HOW_HEALTHY_ARE_YOU_ASSESSMENT = 'how-healthy-are-you-assessment';
export const STOP_SYNC = 'STOP_SYNC';
export const APP_STATE = 'APP_STATE';
export const HEALTH_DATA = 'HEALTH_DATA';
export const HOUR_MILLISECONDS = 60 * 60 * 1000;
