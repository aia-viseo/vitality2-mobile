/**
 *
 * Application Context
 * @format
 */

// @flow

import React, {useState, useEffect, useContext} from 'react';
import {AxiosResponse} from 'axios';
import {AppState} from 'react-native';
import {useTranslation} from 'react-i18next';
import AsyncStorage from '@react-native-community/async-storage';

import AEMApiCenter from '@clients/AEMApiCenter';
import ApiCenter from '@clients/ApiCenter';
import OnlineAssessmentsApiCenter from '@clients/OnlineAssessmentsApiCenter';
import AssessmentApiCenter from '@clients/ApiCenter/Assessment';
import ChallengeApiCenter from '@clients/ApiCenter/Challenge';

import AuthCenter from '@clients/AuthCenter';
import {USER_LANGUAGE_STORAGE_KEY, EN, ID} from '@constants/i18n';
import HealthSync from '@clients/HealthSync';
import DateTime from '@core/handler/DateTime';

import {GlobalContext} from '@contexts/Global';
import {DASHBOARD_SCREEN} from '@pages/Dashboard/config';

import {
  APP_STATE,
  STOP_SYNC,
  HEALTH_DATA,
  HOUR_MILLISECONDS,
  SELECTED,
  COMPLETED_IN_TIME,
  COMPLETED_OUT_OF_TIME,
  COMPLETED_IN_TIME_NO_DEP,
  NOT_ACHIEVED,
} from './config';

import type {
  PropsType,
  AssessmentType,
  ChallengeType,
  QuickLinkType,
  VitalityAgeType,
  RewardType,
  ChallengeSummaryType,
} from './types';
import APIResponseReconstructor from '../../core/handler/APIResponseReconstructor';

export const ApplicationContext = React.createContext<{
  assessments: Array<AssessmentType>,
  challenges: Array<ChallengeType>,
  quickLinks: Array<QuickLinkType>,
  vitalityAge: VitalityAgeType,
  rewards: Array<RewardType>,
  isAssessmentsLoading: boolean,
  isChallengesLoading: boolean,
  isQuickLinksLoading: boolean,
  isVitalityAgeLoading: boolean,
  removeAEMCachedContents: (callback: () => void) => void,
  selectedChallenge: Object,
  selectChallenge: (challenge: Object) => void,
  selectedAssessment: Object,
  selectAssessment: (assessment: Object) => void,
  userLanguage: string,
  refreshData: (lang: string) => void,
  challengesSummary: ChallengeSummaryType,
  reloadScreen: (screen: string) => void,
}>({});

const ApplicationProvider = (props: PropsType) => {
  const {i18n} = useTranslation();
  const {initApp, setInitApp, setMsgSnackbarProps} = useContext(GlobalContext);
  const [assessments, setAssessments] = useState([]);
  const [challenges, setChallenges] = useState([]);
  const [challengesSummary, setChallengesSummary] = useState({});
  const [quickLinks, setQuickLinks] = useState([]);
  const [vitalityAge, setVitalityAge] = useState({});
  const [rewards, setRewards] = useState([]);
  const [isAssessmentsLoading, setIsAssessmentsLoading] = useState(false);
  const [isChallengesLoading, setIsChallengesLoading] = useState(false);
  const [isQuickLinksLoading, setIsQuickLinksLoading] = useState(false);
  const [needUpdateData, setNeedUpdateData] = useState(false);

  const [isVitalityAgeLoading, setIsVitalityAgeLoading] = useState(true);
  const [isRewardsLoading, setIsRewardsLoading] = useState(false);

  const [selectedChallenge, setSelectedChallenge] = useState({});
  const [selectedAssessment, setSelectedAssessment] = useState({});
  const [syncHealthData, setSyncHealthData] = useState(AsyncStorage.getItem(HEALTH_DATA));
  const [userLanguage, setUserLanguage] = useState(i18n.language);

  const {children} = props;

  const getRewards = () => {
    setIsRewardsLoading(true);
    setRewards(ApiCenter.getRewards());
    setIsRewardsLoading(false);
  };

  const getCurrentLanguage = () => {
    return AsyncStorage.getItem(USER_LANGUAGE_STORAGE_KEY);
  };

  const removeChallengesCached = () => {
    ChallengeApiCenter.clearChallengeCache(() => {});
  };

  const getChallenges = async () => {
    setIsChallengesLoading(true);
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getChallengeList(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getChallengeList(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getData = new Promise((resolve) => {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        ChallengeApiCenter.fetchChallengeList(membershipNumberResult, (response) => {
          if (response) {
            const {challengeList} = response;
            resolve(challengeList);
          } else {
            setMsgSnackbarProps('Error: Failed to fetch challenge list from API!');
          }
        });
      });
    });

    const getChallengeSummary = (challengeList: Array<Object>): Object => {
      let summary = {
        inProgress: 0,
        completed: 0,
        targetNotMet: 0,
      };
      challengeList.forEach((item) => {
        const {challengeStatus} = item;
        const {inProgress, completed, targetNotMet} = summary;
        if (challengeStatus === SELECTED) {
          summary = Object.assign(summary, {inProgress: inProgress + 1});
        }
        if (
          challengeStatus === COMPLETED_IN_TIME ||
          challengeStatus === COMPLETED_OUT_OF_TIME ||
          challengeStatus === COMPLETED_IN_TIME_NO_DEP
        ) {
          summary = Object.assign(summary, {completed: completed + 1});
        }
        if (challengeStatus === NOT_ACHIEVED) {
          summary = Object.assign(summary, {targetNotMet: targetNotMet + 1});
        }
      });
      return summary;
    };

    const promises = [getEn, getId, getData];
    const lang = await getCurrentLanguage();
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel, data] = values;
      const label = lang === EN ? enLabel : idLabel;
      const translatedData = APIResponseReconstructor.restructureAEMChallengeList(label, data);
      setChallenges(translatedData);
      setChallengesSummary(getChallengeSummary(data));
      setIsChallengesLoading(false);
    });
  };

  const removeAssessmentsCached = () => {
    AssessmentApiCenter.clearAssessmentCache(() => {});
  };

  const getAssessments = async () => {
    setIsAssessmentsLoading(true);
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getAssessmentList(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getAssessmentList(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getData = new Promise((resolve) => {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        AssessmentApiCenter.fetchAssessmentList(membershipNumberResult, (response) => {
          if (response) {
            const {assessmentList} = response;
            resolve(assessmentList);
          } else {
            setMsgSnackbarProps('Error: Failed to fetch assessment list from API!');
          }
        });
      });
    });

    const promises = [getEn, getId, getData];
    const lang = await getCurrentLanguage();
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel, data] = values;
      const label = lang === EN ? enLabel : idLabel;
      const translatedData = APIResponseReconstructor.restructureAEMAssessmentList(label, data);
      setAssessments(translatedData);
      setIsAssessmentsLoading(false);
    });
  };

  const getQuickLinks = () => {
    setIsQuickLinksLoading(true);
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getQuickLinks(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getQuickLinks(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setQuickLinks(translatedData);
      setIsQuickLinksLoading(false);
    });
  };

  const getVitalityAge = () => {
    setIsVitalityAgeLoading(true);
    OnlineAssessmentsApiCenter.getVitalityAge()
      .then((age) => {
        setVitalityAge(age);
        setIsVitalityAgeLoading(false);
      })
      .catch(() => {});
  };

  const removeAEMCachedContents = (callback: () => void) => {
    AEMApiCenter.removeAEMCachedContents(() => {
      callback();
    });
  };

  const setHealthData = (data: string) => {
    let steps = 0;
    let avgHeartRate = 0;
    let timesOfHeartRate = 0;
    if (data) {
      const dataJsonArr = JSON.parse(data);
      dataJsonArr.forEach((dataJson) => {
        const {readings} = dataJson;
        readings.forEach((reading) => {
          const {workout, startTime} = reading;
          if (DateTime.isToday(new Date(startTime))) {
            const {totalSteps, heartRate} = workout || {};
            steps += parseInt(totalSteps || 0, 10);
            if (heartRate) {
              const {avgMinMax} = heartRate;
              const {average} = avgMinMax || {};
              if (average) {
                timesOfHeartRate += 1;
                avgHeartRate += parseInt(average || 0, 10);
              }
            }
          }
        });
      });
      const result = {
        totalSteps: steps || null,
        avgHeartRate: avgHeartRate ? avgHeartRate / timesOfHeartRate : null,
      };
      AsyncStorage.setItem(HEALTH_DATA, JSON.stringify(result));
      setSyncHealthData(result);
    }
  };

  const uploadHealthData = () => {
    AsyncStorage.getItem(STOP_SYNC).then((stopSync: string) => {
      if (stopSync === 'false') {
        return;
      }
      HealthSync.uploadHealthData((msg, result, data) => {
        if (result) {
          setNeedUpdateData(true);
          setHealthData(data);
        } else {
          AsyncStorage.setItem(STOP_SYNC, 'false');
        }
      });
    });
  };

  const syncHealthDataTimer = (nextAppState) => {
    if (nextAppState === 'inactive' || nextAppState === 'background') {
      AsyncStorage.setItem(APP_STATE, nextAppState);
    } else {
      AsyncStorage.getItem(APP_STATE).then((appState: string) => {
        if (appState !== 'inactive') {
          const timer = () =>
            setTimeout(() => {
              uploadHealthData();
            }, 6 * HOUR_MILLISECONDS);

          if (nextAppState === 'background') {
            timer();
          } else if (nextAppState === 'active') {
            uploadHealthData();
            clearTimeout(timer());
          }
        }
        AsyncStorage.setItem(APP_STATE, nextAppState);
      });
    }
  };

  const initData = () => {
    getQuickLinks();
    getAssessments();
    getChallenges();
    getVitalityAge();
    getRewards();
  };

  const reloadSelectedScreen = (screen: string) => {
    switch (screen) {
      case DASHBOARD_SCREEN: {
        setAssessments([]);
        setChallenges([]);
        removeAssessmentsCached();
        removeChallengesCached();
        getAssessments();
        getChallenges();
        break;
      }
      default: {
        break;
      }
    }
  };

  useEffect(() => {
    initData();
  }, [userLanguage]);

  useEffect(() => {
    if (initApp) {
      setInitApp(false);
      uploadHealthData();
    }
    AppState.addEventListener('change', syncHealthDataTimer);
    return () => {
      AppState.removeEventListener('change', syncHealthDataTimer);
    };
  }, []);

  return (
    <ApplicationContext.Provider
      value={{
        assessments,
        challenges,
        quickLinks,
        vitalityAge,
        rewards,
        isAssessmentsLoading,
        isChallengesLoading,
        isQuickLinksLoading,
        isVitalityAgeLoading,
        isRewardsLoading,
        removeAEMCachedContents: (callback: () => void) => {
          removeAEMCachedContents(callback);
        },
        refreshVitalityAge: getVitalityAge,
        selectedChallenge,
        selectChallenge: (challenge: Object) => {
          setSelectedChallenge(challenge);
        },
        selectedAssessment,
        selectAssessment: (assessment: Object) => {
          setSelectedAssessment(assessment);
        },
        userLanguage,
        refreshData: (lang: string) => {
          setUserLanguage(lang);
        },
        reloadScreen: (screen: string) => {
          reloadSelectedScreen(screen);
        },
        challengesSummary,
        syncHealthData,
        setHealthData: (data: string) => {
          setHealthData(data);
        },
        needUpdateData,
        setNeedUpdateData: (result: boolean) => {
          setNeedUpdateData(result);
        },
      }}>
      {children}
    </ApplicationContext.Provider>
  );
};

export default ApplicationProvider;
