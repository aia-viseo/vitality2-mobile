/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

export type VitalityAgeType = {
  age: number,
  diffYears: number,
};

export type AssessmentType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points: number,
  description?: string,
  keyName?: string,
  assessmentName?: string,
  healthHabitCategory?: string,
  status?: string,
  duration?: string,
  rewardType?: string,
  rewardValue?: string,
  order?: string,
  challengeDesc?: string,
  assessmentType?: string,
  awardedDate?: string,
};

export type ChallengeType = {
  challengeTopIcon: string,
  challengeLinkIcon: string,
  isEarnPoints: boolean,
  challengeName: string,
  chllengeDesc: string,
  challengeIconType: string,
  challengeId: string,
  challengeStatus: string,
  duration: string,
  healthHabitCategory: string,
  keyName: string,
  order: string,
  remainingDay: string,
  remainingDaySuffix: string,
  rewardValue: number,
  targetValue: number,
  rewardUnit: string,
  rewardType: string,
  statusFromDate: string,
  statusToDate: string,
};
export type QuickLinkType = {
  id: string,
  title: string,
  iconName: string,
  type: string,
};

export type RewardType = {
  name: string,
  imageSrc?: string,
  iconName?: string,
  discount: string,
  quantity: number,
  targetDate?: Date,
  terms?: string,
};

export type ChallengeSummaryType = {
  inProgress: number,
  completed: number,
  targetNotMet: number,
};
