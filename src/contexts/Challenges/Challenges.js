/**
 *
 * Challenge Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect, useContext} from 'react';
import {AxiosResponse} from 'axios';
import lodash from 'lodash';

import {EN, ID} from '@constants/i18n';

import AuthCenter from '@clients/AuthCenter';
import AEMApiCenter from '@clients/AEMApiCenter';
import ChallengeApiCenter from '@clients/ApiCenter/Challenge';

import {ApplicationContext} from '@contexts/Application';
import {GlobalContext} from '@contexts/Global';

import type {PropsType} from './types';

export const ChallengesContext = React.createContext<{}>({});

const ChallengesProvider = (props: PropsType) => {
  const {children} = props;
  const {userLanguage, needUpdateData, setNeedUpdateData} = useContext(ApplicationContext);
  const {setMsgSnackbarProps} = useContext(GlobalContext);
  const [challengePeriodList, setChallengePeriodList] = useState([]);
  const [steps, setSteps] = useState([]);
  const [heartRates, setHeartRates] = useState([]);
  const [earnedToday, setEarnedToday] = useState(0);
  const [dailyMax] = useState(0);

  const getWeeklyChallenge = (shouldRefresh: boolean = false) => {
    const getData = new Promise((resolve) => {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        ChallengeApiCenter.fetchWeeklyChallenge(
          membershipNumberResult,
          (response) => {
            if (response) {
              const {challengePeriods} = response;
              resolve(lodash.sortBy(challengePeriods, 'periodSequence'));
            } else {
              setMsgSnackbarProps('Error: Failed to fetch challenge list from API!');
            }
          },
          shouldRefresh
        );
      });
    });
    const promises = [getData];
    Promise.all(promises).then((values) => {
      const [data] = values;
      setChallengePeriodList(data);
    });
  };

  const getEarnedToday = (shouldRefresh: boolean = false) => {
    const getData = new Promise((resolve) => {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        ChallengeApiCenter.fetchEarnedToday(
          membershipNumberResult,
          0,
          'STE4,STE5,PHY3,PHY4,PHY5',
          (response) => {
            if (response) {
              resolve(response);
            } else {
              setMsgSnackbarProps('Error: Failed to fetch challenge list from API!');
            }
          },
          shouldRefresh
        );
      });
    });
    const promises = [getData];
    Promise.all(promises).then((values) => {
      const [data] = values;
      const {pointsTotal} = data;
      setEarnedToday(pointsTotal);
    });
  };

  const getSteps = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getEarnPointSteps(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getEarnPointSteps(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setSteps(translatedData);
    });
  };

  const getHeartRate = () => {
    const getEn = new Promise((resolve) => {
      AEMApiCenter.getEarnPointHeartRate(EN, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const getId = new Promise((resolve) => {
      AEMApiCenter.getEarnPointHeartRate(ID, (result: AxiosResponse) => {
        resolve(result);
      });
    });
    const promises = [getEn, getId];
    Promise.all(promises).then((values) => {
      const [enLabel, idLabel] = values;
      const translatedData = userLanguage === EN ? enLabel : idLabel;
      setHeartRates(translatedData);
    });
  };

  const initData = (shouldRefresh: boolean = false) => {
    getWeeklyChallenge(shouldRefresh);
    getSteps();
    getHeartRate();
    getEarnedToday(shouldRefresh);
  };

  useEffect(() => {
    initData();
  }, [userLanguage]);

  useEffect(() => {
    if (needUpdateData) {
      initData(true);
      setNeedUpdateData(false);
    }
  }, [needUpdateData]);

  return (
    <ChallengesContext.Provider
      value={{
        challengePeriodList,
        steps,
        heartRates,
        earnedToday,
        dailyMax,
      }}>
      {children}
    </ChallengesContext.Provider>
  );
};

export default ChallengesProvider;
