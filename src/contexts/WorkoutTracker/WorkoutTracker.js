/**
 *
 * Rewards Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect, useContext} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

import WorkoutTrackerApiCenter from '@clients/ApiCenter/WorkoutTracker';
import AuthCenter from '@clients/AuthCenter';
import DateTime from '@core/handler/DateTime';

import {GlobalContext} from '@contexts/Global';

import {LINKED, HEART_RATE, STEPS, SLEEP, HEART_RATE_AVERAGE, UPLOAD_DATE_TIME} from './config';

import type {PropsType, LinkedDevice, WorkoutDataType} from './types';

export const WorkoutTrackerContext = React.createContext<{
  workoutTrackerDataList: Array<WorkoutDataType>,
}>({});

const WorkoutTrackerProvider = (props: PropsType) => {
  const {children} = props;
  const {setMsgSnackbarProps} = useContext(GlobalContext);
  const [workoutTrackerDataList, setWorkoutTrackerDataList] = useState([]);
  const [fitnessList, setFitnessList] = useState([]);
  const [filteredFitnessList, setFilteredFitnessList] = useState(fitnessList);
  const [selectedWorkoutData, setSelectedWorkoutData] = useState({});
  const [fitnessListLoading, setFitnessListLoading] = useState(true);
  const [allLoading, setAllLoading] = useState(true);
  const [selectedFilter, setSelectedFilter] = useState('all');
  const [syncTime, setSyncTime] = useState(null);

  const getLinkedDevices = (
    membershipNumberResult: string,
    callback: (Array<LinkedDevice>) => void
  ) => {
    WorkoutTrackerApiCenter.fetchWorkoutTrackerDevices(membershipNumberResult, (response) => {
      if (response) {
        const {devices} = response;
        const linkedDevices = devices
          ? devices.filter(({trackingDetails}) => trackingDetails.linkedStatus === LINKED)
          : [];
        callback(linkedDevices);
      } else {
        setMsgSnackbarProps('Error: Failed to fetch workout tracker devices from API!');
        callback([]);
      }
    });
  };

  const getWorkoutData = (
    membershipNumberResult: string,
    deviceName: string,
    startDate?: Date | null,
    shouldRefresh: boolean = false,
    callback: (data: WorkoutDataType) => void
  ) => {
    WorkoutTrackerApiCenter.fetchWorkoutData(
      membershipNumberResult,
      deviceName,
      startDate || new Date(),
      (response) => {
        if (response) {
          const {workouts} = response || {};
          let heatRate = 0;
          let steps = 0;
          let sleepHours = 0;
          if (workouts) {
            workouts.forEach((workout) => {
              const {measure} = workout || {};
              if (measure) {
                measure.forEach((measureItem) => {
                  const {type, value, attributeBreakup} = measureItem || {};
                  if (type) {
                    switch (type) {
                      case HEART_RATE:
                        {
                          const {key} = attributeBreakup;
                          if (key === HEART_RATE_AVERAGE) {
                            const attValue = (attributeBreakup || {value: '0'}).value;
                            const heartRateValue = parseInt(attValue, 10);
                            if (heartRateValue > heatRate) {
                              heatRate = heartRateValue;
                            }
                          }
                        }
                        break;

                      case STEPS:
                        if (value && value !== '') {
                          const stepValue = parseInt(value, 10);
                          if (stepValue > steps) {
                            steps = stepValue;
                          }
                        }
                        break;

                      case SLEEP:
                        if (value && value !== '') {
                          const sleepValue = parseInt(value, 10);
                          sleepHours = sleepValue / 3600;
                        }
                        break;

                      default:
                        break;
                    }
                  }
                });
              }
            });
          }
          callback({title: deviceName, steps, heatRate, sleepHours});
        } else {
          setMsgSnackbarProps('Error: Failed to fetch workout data from API!');
          callback({});
        }
      },
      shouldRefresh
    );
  };

  const getWorkoutTrackerDataList = (shouldRefresh: boolean = false) => {
    const yesterday = new Date(DateTime.getDaysBeforeDate(1));
    AuthCenter.getMembershipNumber((membershipNumberResult) => {
      getLinkedDevices(membershipNumberResult, (linkedDevices: Array<LinkedDevice>) => {
        const promises = [];
        linkedDevices.forEach((linkedDevice) => {
          const {deviceName} = linkedDevice || {};
          if (deviceName) {
            const promise = new Promise((resolve) => {
              getWorkoutData(
                membershipNumberResult,
                deviceName,
                yesterday,
                shouldRefresh,
                (data: WorkoutDataType) => {
                  resolve(data);
                }
              );
            });
            promises.push(promise);
          }
        });
        Promise.all(promises).then((values) => {
          setWorkoutTrackerDataList(values);
        });
      });
    });
  };

  const getFitnessList = (shouldRefresh: boolean = false) => {
    const getData = new Promise((resolve) => {
      AuthCenter.getMembershipNumber((membershipNumberResult) => {
        WorkoutTrackerApiCenter.fetchWorkoutFitnessPoint(
          membershipNumberResult,
          (response) => {
            if (response) {
              const {pointsPerDay} = response;
              resolve(pointsPerDay);
            } else {
              resolve([]);
              setMsgSnackbarProps('Error: Failed to fetch fitness list from API!');
            }
            setFitnessListLoading(false);
          },
          shouldRefresh
        );
      });
    });
    const promises = [getData];
    Promise.all(promises).then((values) => {
      const [data] = values;
      setFitnessList(data);
    });
  };

  const getUploadDateTime = () => {
    AsyncStorage.getItem(UPLOAD_DATE_TIME).then((uploadDateTime: string) => {
      if (uploadDateTime) {
        const timeDifference = DateTime.differenceInMilliseconds(new Date(uploadDateTime));
        const diffMins = (timeDifference / (1000 * 60)) * -1;
        setSyncTime(parseInt(diffMins, 10));
      }
    });
  };

  const initData = (shouldRefresh: boolean = false) => {
    const promises = [
      getWorkoutTrackerDataList(shouldRefresh),
      getFitnessList(shouldRefresh),
      getUploadDateTime(),
    ];
    Promise.all(promises).then(() => {
      setAllLoading(false);
    });
  };

  const filterFitnessList = () => {
    let filteredArr = [];
    let key;
    switch (selectedFilter) {
      case 'all': {
        filteredArr = fitnessList;
        break;
      }
      case 'steps': {
        key = 'STE';
        break;
      }
      case 'heart rate': {
        key = 'PHY';
        break;
      }
      case 'sleep': {
        key = 'SLPT';
        break;
      }
      default: {
        filteredArr = fitnessList;
        break;
      }
    }
    if (key) {
      fitnessList.forEach((fitnessItem) => {
        const {activityId} = fitnessItem || {};
        if (activityId.indexOf(key) > -1) {
          filteredArr.push(fitnessItem);
        }
      });
    }
    setFilteredFitnessList(filteredArr);
  };

  useEffect(() => {
    initData();
  }, []);

  useEffect(() => {
    filterFitnessList();
  }, [fitnessList, selectedFilter]);

  return (
    <WorkoutTrackerContext.Provider
      value={{
        setFitnessListLoading: () => {
          setFitnessListLoading(true);
          getFitnessList(true);
        },
        fitnessListLoading,
        workoutTrackerDataList,
        selectedWorkoutData,
        setSelectedWorkoutData: (workoutData) => {
          setSelectedWorkoutData(workoutData);
        },
        filteredFitnessList,
        setAllLoading: () => {
          setFitnessListLoading(true);
          setAllLoading(true);
          initData(true);
        },
        allLoading,
        selectedFilter,
        setSelectedFilter: (filterName: string) => {
          setSelectedFilter(filterName);
        },
        syncTime,
      }}>
      {children}
    </WorkoutTrackerContext.Provider>
  );
};

export default WorkoutTrackerProvider;
