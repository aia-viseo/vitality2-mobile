/**
 *
 *
 * @format
 *
 */

// @flow

export const LINKED = 'LINKED';
export const UN_LINK = 'UN_LINK';
export const UPLOAD_DATE_TIME = 'UPLOAD_DATE_TIME';

export const HEART_RATE = 'heartRate';
export const STEPS = 'steps';
export const SLEEP = 'sleep';
export const HEART_RATE_AVERAGE = 'average';
