/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

type TrackingDetails = {
  linkedStatus: string,
  deviceLastSyncDateTime: string,
  deviceLastWorkoutDateTime: string,
  attributeType: string,
  attributeDesc: string,
};

export type LinkedDevice = {
  deviceName: string,
  trackingDetails: TrackingDetails,
};

export type WorkoutDataType = {
  title: string,
  steps: number,
  heatRate: number,
  sleepHours?: number,
};
