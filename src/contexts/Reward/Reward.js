/**
 *
 * Reward Context
 * @format
 *
 */
// @flow

import React from 'react';

import type {PropsType} from './types';

export const RewardContext = React.createContext<{}>({});
const RewardProvider = (props: PropsType) => {
  const {children} = props;

  return <RewardContext.Provider value={{}}>{children}</RewardContext.Provider>;
};

export default RewardProvider;
