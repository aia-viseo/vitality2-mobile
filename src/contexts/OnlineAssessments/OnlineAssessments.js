/**
 *
 * OnlineAssessments Context
 * @format
 *
 */

// @flow

import React, {useState, useEffect} from 'react';
import OnlineAssessmentsApiCenter from '@clients/OnlineAssessmentsApiCenter';

import type {
  OnlineAssessmentsDataType,
  VitalityAgeCompleteDataType,
} from '@clients/OnlineAssessmentsApiCenter/types';
import {sampleData} from '@clients/OnlineAssessmentsApiCenter/config';

import type {PropsType} from './types';

export const OnlineAssessmentsContext = React.createContext<{
  onlineAssessmentsUserData: any,
  setOnlineAssessmentsUserData: (data: OnlineAssessmentsDataType) => void,
  onlineAssessmentsUserAnswers: any,
  setOnlineAssessmentsUserAnswers: (data: OnlineAssessmentsDataType) => void,
  onlineAssessmentsCompletedData: any,
  setOnlineAssessmentsCompletedData: (data: VitalityAgeCompleteDataType) => void,
  completedOnlineAssessments: string[],
  setCompletedOnlineAssessments: (assessments: string[]) => void,
  answeredOnlineAssessments: string[],
  setAnsweredOnlineAssessments: (assessments: string[]) => void,
}>({});

const OnlineAssessmentsProvider = (props: PropsType) => {
  const [onlineAssessmentsUserData, setOnlineAssessmentsUserData] = useState({});
  const [onlineAssessmentsCompletedData, setOnlineAssessmentsCompletedData] = useState({});
  const [onlineAssessmentsUserAnswers, setOnlineAssessmentsUserAnswers] = useState({});
  const [completedOnlineAssessments, setCompletedOnlineAssessments] = useState([]);
  const [answeredOnlineAssessments, setAnsweredOnlineAssessments] = useState([]);

  const {children} = props;

  useEffect(() => {
    if (Object.entries(onlineAssessmentsUserData).length === 0) {
      OnlineAssessmentsApiCenter.getVitalityAgeUserData()
        .then((data) => {
          if (data) {
            setOnlineAssessmentsUserData(JSON.parse(data));
          } else {
            setOnlineAssessmentsUserData(sampleData);
          }
        })
        .catch(() => {
          setOnlineAssessmentsUserData(sampleData);
        });
    }
    if (Object.entries(onlineAssessmentsCompletedData).length === 0) {
      OnlineAssessmentsApiCenter.getVitalityAgeCompleteData()
        .then((data) => {
          setOnlineAssessmentsCompletedData(JSON.parse(data));
        })
        .catch(() => {});
    }
    if (Object.entries(onlineAssessmentsUserAnswers).length === 0) {
      OnlineAssessmentsApiCenter.getVitalityAgeUserAnswers()
        .then((data) => {
          if (data) {
            setOnlineAssessmentsUserAnswers(JSON.parse(data));
          } else {
            setOnlineAssessmentsUserAnswers({en: {}, id: {}});
          }
        })
        .catch(() => {
          setOnlineAssessmentsUserAnswers({en: {}, id: {}});
        });
    }
    if (completedOnlineAssessments.length === 0) {
      OnlineAssessmentsApiCenter.getCompletedOnlineAssessments()
        .then((data) => {
          if (data) {
            setCompletedOnlineAssessments(JSON.parse(data));
          }
        })
        .catch(() => {});
    }
  }, []);

  return (
    <OnlineAssessmentsContext.Provider
      value={{
        onlineAssessmentsUserData,
        setOnlineAssessmentsUserData: (data: OnlineAssessmentsDataType) => {
          setOnlineAssessmentsUserData(data);
        },
        onlineAssessmentsUserAnswers,
        setOnlineAssessmentsUserAnswers: (answers: OnlineAssessmentsDataType) => {
          setOnlineAssessmentsUserAnswers(answers);
        },
        onlineAssessmentsCompletedData,
        setOnlineAssessmentsCompletedData: (data: VitalityAgeCompleteDataType) => {
          setOnlineAssessmentsCompletedData(data);
        },
        completedOnlineAssessments,
        setCompletedOnlineAssessments: (data: string[]) => {
          setCompletedOnlineAssessments(data);
        },
        answeredOnlineAssessments,
        setAnsweredOnlineAssessments: (data: string[]) => {
          setAnsweredOnlineAssessments(data);
        },
      }}>
      {children}
    </OnlineAssessmentsContext.Provider>
  );
};

export default OnlineAssessmentsProvider;
