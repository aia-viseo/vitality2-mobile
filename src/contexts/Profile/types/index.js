/**
 *
 * @format
 *
 */

// @flow

import type {Node} from 'react';

export type PropsType = {
  children: Node,
};

export type VitalityAgeType = {
  age: number,
  diffYears: number,
};

export type AssessmentType = {
  title: string,
  imageSrc?: string,
  iconName?: string,
  iconGroup?: string,
  points: number,
};
