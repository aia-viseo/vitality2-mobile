/**
 *
 * Profile Context
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import type {PropsType} from './types';

export const ProfileContext = React.createContext<{}>({});

const ProfileProvider = (props: PropsType) => {
  const {children} = props;

  useEffect(() => {}, []);

  return <ProfileContext.Provider value={{}}>{children}</ProfileContext.Provider>;
};

export default ProfileProvider;
