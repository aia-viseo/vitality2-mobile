# Vitality Mobile 2020

## Importing

This project provides the following module aliases for convenience.

```js
"@atoms/*": "./src/components/atoms/*",
"@clients/*": "./src/core/clients/*",
"@constants/*": "./src/constants/*",
"@contexts/*": "./src/contexts/*",
"@core/*": "./src/core/*",
"@layouts/*": "./src/components/layouts/*",
"@molecules/*": "./src/components/molecules/*",
"@navigations/*": "./src/navigations/*",
"@organisms/*": "./src/components/organisms/*",
"@pages/*": "./src/pages/*",
"@platform": "./src/core/Singleton/Platform",
"@styles": "./src/core/styles"
"@colors": "./src/core/colors",
```

To add more, please add corresponding entries in

1. `.babelrc` so babel would know how to transpile the imports
2. `.vscode/settings.json` and `jsconfig.json` to support intellisense; and
3. `.flowconfig` so that Flow could resolve modules properly

Note that each file has its own format for defining aliases. Just follow along.

## Deployment

### Alpha via AppCenter

Any changes to `develop` will be included in AppCenter build. Currently, this is manually triggered within AppCenter itself.


### Beta via TestFlight

(consider using fastlane for this)


### Production via TestFlight

(consider using fastlane for this)


### CodePush

First, ensure that `.env.secrets.codepush` is present in the root folder. Ask maintainer for copy if you were tasked to do a deployment.

```sh
VERSION=x.y.z yarn codepush:ios:staging  # for deploying changes to iOS Staging
VERSION=x.y.z yarn codepush:ios:store    # for deploying changes to iOS Production
VERSION=x.y.z yarn codepush:aos:staging  # for deploying changes to aOS Staging
VERSION=x.y.z yarn codepush:aos:store    # for deploying changes to aOS Production
```
