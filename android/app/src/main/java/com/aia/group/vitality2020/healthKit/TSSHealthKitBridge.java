package com.aia.group.vitality2020.healthKit;

import android.content.Intent;
import androidx.annotation.Nullable;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.aia.group.vitality2020.*;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TSSHealthKitBridge extends ReactContextBaseJavaModule {
    private Callback successCallback;
    private Callback errorCallback;
    private HashMap<String, String> jsonMap;
    public static String XVitalityProjectId = "";


    public TSSHealthKitBridge(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "TSSHealthKitBridge";
    }

    @ReactMethod
    public void queryHealthData(String params, Callback successCallback, Callback errorCallback) {
        HashMap<String, String > jsonMap = new Gson().fromJson(params, HashMap.class);
        this.successCallback = successCallback;
        this.errorCallback = errorCallback;
        this.jsonMap = jsonMap;
        XVitalityProjectId = "_" + jsonMap.get("XVitalityProjectId");
        MainActivity main = (MainActivity) this.getCurrentActivity();
        main.queryHealthData();

    }

    @ReactMethod
    public  void backaction() {
        System.exit(0);
    }

    @ReactMethod
    public void  saveUploadDate() {
        MainActivity main = (MainActivity) this.getCurrentActivity();
        main.saveLastSyncDate();
    }

    @ReactMethod
    public void isHealthDataAvailable(Callback successCallback, Callback errorCallback) {
        this.successCallback = successCallback;
        this.errorCallback = errorCallback;
        MainActivity main = (MainActivity) this.getCurrentActivity();
        main.isHealthDataAvailable();
    }


    private void uploadHealthDataNative(ArrayList<HashMap<String, HashMap<String, Object>>> arrayHealthData) {
        if (    this.jsonMap == null ||
                this.jsonMap.get("membershipNo") == null ||
                this.jsonMap.get("membershipNo").equals("") ||
                this.jsonMap.get("Authorization") == null ||
                this.jsonMap.get("Authorization").equals("") ||
                this.jsonMap.get("BaseURL") == null ||
                this.jsonMap.get("BaseURL").equals("") ||
                this.jsonMap.get("URL") == null ||
                this.jsonMap.get("URL").equals("") ||
                this.jsonMap.get("Timeout") == null ||
                this.jsonMap.get("Accept") == null ||
                this.jsonMap.get("Accept").equals("") ||
                this.jsonMap.get("ContentType") == null ||
                this.jsonMap.get("ContentType").equals("") ||
                this.jsonMap.get("XVitalityLegalEntityId") == null ||
                this.jsonMap.get("XVitalityLegalEntityId").equals("") ||
                this.jsonMap.get("XAIARequestId") == null ||
                this.jsonMap.get("XAIARequestId").equals("") ||
                this.jsonMap.get("AccessControlAllowHeader") == null ||
                this.jsonMap.get("AccessControlAllowHeader").equals("") ||
                this.jsonMap.get("AccessControlAllowMethods") == null ||
                this.jsonMap.get("AccessControlAllowMethods").equals("") ||
                this.jsonMap.get("Origin") == null ||
                this.jsonMap.get("Origin").equals("")
                ) {
            return;
        }
        if (arrayHealthData != null) {
            new Thread() {
                public void run() {
                    int flag = 0;
                    for (int i = 0; i < arrayHealthData.size(); i++) {
                        HashMap<String, HashMap<String, Object>> mapHealthData = arrayHealthData.get(i);
                        if (mapHealthData != null && mapHealthData.get("header") != null) {
                            if (mapHealthData.get("header").get("tenantId") != null) {
                                (mapHealthData.get("header")).put("tenantId", jsonMap.get("XVitalityLegalEntityId").toString());
                            }
                        }
                        String jsonString = new Gson().toJson(mapHealthData);
                        MediaType JSON = MediaType.get("application/json; charset=utf-8");
                        OkHttpClient client = new OkHttpClient();
                        RequestBody body = RequestBody.create(JSON, jsonString);
                        Request request = new Request.Builder()
                                .addHeader("Accept", jsonMap.get("Accept"))
                                .addHeader("Content-Type", jsonMap.get("ContentType"))
                                .addHeader("X-Vitality-Legal-Entity-Id", jsonMap.get("XVitalityLegalEntityId"))
                                .addHeader("X-AIA-Request-Id", jsonMap.get("XAIARequestId"))
                                .addHeader("Authorization", jsonMap.get("Authorization"))
                                .addHeader("Origin",jsonMap.get("Origin"))
                                .url(jsonMap.get("BaseURL") + jsonMap.get("URL") + "?membership-no=" + jsonMap.get("membershipNo"))
                                .post(body)
                                .build();
                        try {
                            Response response = client.newCall(request).execute();
                            if (response.code() == 200) {
                                System.out.println("Upload success");
                                flag++;
                            }
                            response.body().string();
                        } catch (Exception e) {
                            System.out.print("Exception");

                        }
                    }
                    if (flag == arrayHealthData.size()) {
                        saveUploadDate();
                    }

                }
            }.start();
        }

    }




    public void nativeCallRn(Map<String, Object> param){
        onScanningResult(param);
    }

    public void onScanningResult(Map<String, Object> param){
        WritableMap nativeParam = Arguments.createMap();
        String json;
        if (param.get("errorCode") != null) {
            ArrayList returnArray = new ArrayList();
            returnArray.add(param);
            json = new Gson().toJson(returnArray);
            if (this.errorCallback != null) {
                this.errorCallback.invoke(json);
                this.errorCallback = null;
            }
        } else if (!param.get("returnMessage").equals("") || param.get("returnMessage") != null) {
            if (param.get("returnCode") == "20000") {//HAS Premission
                ArrayList returnArray = new ArrayList();
                returnArray.add(param);
                json = new Gson().toJson(returnArray);
                if (this.successCallback != null) {
                    this.successCallback.invoke(json);
                    this.successCallback = null;
                }
            } else {
                json = new Gson().toJson(param.get("returnMessage"));
                if (this.successCallback != null) {
                    this.successCallback.invoke(json);
                    this.successCallback = null;
                } else {
                    //12hr
                    this.uploadHealthDataNative((ArrayList<HashMap<String, HashMap<String, Object>>>) param.get("returnMessage"));
                }

            }

        }

    }

    public static void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }

}
