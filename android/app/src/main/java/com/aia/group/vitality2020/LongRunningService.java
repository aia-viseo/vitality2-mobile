package com.aia.group.vitality2020;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import java.util.Calendar;

public class LongRunningService extends Service {

    private static final String TAG = "LongRunningService";
    private static final long UPLOAD_INTERVAL = AlarmManager.INTERVAL_HALF_DAY;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                if (MainActivity.getMainActivity() != null) {
                    MainActivity.getMainActivity().bridgeQuerySHealthData12Hr();
                } else {
                    return;
                }
                Looper.loop();
                Log.e(TAG,"LongRunningService");
            }
        }).start();
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent i = new Intent(this, LongRunningService.class);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            manager.cancel(pi);
            long triggerAtTime = SystemClock.elapsedRealtime() + 1 * 60 * 60 * 1000;
            manager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        } else {
            manager.cancel(pi);
            Calendar calendar = Calendar.getInstance();
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), UPLOAD_INTERVAL, pi);
        }


        return super.onStartCommand(intent, flags, startId);
    }
}
