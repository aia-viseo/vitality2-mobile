/**
 * Copyright (C) 2014 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */

package com.aia.group.vitality2020;

import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthData;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest;
import com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthResultHolder;
import android.os.Build;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class StepCountReporter {
    private final HealthDataStore mStore;
    private StepCountObserver mStepCountObserver;
    private static final long ONE_DAY_IN_MILLIS = 24 * 60 * 60 * 1000L - 1000;
    private ArrayList<HashMap<String, ArrayList<Object>>> dataArray;
    private ArrayList dataInterval;
    private HashMap<String, Object> healthData;
    private ArrayList finalData;
    private DateFormat format;
    private DateFormat formatLocal;
    private int dataflag = 0;
    public static int MAX_UPLOAD_DAYS = 30;

    public StepCountReporter(HealthDataStore store) {
        mStore = store;
    }

    public void start(StepCountObserver listener, String lastUpdate) {

        mStepCountObserver = listener;
        dataArray = new ArrayList();
        finalData = new ArrayList();
        java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        TimeZone chongqing = TimeZone.getTimeZone("Asia/Chongqing");
        TimeZone UTC = TimeZone.getTimeZone("UTC");
        format.setTimeZone(chongqing);
        this.format = format;

        java.text.DateFormat formatLocal = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        TimeZone timezoneLocal = TimeZone.getDefault();
        format.setTimeZone(timezoneLocal);
        this.formatLocal = formatLocal;

        //lastUpdate = "2018-09-17T00:36:03+0800";
        Date lastUpdatedate = null;
        if (lastUpdate != null) {
            String lastUpdateTime = lastUpdate;
            try {
                lastUpdatedate = this.format.parse(lastUpdateTime);
                System.out.println(lastUpdatedate);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
                return;
            }
        }
        Date now = new Date();
        float dateInterval = 0;
        if( lastUpdatedate != null) {
            dateInterval =getBetweenDate(lastUpdate, this.format.format(now));
            dateInterval = dateInterval > MAX_UPLOAD_DAYS ? MAX_UPLOAD_DAYS :dateInterval;
        } else {
            dateInterval = MAX_UPLOAD_DAYS;
        }

        ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i <= (int)dateInterval; i++) {
            if (i == 0) {
                list.add(getStartTimeOfToday());
            } else {
                list.add(getStartTimeOfInterval(-i));
            }
        }
        dataInterval = list;
        if (lastUpdatedate != null) {
            readTodayStepCount(list, lastUpdatedate.getTime());
        } else  {
            readTodayStepCount(list, getStartTimeOfInterval(-MAX_UPLOAD_DAYS));
        }

    }

    // Read the today's step count on demand
    private void readTodayStepCount(ArrayList<Object> list, long lasySyncTime) {
        HealthDataResolver resolver = new HealthDataResolver(mStore, null);

        // Set time range from start time of today to the current time
        long startToday = getStartTimeOfToday();
        long endToday = startToday + ONE_DAY_IN_MILLIS;

        for (int i = 0; i < list.size(); i++) {
            long startTime = (long)list.get(i);
            long endTime = startTime + ONE_DAY_IN_MILLIS;

            //java.util.Date start_date = new Date(startTime);
            //java.util.Date end_date = new Date(endTime);
            //String start_d = format.format(start_date);
            //String end_d = format.format(end_date);

            ReadRequest requestStep = new ReadRequest.Builder()
                    .setDataType(HealthConstants.StepCount.HEALTH_DATA_TYPE)
                    .setProperties(new String[] {HealthConstants.StepCount.COUNT,
                            HealthConstants.Exercise.PACKAGE_NAME,
                            HealthConstants.Exercise.DEVICE_UUID,
                            HealthConstants.Exercise.START_TIME,
                            HealthConstants.Exercise.END_TIME,
                            HealthConstants.Exercise.TIME_OFFSET})
                    .setLocalTimeRange(HealthConstants.StepCount.START_TIME, HealthConstants.StepCount.TIME_OFFSET, startTime, endTime)
                    .build();
            try {
                resolver.read(requestStep).setResultListener(mListener);
            } catch (Exception e) {
                return;
            }
        }


        ReadRequest reuqestFitness = new ReadRequest.Builder()
                    .setDataType(HealthConstants.Exercise.HEALTH_DATA_TYPE)
                    .setProperties(new String[] {   HealthConstants.Exercise.CALORIE,
                                                    HealthConstants.Exercise.DURATION,
                                                    HealthConstants.Exercise.DISTANCE,
                                                    HealthConstants.Exercise.MAX_HEART_RATE,
                                                    HealthConstants.Exercise.MEAN_HEART_RATE,
                                                    HealthConstants.Exercise.MIN_HEART_RATE,
                                                    HealthConstants.Exercise.START_TIME,
                                                    HealthConstants.Exercise.END_TIME,
                                                    HealthConstants.Exercise.PACKAGE_NAME,
                                                    HealthConstants.Exercise.UUID,
                                                    HealthConstants.Exercise.DEVICE_UUID,
                                                    HealthConstants.Exercise.EXERCISE_TYPE,
                                                    HealthConstants.Exercise.EXERCISE_CUSTOM_TYPE,
                                                    HealthConstants.Exercise.MAX_SPEED,
                                                    HealthConstants.Exercise.MEAN_SPEED,
                                                    HealthConstants.Exercise.LIVE_DATA,
                                                    HealthConstants.Exercise.TIME_OFFSET})
                    .setLocalTimeRange(HealthConstants.Exercise.START_TIME, HealthConstants.Exercise.TIME_OFFSET, lasySyncTime, endToday)
                    .build();

        try {
            resolver.read(reuqestFitness).setResultListener(mListener);
        } catch (Exception e) {
            return;
        }

    }

    private long getBetweenDate(String Begin,  String End) {
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        long betweenDate = 1;
        try {
            java.util.Date beginDate = df.parse(Begin);
            java.util.Date endDate = df.parse(End);
            betweenDate = (endDate.getTime() - beginDate.getTime()) / (1000*60*60*24);
            return betweenDate;
        } catch (ParseException e) {
            System.out.print(e.getMessage());
            return 1;
        }

    }

    private long getStartTimeOfToday() {
        TimeZone UTC = TimeZone.getTimeZone("UTC");
        Calendar today = Calendar.getInstance(UTC);
        System.out.println("年：" + today.get(Calendar.YEAR));
        System.out.println("月：" + (today.get(Calendar.MONTH) + 1));
        System.out.println("日：" + today.get(Calendar.DAY_OF_MONTH));
        System.out.println("时：" + today.get(Calendar.HOUR_OF_DAY));
        System.out.println("分：" + today.get(Calendar.MINUTE));
        System.out.println("秒：" + today.get(Calendar.SECOND));

        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        long re = today.getTimeInMillis();
        return re;
    }

    private long getStartTimeOfInterval(int interval) {
        TimeZone UTC = TimeZone.getTimeZone("UTC");
        Calendar today = Calendar.getInstance(UTC);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        today.add(Calendar.DAY_OF_YEAR, interval);
        return today.getTimeInMillis();
    }

    private DateFormat getDateFormat(int timeOffset) {
        java.text.DateFormat newFormat = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        TimeZone tz = TimeZone.getDefault();
        tz.setRawOffset(timeOffset);
        newFormat.setTimeZone(tz);
        return newFormat;
    }

    private final HealthResultHolder.ResultListener<ReadResult> mListener = result -> {
        int count = 0;
        dataflag++;
        String time1 = null;
        String time2 = null;
        String dataUUID = null;
        HashMap<String, Integer> countsMap = new HashMap<String, Integer>();

        try {
            for (HealthData data : result) {
                //Prepare Routine by datasource
                Set<String> keySet =  data.getKeySet();
                if (keySet.size() == 6) {
                    String deviceuuid = data.getString("deviceuuid");
                    int timeOffset = data.getInt("time_offset");
                    DateFormat stepDateFormat = this.getDateFormat(timeOffset);

                    dataUUID = deviceuuid;
                    Integer countsUUID = countsMap.get(dataUUID);
                    if (countsMap.get(dataUUID) == null) {
                        countsUUID = 0;
                        countsMap.put(dataUUID, countsUUID);
                    }

                    countsUUID += data.getInt(HealthConstants.StepCount.COUNT);
                    countsMap.put(dataUUID, countsUUID);

                    if (time1 == null) {
                        long start_long = data.getLong("start_time");
                        java.util.Date start_date = new Date(start_long);
                        String start_time = stepDateFormat.format(start_date);
                        time1 = start_time;

                        long end_long = data.getLong("end_time");
                        java.util.Date end_date = new Date(end_long);
                        String end_time = stepDateFormat.format(end_date);
                        time2 = end_time;
                    }

                } else if (keySet.size() > 6)  {
                    if (data.getString("live_data") == null) {
                        continue;
                    }

                    int timeOffset = data.getInt("time_offset");
                    DateFormat exerciseDateFormat = this.getDateFormat(timeOffset);

                    // Setting Fitness
                    Map<String,Object> map = new HashMap<String,Object>();

                    long start_long = data.getLong("start_time");
                    java.util.Date start_date = new Date(start_long);
                    String start_time = exerciseDateFormat.format(start_date);

                    long end_long = data.getLong("end_time");
                    java.util.Date end_date = new Date(end_long);
                    String end_time = exerciseDateFormat.format(end_date);
                    if (start_time == null || end_time == null) {
                        return;
                    }

                    String exercise_type = data.getString("exercise_type");
                    HealthDataType healthDataType = new HealthDataType();
                    String exercise_type_name = healthDataType.getHealthDataType(exercise_type);
                    String calorie =  data.getString("calorie");
                    String deviceuuid = data.getString("deviceuuid");
                    String pkg_name = data.getString("pkg_name");
                    String dataUUID_ = data.getString("datauuid");

                    String distance = data.getString("distance");
                    String max_heart_rate = data.getString("max_heart_rate");
                    String mean_heart_rate = data.getString("mean_heart_rate");
                    String min_heart_rate = data.getString("min_heart_rate");

                    Map<String, Object> workout = new HashMap<String, Object>();
                    Map<String, Object> distanceM = new HashMap<String, Object>();
                    Map<String, Object> energyExpenditureM = new HashMap<String, Object>();
                    Map<String, Object> heartRateM = new HashMap<String, Object>();
                    Map<String, Object> heartRateMM = new HashMap<String, Object>();

                    if (distance != null && !distance.equals("0.0") ) {
                        distanceM.put("unitOfMeasurement", "METERS");
                        distanceM.put("value", distance);
                        workout.put("distance", distanceM);
                    }

                    if (calorie != null && !calorie.equals("0.0") ) {
                        energyExpenditureM.put("unitOfMeasurement", "KILOCALORIES");
                        energyExpenditureM.put("value", calorie);
                        workout.put("energyExpenditure", energyExpenditureM);
                    }

                    if (mean_heart_rate != null) {
                        heartRateMM.put("average", mean_heart_rate);
                        heartRateMM.put("maximum", max_heart_rate);
                        heartRateMM.put("minimum", min_heart_rate);
                        heartRateM.put("avgMinMax", heartRateMM);
                        workout.put("heartRate", heartRateM);
                    }

                    map.put("workout", workout);
                    //Common Fitness
                    map.put("startTime",start_time);
                    map.put("endTime",end_time);
                    map.put("partnerCreateDate",start_time);
                    map.put("partnerReadingId", dataUUID_ + start_time);
                    if ( Build.BRAND != null && Build.BRAND.toLowerCase().contains("samsung") ) {
                        map.put("manufacturer", "Samsung");
                    } else {
                        map.put("manufacturer", "S Health Third-party");
                    }
                    map.put("integrity","VERIFIED");
                    map.put("model",Build.BRAND + ' ' + Build.MODEL);
                    map.put("notes", "Exercise");
                    map.put("dataCategory","FITNESS");
                    map.put("readingType", exercise_type_name);
                    for (int i=0; i < dataArray.size(); i++) {
                        if ( dataArray.get(i).get(start_time.substring(0, 10)) != null ) {
                            dataArray.get(i).get(start_time.substring(0, 10)).add(map);
                            break;
                        }
                        if (i == dataArray.size()) {
                            HashMap<String, ArrayList<Object>> innerMap = new HashMap<>();
                            ArrayList<Object> innerArray = new ArrayList<Object>();
                            innerArray.add(map);
                            innerMap.put(start_time.substring(0, 10), innerArray);
                            dataArray.add(innerMap);
                        }
                    }

                }

            }
            if (countsMap.keySet().size() != 0) {
                HashMap<String, ArrayList<Object>> innerMap = new HashMap<String, ArrayList<Object>>();
                ArrayList<Object> innerArray = new ArrayList<Object>();
                for (Map.Entry<String, Integer> entry : countsMap.entrySet()) {
                    // Setting Routine
                    Map<String, Object> map = new HashMap<String, Object>();
                    Map<String, Integer> workout = new HashMap<String, Integer>();
                    workout.put("totalSteps", entry.getValue());
                    map.put("workout", workout);
                    map.put("startTime", time1.substring(0, 10) + "T00:00:00");
                    map.put("endTime", time2.substring(0, 10) + "T23:59:59");
                    map.put("partnerCreateDate", time1);
                    if (time1.length() >= 10) {
                        map.put("partnerReadingId",  time1.substring(0, 10) + entry.getKey());
                    }
                    if (Build.BRAND != null && Build.BRAND.toLowerCase().contains("samsung")) {
                        map.put("manufacturer", "Samsung");
                    } else {
                        map.put("manufacturer", "S Health Third-party");
                    }
                    map.put("integrity", "VERIFIED");
                    map.put("model", Build.BRAND + ' ' + Build.MODEL);
                    map.put("dataCategory", "ROUTINE");
                    map.put("readingType", "Walking");
                    innerArray.add(map);
                }

                if (time1.length() >= 10) {
                    innerMap.put(time1.substring(0, 10), innerArray);
                    dataArray.add(innerMap);
                }

            }

        } finally {
            result.close();
        }

        if (dataflag == (dataInterval.size() + 1) && mStepCountObserver != null) {
            if (dataArray == null || dataArray.size() == 0) {
                ArrayList<HashMap<String, HashMap<String, Object>>> arrayList = new ArrayList<HashMap<String, HashMap<String, Object>>>();
                mStepCountObserver.onChanged(arrayList);
                return;
            }
            for (int i = 0; i< dataArray.size(); i++) {
                //0 Health Data
                HashMap<String, Object> healthData = new HashMap<String, Object>();

                //1 Setting Device
                HashMap<String, Object> device = new HashMap<String, Object>();
                if ( Build.BRAND != null && Build.BRAND.toLowerCase().contains("samsung") ) {
                    device.put("manufacturer", "Samsung");
                } else {
                    device.put("manufacturer", "S Health Third-party");
                }
                device.put("model", Build.BRAND + ' ' + Build.MODEL);
                device.put("deviceId", "");
                device.put("make", "Samsung");
                healthData.put("device", device);

                //2 Setting Header
                HashMap<String, Object> header = new HashMap<String, Object>();
                //HashMap<String, Object> userObj = new HashMap<String, Object>();
                //userObj.put("entityNo","");
                //header.put("user", userObj);
                Date uploadDate = new Date();
                String uploadDateString = formatLocal.format(uploadDate);
                header.put("uploadDate", uploadDateString);
                header.put("additionalMessage", "");
                header.put("rawUploadData", "");
                header.put("processingType", "REALTIME");
                header.put("sessionId", "");
                header.put("verified", "true");
                header.put("tenantId", "6");
                header.put("partnerSystem", "Samsung");
                healthData.put("header", header);

                //3 Setting Readings
                healthData.put("readings", dataArray);
                for (String key : dataArray.get(i).keySet()) {
                    if (key != null) {
                        healthData.put("readings", dataArray.get(i).get(key));
                        finalData.add(healthData);
                        break;
                    }
                }
            }

            mStepCountObserver.onChanged(finalData);
            mStepCountObserver = null;
            dataArray = null;
        }

    };

    public interface StepCountObserver {
        void onChanged(ArrayList<HashMap<String, HashMap<String, Object>>> list);
    }
}
