package com.aia.group.vitality2020;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.annotation.Nullable;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.aia.group.vitality2020.MainActivity;
import com.aia.group.vitality2020.MainApplication;
import com.google.gson.Gson;

import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



public class TSSBridgeModule extends ReactContextBaseJavaModule {

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";
    private Callback HealthCallback;
    private String memberShipID;
    private String authorization;

    public TSSBridgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "TSSBridgeModule";
    }

    @ReactMethod
    public void querySHealthData(String name, Callback errorCallback, Callback successCallback) {
        com.aia.group.vitality2020.MainActivity main = (MainActivity) this.getCurrentActivity();
        main.bridgeQuerySHealthData();
        this.HealthCallback = successCallback;
        this.memberShipID = name;
    }

    @ReactMethod
    public void openUrl(String url) {
        com.aia.group.vitality2020.MainActivity main = (com.aia.group.vitality2020.MainActivity) this.getCurrentActivity();
        main.openUrl(url);
    }

    @ReactMethod
    public void queryEncryptKey(String name, Callback errorCallback, Callback successCallback) {
        successCallback.invoke("Message123");
    }

    @ReactMethod
    public void  saveUploadDate(String name) {
        this.authorization = name;
        com.aia.group.vitality2020.MainActivity main = (com.aia.group.vitality2020.MainActivity) this.getCurrentActivity();
        main.saveLastSyncDate();
    }

    private void uploadHealthDataNative(ArrayList<HashMap<String, HashMap<String, Object>>> arrayHealthData) {
        if (this.memberShipID == null || this.memberShipID.equals("") || this.authorization == null || this.authorization.equals("")) {
            WritableMap nativeParam = Arguments.createMap();
            nativeParam.putString("message", "404");
            nativeParam.putString("name", "404");
            sendEvent(getReactApplicationContext(),"sendHelathData", nativeParam);
            return;
        }
        //ArrayList<HashMap<String, HashMap<String, Object>>> arrayHealthData = (ArrayList<HashMap<String, HashMap<String, Object>>>) param.get("message");
        if (arrayHealthData != null) {
            for (int i = 0; i < arrayHealthData.size(); i++) {
                HashMap<String, HashMap<String, Object>> mapHealthData = arrayHealthData.get(i);
                String jsonString = new Gson().toJson(mapHealthData);
                okhttp3.MediaType JSON = MediaType.get("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonString);
                Request request = new Request.Builder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("X-Vitality-Legal-Entity-Id", "6")
                        .addHeader("X-AIA-Request-Id", "1")
                        .addHeader("Authorization", this.authorization)
                        .addHeader("Origin","https://vitality.aia.com.sg")
                        // .url("https://qa.vitality.aia.com/MyPageVitality/1.0/devices/submit-health-data?membership-no=" + this.memberShipID)
                        .url("https://vitality.aia.com/vitality/core/v2/devices/submit-health-data?membership-no=" + this.memberShipID)
                        .post(body)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.code() == 200) {
                        System.out.println("Upload success");
//                        WritableMap nativeParam = Arguments.createMap();
//                        nativeParam.putString("message", "200");
//                        nativeParam.putString("name", "200");
//                        sendEvent(getReactApplicationContext(),"sendHelathData", Arguments.createMap());
                    } else {
                        System.out.println("Upload failed");
//                        WritableMap nativeParam = Arguments.createMap();
//                        nativeParam.putString("message", "401");
//                        nativeParam.putString("name", "401");
//                        sendEvent(getReactApplicationContext(),"sendHelathData", nativeParam);
                    }
                    response.body().string();
                } catch (Exception e) {
//                    WritableMap nativeParam = Arguments.createMap();
//                    nativeParam.putString("message", "401");
//                    nativeParam.putString("name", "401");
//                    sendEvent(getReactApplicationContext(),"sendHelathData", nativeParam);
                }
            }
        }
    }




    public void nativeCallRn(Map<String, Object> param){
        onScanningResult(param);
    }

    public void onScanningResult(Map<String, Object> param){
        WritableMap nativeParam = Arguments.createMap();
        if (param.get("name").toString() == "PushNotification") {
            nativeParam.putString("message", param.get("message").toString());
            nativeParam.putString("name", param.get("name").toString());
            nativeParam.putString("uuid", param.get("uuid").toString());
            sendEvent(getReactApplicationContext(),param.get("name").toString(),nativeParam);
        } else if (param.get("name").toString() == "PushNotificationMessage") {
            nativeParam.putString("message", param.get("message").toString());
            nativeParam.putString("name", param.get("name").toString());
            sendEvent(getReactApplicationContext(),param.get("name").toString(),nativeParam);
        } else if (param.get("name").toString() == "fingerPrint") {
            nativeParam.putString("message", param.get("message").toString());
            nativeParam.putString("name", param.get("name").toString());
            sendEvent(getReactApplicationContext(),param.get("name").toString(),nativeParam);
        } else if (param.get("name").toString() == "samsungHealth") {
            String json = "";
            if (!param.get("message").equals("") || param.get("message") != null) {
                json = new Gson().toJson(param.get("message"));
            }
            if (this.HealthCallback != null) {
                this.HealthCallback.invoke(json);
                this.HealthCallback = null;
            } else {
                //12hr
                //nativeParam.putString("message", json);
                //nativeParam.putString("name", param.get("name").toString());
                //sendEvent(getReactApplicationContext(), param.get("name").toString(), nativeParam);
                this.uploadHealthDataNative((ArrayList<HashMap<String, HashMap<String, Object>>>) param.get("message"));
            }
        } else if (param.get("name").toString() == "sendHelathData") {
            nativeParam.putString("message", param.get("message").toString());
            nativeParam.putString("name", param.get("name").toString());
            sendEvent(getReactApplicationContext(),param.get("name").toString(),nativeParam);
        } else if (param.get("name").toString() == "gobackWebView") {
            nativeParam.putString("message", param.get("message").toString());
            nativeParam.putString("name", param.get("name").toString());
            sendEvent(getReactApplicationContext(),param.get("name").toString(),nativeParam);
        }


    }

    public static void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }





}
