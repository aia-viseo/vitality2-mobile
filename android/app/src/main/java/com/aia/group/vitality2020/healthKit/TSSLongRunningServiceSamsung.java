package com.aia.group.vitality2020.healthKit;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;

import com.aia.group.vitality2020.*;

import java.util.Calendar;

public class TSSLongRunningServiceSamsung extends Service {

    private static final long UPLOAD_INTERVAL = 3 * AlarmManager.INTERVAL_HOUR;
    private static final long UPLOAD_INTERVAL_2 = AlarmManager.INTERVAL_FIFTEEN_MINUTES/5;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                if (MainActivity.getMainActivity() != null) {
                    //samsung
                    MainActivity.getMainActivity().queryHealthData12();
                } else {
                    return;
                }
                Looper.loop();
            }
        }).start();
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent i = new Intent(this, TSSLongRunningServiceSamsung.class);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            manager.cancel(pi);
            long triggerAtTime = SystemClock.elapsedRealtime() + UPLOAD_INTERVAL;
            manager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        } else {
            manager.cancel(pi);
            Calendar calendar = Calendar.getInstance();
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), UPLOAD_INTERVAL, pi);
        }
        return super.onStartCommand(intent, flags, startId);
    }

}
