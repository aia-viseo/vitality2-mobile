package com.aia.group.vitality2020;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.aia.group.vitality2020.TSSBridgeModule;
import com.aia.group.vitality2020.healthKit.TSSHealthKitBridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TSSBridgePackage implements ReactPackage {

    public com.aia.group.vitality2020.TSSBridgeModule bridgeModule;
//    public com.aia.group.vitality2020.healthKit.TSSHealthKitBridge healthbridgeModule;
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();
        bridgeModule = new TSSBridgeModule(reactContext);
//        healthbridgeModule = new TSSHealthKitBridge(reactContext);
        modules.add(bridgeModule);
//        modules.add(healthbridgeModule);

        return modules;
    }




}
