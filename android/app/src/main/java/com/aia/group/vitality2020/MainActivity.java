package com.aia.group.vitality2020;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;
import android.app.Activity;
import android.telephony.TelephonyManager;
import android.content.Context;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ReactApplicationContext;
import com.healthcomponent.TSSHealthKitAuth;
import com.healthcomponent.TSSHealthKitEntrance;
import com.healthcomponent.TSSHealthKitInterface;

import com.facebook.react.ReactActivity;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataService;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionKey;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionResult;
import com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionType;
import com.samsung.android.sdk.healthdata.HealthResultHolder;


import com.aia.group.vitality2020.healthKit.*;
import java.util.*;
import com.adobe.mobile.*;

public class MainActivity extends ReactActivity implements TSSHealthKitInterface{

    private static final String TAG = "MainActivity";
    private static final int permissonAndroid = 2001;
    private String flag;
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {

        return "vitality2020";
    }

    public void registToken(String DEVICE_ID) {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //SHealth
        HealthDataService healthDataService = new HealthDataService();
        try {
            healthDataService.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //SHealth kit
        TSSHealthKitEntrance.initialize(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //https://marketing.adobe.com/resources/help/en_US/mobile/android/dev_qs.html
        HashMap<String,Object> contextData = new HashMap<String, Object>();
        //contextData.put("eVar1", "Vitality:UAT");
        contextData.put("eVar1", "Vitality:PROD");
        Config.setDebugLogging(true);
        Config.setContext(this);
        Config.collectLifecycleData(this, contextData);
    }

    @Override
    public void onPause() {
        super.onPause();
        Config.pauseCollectingLifecycleData();
    }

    /**
     * Below is used for Samsung Health
     * This is used to schedule rendering of the component.
     */
    public String getLastSyncDate () {
        SharedPreferences sharedPreferences = getSharedPreferences("myaia", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("lastSyncDate", null);
        return name;
    }

    private HealthDataStore mStore;
    private StepCountReporter mReporter;

    public void bridgeQuerySHealthData() {

        if (mStore == null) {
            // Create a HealthDataStore instance and set its listener
            mStore = new HealthDataStore(this, mConnectionListener);
            // Request the connection to the health data store
            mStore.connectService();
            return;
        }
        // mReporter = new StepCountReporter(mStore);
        // if (isPermissionAcquired()) {
        //     mReporter.start(mStepCountObserver, getLastSyncDate());
        // }
        // else {
        //     requestPermission();
        // }
    }

    public void bridgeQuerySHealthData12Hr() {
        if (mStore == null) {
            return;
        }
        mReporter = new StepCountReporter(mStore);
        if (isPermissionAcquired()) {
            mReporter.start(mStepCountObserver, getLastSyncDate());
        } else {
            return;
        }
    }

    private final HealthDataStore.ConnectionListener mConnectionListener = new HealthDataStore.ConnectionListener() {

        @Override
        public void onConnected() {
            mReporter = new StepCountReporter(mStore);
            if (isPermissionAcquired()) {
                mReporter.start(mStepCountObserver, getLastSyncDate());
            } else {
                requestPermission();
            }
        }

        @Override
        public void onConnectionFailed(HealthConnectionErrorResult error) {
            showConnectionFailureDialog(error);
        }

        @Override
        public void onDisconnected() {
            //Log.d(APP_TAG, "Health data service is disconnected.");
            if (!isFinishing()) {
                mStore.connectService();
            }
        }
    };

    private void showPermissionAlarmDialog() {
        mStore = null;
        this.nativeCallRnInMainActivity(null);

        if (isFinishing()) {
            return;
        }

    }

    private void showConnectionFailureDialog(final HealthConnectionErrorResult error) {
        mStore = null;
        this.nativeCallRnInMainActivity(null);

        if (isFinishing()) {
            return;
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if (error.hasResolution()) {
            switch (error.getErrorCode()) {
                case HealthConnectionErrorResult.PLATFORM_NOT_INSTALLED:
                    alert.setMessage(com.aia.group.vitality2020.R.string.msg_req_install);
                    break;
                case HealthConnectionErrorResult.OLD_VERSION_PLATFORM:
                    alert.setMessage(com.aia.group.vitality2020.R.string.msg_req_upgrade);
                    break;
                case HealthConnectionErrorResult.PLATFORM_DISABLED:
                    alert.setMessage(com.aia.group.vitality2020.R.string.msg_req_enable);
                    break;
                case HealthConnectionErrorResult.USER_AGREEMENT_NEEDED:
                    alert.setMessage(com.aia.group.vitality2020.R.string.msg_req_agree);
                    break;
                default:
                    alert.setMessage(com.aia.group.vitality2020.R.string.msg_req_available);
                    break;
            }
        } else {
            alert.setMessage(com.aia.group.vitality2020.R.string.msg_conn_not_available);
        }

        alert.setPositiveButton(com.aia.group.vitality2020.R.string.ok, (dialog, id) -> {
            if (error.hasResolution()) {
                error.resolve(MainActivity.this);
            }
        });

        if (error.hasResolution()) {
            alert.setNegativeButton(com.aia.group.vitality2020.R.string.cancel, null);
            this.nativeCallRnInMainActivity(null);
        }

        alert.show();
    }

    private Set<PermissionKey> generatePermissionKeySet() {
        Set<PermissionKey> pmsKeySet = new HashSet<>();
        pmsKeySet.add(new PermissionKey(HealthConstants.StepCount.HEALTH_DATA_TYPE, PermissionType.READ));
        // pmsKeySet.add(new PermissionKey(HealthConstants.HeartRate.HEALTH_DATA_TYPE, PermissionType.READ));
        pmsKeySet.add(new PermissionKey(HealthConstants.Exercise.HEALTH_DATA_TYPE, PermissionType.READ));
        pmsKeySet.add(new PermissionKey(HealthConstants.Sleep.HEALTH_DATA_TYPE, PermissionType.READ));
        return pmsKeySet;
    }

    private boolean isPermissionAcquired() {
        HealthPermissionManager pmsManager = new HealthPermissionManager(mStore);
        try {
            // Check whether the permissions that this application needs are acquired
            Map<PermissionKey, Boolean> resultMap = pmsManager.isPermissionAcquired(generatePermissionKeySet());
            return !resultMap.values().contains(Boolean.FALSE);
        } catch (Exception e) {
            Log.e(TAG, "Permission request fails.", e);
        }
        return false;
    }

    private void requestPermission() {
        HealthPermissionManager pmsManager = new HealthPermissionManager(mStore);
        try {
            pmsManager.requestPermissions(generatePermissionKeySet(), MainActivity.this)
                    .setResultListener(mPermissionListener);
        } catch (Exception e) {
            Log.e(TAG, "Permission setting fails.", e);
        }
    }

    private final HealthResultHolder.ResultListener<PermissionResult> mPermissionListener =
            new HealthResultHolder.ResultListener<PermissionResult>() {

                @Override
                public void onResult(PermissionResult result) {
                    Map<PermissionKey, Boolean> resultMap = result.getResultMap();
                    // Show a permission alarm and clear step count if permissions are not acquired
                    if (resultMap.values().contains(Boolean.FALSE)) {
                        showPermissionAlarmDialog();
                    } else {
                        // Get the daily step count of a particular day and display it
                        mReporter.start(mStepCountObserver, getLastSyncDate());
                    }
                }
            };

    private StepCountReporter.StepCountObserver mStepCountObserver = count -> {
        Log.d(TAG, "Step reported : " + count);
        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String DEVICE_ID = "";

        for (int i = 0; i< ((ArrayList<HashMap<String, HashMap<String, Object>>>)count).size(); i++) {
            HashMap<String, HashMap<String, Object>> item = count.get(i);
            HashMap<String, Object> deviceItem = item.get("device");
            deviceItem.put("deviceId", DEVICE_ID);
        }

        this.nativeCallRnInMainActivity(count);

    };

    private void nativeCallRnInMainActivity(ArrayList<HashMap<String, HashMap<String, Object>>> count) {
        TSSBridgeModule m = MainApplication.bridgePackage.bridgeModule;
        Map<String,Object> map = new HashMap<String,Object>();
        if (count != null) {
            map.put("message", count);
        } else {
            ArrayList empty = new ArrayList();
            empty.add("NoAuth");
            map.put("message", empty);
        }
        map.put("name", "samsungHealth");
        m.nativeCallRn(map);
    }

    private Toast mToast;
    private Handler mHandler = new Handler(Looper.getMainLooper());


    private void toastTipMsg(int messageId) {
        if (mToast == null) {
            mToast = Toast.makeText(this, messageId, Toast.LENGTH_SHORT);
        }
        mToast.setText(String.valueOf(messageId));
        mToast.cancel();
        mHandler.removeCallbacks(mShowToastRunnable);
        mHandler.postDelayed(mShowToastRunnable, 100);
    }

    private void toastTipMsg(String message) {
        if (mToast == null) {
            mToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        }
        mToast.setText(message);
        mToast.cancel();
        mHandler.removeCallbacks(mShowToastRunnable);
        mHandler.postDelayed(mShowToastRunnable, 100);
    }

    private Runnable mShowToastRunnable = new Runnable() {
        @Override
        public void run() {
            mToast.show();
        }
    };

    /**
     * Below is used for Open Url
     * This is used to schedule rendering of the component.
     */
    public void openUrl(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    /**
     * Below is used for Back button event
     * This is used to schedule rendering of the component.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            //moveTaskToBack(true);
            TSSBridgeModule m = MainApplication.bridgePackage.bridgeModule;
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("message", "gobackWebView");
                    map.put("name", "gobackWebView");
                    m.nativeCallRn(map);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Save current acitvity
     *
     */
    public MainActivity() {
        mainActivity = this;
    }

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    private static MainActivity mainActivity;




    /**
     * Destory
     * This is used to schedule rendering of the component.
     */
    @Override
    protected void onDestroy() {
        mShowToastRunnable = null;
        mToast = null;

        //SHealth
        if (mStore != null) {
            mStore.disconnectService();
        }
        super.onDestroy();
    }

    public void start12HrTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(), TSSLongRunningServiceSamsung.class);
                getApplicationContext().startService(intent);
            }
        }, 20000);

    }

    public void queryHealthData() {
        String DEVICE_ID = "";
        String IMEI_ID = this.getSerialNumber(getApplicationContext());
        TSSHealthKitEntrance entrance = new TSSHealthKitEntrance();
        entrance.queryHealthData(this, MainActivity.this, DEVICE_ID + TSSHealthKitBridge.XVitalityProjectId);
    }

    // 获取Serial Number
    public static String getSerialNumber (Context context) {
        String SerialNumber = android.os.Build.SERIAL;
        return SerialNumber;
    }


    public void queryHealthData12() {
        String DEVICE_ID = "";
        TSSHealthKitEntrance entrance = new TSSHealthKitEntrance();
        entrance.queryHealthData(this, MainActivity.this, DEVICE_ID + TSSHealthKitBridge.XVitalityProjectId);
    }

    @Override
    public void queryHealthDataCallback(Map<String, Object> map) {
        TSSHealthKitBridge m = MainApplication.bridgeForHealthKit.bridgeModule;
        m.nativeCallRn(map);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == permissonAndroid) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                queryHealthData();
            } else {
                Map map = new HashMap();
                map.put("errorCode", "20002");
                map.put("errorMessage", "No Device Id");
                queryHealthDataCallback(map);
            }
        }
    }

    public void saveLastSyncDate() {
        TSSHealthKitEntrance entrance = new TSSHealthKitEntrance();
        entrance.saveLastSyncDate(this);
        //12Hr
        if (this.flag == null) {
            start12HrTimer();
            this.flag = "flag";
        }

    }

    public void isHealthDataAvailable() {
        TSSHealthKitAuth auth = new TSSHealthKitAuth();
        auth.isHealthDataAvailable(this, MainActivity.this);
    }

    @Override
    public void isHealthDataAvailableCallback(Boolean isAvailable) {
        TSSHealthKitBridge m = MainApplication.bridgeForHealthKit.bridgeModule;
        Map map = new HashMap();
        if (isAvailable) {
            map.put("returnCode", "20000");
            map.put("returnMessage", "Has Premission");
        } else  {
            map.put("errorCode", "10000");
            map.put("errorMessage", "No Premission");
        }
        m.nativeCallRn(map);
    }

    public static ReactApplicationContext getReactContext() {
        ReactInstanceManager mReactInstanceManager = getMainActivity().getReactNativeHost().getReactInstanceManager();
        ReactApplicationContext context = (ReactApplicationContext) mReactInstanceManager.getCurrentReactContext();
        return context;
    }
}
