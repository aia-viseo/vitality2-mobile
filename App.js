/**
 *
 * @format
 *
 */

// @flow

import React, {useContext} from 'react';

import CodePush from 'react-native-code-push';
import {NavigationContainer} from '@react-navigation/native';

import CodePushOptions from '@constants/CodePush';
import MainNavigation from '@navigations/MainNavigation';
import ActivityIndicator from '@atoms/ActivityIndicator';
import {AuthProvider} from '@contexts/Auth';
import {GlobalContext, GlobalProvider} from '@contexts/Global';

const App = () => {
  const {showActivityIndicator} = useContext(GlobalContext);

  return (
    <NavigationContainer>
      <GlobalProvider>
        <AuthProvider>
          <MainNavigation />
          {showActivityIndicator && <ActivityIndicator />}
        </AuthProvider>
      </GlobalProvider>
    </NavigationContainer>
  );
};

// eslint-disable-next-line no-undef
export default __DEV__ ? App : CodePush(CodePushOptions)(App);
