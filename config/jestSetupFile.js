import MockAsyncStorage from 'mock-async-storage';

const mockImpl = new MockAsyncStorage()
jest.mock('@react-native-community/async-storage', () => mockImpl);
jest.mock('react-native-reanimated', () => require('react-native-reanimated/mock'));
jest.mock('@dynatrace/react-native-plugin', () => {
  return {
    enterAction: () => {

    }
  }
});
