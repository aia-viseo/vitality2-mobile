/**
 *
 * Component Generator
 * @format
 *
 */

/* eslint strict: ["off"] */

'use strict';

const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Add an unconnected component (atoms, molecules, organisms, layouts)',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'Type the name of the component',
      default: 'Button',
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value) ? 'A component with this name already exists' : true;
        }

        return 'The name is required';
      },
    },
    {
      type: 'list',
      name: 'folder',
      message: 'Where do you want to keep this component?',
      default: 'atoms',
      choices: () => ['atoms', 'molecules', 'organisms', 'layouts'],
    },
  ],
  actions: () => {
    const actions = [
      {
        type: 'add',
        path: '../src/components/{{ folder }}/{{properCase name}}/index.js',
        templateFile: './component/index.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/components/{{ folder }}/{{properCase name}}/{{properCase name}}.js',
        templateFile: './component/stateless.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path:
          '../src/components/{{ folder }}/{{properCase name}}/__tests__/{{properCase name}}-test.js',
        templateFile: './component/test.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/components/{{ folder }}/{{properCase name}}/config/index.js',
        templateFile: './component/config.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/components/{{ folder }}/{{properCase name}}/styles/index.js',
        templateFile: './component/style.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/components/{{ folder }}/{{properCase name}}/types/index.js',
        templateFile: './component/types.js.hbs',
        abortOnFail: true,
      },
    ];

    return actions;
  },
};
