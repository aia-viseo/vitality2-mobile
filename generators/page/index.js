/**
 *
 * Page Generator
 * @format
 *
 */

/* eslint strict: ["off"] */

'use strict';

const pageExists = require('../utils/pageExists');

module.exports = {
  description: 'Add an unconnected page',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'Type the name of the page',
      default: 'Page',
      validate: (value) => {
        if (/.+/.test(value)) {
          return pageExists(value) ? 'A page with this name already exists' : true;
        }

        return 'The name is required';
      },
    },
  ],
  actions: () => {
    const actions = [
      {
        type: 'add',
        path: '../src/pages/{{properCase name}}/index.js',
        templateFile: './page/index.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/pages/{{properCase name}}/{{properCase name}}.js',
        templateFile: './page/stateless.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/pages/{{properCase name}}/__tests__/{{properCase name}}-test.js',
        templateFile: './page/test.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/pages/{{properCase name}}/config/index.js',
        templateFile: './page/config.js.hbs',
        abortOnFail: true,
      },
      {
        type: 'add',
        path: '../src/pages/{{properCase name}}/types/index.js',
        templateFile: './page/types.js.hbs',
        abortOnFail: true,
      },
    ];

    return actions;
  },
};
