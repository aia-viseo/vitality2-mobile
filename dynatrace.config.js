module.exports = {
  react : {
      debug : true,

      lifecycle : {
          /**
           * Decide if you want to see Update Cycles as well
           */
          includeUpdate: false,

          /**
           * Filter for Instrumenting Lifecycle of Components / True = Will be instrumented
           */
          instrument: (filename) => {
              return false;
          }
      },

      input : {
          /**
           * Allows you to filter the instrumentation for touch events, refresh events and picker events in certain files
           * True = Will be instrumented
           */
          instrument: (filename) => {
              return true;
          }
      }
  },
  android : {
      // Those configs are copied 1:1
      config : `
      dynatrace {
          configurations {
              defaultConfig {
                  autoStart {
                      applicationId '1dc02a31-5bd9-4081-bdc2-ad843492d4ee'
                      beaconUrl 'https://bf19066hmd.bf.dynatrace.com/mbeacon'
                  }
              }
          }
      }
      `
  },
  ios : {
      // Those configs are copied 1:1
      config : `
      <key>DTXApplicationID</key>
      <string>1dc02a31-5bd9-4081-bdc2-ad843492d4ee</string>
      <key>DTXBeaconURL</key>
      <string>https://bf19066hmd.bf.dynatrace.com/mbeacon</string>
      <key>DTXLogLevel</key>
      <string>ALL</string>
      <key>DTXUserOptIn</key>
      <true/>
      `
  }
}
