//
//  TSSBridgeModule.m
//  VitalityWrapper
//
//  Created by Hamish on 2018/4/23.
//  Copyright © 2018年 TSS. All rights reserved.
//

#import "TSSBridgeModule.h"
#import "TSSTouchIDTool.h"
#import "Reachability.h"
#import "TSSHealthKit.h"
#import "TSSUtil.h"
#import "TSSKeyChain.h"
#import "TSSMaskView.h"
#import "AppDelegate.h"
#import <sys/utsname.h>
@interface TSSBridgeModule ()
@property (nonatomic, strong) NSString *flag;
@end

@implementation TSSBridgeModule
@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(verifyFingerPrint:(NSString *)name block:(RCTResponseSenderBlock)callback) {
  //key in dictionary is message state type

    [TSSTouchIDTool fingerprintVerify:^(NSDictionary *param) {
      callback(@[[NSNull null], param]);
    }];


}

RCT_EXPORT_METHOD(getIphoneFingerOrFaceBolck:(RCTResponseSenderBlock)callback){
  struct utsname systemInfo;
  uname(&systemInfo);
  NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
  NSArray *fingerPrintDevice = @[@"iPhone3,1",@"iPhone3,2",@"iPhone3,3",@"iPhone4,1",@"iPhone5,1",@"iPhone5,2",@"iPhone5,3",@"iPhone5,4",@"iPhone6,1",@"iPhone6,2",@"iPhone7,1",@"iPhone7,2",@"iPhone8,1",@"iPhone8,2",@"iPhone8,4",@"iPhone9,1",@"iPhone9,2",@"iPhone9,3",@"iPhone9,4",@"iPhone10,1",@"iPhone10,4",@"iPhone10,2",@"iPhone10,5"];
  NSInteger index = [fingerPrintDevice indexOfObject:deviceModel];
  if (index <= 22 && 0<=index) {
    callback(@[[NSNull null], @"finger"]);
  }else{
    callback(@[[NSNull null], @"face"]);
  }
}

RCT_EXPORT_METHOD(onlyCheckSupportFinger:(RCTResponseSenderBlock)callback){
  [TSSTouchIDTool onlyCheckSupportFinger:^(NSString *param) {
    callback(@[[NSNull null], param]);
  }];
}

RCT_EXPORT_METHOD(firstLoginSaveTheFingerState:(RCTResponseSenderBlock)callback){
  [TSSTouchIDTool firstLoginSaveTheFingerState:^(NSString *param) {
    callback(@[[NSNull null], param]);
  }];
}

RCT_EXPORT_METHOD(getDeviceID:(NSString *)name block:(RCTResponseSenderBlock)callback) {
  NSString *strIDFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
  NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
  NSString *deviceid = [userDefault objectForKey:@"deviceID"];
  if (deviceid != nil) {
    strIDFV = deviceid;
  }else{
    [userDefault setObject:strIDFV forKey:@"deviceID"];
    [userDefault synchronize];
  }
  callback(@[[NSNull null],strIDFV]);
}

RCT_EXPORT_METHOD(internetStatus:(RCTResponseSenderBlock)callback) {
  Reachability *reachability   = [Reachability reachabilityWithHostName:@"www.apple.com"];
  NetworkStatus internetStatus = [reachability currentReachabilityStatus];
  NSString *net = @"NOT";
  switch (internetStatus) {
    case ReachableViaWiFi:
      net = @"WIFI";
      break;
    case ReachableViaWWAN:
      net = @"WAN";
      break;
    case NotReachable:
      net = @"NOT";
    default:
      break;
  }
  NSDictionary *param = @{@"internetStatus": net};
  callback(@[[NSNull null], param]);
}

RCT_EXPORT_METHOD(saveUploadDate) {
  [TSSUtil savedataInPlistfileWithData:@{@"uploadDate":[NSDate date]}];
  if (![self.flag isEqualToString:@"flag"]) {
    [[[TSSHealthKit alloc] init] setupBackgroundUpload];
    self.flag = @"flag";
  }
}

RCT_EXPORT_METHOD(loginSucessfullAndChangeTheLoadingImage:(NSString *)changesuccess block:(RCTResponseSenderBlock)callback){

  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    for (UIView* view in delegate.window.subviews) {
      if ([view isKindOfClass:[TSSMaskView class]]) {
        [(TSSMaskView *)view loginSuccessChangeImage];
        callback(@[[NSNull null],@{@"key":@"success"}]);
      }
    }

  });

}

RCT_EXPORT_METHOD(loginSuccessRemoveMask){
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    for (UIView* view in delegate.window.subviews) {
      if ([view isKindOfClass:[TSSMaskView class]]) {
        [(TSSMaskView *)view removeFromSuperview];
      }
    }
  });
}

RCT_EXPORT_METHOD(saveFingerState:(NSString *)name block:(RCTResponseSenderBlock)callback){
  if ([name isEqualToString:@"loginsuccess"]) {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([userDefault objectForKey:@"fingerData"] != nil) {
      [userDefault removeObjectForKey:@"fingerData"];
    }
  }
  //  NSArray *languageArry = [NSLocale preferredLanguages];
  //  NSString *currentLanguage = [languageArry objectAtIndex:0];
  callback(@[[NSNull null] , @{@"key":@"success"}]);
}

RCT_EXPORT_METHOD(removeUploadDate) {
  [TSSUtil removedataInPlistfile];
}

RCT_EXPORT_METHOD(removeCachePdfFiles:(NSString *)fileName) {
  [TSSUtil removeDownloadPdfFilesFromCache:fileName];
}

RCT_EXPORT_METHOD(setupBackgroundUpload) {
  [[[TSSHealthKit alloc] init] setupBackgroundUpload];
}

RCT_EXPORT_METHOD(verifyAppleHealth:(NSString *)name stringByAppendingString:(NSString *)stringByAppendingString tenantId:(NSString *)tenantId callback:(RCTResponseSenderBlock)callback) {
  [[[TSSHealthKit alloc] init] getIphoneHealthData:stringByAppendingString tenantId:tenantId callback:^(NSArray *param){
    callback(@[[NSNull null], param]);
  }];
}

RCT_EXPORT_METHOD(getEncryptKey:(NSString *)name block:(RCTResponseSenderBlock)callback) {
  NSString *MasterValue = [TSSKeyChain getMasterValue:name];
  callback(@[[NSNull null], @{@"key": MasterValue}]);
}

RCT_EXPORT_METHOD(verifyOpenUrl:(NSString *)url block:(RCTResponseSenderBlock)callback) {
  NSURL *openUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@", url]];
  [[UIApplication sharedApplication] openURL:openUrl];
}

//iosOpenSettingApp
RCT_EXPORT_METHOD(iosOpenSettingApp) {
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//  NSString *version = [UIDevice currentDevice].systemVersion;
//  NSURL *url = [NSURL URLWithString:@"App-Prefs:root=WIFI"];
//  if (version.doubleValue >= 10.0) {
//    // 针对 10.0 以上的iOS系统进行处理
//      [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
//  } else {
//    // 针对 10.0 以下的iOS系统进行处理
//    if ([[UIApplication sharedApplication] canOpenURL:url]) {
//      [[UIApplication sharedApplication] openURL:url];
//    }
//  }
}


// The list of available events
- (NSArray<NSString *> *)supportedEvents {
  return @[@"EventReminder", @"fingerPrint", @"FingerPrintLoginIsAllowed", @"PushNotification", @"PushNotificationMessage", @"sendHelathData"];
}

// This function listens for the events we want to send out and will then pass the
// payload over to the emitEventInternal function for sending to Javascript
- (void)startObserving {
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(emitEventInternal:)
                                               name:@"event-emitted"
                                             object:nil];
}

// This will stop listening if we require it
- (void)stopObserving {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// This will actually throw the event out to our Javascript
- (void)emitEventInternal:(NSNotification *)notification {
  // We will receive the dictionary here - we now need to extract the name
  // and payload and throw the event
  NSArray *eventDetails = [notification.userInfo valueForKey:@"detail"];
  NSString *eventName = [eventDetails objectAtIndex:0];
  // the key value is defined in AppDelegate, in payload dictionary
  if ([eventName isEqualToString:@"PushNotificationMessage"]) {
    NSDictionary *eventMessage = [[eventDetails objectAtIndex:1] objectForKey:@"message"];
    [self sendEventWithName:eventName body:@{@"name": eventName, @"message": eventMessage}];
  } else if ([eventName isEqualToString:@"PushNotification"]) {
    NSDictionary *eventMessage = [[eventDetails objectAtIndex:1] objectForKey:@"message"];
    NSDictionary *eventUUid = [[eventDetails objectAtIndex:1] objectForKey:@"uuid"];
    [self sendEventWithName:eventName body:@{@"name": eventName, @"message": eventMessage, @"uuid":eventUUid}];
  } else if ([eventName isEqualToString:@"sendHelathData"]) {
    [self sendEventWithName:eventName body:@{@"name": eventName, @"message": @"appleHealth"}];
  }
}

// This is our static function that we call from our code
+ (void)emitEventWithName:(NSString *)name andPayload:(NSDictionary *)payload {
  // userInfo requires a dictionary so we wrap out name and payload into an array and stick
  // that into the dictionary with a key of 'detail'
  NSDictionary *eventDetail = @{@"detail":@[name,payload]};
  [[NSNotificationCenter defaultCenter] postNotificationName:@"event-emitted"
                                                      object:self
                                                    userInfo:eventDetail];
}

@end
