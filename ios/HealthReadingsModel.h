//
//  healthReadingsModel.h
//  VitalityWrapper
//
//  Created by Wind on 2018/8/23.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthReadingsModel : NSObject
@property (nonatomic,strong) NSString *manufacturer;
@property (nonatomic,strong) NSString *endTime;
@property (nonatomic,strong) NSString *notes;
@property (nonatomic,strong) NSString *model;
@property (nonatomic,strong) NSString *partnerReadingId;
@property (nonatomic,strong) NSString *partnerCreateDate;
@property (nonatomic,strong) NSString *integrity;
@property (nonatomic,strong) NSString *readingType;
@property (nonatomic,strong) NSString *startTime;
@property (nonatomic,strong) NSString *dataCategory;
@property (nonatomic,strong) NSString *source;
@property (nonatomic,strong) NSMutableDictionary *workout;
@end
