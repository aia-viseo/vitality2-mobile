//
//  TSSKeyChain.h
//  VitalityWrapper
//
//  Created by Wind on 2018/7/30.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Security/Security.h"

@interface TSSKeyChain : NSObject
+ (void)save:(NSString *)service data:(id)data;
+ (id)get:(NSString *)service;
+ (void)delete:(NSString *)service;
+ (NSString *)getMasterValue:(NSString *)service;
@end
