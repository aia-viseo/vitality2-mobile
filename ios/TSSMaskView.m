//
//  TSSMaskView.m
//  VitalityWrapper
//
//  Created by kenneth ye on 2019/10/25.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "TSSMaskView.h"

@implementation TSSMaskView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor grayColor];
    self.alpha = .8;
    
    [self addSubview:[self logoLoadingImage]];
    
  }
  return self;
}
-(void)loginSuccessChangeImage{
  [self addSubview:[self finishloading]];
}

-(UIView *)logoLoadingImage{
  UIView *maskView = [[UIView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-112)/2, ([UIScreen mainScreen].bounds.size.height-112)/2, 112, 112)];
  maskView.backgroundColor = [UIColor whiteColor];
  maskView.layer.cornerRadius = 8;
  UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(16, 16, 80, 80)];
  //  [logoImage setImage:[UIImage imageNamed:@"aiaRed"]];
  
  NSURL *url = [[NSBundle mainBundle] URLForResource:@"aia_animated_logo.gif" withExtension:nil];
  CGImageSourceRef csf = CGImageSourceCreateWithURL((__bridge CFTypeRef)url, NULL);
  size_t const count = CGImageSourceGetCount(csf);
  UIImage *frames[count];
  CGImageRef images[count];
  for (size_t i = 0; i < count; ++i) {
    images[i] = CGImageSourceCreateImageAtIndex(csf, i, NULL);
    UIImage *image =[[UIImage alloc] initWithCGImage:images[i]];
    frames[i] = image;
    CFRelease(images[i]);
  }
  UIImage *const animation = [UIImage animatedImageWithImages:[NSArray arrayWithObjects:frames count:count] duration:2.0]; // 可以设置gif的播放时间
  [logoImage setImage:animation];
  CFRelease(csf);
  
  
  [maskView addSubview:logoImage];
  return maskView;
}

// the login in loading
-(UIView *)finishloading{
  UIView *view = [[UIView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-112)/2, ([UIScreen mainScreen].bounds.size.height-112)/2, 112, 112)];
  view.layer.cornerRadius = 8;
  view.backgroundColor = [UIColor whiteColor];
  UIImageView *finishImage = [[UIImageView alloc]initWithFrame:CGRectMake(28, 16, 56, 56)];
  [finishImage setImage:[UIImage imageNamed:@"accept"]];
  [view addSubview:finishImage];
  UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, 112, 24)];
  label.text = @"Logged in";
  label.textColor = [UIColor grayColor];
  label.textAlignment = NSTextAlignmentCenter;
  label.font = [UIFont systemFontOfSize:15];
  [view addSubview:label];
  return view;
}
@end
