//
//  TSSTouchID.m
//  VitalityWrapper
//
//  Created by 刘洋 on 2018/5/29.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "TSSTouchID.h"


@implementation TSSTouchID
+ (instancetype)sharedInstance {
  static TSSTouchID *instance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance = [[TSSTouchID alloc] init];
  });
  return instance;
}

-(void)firstLoginSaveTheFingerState{
  LAContext *context = [[LAContext alloc]init];
  context.localizedFallbackTitle = @"";
  NSError *error = nil;
  if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:context.evaluatedPolicyDomainState forKey:@"fingerData"];
    [userDefault synchronize];
  }
}

-(void)onlyCheckSupportFingerPoint:(StateBlock)block{
  if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_8_0) {
    
    dispatch_async(dispatch_get_main_queue(), ^{
      NSLog(@"Require iOS 8.0 or higher to support TouchID.");
      block(TDTouchIDStateVersionNotSupport,nil);
    });
    
    return;
  }
  LAContext *context = [[LAContext alloc]init];
  context.localizedFallbackTitle = @"";
  NSError *error = nil;
  if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
    dispatch_async(dispatch_get_main_queue(), ^{
      NSLog(@"Require iOS 8.0 or higher to support TouchID.");
      block(ApproveFinger,nil);
    });
  }else{
    dispatch_async(dispatch_get_main_queue(), ^{
      NSLog(@"Require iOS 8.0 or higher to support TouchID.");
      block(TDTouchIDStateVersionNotSupport,nil);
    });
  }
  
}



-(void)td_showTouchIDWithDescribe:(NSString *)desc BlockState:(StateBlock)block{
  
  if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_8_0) {
    
    dispatch_async(dispatch_get_main_queue(), ^{
      NSLog(@"Require iOS 8.0 or higher to support TouchID.");
      block(TDTouchIDStateVersionNotSupport,nil);
    });
    
    return;
  }
  
  LAContext *context = [[LAContext alloc]init];
  context.localizedFallbackTitle = @"";
  
  
  NSError *error = nil;
  
  if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
    
    
    //if fingerpoint was changed need alert user re-login
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *fingerData = [userDefault objectForKey:@"fingerData"];
    if (fingerData != nil) {
      if (![fingerData isEqualToData: context.evaluatedPolicyDomainState]) {
        dispatch_async(dispatch_get_main_queue(), ^{
          NSLog(@"fingerpoint changed the password");
          block(ChangedFinger,error);
        });

        return;
      }
    }else{
      [userDefault setObject:context.evaluatedPolicyDomainState forKey:@"fingerData"];
      [userDefault synchronize];
    }
    
    
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:desc == nil ? @"Press HOME to verify fingerprint":desc reply:^(BOOL success, NSError * _Nullable error) {
      
      if (success) {
        dispatch_async(dispatch_get_main_queue(), ^{
          NSLog(@"TouchID verified successfully.");
          block(TDTouchIDStateSuccess,error);
        });
      }else if(error){
        
        switch (error.code) {
          case LAErrorAuthenticationFailed:{ //3 error
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"TouchID verified failure.");
              block(TDTouchIDStateFail,error);
            });
            break;
          }
          case LAErrorUserCancel:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"User cancelled TouchID.");
              block(TDTouchIDStateUserCancel,error);
            });
          }
            break;
          case LAErrorUserFallback:{ //exit
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"User input password.");
              block(TDTouchIDStateInputPassword,error);
            });
          }
            break;
          case LAErrorSystemCancel:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"TouchID is cancelled due to incoming call, screen-lock or pressed HOME.");
              block(TDTouchIDStateSystemCancel,error);
            });
          }
            break;
          case LAErrorPasscodeNotSet:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"User doesn't set password for TouchID.");
              block(TDTouchIDStatePasswordNotSet,error);
            });
          }
            break;
          case LAErrorTouchIDNotEnrolled:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"User doesn't enable TouchID.");
              block(TDTouchIDStateTouchIDNotSet,error);
            });
          }
            break;
          case LAErrorTouchIDNotAvailable:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"TouchID invalid.");
              block(TDTouchIDStateTouchIDNotAvailable,error);
            });
          }
            break;
          case LAErrorTouchIDLockout:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"TouchID lockout due to max error reached.");
              block(TDTouchIDStateTouchIDLockout,error);
            });
          }
            break;
          case LAErrorAppCancel:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"App enter background.");
              block(TDTouchIDStateAppCancel,error);
            });
          }
            break;
          case LAErrorInvalidContext:{
            dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"App enter background (invalid LAContext).");
              block(TDTouchIDStateInvalidContext,error);
            });
          }
            break;
          default:
            NSLog(@"No TouchID found :%d",error.code);
            break;
        }
      }
    }];
  
  }else{
    
    dispatch_async(dispatch_get_main_queue(), ^{
      NSLog(@"Device doesn't support TouchID");
      block(TDTouchIDStateNotSupport,error);
    });
    
  }
  
  
}


@end
