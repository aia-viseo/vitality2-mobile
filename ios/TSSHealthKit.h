//
//  TSSHealthKit.h
//  VitalityWrapper
//
//  Created by Hamish on 2018/4/26.
//  Copyright © 2018年 TSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSSHealthKit : NSObject

- (void)getIphoneHealthData:(NSString *)stringByAppendingString tenantId:(NSString *)tenantId callback:(void (^)(NSArray *))finish;
- (void)setupBackgroundUpload;

@end
