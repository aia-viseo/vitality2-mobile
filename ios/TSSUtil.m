//
//  TSSUtil.m
//  VitalityWrapper
//
//  Created by Wind on 2018/8/28.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "TSSUtil.h"
#import<objc/runtime.h>
#import "TSSBridgeModule.h"

@interface TSSUtil ()
@property (nonatomic, strong) dispatch_source_t timer;
@end

@implementation TSSUtil

+ (NSString *)repeatNull:(NSString *)str {
  if (str == nil || [str isKindOfClass:[NSNull class]] || str.length == 0) {
    return @"";
  } else {
    return str;
  }
}

+ (int)compareDate:(NSString*)date01 withDate:(NSString*)date02 {
  int ci;
  NSDateFormatter *df = [[NSDateFormatter alloc]init];
  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
  [df setCalendar: [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
  [df setLocale:enUSPOSIXLocale];
  [df setTimeZone:[NSTimeZone systemTimeZone]];
  [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
  NSDate *dt1 = [[NSDate alloc] init];
  NSDate *dt2 = [[NSDate alloc] init];
  dt1 = [df dateFromString:date01];
  dt2 = [df dateFromString:date02];
  NSComparisonResult result = [dt1 compare:dt2];
  switch (result) {
      //date02比date01大
    case NSOrderedAscending:
      ci=1;
      break;
      //date02比date01小
    case NSOrderedDescending:
      ci=-1;
      break;
      //date02=date01
    case NSOrderedSame:
      ci=0;
      break;
    default: NSLog(@"erorr dates %@, %@", dt2, dt1);
      break;
  }
  return ci;
}

+(id)readThePlistfile {
  NSString *cachePatch = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
  NSString *filePath = [cachePatch stringByAppendingPathComponent:@"health.plist"];
  NSDictionary *products = [NSDictionary dictionaryWithContentsOfFile:filePath];
  return products;
}

+ (void)savedataInPlistfileWithData:(NSDictionary *)data {
  NSString *cachePatch = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
  NSString *filePath = [cachePatch stringByAppendingPathComponent:@"health.plist"];
  NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
  [data writeToURL:fileUrl atomically:YES];
}

+ (void)removedataInPlistfile {
  NSFileManager *fileMger = [NSFileManager defaultManager];
  NSString *health = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"health.plist"];
  BOOL bRet = [fileMger fileExistsAtPath:health];
  if (bRet) {
    NSError *err;
    [fileMger removeItemAtPath:health error:&err];
  }
}

+ (void)removeDownloadPdfFilesFromCache:(NSString *)fileName {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSString * cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
  NSLog(@"%@",cachePath);
  NSDirectoryEnumerator *fileEnumerator = [fileManager enumeratorAtPath:cachePath];
  for (NSString *fileNameCache in fileEnumerator) {
    if([fileName containsString: fileNameCache]){
      NSLog(@"%@",fileNameCache);
      NSString *filePath = [cachePath stringByAppendingPathComponent:fileNameCache];
      [fileManager removeItemAtPath:filePath error:nil];
    }
  }
}

- (void) startTimer {
  __block int count = 3;
  dispatch_queue_t queue = dispatch_get_main_queue();
  if (self.timer) {
    dispatch_cancel(self.timer);
    self.timer = nil;
  } else {
    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
  }
  // set timer
  dispatch_time_t start = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
  uint64_t interval = (uint64_t)(43200.0 * NSEC_PER_SEC);
  dispatch_source_set_timer(self.timer, start, interval, 0);
  // call back
  dispatch_source_set_event_handler(self.timer, ^{
    count--;
    dispatch_async(dispatch_get_main_queue(), ^{
      if (count != 2) {
        [TSSBridgeModule emitEventWithName:@"sendHelathData" andPayload:@{@"message":@""}];
      }
      if (count == 0) {
        // remove timer
        dispatch_cancel(self.timer);
        self.timer = nil;
        // TODO
      } else {
        // TODO
      }
    });
  });
  // start timer
  dispatch_resume(self.timer);
}

- (void)stopTime {
  if (self.timer) {
    dispatch_cancel(self.timer);
    self.timer = nil;
  }
}

@end
