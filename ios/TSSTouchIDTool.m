//
//  TSSTouchIDTool.m
//  VitalityWrapper
//
//  Created by 刘洋 on 2018/5/29.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import "TSSTouchIDTool.h"
#import <UIKit/UIKit.h>
#import "TSSTouchID.h"
#import "AppDelegate.h"
#import "TSSMaskView.h"
#import <sys/utsname.h>
#define Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#ifdef DEBUG
#define SIWLog(...) NSLog(__VA_ARGS__)
#else
#define SIWLog(...)
#endif


@implementation TSSTouchIDTool

+(void)firstLoginSaveTheFingerState:(void (^)(NSString *))finish{
  TSSTouchID *touchID = [[TSSTouchID alloc]init];
  [touchID firstLoginSaveTheFingerState];
  finish(@"");
}

+(void)onlyCheckSupportFinger:(void (^)(NSString *))finish{
  TSSTouchID *touchID = [[TSSTouchID alloc]init];
  [touchID onlyCheckSupportFingerPoint:^(TDTouchIDState state, NSError *error) {
    if (state == ApproveFinger) {
      finish(@"support");
    }else if(state == TDTouchIDStateVersionNotSupport){
      UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
      
      NSString *alermessage = @"Please enable Touch ID on your device";
      NSString *titleHeader = @"Touch ID";
      struct utsname systemInfo;
      uname(&systemInfo);
      NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
      NSArray *fingerPrintDevice = @[@"iPhone3,1",@"iPhone3,2",@"iPhone3,3",@"iPhone4,1",@"iPhone5,1",@"iPhone5,2",@"iPhone5,3",@"iPhone5,4",@"iPhone6,1",@"iPhone6,2",@"iPhone7,1",@"iPhone7,2",@"iPhone8,1",@"iPhone8,2",@"iPhone8,4",@"iPhone9,1",@"iPhone9,2",@"iPhone9,3",@"iPhone9,4",@"iPhone10,1",@"iPhone10,4",@"iPhone10,2",@"iPhone10,5"];
      NSInteger index = [fingerPrintDevice indexOfObject:deviceModel];
      if (index <= 22 && 0<=index) {
        alermessage = @"Please enable Touch ID on your device";
        titleHeader = @"Touch ID";
      }else{
        alermessage = @"Please enable Face ID on your device";
        titleHeader = @"Face ID";
      }
      
      
      
      UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleHeader message:alermessage preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
      UIAlertAction *centain = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
      }];
      [alert addAction:cancel];
      [alert addAction:centain];
      [rootViewController presentViewController:alert animated:NO completion:nil];
      finish(@"notsupport");
    }
  }];
}


+(void)fingerprintStatueUseAccountLogin:(void (^)(NSDictionary *))finish{
  TSSTouchID *touchID = [[TSSTouchID alloc] init];
  NSString *na = @"accountlogin";
  [touchID td_showTouchIDWithDescribe:na BlockState:^(TDTouchIDState state, NSError *error) {
    
  }];
}

// hamish VerifyFingerprint
+(void)fingerprintVerify:(void (^)(NSDictionary *))finish{
  TSSTouchID *touchID = [[TSSTouchID alloc] init];
  //    NSString *des = @"Please verify the existing fingerprint for login";
  NSString *des = @"Enter Touch/Face ID to login.";
  [touchID td_showTouchIDWithDescribe:des BlockState:^(TDTouchIDState state, NSError *error) {
    
    NSString *message = @"Verify Touch/Face ID failed, switching to ID & password login.";
    if (state == ChangedFinger) {
#pragma mark -- if changed the fingerpoint will alert and login use account
      NSLog(@"changefinger");
      UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
      UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"There is a change in your device's fingerprint settings.For security reasons,your fingerprint login has been disabled. Please log in with your credentials to proceed." preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *centain = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
      [alert addAction:centain];
      [rootViewController presentViewController:alert animated:NO completion:nil];
    }else if (state == TDTouchIDStateNotSupport) {
      message = @"Touch/Face ID isn't valid now.";
    } else if (state == TDTouchIDStateSuccess) {
#pragma mark -- if verify success wait for use login should show the gray view
      //      [[[TSSTouchIDTool alloc]init] verifyFingerprintSuccessShowTheLoading];
      AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
      TSSMaskView *maskView = [[TSSMaskView alloc]initWithFrame:[UIScreen mainScreen].bounds];
//      [delegate.window addSubview:maskView];
      message = @"success";
    } else if (state == TDTouchIDStateInputPassword) {
      message = @"User switch to password input.";
    } else if (state == TDTouchIDStateTouchIDLockout){
      if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        TSSTouchID *touchIDs = [[TSSTouchID alloc] init];
        [touchIDs evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:@"Enter your Touch/Face ID" reply:^(BOOL success, NSError * _Nullable error) {
          //NSLog(@"success:%d,error:%d",success,error.code);
          dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"message"]= @"Touch/Face ID is invalid now, switch to ID & password login.";
            dic[@"state"] = @(0);
            finish(dic);
          });
          
        }];
        return;
      }
      
      message = @"Touch/Face ID is invalid now, switch to ID & password login.";
    } else if (state == TDTouchIDStateUserCancel) {
      return;
    }
    
    int stateCode;
    if (state == TDTouchIDStateSuccess){
      stateCode = 1;
    }
    else if (state == TDTouchIDStateUserCancel){
      stateCode = 3;
    }
    else if (state == TDTouchIDStateNotSupport){
      stateCode = 4;
    
      UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
      UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Touch ID" message:@"Please enable Touch ID your device" preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
      UIAlertAction *centain = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
      }];
      [alert addAction:cancel];
      [alert addAction:centain];
      [rootViewController presentViewController:alert animated:NO completion:nil];
    }
    else{
      stateCode = 2;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"message"]= message;
    dic[@"state"] = @(stateCode);
    finish(dic);
  }];
}

//touchIDIsFpEnabled Action_fPStatus
+(void)fingerprintStatue:(void (^)(NSDictionary *))finish{
  TSSTouchID *la = [[TSSTouchID alloc] init];
  
  NSError *error;
  [la canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
  NSInteger statue = 0;
  NSString *message = @"Device doesn't support Touch/Face ID.";
  switch (error.code) {
    case LAErrorAuthenticationFailed:
      NSLog(@"TouchID verifying failed.");
      break;
    case LAErrorUserCancel:
      NSLog(@"TouchID is cancelled by user.");
      break;
    case LAErrorUserFallback:
      NSLog(@"User choose to input password.");
      break;
    case LAErrorSystemCancel:
      NSLog(@"TouchID is cancelled due to incoming call, screen lockout or HOME pressing.");
      break;
    case LAErrorPasscodeNotSet:
      statue = 1;
      message = @"User didn't enable Touch/Face ID in system";
      NSLog(@"No password set for TouchID.");
      break;
    case LAErrorTouchIDNotEnrolled:
      NSLog(@"TouchID isn't configured.");
      break;
    case LAErrorTouchIDNotAvailable:
      NSLog(@"TouchID invalid.");
      break;
    case LAErrorTouchIDLockout:
      NSLog(@"TouchID is lockout due to max error count reached.");
      break;
    case LAErrorAppCancel:
      NSLog(@"App enter background.");
      break;
    case LAErrorInvalidContext:
      NSLog(@"Authorization is cancelled (Invalid LAContext)");
      
      break;
    default:{
      /*
      SIWKeyChainStore *keychainStore = [SIWKeyChainStore sharedKeyChain];
      NSString *pwd = [keychainStore stringForKey:@"pwd" shouldDecrpyt:YES];
      if (pwd&&![pwd isEqualToString:@""]) {
        statue = 3;
        message = @"User has already enabled Touch/Face ID login.";
      }else{
        statue = 2;
        message = @"User didn't enable Touch/Face ID login.";
      }*/
    }
      break;
  }
  NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  dic[@"message"]= message;
  dic[@"state"] = @(statue);
  dic[@"type"] = Is_iPhoneX ?  @"Face ID" : @"Touch ID";
  
  finish(dic);
}

@end
