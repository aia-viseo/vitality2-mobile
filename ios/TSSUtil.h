//
//  TSSUtil.h
//  VitalityWrapper
//
//  Created by Wind on 2018/8/28.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSSUtil : NSObject
+ (int)compareDate:(NSString*)date01 withDate:(NSString*)date02;
+ (id)readThePlistfile;
+ (void)savedataInPlistfileWithData: (NSDictionary *)data;
+ (void)removedataInPlistfile;
+ (void)removeDownloadPdfFilesFromCache:(NSString*)fileName;
- (void)startTimer;
- (void)stopTime;
+ (NSString *)repeatNull:(NSString *)value;
@end
