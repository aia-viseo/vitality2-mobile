//
//  TSSBridgeModule.h
//  VitalityWrapper
//
//  Created by Hamish on 2018/4/23.
//  Copyright © 2018年 TSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTEventEmitter.h>

@interface TSSBridgeModule : RCTEventEmitter<RCTBridgeModule>

+ (void)emitEventWithName:(NSString *)name andPayload:(NSDictionary *)payload;

@end
