//
//  TSSTouchIDTool.h
//  VitalityWrapper
//
//  Created by 刘洋 on 2018/5/29.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSSTouchIDTool : NSObject

+(void)fingerprintVerify:(void (^) (NSDictionary *param))finish;
+(void)fingerprintStatue:(void (^)(NSDictionary *param))finish;
+(void)fingerprintStatueUseAccountLogin:(void (^)(NSDictionary *))finish;

+(void)onlyCheckSupportFinger:(void (^)(NSString *param))finish;
+(void)firstLoginSaveTheFingerState:(void (^)(NSString *param))finish;
@end
