//
//  healthDataModel.h
//  VitalityWrapper
//
//  Created by Wind on 2018/8/23.
//  Copyright © 2018年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthDataModel : NSObject
@property (nonatomic,strong) NSString *endTime;
@property (nonatomic,strong) NSString *startTime;
@property (nonatomic,strong) NSString *source;
@property (nonatomic,strong) NSString *value;
@property (nonatomic,strong) NSString *average;
@property (nonatomic,strong) NSString *maximum;
@property (nonatomic,strong) NSString *minimum;
@property (nonatomic,strong) NSString *unitOfMeasurement;

+ (NSString *)getWorkoutTypeStrWithNumber:(NSInteger)num;

@end
