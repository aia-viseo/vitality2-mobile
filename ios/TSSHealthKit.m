//
//  TSSHealthKit.m
//  VitalityWrapper
//
//  Created by Hamish on 2018/4/26.
//  Copyright © 2018年 TSS. All rights reserved.
//

#import "TSSHealthKit.h"
#import <HealthKit/HealthKit.h>
#import "TSSKeyChain.h"
#import "HealthDataModel.h"
#import "HealthReadingsModel.h"
#import "MJExtension.h"
#import "TSSUtil.h"
#define KEY_UUID @"MYAIAUUID"
#define KEY_STARTDATE @"HKSTARTDATE"
#import "TSSBridgeModule.h"

@interface TSSHealthKit()
//ROUTINE
@property (nonatomic, strong) NSMutableArray *healthSteps;
@property (nonatomic, strong) NSMutableArray *healthDistances;
@property (nonatomic, strong) NSMutableArray *healthBasalCalories;
@property (nonatomic, strong) NSMutableArray *healthActiveCalories;
@property (nonatomic, strong) NSMutableArray *healthHeartRate;
@property (nonatomic, strong) NSMutableArray *sleeps;
//FITNESS
@property (nonatomic, strong) NSMutableArray *workoutHeartRate;
@property (nonatomic, strong) NSMutableArray *healthWorkout;
@property (nonatomic, strong) HKHealthStore *store;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *startCalculateDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) NSString *appleDeviceName;
@property (nonatomic, strong) NSDate *anchorDate;
@property (nonatomic, strong) NSPredicate *predicate;

@property (nonatomic, strong) dispatch_group_t disGroup;
@property (nonatomic, strong) dispatch_queue_t queue;

@end

@implementation TSSHealthKit

+ (TSSHealthKit *)shareInstance{
  static TSSHealthKit * tss_instance_singleton = nil ;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    if (tss_instance_singleton == nil) {
      tss_instance_singleton = [[TSSHealthKit alloc] init];
    }
  });
  return (TSSHealthKit *)tss_instance_singleton;
}

- (NSSet *)getQuantityTypeData{
  HKQuantityType *step = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
  HKQuantityType *distance = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
  HKQuantityType *activeEB = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
  HKSampleType *sleep = [HKSampleType categoryTypeForIdentifier:HKCategoryTypeIdentifierSleepAnalysis];

  if (@available(iOS 11.0, *)) {
    HKQuantityType *heartRate = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    return [NSSet setWithObjects:step, sleep, distance, heartRate, activeEB, [HKObjectType workoutType], nil];
  } else {
    return [NSSet setWithObjects:step, sleep, distance, activeEB, [HKObjectType workoutType], nil];
  }
}

- (void)setupBackgroundUpload {
  self.store = [[HKHealthStore alloc] init];
  HKQuantityType *step = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
  NSSet *getQuantityTypeData = [NSSet setWithObjects:step, nil];

  for (HKObjectType *type in getQuantityTypeData) {
    __block long count = 3;
    HKObserverQuery *observerQuery = [[HKObserverQuery alloc] initWithSampleType:type predicate:nil updateHandler:^(HKObserverQuery * _Nonnull query, HKObserverQueryCompletionHandler  _Nonnull completionHandler, NSError * _Nullable error) {
      if (count != 3 ) {
        [TSSBridgeModule emitEventWithName:@"sendHelathData" andPayload:@{@"message":@""}];
      }
      count--;
      if (completionHandler) {
        completionHandler();
      }
    }];
    [self.store executeQuery:observerQuery];
    [self.store enableBackgroundDeliveryForType:type frequency:HKUpdateFrequencyHourly withCompletion:^(BOOL success, NSError * _Nullable error) {
      if (success) {
//        dispatch_sync(dispatch_get_main_queue(), ^{
//          NSLog(@"sync - %@", [NSThread currentThread]);
//        });
      }
    }];
  }


}

- (void)getIphoneHealthData:(NSString *)stringByAppendingString tenantId:(NSString *)tenantId callback:(void (^)(NSArray *))finish {
  self.healthSteps = [NSMutableArray array];
  self.sleeps = [NSMutableArray array];
  self.healthDistances = [NSMutableArray array];
  self.healthBasalCalories = [NSMutableArray array];
  self.healthActiveCalories = [NSMutableArray array];
  self.healthHeartRate = [NSMutableArray array];
  self.healthWorkout = [NSMutableArray array];
  self.formatter = [[NSDateFormatter alloc] init];
  self.workoutHeartRate = [NSMutableArray array];
  [self.formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
  NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
  [self.formatter setLocale:enUSPOSIXLocale];
  [self.formatter setCalendar: [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
  [self.formatter setTimeZone: [NSTimeZone systemTimeZone]];
  self.appleDeviceName = @"com.apple.health.";

  self.store = [[HKHealthStore alloc] init];
  NSSet *getQuantityTypeData = [self getQuantityTypeData];
  if (![HKHealthStore isHealthDataAvailable]) {
    finish(@[@{@"notAvalialbe":@"health data not avalialbe"}]);
    return;
  }

  //init time
  NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  [calendar setTimeZone:[NSTimeZone systemTimeZone]];
  [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
  NSDateComponents *anchorComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:[NSDate date]];
  //2018-04-25 16:00:00 +0000 : today's begin time, midlle night 0.
  self.anchorDate = [calendar dateFromComponents:anchorComponents];
  //2018-04-26 07:53:58 +0000 : now
  self.endDate = [NSDate date];
  NSDictionary *dic = [TSSUtil readThePlistfile];
  if (!dic) {
    self.startDate = nil;
  } else {
    self.startDate = [dic objectForKey:@"uploadDate"];
  }
  if (!self.startDate) {
    self.startDate = [NSDate dateWithTimeInterval:-29*24*60*60 sinceDate:self.anchorDate];
    self.startCalculateDate = [NSDate dateWithTimeInterval:-29*24*60*60 sinceDate:self.anchorDate];
  } else {
    NSTimeInterval timeBetween = [self.endDate timeIntervalSinceDate:self.startDate];
    if (timeBetween >= 30*24*60*60) {
      self.startDate = [NSDate dateWithTimeInterval:-29*24*60*60 sinceDate:self.anchorDate];
      self.startCalculateDate = [NSDate dateWithTimeInterval:-29*24*60*60 sinceDate:self.anchorDate];
    } else {
      NSDateComponents *anchorComponents1 = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:self.startDate];
      self.startCalculateDate = [calendar dateFromComponents:anchorComponents1];
      self.startCalculateDate =  [NSDate dateWithTimeInterval:0 sinceDate:self.startCalculateDate];
    }
  }
  //TEST
  //    self.startDate = [NSDate dateWithTimeInterval:-60*24*60*60 sinceDate:self.anchorDate];
  //    self.endDate = [NSDate dateWithTimeInterval:-55*24*60*60 sinceDate:self.anchorDate];
  //    self.startCalculateDate = [NSDate dateWithTimeInterval:-60*24*60*60 sinceDate:self.anchorDate];
  //END
  self.predicate = [HKQuery predicateForSamplesWithStartDate:self.startCalculateDate endDate:self.endDate options:HKQueryOptionStrictStartDate];

  [self.store requestAuthorizationToShareTypes:nil readTypes:getQuantityTypeData completion:^(BOOL success, NSError * _Nullable error) {
    if (!success) {
      finish(@[@{@"notAvalialbe":@"please do authorization"}]);
      return;
    }

    self.queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    self.disGroup = dispatch_group_create();
    //steps
    dispatch_group_enter(self.disGroup);
    dispatch_group_async(self.disGroup, self.queue, ^{
      [self getHealthDataWithType:@"healthSteps" andStartDateFlag:true];
    });
    // //sleep
    dispatch_group_enter(self.disGroup);
    dispatch_group_async(self.disGroup, self.queue, ^{
      [self getHealthDataWithType:@"sleep" andStartDateFlag:true];
    });
    //distance
    dispatch_group_enter(self.disGroup);
    dispatch_group_async(self.disGroup, self.queue, ^{
      [self getHealthDataWithType:@"distance" andStartDateFlag:true];
    });
    //activeEnergy
    dispatch_group_enter(self.disGroup);
    dispatch_group_async(self.disGroup, self.queue, ^{
      [self getHealthDataWithType:@"activeEB" andStartDateFlag:true];
    });
    //heartRate
    if (@available(iOS 11.0, *)) {
      dispatch_group_enter(self.disGroup);
      dispatch_group_async(self.disGroup, self.queue, ^{
        [self getHealthDataWithType:@"heartRate" andStartDateFlag:true];
      });
    }
    //workout
    dispatch_group_enter(self.disGroup);
    dispatch_group_async(self.disGroup, self.queue, ^{
      [self getWorkoutData];
    });
    dispatch_group_notify(self.disGroup, dispatch_get_main_queue(), ^{
      [self callBackWithHealthData:stringByAppendingString tenantId:tenantId finish:finish];
    });
  }];
}

- (HealthReadingsModel *) organizeDatawithData:(id )data andDataType:(NSString *)type {
  HealthReadingsModel *model = [[HealthReadingsModel alloc] init];
  model.workout = [[NSMutableDictionary alloc] init];
  model.startTime = data[@"startTime"];
  model.endTime = data[@"endTime"];
  model.partnerCreateDate = [NSString stringWithFormat:@"%@",[self.formatter stringFromDate:[NSDate date]]];
  model.source = data[@"source"];
  model.manufacturer = @"Apple";
  model.notes = @"Exercise";
  if ([[model.source lowercaseString] containsString:@"watch"]) {
    model.model = @"AppleWatch";
  } else {
    model.model = @"iPhone";
  }
  model.partnerReadingId = [NSString stringWithFormat:@"%@",[data[@"bundleId"] stringByReplacingOccurrencesOfString:self.appleDeviceName withString:@""]];
  model.integrity = @"VERIFIED";
  model.readingType = @"Walking";
  model.dataCategory = @"ROUTINE";
  if ([type isEqualToString:@"step"]) {
    [model.workout setValue:@"General" forKey:@"intensity"];
    [model.workout setValue:data[@"steps"] forKey:@"totalSteps"];
  } else if ([type isEqualToString:@"distances"]) {
    [model.workout setValue:@{@"value":[TSSUtil repeatNull:data[@"value"]], @"unitOfMeasurement":[TSSUtil repeatNull:data[@"unitOfMeasurement"]]} forKey:@"distance"];
  } else if ([type isEqualToString:@"heartRate"]) {
    [model.workout setValue:@{@"avgMinMax":@{@"average":[TSSUtil repeatNull:data[@"average"]],@"minimum":[TSSUtil repeatNull:data[@"minimum"]],@"maximum":[TSSUtil repeatNull:data[@"maximum"]],}} forKey:@"heartRate"];
  } else if ([type isEqualToString:@"calories"]) {
    [model.workout setValue:@{@"value":[TSSUtil repeatNull:data[@"value"]],@"unitOfMeasurement":[TSSUtil repeatNull:data[@"unitOfMeasurement"]]} forKey:@"energyExpenditure"];
  }
  else if ([type isEqualToString:@"sleep"]) {
    [model.workout setValue:data[@"sleep"] forKey:@"sleep"];
  }
  return model;
}

- (void)callBackWithHealthData:(NSString *)stringByAppendingString tenantId:(NSString *)tenantId finish:(void (^)(NSArray *))finish{
  NSMutableArray <HealthReadingsModel *>*dataModelArray = [[NSMutableArray alloc] init];
  NSMutableArray <HealthReadingsModel *>*dataModelWorkout = [[NSMutableArray alloc] init];
  for (int i =0; i< self.healthSteps.count; i++ ) {
    NSString *startTime = self.healthSteps[i][@"startTime"];
    NSString *endTime = self.healthSteps[i][@"endTime"];
    NSString *steps = (NSString *)self.healthSteps[i][@"steps"];
    if (startTime != nil && endTime != nil && steps != nil) {
    HealthReadingsModel *model = [self organizeDatawithData:self.healthSteps[i] andDataType:@"step"];
      [dataModelArray addObject:model];
    }
  }

  for (int i = 0; i < self.healthDistances.count; i++ ) {
    NSString *startTime = self.healthDistances[i][@"startTime"];
    NSString *endTime = self.healthDistances[i][@"endTime"];
    NSString *source = (NSString *)self.healthDistances[i][@"source"];
    if (startTime == nil || endTime == nil || source == nil) {
      continue;
    }
    int flag = 0;
    for (int j = 0; j < dataModelArray.count; j++) {
      NSString *startTime2 = dataModelArray[j].startTime;
      NSString *endTime2 = dataModelArray[j].endTime;
      NSString *sourceTemp = dataModelArray[j].source;
      if ([startTime isEqualToString:startTime2] && [endTime isEqualToString: endTime2] && [sourceTemp isEqualToString: source]) {
        flag = 1;
        HealthReadingsModel * model= (HealthReadingsModel *)dataModelArray[j];
        [model.workout setValue:@{@"value":[TSSUtil repeatNull:self.healthDistances[i][@"value"]], @"unitOfMeasurement":[TSSUtil repeatNull:self.healthDistances[i][@"unitOfMeasurement"]]} forKey:@"distance"];
        break;
      }
    }
    if (flag == 0 ) {
      HealthReadingsModel *model = [self organizeDatawithData:self.healthDistances[i] andDataType:@"distances"];
      [dataModelArray addObject:model];
    }
  }

  for (int i =0; i< self.healthHeartRate.count; i++ ) {
    NSString *startTime = self.healthHeartRate[i][@"startTime"];
    NSString *endTime = self.healthHeartRate[i][@"endTime"];
    NSString *source = (NSString *)self.healthHeartRate[i][@"source"];
    if (startTime == nil || endTime == nil || source == nil) {
      continue;
    }
    int flag = 0;
    for (int j = 0; j < dataModelArray.count; j++) {
      NSString *startTime2 = dataModelArray[j].startTime;
      NSString *endTime2 = dataModelArray[j].endTime;
      NSString *sourceTemp = dataModelArray[j].source;
      if ([startTime isEqualToString:startTime2] && [endTime isEqualToString: endTime2] && [sourceTemp isEqualToString: source]) {
        flag = 1;
        HealthReadingsModel * model= (HealthReadingsModel *)dataModelArray[j];
        [model.workout setValue:@{@"avgMinMax":@{@"average":[TSSUtil repeatNull:self.healthHeartRate[i][@"average"]],@"minimum":[TSSUtil repeatNull:self.healthHeartRate[i][@"minimum"]],@"maximum":[TSSUtil repeatNull:self.healthHeartRate[i][@"maximum"]],}} forKey:@"heartRate"];
        break;
      }
    }
    if (flag == 0 ) {
      HealthReadingsModel *model = [self organizeDatawithData:self.healthHeartRate[i] andDataType:@"heartRate"];
      [dataModelArray addObject:model];
    }

  }

  for (int i =0; i< self.healthActiveCalories.count; i++ ) {
    NSString *startTime = self.healthActiveCalories[i][@"startTime"];
    NSString *endTime = self.healthActiveCalories[i][@"endTime"];
    NSString *source = (NSString *)self.healthActiveCalories[i][@"source"];
    if (startTime == nil || endTime == nil || source == nil) {
      continue;
    }
    int flag = 0;
    for (int j = 0; j < dataModelArray.count; j++) {
      NSString *startTime2 = dataModelArray[j].startTime;
      NSString *endTime2 = dataModelArray[j].endTime;
      NSString *sourceTemp = dataModelArray[j].source;
      if ([startTime isEqualToString:startTime2] && [endTime isEqualToString: endTime2] && [sourceTemp isEqualToString: source]) {
        flag = 1;
        HealthReadingsModel * model= (HealthReadingsModel *)dataModelArray[j];
        [model.workout setValue:@{@"value":[TSSUtil repeatNull:self.healthActiveCalories[i][@"value"]],@"unitOfMeasurement":[TSSUtil repeatNull:self.healthActiveCalories[i][@"unitOfMeasurement"]]} forKey:@"energyExpenditure"];
        break;
      }
    }
    if (flag == 0 ) {
      HealthReadingsModel *model = [self organizeDatawithData:self.healthActiveCalories[i] andDataType:@"calories"];
      [dataModelArray addObject:model];
    }
  }

  for (int i = 0; i < self.healthWorkout.count; i++) {
    NSString *startTime = self.healthWorkout[i][@"startTime"];
    NSString *endTime = self.healthWorkout[i][@"endTime"];
    NSString *workoutType = [HealthDataModel getWorkoutTypeStrWithNumber:[self.healthWorkout[i][@"workoutType"] integerValue]];
    NSString *source = (NSString *)self.healthWorkout[i][@"source"];
//    NSString *duration = (NSString *)self.healthWorkout[i][@"duration"];
    HealthReadingsModel *model = [[HealthReadingsModel alloc] init];
    model.workout = [[NSMutableDictionary alloc] init];
    model.startTime = startTime;
    model.endTime = endTime;
    model.partnerCreateDate = [NSString stringWithFormat:@"%@",[self.formatter stringFromDate:[NSDate date]]];
    model.manufacturer = @"Apple";
    model.notes = @"Exercise";
    if ([[source lowercaseString] containsString:@"watch"]) {
      model.model = @"AppleWatch";
    } else {
      model.model = @"iPhone";
    }
    model.integrity = @"VERIFIED";
    model.readingType = workoutType;
    model.dataCategory = @"FITNESS";
    model.partnerReadingId = [NSString stringWithFormat:@"%@%@", [self.healthWorkout[i][@"bundleId"] stringByReplacingOccurrencesOfString:self.appleDeviceName withString:@""], startTime];
    //TEST
    //    model.partnerReadingId = [NSString stringWithFormat:@"%@%@", @"FB1D17F1-1089-4191-9B5B-73A0DEB012CD", startTime];
    //END
    if (![self.healthWorkout[i][@"totalEnergy"] isEqualToString:@"0"]) {
      [model.workout setValue:@{@"value":[TSSUtil repeatNull:self.healthWorkout[i][@"totalEnergy"]],@"unitOfMeasurement":[TSSUtil repeatNull:self.healthWorkout[i][@"unitOfEnergy"]]} forKey:@"energyExpenditure"];
    }
    if (![self.healthWorkout[i][@"totalDistance"] isEqualToString:@"0"]) {
      [model.workout setValue:@{@"value":[TSSUtil repeatNull:self.healthWorkout[i][@"totalDistance"]],@"unitOfMeasurement":[TSSUtil repeatNull:self.healthWorkout[i][@"unitOfDistance"]]} forKey:@"distance"];
    }
    for (int j=0; j<self.workoutHeartRate.count; j++) {
      if ([startTime isEqualToString:self.workoutHeartRate[j][@"startTime"]] && [endTime isEqualToString:self.workoutHeartRate[j][@"endTime"]]) {
        [model.workout setValue:@{@"avgMinMax":@{@"average":[TSSUtil repeatNull:self.workoutHeartRate[j][@"average"]],@"maximum":[TSSUtil repeatNull:self.workoutHeartRate[j][@"maximum"]],@"minimum": [TSSUtil repeatNull:self.workoutHeartRate[j][@"minimum"]]}} forKey:@"heartRate"];
        break;
      }
    }
    [dataModelWorkout addObject:model];
  }

  NSMutableArray *LessonArr=[NSMutableArray array];
  [dataModelArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    HealthReadingsModel *model=obj;
    NSString *LessonID=model.startTime;
    [LessonArr addObject:LessonID];
  }];
  NSSet *set = [NSSet setWithArray:LessonArr];
  NSArray *userArray = [set allObjects];
  NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:nil ascending:NO];
  NSArray *myary = [userArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sd1, nil]];
  NSMutableArray *readings=[NSMutableArray array];
  [myary enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    NSMutableArray *arr=[NSMutableArray array];
    [readings addObject:arr];
  }];
  [dataModelArray enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
    HealthReadingsModel *model2=obj;
    for (NSString *str in myary) {
      if([str isEqualToString:model2.startTime]) {
        NSMutableArray *arr=[readings objectAtIndex:[myary indexOfObject:str]];
        NSMutableDictionary *modelDictionary = model2.mj_keyValues;
        [modelDictionary removeObjectForKey:@"source"];
        [arr addObject:modelDictionary];
      }
    }
  }];
  for (int i=0; i<dataModelWorkout.count; i++) {
    for (int j=0; j<readings.count; j++) {
      if (([TSSUtil compareDate:dataModelWorkout[i].startTime withDate:readings[j][0][@"startTime"]] <= 0) && ([TSSUtil compareDate:dataModelWorkout[i].startTime withDate:readings[j][0][@"endTime"]] >= 0)) {
        HealthReadingsModel *model3 = dataModelWorkout[i];
        NSMutableDictionary *modelDictionary = model3.mj_keyValues;
        [modelDictionary removeObjectForKey:@"source"];
        [readings[j] addObject:modelDictionary];
        break;
      }
    }
  }

  if (readings.count == 0) {
    finish(@[@{@"noData":@"true"}]);
    return;
  }

  //TEST
  //  readings[0][0][@"workout"][@"totalSteps"] = @"9999";
  //  readings[1][0][@"workout"][@"totalSteps"] = @"11000";
  //  readings[2][0][@"workout"][@"totalSteps"] = @"12499";
  //  [readings[2][2][@"workout"] setObject:@{@"avgMinMax":@{@"average":@"88",@"maximum":@"158",@"minimum":@"75"}} forKey:@"heartRate"];
  //  readings[3][0][@"workout"][@"totalSteps"] = @"12500";
  //  [readings[3][1][@"workout"] setObject:@{@"avgMinMax":@{@"average":@"98",@"maximum":@"160",@"minimum":@"65"}} forKey:@"heartRate"];
  //  [readings[3][2][@"workout"] setObject:@{@"avgMinMax":@{@"average":@"80",@"maximum":@"170",@"minimum":@"98"}} forKey:@"heartRate"];
  //  [readings[4][1][@"workout"] setObject:@{@"avgMinMax":@{@"average":@"92",@"maximum":@"152",@"minimum":@"65"}} forKey:@"heartRate"];
  //  [readings[4][2][@"workout"] setObject:@{@"avgMinMax":@{@"average":@"78",@"maximum":@"90",@"minimum":@"60"}} forKey:@"heartRate"];
  //END

  NSMutableArray *healthData = [NSMutableArray array];
  for (int k=0; k<readings.count; k++) {
    NSMutableDictionary *transfer = [NSMutableDictionary dictionaryWithObject:readings[k] forKey:@"readings"];
    NSMutableDictionary *device = [[NSMutableDictionary alloc] init];
    [device setObject:@"Apple" forKey:@"manufacturer"];
    [device setObject:@"iOSHealthApp" forKey:@"model"];
    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor].UUIDString stringByAppendingString:stringByAppendingString];
    if (deviceID) {
      [device setObject:deviceID forKey:@"deviceId"];
    }
    [device setObject:@"Apple" forKey:@"make"];
    [transfer setObject:device forKey:@"device"];

    NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
    [header setObject:@{@"entityNo":@""} forKey:@"user"];
    [header setObject:[NSString stringWithFormat:@"%@",[self.formatter stringFromDate:[NSDate date]]] forKey:@"uploadDate"];
    [header setObject:@"" forKey:@"additionalMessage"];
    [header setObject:@"" forKey:@"rawUploadData"];
    [header setObject:@"REALTIME" forKey:@"processingType"];
    [header setObject:@"" forKey:@"sessionId"];
    [header setObject:@"true" forKey:@"verified"];
    [header setObject:tenantId forKey:@"tenantId"];
    [header setObject:@"Apple" forKey:@"partnerSystem"];

    [transfer setObject:header forKey:@"header"];
    [healthData addObject:transfer];
  }
  //  NSData *jsonData =  [NSJSONSerialization dataWithJSONObject:transfer options:NSJSONWritingPrettyPrinted error:nil];
  //  NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  finish(healthData);
}

- (void) getWorkoutData {
  HKSampleType *workOutsSampleType = [HKSampleType workoutType];
  NSSortDescriptor *sortDescrptor = [[NSSortDescriptor alloc] initWithKey:HKSampleSortIdentifierStartDate ascending:false];
  HKSampleQuery *workOutsQuery = [[HKSampleQuery alloc] initWithSampleType:workOutsSampleType predicate:self.predicate limit:HKObjectQueryNoLimit sortDescriptors:@[sortDescrptor] resultsHandler:^(HKSampleQuery * _Nonnull query, NSArray<__kindof HKSample *> * _Nullable results, NSError * _Nullable error) {
      for (HKWorkout *sample in results) {
        //TEST self.appleDeviceName  com.apple
        if ([sample.source.bundleIdentifier containsString: self.appleDeviceName]) {
          NSDate *sdate = sample.startDate;
          NSString *sdateTime = [self.formatter stringFromDate:sdate];
          NSDate *edate = sample.endDate;
          NSString *edateTime = [self.formatter stringFromDate:edate];
          NSString *workoutType = [NSString stringWithFormat:@"%lu", (unsigned long)sample.workoutActivityType];
          NSString *totalEnergy = [NSString stringWithFormat:@"%0.2f",[sample.totalEnergyBurned doubleValueForUnit:[HKUnit kilocalorieUnit]]];
          NSString *totalDistance = [NSString stringWithFormat:@"%d",(int)[sample.totalDistance doubleValueForUnit:[HKUnit meterUnit]]];
          NSString *duration = [NSString stringWithFormat:@"%lf", sample.duration];
          NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:sdateTime,@"startTime",edateTime,@"endTime",workoutType,@"workoutType",totalDistance,@"totalDistance",@"METERS",@"unitOfDistance",totalEnergy,@"totalEnergy",@"KILOCALORIES",@"unitOfEnergy",duration,@"duration",sample.source.name,@"source",sample.source.bundleIdentifier,@"bundleId",nil];
          //select heartRate
          if (@available(iOS 11.0, *)) {
            dispatch_group_enter(self.disGroup);
            dispatch_group_async(self.disGroup, self.queue, ^{
              [self getWorkoutHeartRateWithStartDate:sdateTime andEndDate:edateTime];
            });
          }
          [self.healthWorkout addObject:dic];
        }
      }
    dispatch_group_leave(self.disGroup);
  }];
  [self.store executeQuery:workOutsQuery];
}

- (void) getHealthDataWithType:(NSString *)healthType andStartDateFlag:(BOOL) flag {
  HKQuantityType *type;
  if ([healthType isEqualToString:@"distance"]) {
    type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
  } else if ([healthType isEqualToString:@"healthSteps"]) {
    type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
  } else if ([healthType isEqualToString:@"basalEB"]) {
    type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBasalEnergyBurned];
  } else if ([healthType isEqualToString:@"activeEB"]) {
    type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
  } else if ([healthType isEqualToString:@"sleep"]) {
    type = [HKSampleType categoryTypeForIdentifier:HKCategoryTypeIdentifierSleepAnalysis];
  } else if ([healthType isEqualToString:@"heartRate"]) {
    if (@available(iOS 11.0, *)) {
      type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    } else {
      // Fallback on earlier versions
    }
  }

  NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
  intervalComponents.day = 1;

  if([healthType isEqualToString:@"sleep"]) {
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSDate *now = [NSDate date];

    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];

    NSDate *startDate = [calendar dateFromComponents:components];

    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];

    HKSampleType *sampleType = [HKSampleType categoryTypeForIdentifier:HKCategoryTypeIdentifierSleepAnalysis];
    NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:startDate endDate:endDate options:HKQueryOptionNone];

    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:predicate limit:0 sortDescriptors:nil resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
      if (!results) {
          NSLog(@"An error occured fetching the user's sleep duration. In your app, try to handle this gracefully. The error was: %@.", error);
      }
        for (HKCategorySample *sample in results) {
          double minutesSleepAggr = 0;
          NSTimeInterval distanceBetweenDates = [sample.endDate timeIntervalSinceDate:sample.startDate];
          double minutesInAnHour = 60;
          double minutesBetweenDates = distanceBetweenDates / minutesInAnHour;

          minutesSleepAggr += minutesBetweenDates;
          NSDate *sdate = sample.startDate;
          NSString *sdateTime = [self.formatter stringFromDate:sdate];
          NSDate *edate = [sample.endDate dateByAddingTimeInterval: -1];
          NSString *edateTime = [self.formatter stringFromDate:edate];
          NSString *minutesSleepAggrHR = [NSString stringWithFormat:@"%0.2f", minutesSleepAggr];
          NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:minutesSleepAggrHR,@"sleep",sdateTime,@"startTime",edateTime,@"endTime",@"COUNT",@"unitOfMeasurement",nil];
          [self.sleeps addObject:dic];
        }

        dispatch_group_leave(self.disGroup);
    }];

    [self.store executeQuery:query];
  } else {
    HKStatisticsCollectionQuery *query;
    if ([healthType isEqualToString:@"heartRate"]) {
      query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:type quantitySamplePredicate:self.predicate options:HKStatisticsOptionSeparateBySource|HKStatisticsOptionDiscreteAverage|HKStatisticsOptionDiscreteMin|HKStatisticsOptionDiscreteMax anchorDate:self.anchorDate intervalComponents:intervalComponents];
    } else {
      query = [[HKStatisticsCollectionQuery alloc] initWithQuantityType:type quantitySamplePredicate:self.predicate options:HKStatisticsOptionSeparateBySource|HKStatisticsOptionCumulativeSum anchorDate:self.anchorDate intervalComponents:intervalComponents];
    }
    query.initialResultsHandler = ^(HKStatisticsCollectionQuery *query, HKStatisticsCollection *result, NSError *error) {
    //info
    for (HKStatistics *sample in [result statistics]) {
      NSDate *sdate = sample.startDate;
      NSString *sdateTime = [self.formatter stringFromDate:sdate];
      NSDate *edate = [sample.endDate dateByAddingTimeInterval: -1];
      NSString *edateTime = [self.formatter stringFromDate:edate];
      if ([healthType isEqualToString:@"distance"]) {
//        double totalDistance = [sample.sumQuantity doubleValueForUnit:[HKUnit meterUnit]];
        double editDistance  = 0.0;
        for (HKSource *source in sample.sources) {
            if ([source.bundleIdentifier containsString:self.appleDeviceName]) {
              HKSource *healthSource = source;
              editDistance = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit meterUnit]];
              NSString *value = [NSString stringWithFormat:@"%0.2f",editDistance];
              NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:sdateTime,@"startTime",edateTime,@"endTime",healthSource.name,@"source",value,@"value",@"METERS",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
              [self.healthDistances addObject:dic];
            }
        }
      } else if ([healthType isEqualToString:@"healthSteps"]) {
        for (HKSource *healthSource in sample.sources) {
          if ([healthSource.bundleIdentifier containsString:self.appleDeviceName]) {
            double sourceSteps = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit countUnit]];
            NSString *value = [NSString stringWithFormat:@"%d",(int)sourceSteps];
            if ([value isEqualToString:@"0"] || value == nil) {
              sourceSteps = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit countUnit]];
              value = [NSString stringWithFormat:@"%d",(int)sourceSteps];
            }
            if ([value isEqualToString:@"0"] || value == nil) {
              sourceSteps = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit countUnit]];
              value = [NSString stringWithFormat:@"%d",(int)sourceSteps];
            }
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:sdateTime,@"startTime",edateTime,@"endTime",healthSource.name,@"source",value,@"steps",@"STEPS",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
            [self.healthSteps addObject:dic];
          }
        }
      } else if ([healthType isEqualToString:@"basalEB"]) {
        for (HKSource *healthSource in sample.sources) {
          if ([healthSource.bundleIdentifier containsString:self.appleDeviceName]) {
            double totalEnergy = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit kilocalorieUnit]];
            NSString *value = [NSString stringWithFormat:@"%0.2f",totalEnergy];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:sdateTime,@"startTime",edateTime,@"endTime",healthSource.name,@"source",value,@"value",@"KILOCALORIES",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
            [self.healthBasalCalories addObject:dic];
          }
        }
      } else if ([healthType isEqualToString:@"activeEB"]) {
        for (HKSource *healthSource in sample.sources) {
          if ([healthSource.bundleIdentifier containsString:self.appleDeviceName]) {
            double totalEnergy = [[sample sumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit kilocalorieUnit]];
            NSString *value = [NSString stringWithFormat:@"%0.2f",totalEnergy];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:sdateTime,@"startTime",edateTime,@"endTime",healthSource.name,@"source",value,@"value", @"KILOCALORIES",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
            [self.healthActiveCalories addObject:dic];
          }
        }
      } else if ([healthType isEqualToString:@"heartRate"]) {
        for (HKSource *healthSource in sample.sources) {
          if ([healthSource.bundleIdentifier containsString:self.appleDeviceName]) {
            double average = [[sample averageQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
            NSString *averageHR = [NSString stringWithFormat:@"%0.2f", average];
            double maximum = [[sample maximumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
            NSString *maximumHR = [NSString stringWithFormat:@"%0.2f", maximum];
            double minimum = [[sample minimumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
            NSString *minimumHR = [NSString stringWithFormat:@"%0.2f", minimum];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:averageHR,@"average",maximumHR,@"maximum",minimumHR,@"minimum",sdateTime,@"startTime",edateTime,@"endTime",healthSource.name,@"source",@"COUNT",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
            [self.healthHeartRate addObject:dic];
          }
        }
      }
    }
    dispatch_group_leave(self.disGroup);
  };
  [self.store executeQuery:query];
  }
}

-(void) getWorkoutHeartRateWithStartDate:(NSString *)startDate andEndDate:(NSString *)endDate{
  HKQuantityType *type = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
  NSDateComponents *intervalComponents = [[NSDateComponents alloc] init];
  NSDate *start = [self.formatter dateFromString: startDate];
  NSDate *end = [self.formatter dateFromString: endDate];
  NSTimeInterval time = [end timeIntervalSinceDate:start];
  intervalComponents.second = time;
  NSPredicate *predicate = [HKQuery predicateForSamplesWithStartDate:start endDate:end options:HKQueryOptionStrictStartDate | HKQueryOptionStrictEndDate];
  NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
  [calendar setTimeZone:[NSTimeZone systemTimeZone]];
//  NSDateComponents *anchorComponents = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday fromDate:start];
//  NSDate *anchorDate = [calendar dateFromComponents:anchorComponents];
//  HKStatisticsQuery
  HKStatisticsQuery *query3 = [[HKStatisticsQuery alloc] initWithQuantityType:type quantitySamplePredicate:predicate options:HKStatisticsOptionSeparateBySource|HKStatisticsOptionDiscreteAverage|HKStatisticsOptionDiscreteMin|HKStatisticsOptionDiscreteMax completionHandler:^(HKStatisticsQuery * _Nonnull query, HKStatistics * _Nullable result, NSError * _Nullable error) {
      for (HKSource *healthSource in result.sources) {
        if ([healthSource.bundleIdentifier containsString:self.appleDeviceName]) {
          double average = [[result averageQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
          NSString *averageHR = [NSString stringWithFormat:@"%0.2f", average];
          double maximum = [[result maximumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
          NSString *maximumHR = [NSString stringWithFormat:@"%0.2f", maximum];
          double minimum = [[result minimumQuantityForSource:healthSource] doubleValueForUnit:[HKUnit unitFromString:@"count/min"]];
          NSString *minimumHR = [NSString stringWithFormat:@"%0.2f", minimum];
          NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:averageHR,@"average",maximumHR,@"maximum",minimumHR,@"minimum",startDate,@"startTime",endDate,@"endTime",healthSource.name,@"source",@"COUNT",@"unitOfMeasurement",healthSource.bundleIdentifier,@"bundleId",nil];
          [self.workoutHeartRate addObject:dic];
        }
      }
    dispatch_group_leave(self.disGroup);
  }];
  [self.store executeQuery:query3];
}

@end
