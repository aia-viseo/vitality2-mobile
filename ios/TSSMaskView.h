//
//  TSSMaskView.h
//  VitalityWrapper
//
//  Created by kenneth ye on 2019/10/25.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSSMaskView : UIView
-(void)loginSuccessChangeImage;
@end

NS_ASSUME_NONNULL_END
